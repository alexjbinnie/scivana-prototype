﻿# Contributors 

Scivana is written and maintained by Alex Jamieson-Binnie.

It is based upon the Narupa iMD application. The version on which this is based is represented by 
the initial commit of this repository. This version was taken on the 22nd July 2020 and
consisted of a merged combination of the master branch and relevant outstanding merge requests.

### Icons

All icons used in the user interface of Scivana were designed by Alex Jamieson-Binnie, and
licensed under CC-BY-SA.

### Logo

The Scivana logo was designed by Alex Jamieson-Binnie, and licensed under CC-BY-CA.

## Narupa iMD

The following people made contributions to Narupa iMD that were gratefully received. Consult the repository
history of [Narupa iMD](https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd) for individual contributions.

### 2019:

Jonathan Barnoud (Intangible Realities Laboratory, University of Bristol, U.K.)
Alex Jamieson-Binnie (Intangible Realities Laboratory, University of Bristol, U.K.)
Mike O'Connor (Intangible Realities Laboratory, University of Bristol, U.K.)
Mark Wonnacott (Intangible Realities Laboratory, University of Bristol, U.K.)

### Logo 

The Narupa iMD logo was designed by Alex Jamieson-Binnie, licensed under CC-BY-SA.