﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Scivana.Core.Testing.Async
{
    /// <summary>
    /// Example of asynchronous tests
    /// </summary>
    /// <remarks>
    /// Each class that would like to run asynchronous unit tests will require two
    /// methods. The
    /// <see cref="AsyncExampleTests.GetTests" /> static method is needed to feed the
    /// tests into
    /// a single unit test (called <see cref="TestAsync" />) that iterates over them
    /// and executes
    /// each one. Both methods must be in each class, as inheritance would not work
    /// with static
    /// methods.
    /// </remarks>
    public class AsyncExampleTests
    {
        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<AsyncExampleTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        [AsyncTest]
        public async Task DoesDelayOccur()
        {
            var startTime = DateTime.Now;

            await Task.Delay(2000);

            var endTime = DateTime.Now;

            var duration = (endTime - startTime).TotalMilliseconds;
            Assert.AreEqual(2000, duration, 200);
        }

        private static async Task DelayThenException()
        {
            await Task.Delay(1000);
            throw new Exception();
        }

        [AsyncTest]
        public async Task IsExceptionThrown()
        {
            await AsyncAssert.ThrowsAsync<Exception>(async () => await DelayThenException());
        }
    }
}