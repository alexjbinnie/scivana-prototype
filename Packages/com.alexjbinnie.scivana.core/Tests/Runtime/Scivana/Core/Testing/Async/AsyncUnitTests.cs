/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace Scivana.Core.Testing.Async
{
    /// <summary>
    /// Utility methods for finding and running asynchronous unit tests using NUnit
    /// </summary>
    public static class AsyncUnitTests
    {
        /// <summary>
        /// Wrapper for a test definition, containing the method that represents the test
        /// </summary>
        public struct AsyncTestInfo
        {
            internal MethodInfo TestMethod;

            /// <inheritdoc />
            public override string ToString()
            {
                return TestMethod.Name;
            }
        }

        /// <summary>
        /// Uses reflection to find all methods marked with [AsyncTest] in a class.
        /// </summary>
        public static IEnumerable<AsyncTestInfo> FindAsyncTestsInClass<T>()
        {
            var methods = typeof(T).GetMethods(BindingFlags.Instance | BindingFlags.Public);
            foreach (var method in methods)
            {
                if (method.ReturnType != typeof(Task))
                    continue;
                var attr = method.GetCustomAttribute<AsyncTestAttribute>();
                if (attr == null)
                    continue;
                yield return new AsyncTestInfo
                {
                    TestMethod = method,
                };
            }
        }

        /// <summary>
        /// Execute an asynchronous test by calling the method described in <paramref name="method" /> on the
        /// object <paramref name="instance" />.
        /// </summary>
        public static void RunAsyncTest(object instance, AsyncTestInfo method)
        {
            // Invoke the test method on testObject, with no parameters
            Task UnitTest() => method.TestMethod.Invoke(instance, new object[0]) as Task;

            var task = Task.Run(UnitTest);

            task.GetAwaiter().GetResult();
        }

        /// <summary>
        /// Execute an asynchronous setup for unit tests.
        /// </summary>
        public static void RunAsyncSetUp(object instance)
        {
            var setUp = FindAsyncMethodWithAttribute<AsyncSetUpAttribute>(instance);

            if (setUp == null)
                return;

            var task = Task.Run(setUp);

            task.GetAwaiter().GetResult();
        }

        /// <summary>
        /// Execute an asynchronous teardown for unit tests.
        /// </summary>
        public static void RunAsyncTearDown(object instance)
        {
            var setUp = FindAsyncMethodWithAttribute<AsyncTearDownAttribute>(instance);

            if (setUp == null)
                return;

            var task = Task.Run(setUp);

            task.GetAwaiter().GetResult();
        }

        private static Func<Task> FindAsyncMethodWithAttribute<TAttribute>(object instance)
            where TAttribute : Attribute
        {
            var methods =
                instance.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public);
            foreach (var method in methods)
            {
                if (method.ReturnType != typeof(Task))
                    continue;
                var attr = method.GetCustomAttribute<TAttribute>();
                if (attr == null)
                    continue;
                return () => method.Invoke(instance, new object[0]) as Task;
            }

            return null;
        }
    }
}