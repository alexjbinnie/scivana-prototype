﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools.Utils;

namespace Scivana.Core.Testing
{
    /// <summary>
    /// Compares if two matrixes are equal by comparing each row using unity's
    /// standard Vector4EqualityComparer.
    /// </summary>
    public class Matrix4x4RowEqualityComparer : IEqualityComparer<Matrix4x4>
    {
        private static Matrix4x4RowEqualityComparer instance;

        /// <summary>
        /// Get a global instance of a <see cref="Matrix4x4RowEqualityComparer" />.
        /// </summary>
        public static Matrix4x4RowEqualityComparer Instance
        {
            get
            {
                instance ??= new Matrix4x4RowEqualityComparer();
                return instance;
            }
        }

        private readonly Vector4EqualityComparer vector4Comparer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4x4RowEqualityComparer" /> class.
        /// </summary>
        public Matrix4x4RowEqualityComparer(float allowedError = 0.0001f)
        {
            vector4Comparer = new Vector4EqualityComparer(allowedError);
        }

        /// <inheritdoc />
        public bool Equals(Matrix4x4 x, Matrix4x4 y)
        {
            var equals = true;

            for (var row = 0; row < 4; ++row)
            {
                equals &= vector4Comparer.Equals(x.GetRow(row), y.GetRow(row));
            }

            return equals;
        }

        /// <inheritdoc />
        public int GetHashCode(Matrix4x4 matrix) => matrix.GetHashCode();
    }
}