﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Scivana.Core.Testing
{
    /// <summary>
    /// Utility method for randomly generating test data.
    /// </summary>
    public static class RandomTestData
    {
        public const int RandomSeed = 40423;

        /// <summary>
        /// Given a data generator function, use the fixed random seed to repeatably
        /// generate random test data.
        /// </summary>
        public static IEnumerable<T> SeededRandom<T>(Func<T> dataGenerator, int? seed = null)
        {
            Random.InitState(seed ?? RandomSeed);

            while (true)
            {
                var data = dataGenerator();
                var state = Random.state;

                yield return data;

                // it's possible that the calling code used Random outside of this
                // function, so make sure we restore the state for repeatability.
                Random.state = state;
            }
        }
    }
}