﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools.Utils;

namespace Scivana.Core.Testing
{
    /// <summary>
    /// Compares if two TRS matrixes are equal based on the error in their
    /// independent translation, rotation, scale components.
    /// </summary>
    public class Matrix4x4TRSEqualityComparer : IEqualityComparer<Matrix4x4>
    {
        private static Matrix4x4TRSEqualityComparer instance;

        public static Matrix4x4TRSEqualityComparer Instance
        {
            get
            {
                instance ??= new Matrix4x4TRSEqualityComparer();
                return instance;
            }
        }

        private readonly IEqualityComparer<Vector3> translationComparer;
        private readonly IEqualityComparer<Quaternion> rotationComparer;
        private readonly IEqualityComparer<Vector3> scaleComparer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4x4TRSEqualityComparer" /> class.
        /// </summary>
        public Matrix4x4TRSEqualityComparer(float allowedTranslationError = 0.0001f,
                                            float allowedRotationError = 0.0001f,
                                            float allowedScaleError = 0.0001f)
        {
            translationComparer = new Vector3EqualityComparer(allowedTranslationError);
            rotationComparer = new QuaternionEqualityComparer(allowedRotationError);
            scaleComparer = new Vector3EqualityComparer(allowedScaleError);
        }

        /// <inheritdoc />
        public bool Equals(Matrix4x4 x, Matrix4x4 y)
        {
            Assert.True(x.ValidTRS());
            Assert.True(y.ValidTRS());

            return translationComparer.Equals(x.GetColumn(3), y.GetColumn(3))
                && rotationComparer.Equals(x.rotation, y.rotation)
                && scaleComparer.Equals(x.lossyScale, y.lossyScale);
        }

        /// <inheritdoc />
        public int GetHashCode(Matrix4x4 matrix)
        {
            return matrix.GetHashCode();
        }
    }
}