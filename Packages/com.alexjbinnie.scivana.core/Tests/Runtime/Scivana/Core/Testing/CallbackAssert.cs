﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using NSubstitute;

namespace Scivana.Core.Testing
{
    /// <summary>
    /// Assertion methods relating to callbacks and events.
    /// </summary>
    public static class CallbackAssert
    {
        /// <summary>
        /// Get a callback for an action that takes no parameters.
        /// </summary>
        public static Action Callback()
        {
            return Substitute.For<Action>();
        }

        /// <summary>
        /// Get a callback for an action that takes a single parameter of type <typeparamref name="T1" />.
        /// </summary>
        public static Action<T1> Callback<T1>()
        {
            return Substitute.For<Action<T1>>();
        }

        /// <summary>
        /// Get a callback for an action that takes two parameters of types <typeparamref name="T1" /> and
        /// <typeparamref name="T2" />.
        /// </summary>
        public static Action<T1, T2> Callback<T1, T2>()
        {
            return Substitute.For<Action<T1, T2>>();
        }

        /// <summary>
        /// Assert that the given callback was called at least once.
        /// </summary>
        public static void WasCalled(Action substitute)
        {
            substitute.Received().Invoke();
        }

        /// <summary>
        /// Assert that a given callback was called at least once with any argument.
        /// </summary>
        public static void WasCalled<T1>(Action<T1> substitute)
        {
            substitute.ReceivedWithAnyArgs().Invoke(Arg.Any<T1>());
        }

        /// <summary>
        /// Assert that a given callback was called at least once with any arguments.
        /// </summary>
        public static void WasCalled<T1, T2>(Action<T1, T2> substitute)
        {
            substitute.ReceivedWithAnyArgs().Invoke(Arg.Any<T1>(), Arg.Any<T2>());
        }

        /// <summary>
        /// Assert that a given callback was called at least once with the given arguments.
        /// </summary>
        public static void WasCalled<T1, T2>(Action<T1, T2> substitute, T1 arg1, T2 arg2)
        {
            substitute.Received().Invoke(arg1, arg2);
        }
    }
}