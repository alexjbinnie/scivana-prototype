/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;

namespace Scivana.Core.Testing
{
    /// <summary>
    /// General tests of an IDictionary implementation. Override Setup() to provide the
    /// dictionary
    /// </summary>
    [Ignore("")]
    public abstract class DictionaryImplementationTests
    {
        protected IDictionary<string, int> Dictionary;

        [SetUp]
        public void TestSetup()
        {
            Dictionary = Setup();
        }

        protected abstract IDictionary<string, int> Setup();

        [Test]
        public void GetIndexer()
        {
            Assert.AreEqual(0, Dictionary["a"]);
            Assert.AreEqual(1, Dictionary["b"]);
        }

        [Test]
        public void SetIndexer()
        {
            Dictionary["c"] = 2;
            Assert.AreEqual(3, Dictionary.Count);
            Assert.AreEqual(2, Dictionary["c"]);
        }

        [Test]
        public void Keys()
        {
            Assert.AreEqual(new[]
            {
                "a", "b"
            }, Dictionary.Keys);
        }

        [Test]
        public void Values()
        {
            Assert.AreEqual(new[]
            {
                0, 1
            }, Dictionary.Values);
        }

        [Test]
        public void Add()
        {
            Dictionary.Add("c", 2);
            Assert.AreEqual(3, Dictionary.Count);
            Assert.AreEqual(2, Dictionary["c"]);
        }

        [Test]
        public void ContainsKey()
        {
            Assert.IsTrue(Dictionary.ContainsKey("b"));
            Assert.IsFalse(Dictionary.ContainsKey("c"));
        }

        [Test]
        public void Remove()
        {
            Dictionary.Remove("b");
            Assert.AreEqual(1, Dictionary.Count);
            Assert.IsFalse(Dictionary.ContainsKey("b"));
        }

        [Test]
        public void TryGetValue()
        {
            var hasA = Dictionary.TryGetValue("a", out var valueA);
            Assert.IsTrue(hasA);
            Assert.AreEqual(0, valueA);

            var hasC = Dictionary.TryGetValue("c", out _);
            Assert.IsFalse(hasC);
        }

        [Test]
        public void Count()
        {
            Assert.AreEqual(2, Dictionary.Count);
        }
    }
}