﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Science;
using Assert = UnityEngine.Assertions.Assert;

namespace Scivana.Core.Tests.Science
{
    internal class AminoAcidTests
    {
        [Test]
        public void Name()
        {
            Assert.AreEqual("Alanine", AminoAcid.Alanine.Name);
        }

        [Test]
        public void ThreeLetterCode()
        {
            StringAssert.AreEqualIgnoringCase("Ala", AminoAcid.Alanine.ThreeLetterCode);
        }

        [Test]
        public void SingleLetterCode()
        {
            Assert.AreEqual('A', AminoAcid.Alanine.SingleLetterCode);
        }

        [Test]
        public void AsAminoAcid_Valid()
        {
            Assert.IsNotNull(AminoAcid.GetAminoAcidFromResidue("Ala"));
        }

        [Test]
        public void AsAminoAcid_WrongCase()
        {
            Assert.IsNotNull(AminoAcid.GetAminoAcidFromResidue("aLa"));
        }

        [Test]
        public void AsAminoAcid_Invalid()
        {
            Assert.IsNull(AminoAcid.GetAminoAcidFromResidue("xyz"));
        }

        [Test]
        public void IsAminoAcid_Valid()
        {
            Assert.IsTrue(AminoAcid.IsStandardAminoAcid("Ala"));
        }

        [Test]
        public void IsAminoAcid_WrongCase()
        {
            Assert.IsTrue(AminoAcid.IsStandardAminoAcid("aLa"));
        }

        [Test]
        public void IsAminoAcid_Invalid()
        {
            Assert.IsNull(AminoAcid.GetAminoAcidFromResidue("xyz"));
        }
    }
}