/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Science;

namespace Scivana.Core.Tests.Science
{
    internal class ElementTests
    {
        [Test]
        public void IntToElement()
        {
            Assert.AreEqual(Element.Carbon, (Element) 6);
        }

        [Test]
        public void ElementToInt()
        {
            Assert.AreEqual(8, (int) Element.Oxygen);
        }

        [Test]
        public void Element_Symbol()
        {
            Assert.AreEqual("C", Element.Carbon.GetSymbol());
        }

        [Test]
        public void Element_Vdw()
        {
            Assert.AreEqual(0.17f, Element.Carbon.GetVdwRadius());
        }

        [Test]
        public void Element_AtomicWeight()
        {
            Assert.AreEqual(12.01f, Element.Carbon.GetStandardAtomicWeight());
        }

        [Test]
        public void FromSymbol()
        {
            Assert.AreEqual(Element.Carbon, ElementSymbols.GetFromSymbol("C"));
        }

        [Test]
        public void FromSymbol_MultipleCharacters()
        {
            Assert.AreEqual(Element.Chlorine, ElementSymbols.GetFromSymbol("Cl"));
        }

        [Test]
        public void FromSymbol_LowerCase()
        {
            Assert.AreEqual(Element.Chlorine, ElementSymbols.GetFromSymbol("cl"));
        }

        [Test]
        public void FromSymbol_UpperCase()
        {
            Assert.AreEqual(Element.Chlorine, ElementSymbols.GetFromSymbol("CL"));
        }

        [Test]
        public void FromSymbol_Whitespace()
        {
            Assert.AreEqual(Element.Chlorine, ElementSymbols.GetFromSymbol(" Cl  "));
        }

        [Test]
        public void FromSymbol_Missing()
        {
            Assert.AreEqual(null, ElementSymbols.GetFromSymbol("Kp"));
        }

        [Test]
        public void FromSymbol_Null()
        {
            Assert.AreEqual(null, ElementSymbols.GetFromSymbol(null));
        }

        [Test]
        public void FromSymbol_Blank()
        {
            Assert.AreEqual(null, ElementSymbols.GetFromSymbol(""));
        }
    }
}