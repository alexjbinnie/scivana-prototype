/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using NUnit.Framework;
using Scivana.Core.Async;

namespace Scivana.Core.Tests.Async
{
    public class CancellableTests
    {
        private class ExampleCancellable : Cancellable
        {
            public ExampleCancellable(params CancellationToken[] tokens) : base(tokens)
            {
            }
        }

        [Test]
        public void Initial_Uncancelled()
        {
            using (var cancellable = new ExampleCancellable())
            {
                Assert.IsFalse(cancellable.IsCancelled);
            }
        }

        [Test]
        public void Cancelled_IsCancelled()
        {
            using (var cancellable = new ExampleCancellable())
            {
                cancellable.Cancel();
                Assert.IsTrue(cancellable.IsCancelled);
            }
        }

        [Test]
        public void ExternalUncancelled_IsNotCancelled()
        {
            using (var external = new CancellationTokenSource())
            {
                using (var cancellable = new ExampleCancellable(external.Token))
                {
                    Assert.IsFalse(cancellable.IsCancelled);
                }
            }
        }

        [Test]
        public void ExternalCancelled_IsCancelled()
        {
            using (var external = new CancellationTokenSource())
            {
                using (var cancellable = new ExampleCancellable(external.Token))
                {
                    external.Cancel();
                    Assert.IsTrue(cancellable.IsCancelled);
                }
            }
        }

        [Test]
        public void ExternalCancelledBeforehand_IsCancelled()
        {
            using (var external = new CancellationTokenSource())
            {
                var token = external.Token;
                external.Cancel();
                using (var cancellable = new ExampleCancellable(token))
                {
                    Assert.IsTrue(cancellable.IsCancelled);
                }
            }
        }

        [Test]
        public void Cancelled_ExternalDoesNotGetCancelled()
        {
            using (var external = new CancellationTokenSource())
            {
                var token = external.Token;
                using (var cancellable = new ExampleCancellable(token))
                {
                    cancellable.Cancel();
                }

                Assert.IsFalse(token.IsCancellationRequested);
            }
        }

        [Test]
        public void GetCancellationToken_Initial()
        {
            using (var cancellable = new ExampleCancellable())
            {
                var token = cancellable.GetCancellationToken();
                Assert.IsFalse(token.IsCancellationRequested);
            }
        }

        [Test]
        public void GetCancellationToken_AlreadyCancelled()
        {
            using (var cancellable = new ExampleCancellable())
            {
                cancellable.Cancel();
                Assert.Throws<InvalidOperationException>(() => cancellable.GetCancellationToken());
            }
        }

        [Test]
        public void GetCancellationToken_WithExternalToken()
        {
            using (var external = new CancellationTokenSource())
            {
                using (var cancellable = new ExampleCancellable(external.Token))
                {
                    var token = cancellable.GetCancellationToken();
                    Assert.IsFalse(token.IsCancellationRequested);
                }
            }
        }

        [Test]
        public void GetCancellationToken_ExternalCancelled()
        {
            using (var external = new CancellationTokenSource())
            {
                using (var cancellable = new ExampleCancellable(external.Token))
                {
                    external.Cancel();
                    Assert.Throws<InvalidOperationException>(() => cancellable.GetCancellationToken());
                }
            }
        }

        [Test]
        public void GetCancellationToken_ExternalCancelledBeforeConstruction()
        {
            using (var external = new CancellationTokenSource())
            {
                var externalToken = external.Token;
                external.Cancel();
                using (var cancellable = new ExampleCancellable(externalToken))
                {
                    Assert.Throws<InvalidOperationException>(() => cancellable.GetCancellationToken());
                }
            }
        }
    }
}