/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Collections;

namespace Scivana.Core.Tests
{
    public class EnumerableExtensionsTest
    {
        [Test]
        public void GetPairs_Empty()
        {
            CollectionAssert.IsEmpty(new string[0].GetPairs());
        }

        [Test]
        public void GetPairs_Single()
        {
            CollectionAssert.IsEmpty((new[]
                                         {
                                             "abc"
                                         }).GetPairs());
        }

        [Test]
        public void GetPairs_Double()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          ("abc", "def")
                                      }, (new[]
                                             {
                                                 "abc", "def"
                                             }).GetPairs());
        }

        [Test]
        public void GetPairs_Three()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          ("abc", "def"), ("def", "ghi")
                                      },
                                      (new[]
                                          {
                                              "abc", "def", "ghi"
                                          }).GetPairs());
        }

        [Test]
        public void IndexOf_Empty()
        {
            Assert.AreEqual(-1,
                            new string[]
                            {
                            }.IndexOf("def"));
        }

        [Test]
        public void IndexOf_First()
        {
            Assert.AreEqual(0,
                            new[]
                            {
                                "abc", "def", "ghi"
                            }.IndexOf("abc"));
        }

        [Test]
        public void IndexOf_Middle()
        {
            Assert.AreEqual(1,
                            new[]
                            {
                                "abc", "def", "ghi"
                            }.IndexOf("def"));
        }

        [Test]
        public void IndexOf_Last()
        {
            Assert.AreEqual(2,
                            new[]
                            {
                                "abc", "def", "ghi"
                            }.IndexOf("ghi"));
        }

        [Test]
        public void IndexOf_Missing()
        {
            Assert.AreEqual(-1,
                            new[]
                            {
                                "abc", "def", "ghi"
                            }.IndexOf("xyz"));
        }

        [Test]
        public void IndexOf_Null()
        {
            Assert.AreEqual(-1,
                            new[]
                            {
                                "abc", "def", "ghi"
                            }.IndexOf(null));
        }

        [Test]
        public void IndexOf_NullInList()
        {
            Assert.AreEqual(1,
                            new[]
                            {
                                null, "def", "ghi"
                            }.IndexOf("def"));
        }

        [Test]
        public void WithoutIndex_Empty()
        {
            CollectionAssert.IsEmpty(new string[0].WithoutIndex(1));
        }

        [Test]
        public void WithoutIndex_Single_IndexBelow()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc"
                                      },
                                      new[]
                                      {
                                          "abc"
                                      }.WithoutIndex(-1)
            );
        }

        [Test]
        public void WithoutIndex_Single()
        {
            CollectionAssert.IsEmpty(new[]
                {
                    "abc"
                }.WithoutIndex(0)
            );
        }

        [Test]
        public void WithoutIndex_Single_IndexAbove()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc"
                                      },
                                      new[]
                                      {
                                          "abc"
                                      }.WithoutIndex(1)
            );
        }

        [Test]
        public void WithoutIndex_Several_IndexBelow()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def", "ghi"
                                      },
                                      new[]
                                      {
                                          "abc", "def", "ghi"
                                      }.WithoutIndex(-1)
            );
        }

        [Test]
        public void WithoutIndex_Several()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "ghi"
                                      },
                                      new[]
                                      {
                                          "abc", "def", "ghi"
                                      }.WithoutIndex(1)
            );
        }

        [Test]
        public void WithoutIndex_Several_IndexAbove()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def", "ghi"
                                      },
                                      new[]
                                      {
                                          "abc", "def", "ghi"
                                      }.WithoutIndex(4)
            );
        }

        [Test]
        public void AsEnumerable()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc"
                                      },
                                      "abc".AsEnumerable()
            );
        }

        [Test]
        public void GetPermutations_Empty()
        {
            CollectionAssert.IsEmpty(new string[0].GetPermutations());
        }

        [Test]
        public void GetPermutations_Single()
        {
            CollectionAssert.AreEqual(new[]
            {
                new[]
                {
                    "abc"
                }
            }, new[]
            {
                "abc"
            }.GetPermutations());
        }

        [Test]
        public void GetPermutations_TwoItems()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          new[]
                                          {
                                              "abc", "def"
                                          },
                                          new[]
                                          {
                                              "def", "abc"
                                          },
                                      },
                                      new[]
                                      {
                                          "abc", "def"
                                      }.GetPermutations());
        }

        [Test]
        public void GetPermutations_ThreeItems()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          new[]
                                          {
                                              "abc", "def", "ghi"
                                          },
                                          new[]
                                          {
                                              "abc", "ghi", "def"
                                          },
                                          new[]
                                          {
                                              "def", "abc", "ghi"
                                          },
                                          new[]
                                          {
                                              "def", "ghi", "abc"
                                          },
                                          new[]
                                          {
                                              "ghi", "abc", "def"
                                          },
                                          new[]
                                          {
                                              "ghi", "def", "abc"
                                          }
                                      },
                                      new[]
                                      {
                                          "abc", "def", "ghi"
                                      }.GetPermutations());
        }
    }
}