﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Scivana.Core.Math;
using Scivana.Core.Testing;
using UnityEngine.TestTools.Utils;
using MatrixPair = System.Tuple<UnityEngine.Matrix4x4, UnityEngine.Matrix4x4>;

namespace Scivana.Core.Tests.Math
{
    internal class MatrixExtensionsTests
    {
        private static MatrixPair GenerateRandomMatrixPair()
        {
            return new MatrixPair(SpatialTestData.GetRandomTransformation().matrix,
                                  SpatialTestData.GetRandomTransformation().matrix);
        }

        private static IEnumerable<Transformation> RandomTransformation =>
            SpatialTestData.GetRandomTransformations(16);

        private static IEnumerable<MatrixPair> RandomMatrixPairs =>
            RandomTestData.SeededRandom(GenerateRandomMatrixPair).Take(16);

        [Test]
        public void GetTranslation_OfRandomTRS_ReturnsInputTranslation(
            [ValueSource(nameof(RandomTransformation))] Transformation transformation)
        {
            Assert.That(transformation.position,
                        Is.EqualTo(transformation.matrix.GetTranslation())
                          .Using(Vector3EqualityComparer.Instance));
        }

        [Test]
        public void GetRotation_OfRandomTRS_ReturnsInputRotation(
            [ValueSource(nameof(RandomTransformation))] Transformation transformation)
        {
            var realRotation = transformation.rotation;
            var testRotation = transformation.matrix.GetRotation();

            Assert.That(realRotation,
                        Is.EqualTo(testRotation)
                          .Using(QuaternionEqualityComparer.Instance));
        }

        [Test]
        public void GetScale_WithScaleTRS_ReturnsInputScale(
            [ValueSource(nameof(RandomTransformation))] Transformation transformation)
        {
            Assert.That(transformation.scale,
                        Is.EqualTo(transformation.matrix.GetScale())
                          .Using(Vector3EqualityComparer.Instance));
        }

        [Test]
        public void GetRelativeTransformationTo_WithRandomTRS_TransformsAToB(
            [ValueSource(nameof(RandomMatrixPairs))] MatrixPair pair)
        {
            var transformation = pair.Item1.GetTransformationTo(pair.Item2);

            Assert.That(pair.Item1.TransformedBy(transformation),
                        Is.EqualTo(pair.Item2)
                          .Using(Matrix4x4TRSEqualityComparer.Instance));
        }

        [Test]
        public void TransformedBy_WithRandomMatrixInverse_IsIdentity(
            [ValueSource(nameof(RandomTransformation))] Transformation transformation)
        {
            var matrix = transformation.matrix;
            var compare = matrix.TransformedBy(matrix.inverse)
                                .TransformedBy(matrix);

            Assert.That(matrix,
                        Is.EqualTo(compare)
                          .Using(Matrix4x4TRSEqualityComparer.Instance));
        }
    }
}