/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Scivana.Core.Math;
using Scivana.Core.Testing;
using UnityEngine;
using Assert = UnityEngine.Assertions.Assert;

namespace Scivana.Core.Tests.Math
{
    public class LinearTransformationTests
    {
        private const int TestCount = 128;

        private static IEnumerable<LinearTransformation> GetTransformations()
        {
            return SpatialTestData.GetRandomLinearTransformations(TestCount, 2412);
        }

        private static IEnumerable<(LinearTransformation, LinearTransformation)>
            GetTransformationPairs()
        {
            return Enumerable.Zip(
                SpatialTestData.GetRandomLinearTransformations(TestCount, 82),
                SpatialTestData.GetRandomLinearTransformations(TestCount, 940),
                (a, b) => (a, b)
            );
        }

        private static IEnumerable<(LinearTransformation, Vector3)>
            GetTransformationAndVectors()
        {
            return Enumerable.Zip(
                SpatialTestData.GetRandomLinearTransformations(TestCount, 21),
                SpatialTestData.GetRandomPositions(TestCount, 2918),
                (a, b) => (a, b)
            );
        }


        #region Constants

        [Test]
        public void Identity()
        {
            Assert.AreEqual(Matrix4x4.identity, LinearTransformation.identity.matrix);
        }

        #endregion


        #region Inverse

        [Test]
        public void Inverse(
            [ValueSource(nameof(GetTransformations))] LinearTransformation transformation)
        {
            TransformationAssert.IsInverseCorrect(transformation);
        }

        #endregion


        #region Matrices

        [Test]
        public void InverseMatrix(
            [ValueSource(nameof(GetTransformations))] LinearTransformation transformation)
        {
            TransformationAssert.IsInverseMatrixCorrect(transformation);
        }

        #endregion


        #region Conversions

        [Test]
        public void AsMatrix(
            [ValueSource(nameof(GetTransformations))] LinearTransformation transformation)
        {
            MathAssert.AreEqual(transformation.matrix, transformation);
        }

        [Test]
        public void AsAffineTransformation(
            [ValueSource(nameof(GetTransformations))] LinearTransformation transformation)
        {
            MathAssert.AreEqual(transformation.matrix,
                                ((AffineTransformation) transformation).matrix);
        }

        #endregion


        #region Multiplication

        [Test]
        public void Multiplication(
            [ValueSource(nameof(GetTransformationPairs))] (LinearTransformation, LinearTransformation) input)
        {
            TransformationAssert.Multiplication(input, input.Item1 * input.Item2);
        }

        #endregion


        #region Transformation of Points

        [Test]
        public void TransformPoint(
            [ValueSource(nameof(GetTransformationAndVectors))]
            (LinearTransformation, Vector3) input)
        {
            TransformationAssert.TransformPoint(input);
        }

        [Test]
        public void InverseTransformPoint(
            [ValueSource(nameof(GetTransformationAndVectors))]
            (LinearTransformation, Vector3) input)
        {
            TransformationAssert.InverseTransformPoint(input);
        }

        #endregion


        #region Transformation of Directions

        [Test]
        public void TransformDirection(
            [ValueSource(nameof(GetTransformationAndVectors))]
            (LinearTransformation, Vector3) input)
        {
            TransformationAssert.TransformDirection(input);
        }

        [Test]
        public void InverseTransformDirection(
            [ValueSource(nameof(GetTransformationAndVectors))]
            (LinearTransformation, Vector3) input)
        {
            TransformationAssert.TransformDirection(input);
        }

        #endregion
    }
}