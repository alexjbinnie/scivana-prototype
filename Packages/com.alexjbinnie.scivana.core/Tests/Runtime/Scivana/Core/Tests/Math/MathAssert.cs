/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Testing;
using UnityEngine;
using UnityEngine.TestTools.Utils;

namespace Scivana.Core.Tests.Math
{
    public class MathAssert
    {
        /// <summary>
        /// Assert that two matrices are equal.
        /// </summary>
        public static void AreEqual(Matrix4x4 expected, Matrix4x4 actual, float error = 1e-3f)
        {
            var comparer = new Matrix4x4RowEqualityComparer(error);
            Assert.That(actual,
                        Is.EqualTo(expected)
                          .Using(comparer));
        }

        /// <summary>
        /// Assert that two quaternions are equal.
        /// </summary>
        public static void AreEqual(Quaternion expected, Quaternion actual, float error = 1e-3f)
        {
            var comparer = new QuaternionEqualityComparer(error);
            Assert.That(actual,
                        Is.EqualTo(expected)
                          .Using(comparer));
        }

        /// <summary>
        /// Assert that two vectors are equal.
        /// </summary>
        public static void AreEqual(Vector3 expected, Vector3 actual, float error = 1e-3f)
        {
            var comparer = new Vector3EqualityComparer(error);
            Assert.That(actual,
                        Is.EqualTo(expected)
                          .Using(comparer));
        }

        /// <summary>
        /// Assert that two floats are equal.
        /// </summary>
        public static void AreEqual(float expected, float actual, float error = 1e-3f)
        {
            var comparer = new FloatEqualityComparer(error);
            Assert.That(actual,
                        Is.EqualTo(expected)
                          .Using(comparer));
        }
    }
}