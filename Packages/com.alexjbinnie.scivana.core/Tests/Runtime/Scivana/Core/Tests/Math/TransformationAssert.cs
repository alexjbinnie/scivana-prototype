/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core.Math;
using UnityEngine;

namespace Scivana.Core.Tests.Math
{
    public static class TransformationAssert
    {
        #region Multiplication

        public static void Multiplication(
            (ITransformation, ITransformation) input,
            ITransformation result)
        {
            var (transformation1, transformation2) = input;
            var matrix1 = transformation1.matrix;
            var matrix2 = transformation2.matrix;
            MathAssert.AreEqual(matrix1 * matrix2, result.matrix);
        }

        #endregion


        #region Transformation of Points

        public static void TransformPoint((ITransformation, Vector3) input)
        {
            var (transformation, vector) = input;
            MathAssert.AreEqual(transformation.matrix.MultiplyPoint3x4(vector),
                                transformation.TransformPoint(vector));
        }

        public static void InverseTransformPoint((ITransformation, Vector3) input)
        {
            var (transformation, vector) = input;
            MathAssert.AreEqual(transformation.matrix.inverse.MultiplyPoint3x4(vector),
                                transformation.InverseTransformPoint(vector));
        }

        #endregion


        #region Transformation of Directions

        public static void TransformDirection((ITransformation, Vector3) input)
        {
            var (transformation, vector) = input;
            MathAssert.AreEqual(transformation.matrix.MultiplyVector(vector),
                                transformation.TransformDirection(vector));
        }

        public static void InverseTransformDirection((ITransformation, Vector3) input)
        {
            var (transformation, vector) = input;
            MathAssert.AreEqual(transformation.matrix.inverse.MultiplyVector(vector),
                                transformation.InverseTransformDirection(vector));
        }

        #endregion

        public static void IsInverseMatrixCorrect(ITransformation transformation)
        {
            var matrix = transformation.matrix;
            MathAssert.AreEqual(matrix.inverse, transformation.inverseMatrix);
        }

        public static void IsInverseCorrect(ITransformation transformation)
        {
            var matrix = transformation.matrix;
            MathAssert.AreEqual(matrix.inverse, transformation.inverse.matrix);
        }
    }
}