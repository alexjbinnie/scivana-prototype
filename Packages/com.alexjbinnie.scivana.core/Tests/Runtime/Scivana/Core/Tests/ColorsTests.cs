/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Globalization;
using NUnit.Framework;
using UnityEngine;

namespace Scivana.Core.Tests
{
    public class ColorsTests
    {
        [Test]
        public void Red()
        {
            Assert.AreEqual(Color.red, Colors.Red);
        }

        [Test]
        public void Blue()
        {
            Assert.AreEqual(Color.blue, Colors.Blue);
        }

        [Test]
        public void Green()
        {
            Assert.AreEqual(Color.green, Colors.Lime);
        }

        [Test]
        public void Black()
        {
            Assert.AreEqual(Color.black, Colors.Black);
        }

        [Test]
        public void White()
        {
            Assert.AreEqual(Color.white, Colors.White);
        }

        [Test]
        public void AquaCyan_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("aqua"),
                            Colors.GetColorFromName("cyan"));
        }

        [Test]
        public void FuchsiaMagenta_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("fuchsia"),
                            Colors.GetColorFromName("magenta"));
        }

        [Test]
        public void GreyGray_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("grey"),
                            Colors.GetColorFromName("gray"));
        }

        [Test]
        public void DarkGreyGray_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("darkgrey"),
                            Colors.GetColorFromName("darkgray"));
        }

        [Test]
        public void DimGreyGray_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("dimgrey"),
                            Colors.GetColorFromName("dimgray"));
        }

        [Test]
        public void DarkSlateGreyGray_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("darkslategrey"),
                            Colors.GetColorFromName("darkslategray"));
        }

        [Test]
        public void LightGreyGray_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("lightgrey"),
                            Colors.GetColorFromName("lightgray"));
        }

        [Test]
        public void LightSlateGreyGray_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("lightslategrey"),
                            Colors.GetColorFromName("lightslategray"));
        }

        [Test]
        public void SlateGreyGray_Equivalent()
        {
            Assert.AreEqual(Colors.GetColorFromName("slategrey"),
                            Colors.GetColorFromName("slategray"));
        }

        private static IEnumerable<string> GetCssColorNames()
        {
            return new[]
            {
                "aliceblue",
                "antiquewhite",
                "aqua",
                "aquamarine",
                "azure",
                "beige",
                "bisque",
                "black",
                "blanchedalmond",
                "blue",
                "blueviolet",
                "brown",
                "burlywood",
                "cadetblue",
                "chartreuse",
                "chocolate",
                "coral",
                "cornflowerblue",
                "cornsilk",
                "crimson",
                "cyan",
                "darkblue",
                "darkcyan",
                "darkgoldenrod",
                "darkgray",
                "darkgreen",
                "darkgrey",
                "darkkhaki",
                "darkmagenta",
                "darkolivegreen",
                "darkorange",
                "darkorchid",
                "darkred",
                "darksalmon",
                "darkseagreen",
                "darkslateblue",
                "darkslategray",
                "darkslategrey",
                "darkturquoise",
                "darkviolet",
                "deeppink",
                "deepskyblue",
                "dimgray",
                "dimgrey",
                "dodgerblue",
                "firebrick",
                "floralwhite",
                "forestgreen",
                "fuchsia",
                "gainsboro",
                "ghostwhite",
                "gold",
                "goldenrod",
                "gray",
                "green",
                "greenyellow",
                "grey",
                "honeydew",
                "hotpink",
                "indianred",
                "indigo",
                "ivory",
                "khaki",
                "lavender",
                "lavenderblush",
                "lawngreen",
                "lemonchiffon",
                "lightblue",
                "lightcoral",
                "lightcyan",
                "lightgoldenrodyellow",
                "lightgray",
                "lightgreen",
                "lightgrey",
                "lightpink",
                "lightsalmon",
                "lightseagreen",
                "lightskyblue",
                "lightslategray",
                "lightslategrey",
                "lightsteelblue",
                "lightyellow",
                "lime",
                "limegreen",
                "linen",
                "magenta",
                "maroon",
                "mediumaquamarine",
                "mediumblue",
                "mediumorchid",
                "mediumpurple",
                "mediumseagreen",
                "mediumslateblue",
                "mediumspringgreen",
                "mediumturquoise",
                "mediumvioletred",
                "midnightblue",
                "mintcream",
                "mistyrose",
                "moccasin",
                "navajowhite",
                "navy",
                "oldlace",
                "olive",
                "olivedrab",
                "orange",
                "orangered",
                "orchid",
                "palegoldenrod",
                "palegreen",
                "paleturquoise",
                "palevioletred",
                "papayawhip",
                "peachpuff",
                "peru",
                "pink",
                "plum",
                "powderblue",
                "purple",
                "red",
                "rosybrown",
                "royalblue",
                "saddlebrown",
                "salmon",
                "sandybrown",
                "seagreen",
                "seashell",
                "sienna",
                "silver",
                "skyblue",
                "slateblue",
                "slategray",
                "slategrey",
                "snow",
                "springgreen",
                "steelblue",
                "tan",
                "teal",
                "thistle",
                "tomato",
                "turquoise",
                "violet",
                "wheat",
                "white",
                "whitesmoke",
                "yellow",
                "yellowgreen"
            };
        }

        [Test]
        public void GetLowerCaseCssColor([ValueSource(nameof(GetCssColorNames))] string name)
        {
            Assert.IsNotNull(Colors.GetColorFromName(name));
        }

        [Test]
        public void GetUpperCaseCssColor([ValueSource(nameof(GetCssColorNames))] string name)
        {
            Assert.IsNotNull(Colors.GetColorFromName(name.ToUpper(CultureInfo.InvariantCulture)));
        }

        [Test]
        public void GetMissingColor()
        {
            Assert.IsNull(Colors.GetColorFromName("definately_not_a_color"));
        }

        [Test]
        public void GetEmptyStringColor()
        {
            Assert.IsNull(Colors.GetColorFromName(""));
        }

        [Test]
        public void GetNullColor()
        {
            Assert.IsNull(Colors.GetColorFromName(null));
        }

        [Test]
        public void DoesColorModificationChangeDefinition()
        {
            var red = Colors.Red;
            red.r = 0;
            var newRed = Colors.Red;
            Assert.AreEqual(1, newRed.r);
        }
    }
}