/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Specialized;
using NUnit.Framework;
using Scivana.Core.Collections;

namespace Scivana.Core.Tests.Collections
{
    internal class NotifyCollectionChangedTests
    {
        private ObservableDictionary<string, object> dictionary
            = new ObservableDictionary<string, object>();

        private NotifyCollectionChangedEventArgs eventArgs;

        [SetUp]
        public void Setup()
        {
            dictionary = new ObservableDictionary<string, object>();
            dictionary.CollectionChanged += (sender, args) => eventArgs = args;
        }

        [Test]
        public void ClearEmptyDictionary()
        {
            dictionary.Clear();
            var (changed, removals) = eventArgs.AsChangesAndRemovals<string>();
            CollectionAssert.IsEmpty(changed);
            CollectionAssert.IsEmpty(removals);
        }

        [Test]
        public void AddItem()
        {
            dictionary.Add("abc", 1.4);
            var (changed, removals) = eventArgs.AsChangesAndRemovals<string>();
            CollectionAssert.AreEquivalent(new[]
            {
                "abc"
            }, changed);
            CollectionAssert.IsEmpty(removals);
        }

        [Test]
        public void RemoveItem()
        {
            dictionary.Add("abc", 1.4);
            dictionary.Remove("abc");
            var (changed, removals) = eventArgs.AsChangesAndRemovals<string>();
            CollectionAssert.IsEmpty(changed);
            CollectionAssert.AreEquivalent(new[]
            {
                "abc"
            }, removals);
        }

        [Test]
        public void SetItem()
        {
            dictionary.Add("abc", 1.4);
            eventArgs = null;
            dictionary["abc"] = "xyz";
            var (changed, removals) = eventArgs.AsChangesAndRemovals<string>();
            CollectionAssert.AreEquivalent(new[]
            {
                "abc"
            }, changed);
            CollectionAssert.IsEmpty(removals);
        }

        [Test]
        public void Clear()
        {
            dictionary.Add("abc", 1.4);
            dictionary.Add("def", "xyz");
            eventArgs = null;
            dictionary.Clear();
            var (changed, removals) = eventArgs.AsChangesAndRemovals<string>();
            CollectionAssert.IsEmpty(changed);
            CollectionAssert.AreEquivalent(new[]
            {
                "abc", "def"
            }, removals);
        }
    }
}