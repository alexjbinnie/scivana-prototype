/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Collections;

namespace Scivana.Core.Tests.Collections
{
    public class PrettyEnumerableTests
    {
        [Test]
        public void EmptyEnumerable()
        {
            Assert.AreEqual("", new int[0].AsPretty().ToString());
        }

        [Test]
        public void SingleInt()
        {
            Assert.AreEqual("32", new[]
            {
                32
            }.AsPretty().ToString());
        }

        [Test]
        public void MultipleInts()
        {
            Assert.AreEqual("32, 56", new[]
            {
                32, 56
            }.AsPretty().ToString());
        }

        [Test]
        public void NullArgument()
        {
            Assert.AreEqual("abc, null, def", new[]
            {
                "abc", null, "def"
            }.AsPretty().ToString());
        }

        [Test]
        public void CustomNamer()
        {
            Assert.AreEqual("A, A", new[]
            {
                32, 56
            }.AsPretty(i => "A").ToString());
        }

        [Test]
        public void WorksAsEnumerable()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def"
                                      },
                                      new[]
                                      {
                                          "abc", "def"
                                      }.AsPretty());
        }
    }
}