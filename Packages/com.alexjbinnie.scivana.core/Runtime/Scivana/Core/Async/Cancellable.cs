/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;

namespace Scivana.Core.Async
{
    /// <summary>
    /// An abstract base class of an object which can be cancelled. It can be cancelled directly using
    /// <see cref="Cancel" />, but can also be passed multiple external <see cref="CancellationToken" />s.
    /// </summary>
    public abstract class Cancellable : ICancellable, IDisposable
    {
        private readonly CancellationTokenSource cancellationSource;

        /// <summary>
        /// Gets a value indicating whether this object been cancelled. If so, further calls to
        /// <see cref="GetCancellationToken" /> will raise an <see cref="InvalidOperationException" />.
        /// </summary>
        public bool IsCancelled => cancellationSource.IsCancellationRequested;

        /// <summary>
        /// Initializes a new instance of the <see cref="Cancellable" /> class, with zero or more cancellation
        /// tokens which can cancel it.
        /// </summary>
        protected Cancellable(params CancellationToken[] externalTokens)
        {
            if (externalTokens.Length == 0)
            {
                cancellationSource = new CancellationTokenSource();
            }
            else
            {
                cancellationSource =
                    CancellationTokenSource.CreateLinkedTokenSource(externalTokens);
            }
        }

        /// <inheritdoc cref="ICancellationTokenSource.GetCancellationToken" />
        public CancellationToken GetCancellationToken()
        {
            if (IsCancelled)
                throw new InvalidOperationException("Object already cancelled.");

            return cancellationSource.Token;
        }

        /// <inheritdoc cref="ICancellable.Cancel" />
        public void Cancel()
        {
            cancellationSource.Cancel();
        }

        /// <inheritdoc cref="IDisposable.Dispose" />
        public void Dispose()
        {
            cancellationSource?.Dispose();
        }
    }
}