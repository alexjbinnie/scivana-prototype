/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading.Tasks;

namespace Scivana.Core.Async
{
    /// <summary>
    /// Extension methods for <see cref="Task" />.
    /// </summary>
    public static class TaskExtensions
    {
        /// <summary>
        /// Force a <see cref="Task" /> to complete synchronously.
        /// </summary>
        public static void AwaitInForegroundWithTimeout(this Task task,
                                                        int? timeoutMilliseconds = null)
        {
            if (timeoutMilliseconds.HasValue)
                task.Wait(timeoutMilliseconds.Value);
            else
                task.Wait();
        }

        /// <summary>
        /// Await an async <see cref="Task" /> in the background and defer exception handling to Unity.
        /// </summary>
        /// <remarks>
        /// Exceptions thrown in this task will appear in the Unity console whereas exceptions thrown in a
        /// non-awaited async <see cref="Task" /> are not reliably caught.
        /// </remarks>
        public static async void AwaitInBackground(this Task task)
        {
            await task;
        }

        /// <summary>
        /// Await an async <see cref="Task" /> in the background and defer exception handling to Unity, except
        /// for
        /// exceptions relating to task cancellation which are ignore.
        /// </summary>
        /// <remarks>
        /// Exceptions thrown in this task will appear in the Unity console whereas exceptions thrown in a
        /// non-awaited async <see cref="Task" /> are not reliably caught.
        /// </remarks>
        public static async void AwaitInBackgroundIgnoreCancellation(this Task task)
        {
            try
            {
                await task;
            }
            catch (OperationCanceledException)
            {
            }
        }

        /// <summary>
        /// Await an async <see cref="Task" /> in the background and defer exception handling to Unity, except
        /// for
        /// exceptions relating to task cancellation which are ignore.
        /// </summary>
        /// <remarks>
        /// Exceptions thrown in this task will appear in the Unity console whereas exceptions thrown in a
        /// non-awaited async <see cref="Task" /> are not reliably caught.
        /// </remarks>
        public static async void AwaitInBackgroundHandleException(this Task task,
                                                                  Action<Exception> handler)
        {
            try
            {
                await task;
            }
            catch (Exception e)
            {
                handler(e);
            }
        }
    }
}