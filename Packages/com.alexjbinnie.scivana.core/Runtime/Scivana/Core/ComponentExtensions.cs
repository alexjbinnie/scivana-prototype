﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace Scivana.Core
{
    /// <summary>
    /// Extension methods for Unity components.
    /// </summary>
    public static class ComponentExtensions
    {
        /// <summary>
        /// Set the game object this component is attached to as active.
        /// </summary>
        public static TComponent SetActive<TComponent>(this TComponent component, bool active)
            where TComponent : Component
        {
            component.gameObject.SetActive(active);
            return component;
        }

        /// <summary>
        /// Set the parent of the game object this component is attached to.
        /// </summary>
        public static TComponent SetParent<TComponent>(this TComponent component,
                                                       Transform parent,
                                                       bool worldPositionStays = true)
            where TComponent : Component
        {
            component.transform.SetParent(parent, worldPositionStays);
            return component;
        }

        /// <summary>
        /// Set the game object that contains this component to be positioned in the same world
        /// position and orientation as its parent.
        /// </summary>
        public static TComponent SetToLocalIdentity<TComponent>(this TComponent component)
            where TComponent : Component
        {
            var transform = component.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
            return component;
        }

        public static GameObject SetToLocalIdentity(this GameObject component)
        {
            var transform = component.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
            return component;
        }

        public static TComponent SetWorldPosition<TComponent>(this TComponent component, Vector3 position)
            where TComponent : Component
        {
            var transform = component.transform;
            transform.position = position;
            return component;
        }
    }
}