/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace Scivana.Core
{
    /// <summary>
    /// Defines an interface which provides a mapping between two types. It is
    /// essentially a <see cref="Converter{TInput,TOutput}" /> as an interface.
    /// </summary>
    public interface IMapping<in TFrom, out TTo>
    {
        /// <summary>
        /// Map an input value to an output value.
        /// </summary>
        TTo Map(TFrom from);
    }

    internal class DictionaryAsMapping<TFrom, TTo> : IMapping<TFrom, TTo>
    {
        private IReadOnlyDictionary<TFrom, TTo> dictionary;

        private TTo defaultValue;

        internal DictionaryAsMapping(IReadOnlyDictionary<TFrom, TTo> dict,
                                     TTo defaultValue = default)
        {
            dictionary = dict;
            this.defaultValue = defaultValue;
        }

        /// <inheritdoc cref="IMapping{TFrom,TTo}.Map" />
        public TTo Map(TFrom from)
        {
            return dictionary.TryGetValue(from, out var value) ? value : defaultValue;
        }
    }

    /// <summary>
    /// Extensions methods for <see cref="IMapping{TFrom,TTo}" />.
    /// </summary>
    public static class MappingExtensions
    {
        /// <summary>
        /// Convert a dictionary into an <see cref="IMapping{TFrom,TTo}" />.
        /// </summary>
        public static IMapping<TFrom, TTo> AsMapping<TFrom, TTo>(
            this IReadOnlyDictionary<TFrom, TTo> dict,
            TTo defaultValue = default)
        {
            return new DictionaryAsMapping<TFrom, TTo>(dict, defaultValue);
        }
    }
}