/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Scivana.Core.Serialization
{
    /// <summary>
    /// A <see cref="JsonReader" /> for use with json.NET that can convert an object
    /// from a
    /// JSON-like representation, consisting of <see cref="Dictionary{TKey,TValue}" />,
    /// <see cref="List{Object}" />, <see cref="string" />, <see cref="float" />,
    /// <see cref="string" /> and <see cref="bool" />.
    /// </summary>
    public class CSharpObjectReader : JsonReader
    {
        private IEnumerator<(JsonToken, object)> iterator;

        /// <summary>
        /// Initializes a new instance of the <see cref="CSharpObjectReader" /> class, which reads
        /// a given object into a JSON format.
        /// </summary>
        public CSharpObjectReader(object obj)
        {
            iterator = Iterate(obj).GetEnumerator();
        }

        private static IEnumerable<(JsonToken, object)> Iterate(object obj)
        {
            switch (obj)
            {
                case IReadOnlyDictionary<string, object> dict:
                    yield return (JsonToken.StartObject, null);
                    foreach (var (key, value) in dict)
                    {
                        yield return (JsonToken.PropertyName, key);
                        foreach (var v in Iterate(value))
                            yield return v;
                    }

                    yield return (JsonToken.EndObject, null);
                    break;
                case IReadOnlyList<object> list:
                    yield return (JsonToken.StartArray, null);
                    foreach (var item in list)
                    {
                        foreach (var v in Iterate(item))
                            yield return v;
                    }

                    yield return (JsonToken.EndArray, null);
                    break;
                case string str:
                    yield return (JsonToken.String, str);
                    break;
                case bool bol:
                    yield return (JsonToken.Boolean, bol);
                    break;
                case int nt:
                    yield return (JsonToken.Integer, nt);
                    break;
                case float flt:
                    yield return (JsonToken.Float, flt);
                    break;
                case double dbl:
                    yield return (JsonToken.Float, dbl);
                    break;
                case null:
                    yield return (JsonToken.Null, null);
                    break;
                default:
                    throw new ArgumentException($"Cannot parse {obj}");
            }
        }

        /// <inheritdoc />
        public override bool Read()
        {
            if (!iterator.MoveNext())
                return false;
            var (jsonToken, value) = iterator.Current;
            SetToken(jsonToken, value);
            return true;
        }
    }
}