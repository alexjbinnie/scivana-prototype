/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Scivana.Core.Serialization
{
    /// <summary>
    /// Serialization methods for converting to and from serializable C# objects
    /// (consisting of
    /// dictionaries, lists and primitives) using JSON.NET.
    /// </summary>
    public static class Serialization
    {
        private static readonly JsonSerializer serializer = new JsonSerializer()
        {
            Converters =
            {
                new Vector3Converter(),
                new QuaternionConverter(),
                new ColorConverter(),
                new ToObjectConverter(),
            },
        };

        /// <summary>
        /// Add a <see cref="JsonConverter" /> for handling a specific type.
        /// </summary>
        public static void AddConverter(JsonConverter converter)
        {
            serializer.Converters.Add(converter);
        }

        /// <summary>
        /// Serialize an object from a JSON string.
        /// </summary>
        public static T FromJSON<T>(string json)
        {
            using (var str = new StringReader(json))
            using (var reader = new JsonTextReader(str))
            {
                return serializer.Deserialize<T>(reader);
            }
        }

        /// <summary>
        /// Serialize an object from a data structure consisting of
        /// <see cref="Dictionary{TKey,TValue}" />,
        /// <see cref="List{Object}" />, <see cref="string" />, <see cref="float" />,
        /// <see cref="string" /> and <see cref="bool" />, using a
        /// <see cref="JsonSerializer" />.
        /// </summary>
        public static T FromDataStructure<T>(object data)
        {
            using (var reader = new CSharpObjectReader(data))
            {
                return serializer.Deserialize<T>(reader);
            }
        }

        /// <summary>
        /// Update an object from a data structure consisting of
        /// <see cref="Dictionary{TKey,TValue}" />,
        /// <see cref="List{Object}" />, <see cref="string" />, <see cref="float" />,
        /// <see cref="string" /> and <see cref="bool" />, using a
        /// <see cref="JsonSerializer" />.
        /// </summary>
        public static void UpdateFromDataStructure(object data, object target)
        {
            using (var reader = new CSharpObjectReader(data))
            {
                serializer.Populate(reader, target);
            }
        }

        /// <summary>
        /// Deserialize an object from a data structure consisting of
        /// <see cref="Dictionary{TKey,TValue}" />,
        /// <see cref="List{Object}" />, <see cref="string" />, <see cref="float" />,
        /// <see cref="string" /> and <see cref="bool" />, using a
        /// <see cref="JsonSerializer" />.
        /// </summary>
        /// <remarks>
        /// If dictionaries or lists are present in data, then these will be deserialized
        /// into <see cref="JObject" /> and <see cref="JArray" />.
        /// </remarks>
        public static object FromDataStructure(object data)
        {
            using (var reader = new CSharpObjectReader(data))
            {
                return serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Deserialize an object of a specific type from a data structure consisting of
        /// <see cref="Dictionary{TKey,TValue}" />,
        /// <see cref="List{Object}" />, <see cref="string" />, <see cref="float" />,
        /// <see cref="string" /> and <see cref="bool" />, using a
        /// <see cref="JsonSerializer" />.
        /// </summary>
        /// <remarks>
        /// If raw dictionaries or lists are present in data, then these will be
        /// deserialized into <see cref="JObject" /> and <see cref="JArray" />.
        /// </remarks>
        public static object ToDataStructure(object data)
        {
            using (var writer = new CSharpObjectWriter())
            {
                serializer.Serialize(writer, data);
                return writer.Object;
            }
        }
    }
}