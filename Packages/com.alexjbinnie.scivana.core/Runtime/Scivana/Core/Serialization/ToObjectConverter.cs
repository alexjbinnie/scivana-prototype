/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Scivana.Core.Serialization
{
    public class ToObjectConverter : JsonConverter
    {
        /// <inheritdoc />
        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer)
        {
            var obj = JToken.Load(reader);
            if (obj.Type == JTokenType.Object)
            {
                var arr = (JObject) obj;
                return ToDictionary(arr);
            }

            if (obj.Type == JTokenType.Array)
            {
                var arr = (JArray) obj;
                return ToArray(arr);
            }

            return obj.ToObject<object>();
        }

        private object[] ToArray(JArray arr)
        {
            var array = arr.ToObject<object[]>();
            for (var i = 0; i < array.Length; i++)
            {
                switch (array[i])
                {
                    case JObject nestedObject:
                        array[i] = ToDictionary(nestedObject);
                        break;
                    case JArray nestedArray:
                        array[i] = ToArray(nestedArray);
                        break;
                }
            }

            return array;
        }

        private Dictionary<string, object> ToDictionary(JObject obj)
        {
            var dictionary = obj.ToObject<Dictionary<string, object>>();

            foreach (var key in dictionary.Keys.ToArray())
            {
                switch (dictionary[key])
                {
                    case JObject nestedObject:
                        dictionary[key] = ToDictionary(nestedObject);
                        break;
                    case JArray nestedArray:
                        dictionary[key] = ToArray(nestedArray);
                        break;
                }
            }

            return dictionary;
        }

        public override bool CanWrite => false;

        /// <inheritdoc />
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(object);
        }
    }
}