﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scivana.Core
{
    /// <summary>
    /// A pool of objects which can store excess objects in an inactive state to conteract
    /// performance issues relating to instantiating and destroying objects.
    /// </summary>
    public static class IndexedPool
    {
        /// <summary>
        /// Create an <see cref="IndexedPool" /> consisting of instances of a given object, with
        /// the objects having their <see cref="GameObject" /> set to inactive when not in use.
        /// </summary>
        public static IndexedPool<TItem> Create<TItem>(TItem prefab)
            where TItem : MonoBehaviour
        {
            return new IndexedPool<TItem>(
                () => prefab.InstantiateInactive(),
                transform => transform.gameObject.SetActive(true),
                transform => transform.gameObject.SetActive(false)
            );
        }

        /// <summary>
        /// Create an <see cref="IndexedPool" /> consisting of instances of a given object, with
        /// the objects being set to inactive when not in use.
        /// </summary>
        public static IndexedPool<GameObject> Create(GameObject prefab)
        {
            return new IndexedPool<GameObject>(
                () => prefab.InstantiateInactive(),
                transform => transform.gameObject.SetActive(true),
                transform => transform.gameObject.SetActive(false));
        }

        /// <summary>
        /// Create an <see cref="IndexedPool" /> consisting of instances of a given object with the
        /// given parent, and the objects having their <see cref="GameObject" /> set to inactive when
        /// not in use.
        /// </summary>
        public static IndexedPool<TItem> Create<TItem>(TItem prefab, Transform parent)
            where TItem : MonoBehaviour
        {
            return new IndexedPool<TItem>(
                () => prefab.InstantiateInactive(parent),
                transform => transform.gameObject.SetActive(true),
                transform => transform.gameObject.SetActive(false));
        }
    }

    /// <summary>
    /// Utility class for creating and managing a pool of instances of some
    /// object.
    /// </summary>
    /// <remarks>
    /// The purpose of pooling is to avoid creating new instances from scratch
    /// when the number of instances may be fluctuating frequently and instance
    /// creation has some non-negligible cost.
    /// </remarks>
    public sealed class IndexedPool<TInstance> : IReadOnlyCollection<TInstance>
    {
        /// <inheritdoc />
        int IReadOnlyCollection<TInstance>.Count => ActiveInstanceCount;

        /// <inheritdoc />
        public IEnumerator<TInstance> GetEnumerator()
        {
            for (var i = 0; i < ActiveInstanceCount; ++i)
            {
                yield return instances[i];
            }
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// The number of active instances.
        /// </summary>
        public int ActiveInstanceCount { get; private set; }

        /// <summary>
        /// Return the instance as the given index. There are always at least
        /// `ActiveInstanceCount` instances.
        /// </summary>
        public TInstance this[int index] => instances[index];

        /// <summary>
        /// Raised when an instance is about to be used.
        /// </summary>
        public event Action<TInstance> InstanceActivated;

        /// <summary>
        /// Raised when a previously used instance becomes unused.
        /// </summary>
        public event Action<TInstance> InstanceDeactivated;

        /// <summary>
        /// Callbacks for creating a new instance from scratch.
        /// </summary>
        private readonly Func<TInstance> createInstanceCallback;

        /// <summary>
        /// Callback for activating a previously deactivated instance.
        /// </summary>
        private readonly Action<TInstance> activateInstanceCallback;

        /// <summary>
        /// Callback for deactivating a previously active instance.
        /// </summary>
        private readonly Action<TInstance> deactivateInstanceCallback;

        /// <summary>
        /// The store of existing instances, including inactive instances that
        /// may be reused later if necessary.
        /// </summary>
        private readonly List<TInstance> instances = new List<TInstance>();

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexedPool{TInstance}" /> class with a
        /// given callback for creating new instances, and optionally callbacks for activating and
        /// deactivating instances when they switch in and out of use.
        /// </summary>
        public IndexedPool(Func<TInstance> createInstanceCallback,
                           Action<TInstance> activateInstanceCallback = null,
                           Action<TInstance> deactivateInstanceCallback = null)
        {
            this.createInstanceCallback = createInstanceCallback;
            this.activateInstanceCallback = activateInstanceCallback;
            this.deactivateInstanceCallback = deactivateInstanceCallback;
        }

        /// <summary>
        /// Ensure an exact number of instances are active, creating new
        /// instances if necessary, otherwise reusing existing instances and
        /// deactivating surplus instances.
        /// </summary>
        public void SetActiveInstanceCount(int count)
        {
            for (var i = instances.Count; i < count; ++i)
                instances.Add(createInstanceCallback());

            for (var i = ActiveInstanceCount; i < count; ++i)
            {
                activateInstanceCallback?.Invoke(instances[i]);
                InstanceActivated?.Invoke(instances[i]);
            }

            for (var i = count; i < instances.Count; ++i)
            {
                deactivateInstanceCallback?.Invoke(instances[i]);
                InstanceDeactivated?.Invoke(instances[i]);
            }

            ActiveInstanceCount = count;
        }

        /// <summary>
        /// Make sure that there are exactly as many instances as configs given
        /// then run a mapping function pairing every config to its own
        /// instance.
        /// </summary>
        public void MapConfig<TConfig>(IEnumerable<TConfig> configs,
                                       Action<TConfig, TInstance> mapConfigToInstance)
        {
            var count = 0;

            foreach (var config in configs)
            {
                if (instances.Count <= count)
                    instances.Add(createInstanceCallback());

                mapConfigToInstance(config, instances[count]);
                count += 1;
            }

            SetActiveInstanceCount(count);
        }

        /// <summary>
        /// Set the count and then run a mapping function over every active
        /// instance.
        /// </summary>
        public void MapIndex(int count, Action<int, TInstance> mapIndexToInstance)
        {
            SetActiveInstanceCount(count);

            for (var i = 0; i < ActiveInstanceCount; ++i)
            {
                mapIndexToInstance(i, instances[i]);
            }
        }
    }
}