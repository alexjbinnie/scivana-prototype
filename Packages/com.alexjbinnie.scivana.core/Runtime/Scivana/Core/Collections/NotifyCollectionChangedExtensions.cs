/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Scivana.Core.Collections
{
    /// <summary>
    /// Extension methods relating to <see cref="INotifyCollectionChanged" />.
    /// </summary>
    public static class NotifyCollectionChangedExtensions
    {
        /// <summary>
        /// Expresses a <see cref="NotifyCollectionChangedEventArgs" /> as a set of changed
        /// values and a set of removed values.
        /// </summary>
        public static (IEnumerable<TValue> Changes, IEnumerable<TValue> Removals)
            AsChangesAndRemovals<TValue>(this NotifyCollectionChangedEventArgs args)
        {
            var newItems = args.NewItems?.Cast<TValue>().ToArray() ?? new TValue[0];
            var oldItems = args.OldItems?.Cast<TValue>().ToArray() ?? new TValue[0];
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    return (newItems, new TValue[0]);
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Remove:
                    return (new TValue[0], oldItems);
                case NotifyCollectionChangedAction.Replace:
                    return (newItems, oldItems.Except(newItems));
                case NotifyCollectionChangedAction.Reset:
                    return (new TValue[0], oldItems);
                default:
                    throw new ArgumentException($"No support for {args}");
            }

            return (new TValue[0], new TValue[0]);
        }
    }
}