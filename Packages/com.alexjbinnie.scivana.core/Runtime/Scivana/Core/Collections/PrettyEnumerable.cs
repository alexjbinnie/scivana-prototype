/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Scivana.Core.Collections
{
    /// <summary>
    /// Wrapper around an <see cref="IEnumerable" /> that overrides the
    /// <see cref="ToString()" /> method to provide a list of its contents, optionally
    /// with a custom function to generate the names of each element.
    /// </summary>
    /// <remarks>
    /// Useful for Unit Tests which accept a generated <see cref="IEnumerable" />
    /// parameter, so that the test log is more clear on the contents of the parameter.
    /// </remarks>
    internal class PrettyEnumerable<T> : IEnumerable<T>
    {
        private readonly IEnumerable<T> enumerable;
        private readonly Func<T, string> getName;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrettyEnumerable{T}" /> class, which wraps
        /// around an <see cref="IEnumerable{T}" /> using the items built in
        /// <see cref="ToString" /> function.
        /// </summary>
        internal PrettyEnumerable(IEnumerable<T> enumerable)
            : this(enumerable, t => t?.ToString() ?? "null")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrettyEnumerable{T}" /> class, which wraps
        /// an <see cref="IEnumerable{T}" />, with a function which generates the display name for
        /// each element.
        /// </summary>
        internal PrettyEnumerable(IEnumerable<T> enumerable, Func<T, string> getName)
        {
            this.enumerable = enumerable;
            this.getName = getName;
        }

        /// <inheritdoc />
        public IEnumerator<T> GetEnumerator()
        {
            return enumerable.GetEnumerator();
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return string.Join(", ", enumerable.Select(getName));
        }
    }
}