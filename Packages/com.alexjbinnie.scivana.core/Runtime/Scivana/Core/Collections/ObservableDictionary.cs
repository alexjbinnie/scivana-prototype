/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using JetBrains.Annotations;

namespace Scivana.Core.Collections
{
    /// <summary>
    /// A dictionary which implements <see cref="INotifyCollectionChanged" /> on the
    /// keys, such that <see cref="CollectionChanged" /> is invoked when something is
    /// added, removed or updated in the dictionary.
    /// </summary>
    public class ObservableDictionary<TKey, TValue> : INotifyCollectionChanged,
                                                      IDictionary<TKey, TValue>
    {
        [NotNull]
        private readonly IDictionary<TKey, TValue> dictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableDictionary{TKey, TValue}" /> class,
        /// wrapping an existing <paramref name="dictionary" />.
        /// </summary>
        public ObservableDictionary([NotNull] IDictionary<TKey, TValue> dictionary)
        {
            this.dictionary = dictionary;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableDictionary{TKey, TValue}" /> class,
        /// instantiating a new empty dictionary to be wrapped.
        /// </summary>
        public ObservableDictionary()
        {
            dictionary = new Dictionary<TKey, TValue>();
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            dictionary.Add(item);

            var eventArgs = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Add,
                new[] { item.Key });

            CollectionChanged?.Invoke(this, eventArgs);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public void Clear()
        {
            var keys = Keys.ToArray();
            dictionary.Clear();
            CollectionChanged?.Invoke(this,
                                      new NotifyCollectionChangedEventArgs(
                                          NotifyCollectionChangedAction.Remove,
                                          keys));
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return dictionary.Contains(item);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            dictionary.CopyTo(array, arrayIndex);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            var removed = dictionary.Remove(item);

            var eventArgs = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Remove,
                new[] { item.Key });

            if (removed)
                CollectionChanged?.Invoke(this, eventArgs);

            return removed;
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public int Count => dictionary.Count;

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool IsReadOnly => dictionary.IsReadOnly;

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public void Add(TKey key, TValue value)
        {
            dictionary.Add(key, value);

            var eventArgs = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Add,
                new[] { key });

            CollectionChanged?.Invoke(this, eventArgs);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool ContainsKey(TKey key)
        {
            return dictionary.ContainsKey(key);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool Remove(TKey key)
        {
            var result = dictionary.Remove(key);

            var eventArgs = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Remove,
                new[] { key });

            CollectionChanged?.Invoke(this, eventArgs);
            return result;
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool TryGetValue(TKey key, out TValue value)
        {
            return dictionary.TryGetValue(key, out value);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public TValue this[TKey key]
        {
            get => dictionary[key];
            set
            {
                var existingKey = ContainsKey(key);
                dictionary[key] = value;
                if (existingKey)
                {
                    var eventArgs = new NotifyCollectionChangedEventArgs(
                        NotifyCollectionChangedAction.Replace,
                        new[] { key },
                        new[] { key });

                    CollectionChanged?.Invoke(this, eventArgs);
                }
                else
                {
                    var eventArgs = new NotifyCollectionChangedEventArgs(
                        NotifyCollectionChangedAction.Add,
                        new[] { key });

                    CollectionChanged?.Invoke(this, eventArgs);
                }
            }
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public ICollection<TKey> Keys => dictionary.Keys;

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public ICollection<TValue> Values => dictionary.Values;

        /// <inheritdoc cref="INotifyCollectionChanged" />
        public event NotifyCollectionChangedEventHandler CollectionChanged;
    }
}