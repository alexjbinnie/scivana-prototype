/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Scivana.Core.Collections
{
    /// <summary>
    /// Extension methods for shorthand methods for dealing with all enumerables.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Enumerate over all pairs of adjacent items in a list, such that the set [A, B,
        /// C, D] yields the pairs (A, B), (B, C) and (C, D).
        /// </summary>
        public static IEnumerable<(TElement First, TElement Second)> GetPairs<TElement>(
            this IEnumerable<TElement> enumerable)
        {
            var started = false;
            var prev = default(TElement);
            foreach (var e in enumerable)
            {
                if (!started)
                {
                    started = true;
                    prev = e;
                }
                else
                {
                    yield return (prev, e);
                    prev = e;
                }
            }
        }

        /// <summary>
        /// Find the index of an item in an enumerable, returning -1 if the item is not
        /// present.
        /// </summary>
        public static int IndexOf<TElement>(this IEnumerable<TElement> list, TElement item)
        {
            var i = 0;
            foreach (var thing in list)
            {
                if (Equals(thing, item))
                    return i;
                i++;
            }

            return -1;
        }

        /// <summary>
        /// Yield an enumerable with the provided index skipped over.
        /// </summary>
        public static IEnumerable<T> WithoutIndex<T>(this IEnumerable<T> list, int index)
        {
            var i = 0;
            foreach (var item in list)
            {
                if (i != index)
                {
                    yield return item;
                }

                i++;
            }
        }

        /// <summary>
        /// Treat a single item as an <see cref="IEnumerable{T}" /> of one item.
        /// </summary>
        public static IEnumerable<T> AsEnumerable<T>(this T value)
        {
            yield return value;
        }

        /// <summary>
        /// Get all permutations of a set.
        /// </summary>
        public static IEnumerable<IEnumerable<T>> GetPermutations<T>(this IEnumerable<T> set)
        {
            var i = 0;
            var array = set.ToArray();
            foreach (var item in array)
            {
                var subsequence = array.WithoutIndex(i);
                foreach (var subpermutation in subsequence.GetPermutations())
                {
                    yield return item.AsEnumerable().Concat(subpermutation);
                }

                i++;
            }

            if (i == 1)
            {
                yield return array;
            }
        }

        /// <summary>
        /// Wraps an <see cref="IEnumerable{T}" /> so the <see cref="object.ToString" />
        /// method prints a list of the elements separated by commas.
        /// </summary>
        public static IEnumerable<T> AsPretty<T>(this IEnumerable<T> enumerable)
        {
            return new PrettyEnumerable<T>(enumerable);
        }

        /// <summary>
        /// Wraps an <see cref="IEnumerable{T}" /> so the <see cref="object.ToString" />
        /// method prints a list of the function <paramref name="toString" /> applied to
        /// each element, separated by commas.
        /// </summary>
        public static IEnumerable<T> AsPretty<T>(this IEnumerable<T> enumerable,
                                                 Func<T, string> toString)
        {
            return new PrettyEnumerable<T>(enumerable, toString);
        }

        /// <summary>
        /// Get all key value pairs for which a given predicate based upon the key is true.
        /// </summary>
        public static IEnumerable<KeyValuePair<TKey, TValue>> WhereKey<TKey, TValue>(
            this IEnumerable<KeyValuePair<TKey, TValue>> enumerable,
            Predicate<TKey> predicate)
        {
            return enumerable.Where(kvp => predicate(kvp.Key));
        }

        /// <summary>
        /// Get all indices for which a given predicate is true;
        /// </summary>
        public static IEnumerable<int> IndicesWhere<TValue>(this IEnumerable<TValue> enumerable,
                                                            Predicate<TValue> predicate)
        {
            var i = 0;
            foreach (var item in enumerable)
            {
                if (predicate(item))
                    yield return i;
                i++;
            }
        }
    }
}