/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Scivana.Core
{
    /// <summary>
    /// Utility methods for reflection methods not included in C#.
    /// </summary>
    public static class ReflectionUtility
    {
        /// <summary>
        /// Get the field in a given type or its ancestors of the given name, with the
        /// given <see cref="BindingFlags" />.
        /// </summary>
        public static FieldInfo GetFieldInSelfOrParents(this Type type,
                                                        string name,
                                                        BindingFlags flags)
        {
            while (type != null && type != typeof(object))
            {
                var field = type.GetField(name, flags);
                if (field != null)
                    return field;
                type = type.BaseType;
            }

            return null;
        }

        /// <summary>
        /// Get all fields in a given type and its ancestors, with the given
        /// <see cref="BindingFlags" />.
        /// </summary>
        public static IEnumerable<FieldInfo> GetFieldsInSelfOrParents(this Type type,
                                                                      BindingFlags flags)
        {
            while (type != null && type != typeof(object))
            {
                foreach (var field in type.GetFields(flags))
                    yield return field;
                type = type.BaseType;
            }
        }

        /// <summary>
        /// Get the property in a given type or its ancestors of the given name, with the
        /// given <see cref="BindingFlags" />.
        /// </summary>
        public static PropertyInfo GetPropertyInSelfOrParents(this Type type,
                                                              string name,
                                                              BindingFlags flags)
        {
            while (type != null && type != typeof(object))
            {
                var property = type.GetProperty(name, flags);
                if (property != null)
                    return property;
                type = type.BaseType;
            }

            return null;
        }
    }
}