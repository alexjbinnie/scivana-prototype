﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace Scivana.Core
{
    /// <summary>
    /// Utility methods for instantiating objects in an inactive state, to prevent enable being
    /// called straight away. This allows OnEnable to be treated as a constructor, with the object
    /// being configured before it is set to being active for the first time.
    /// </summary>
    public static class Instantiation
    {
        /// <summary>
        /// Instantiate a new game object with the given <see cref="MonoBehaviour" /> and optional
        /// parent in an inactive state.
        /// </summary>
        public static TMonoBehaviour InstantiateInactive<TMonoBehaviour>(Transform parent = null)
            where TMonoBehaviour : Component
        {
            var obj = new GameObject();
            obj.SetActive(false);
            obj.name = typeof(TMonoBehaviour).Name;
            if (parent != null)
            {
                obj.transform.parent = parent;
                obj.transform.SetToLocalIdentity();
            }

            return obj.AddComponent<TMonoBehaviour>();
        }

        /// <summary>
        /// Instantiate the given <see cref="MonoBehaviour" /> and optionally parent in an inactive
        /// state.
        /// </summary>
        public static TMonoBehaviour InstantiateInactive<TMonoBehaviour>(this TMonoBehaviour prefab,
                                                                         Transform parent = null)
            where TMonoBehaviour : MonoBehaviour
        {
            var existingActive = prefab.gameObject.activeSelf;
            prefab.gameObject.SetActive(false);
            var instance = Object.Instantiate(prefab, parent);
            prefab.gameObject.SetActive(existingActive);
            return instance;
        }

        /// <summary>
        /// Instantiate the given game object and optionally parent in an inactive state.
        /// </summary>
        public static GameObject InstantiateInactive(this GameObject prefab,
                                                     Transform parent = null)
        {
            var existingActive = prefab.activeSelf;
            prefab.SetActive(false);
            var instance = Object.Instantiate(prefab, parent);
            prefab.SetActive(existingActive);
            return instance;
        }
    }
}