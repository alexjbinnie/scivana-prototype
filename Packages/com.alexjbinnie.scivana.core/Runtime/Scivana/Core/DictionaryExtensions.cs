/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace Scivana.Core
{
    /// <summary>
    /// Useful extensions for using dictionaries.
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Deconstruct method for <see cref="KeyValuePair{TKey,TValue}" />, so
        /// dictionaries can be used with tuples.
        /// </summary>
        public static void Deconstruct<TKey, TValue>(
            this KeyValuePair<TKey, TValue> kvp,
            out TKey key,
            out TValue value)
        {
            key = kvp.Key;
            value = kvp.Value;
        }

        /// <summary>
        /// Try to get a value out of a dictionary of type T, returning false if the key is
        /// not present or the present value is of the wrong type.
        /// </summary>
        public static bool TryGetValue<T>(this IDictionary<string, object> dictionary,
                                          string key,
                                          out T value)
        {
            if (dictionary.TryGetValue(key, out var potentialValue)
             && potentialValue is T valueOfCorrectType)
            {
                value = valueOfCorrectType;
                return true;
            }

            value = default;
            return false;
        }

        /// <summary>
        /// Get an item of the dictionary of the given type <typeparamref name="T" />,
        /// returning the default if the key is not present and throwing an exception if
        /// there is a value for the given key, but the type is incompatible.
        /// </summary>
        public static T GetValueOrDefault<T>(this IDictionary<string, object> dictionary,
                                             string id,
                                             T defaultValue = default)
        {
            if (!dictionary.TryGetValue(id, out var value))
                return defaultValue;

            return value switch
            {
                T cast => cast,
                null => defaultValue,
                _ => throw new InvalidOperationException(
                         $"Value with id {id} is of incompatible type {value.GetType()}"),
            };
        }

        /// <summary>
        /// Get an array in the dictionary of the given type <typeparamref name="T" />
        /// returning an empty array if the key is not present and throwing an exception if
        /// there is an array for the given key, but the type is incompatible.
        /// </summary>
        public static T[] GetArrayOrEmpty<T>(this IDictionary<string, object> dictionary,
                                             string id)
        {
            return dictionary.GetValueOrDefault<T[]>(id) ?? new T[0];
        }
    }
}