/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using UnityEngine;

namespace Scivana.Core.Math
{
    /// <summary>
    /// A transformation between two 3D spaces, defined by where it maps the three axes of cartesian
    /// space and the origin. This can represent any combination of rotations, reflections,
    /// translations, scaling and shears.
    /// </summary>
    /// <remarks>
    /// Every affine transformation can be represented as an augmented 4x4 matrix, with
    /// the three axes (with the 4th component 0) and the origin (with the 4th
    /// component 1) representing the three columns of the matrix.
    /// </remarks>
    [Serializable]
    public struct AffineTransformation : IEquatable<AffineTransformation>, ITransformation
    {
        /// <inheritdoc cref="ITransformation.inverse" />
        ITransformation ITransformation.inverse => inverse;

        /// <summary>
        /// The vector to which this transformation maps the direction (1, 0, 0).
        /// </summary>
        public Vector3 xAxis;

        /// <summary>
        /// The vector to which this transformation maps the direction (0, 1, 0).
        /// </summary>
        public Vector3 yAxis;

        /// <summary>
        /// The vector to which this transformation maps the direction (0, 0, 1).
        /// </summary>
        public Vector3 zAxis;

        /// <summary>
        /// The translation that this transformation applies.
        /// </summary>
        public Vector3 origin;

        /// <summary>
        /// Initializes a new instance of the <see cref="AffineTransformation" /> struct, which
        /// represents an affine transformation which maps the x, y and z directions to new
        /// vectors and translates to a new origin.
        /// </summary>
        public AffineTransformation(Vector3 xAxis,
                                    Vector3 yAxis,
                                    Vector3 zAxis,
                                    Vector3 position)
        {
            this.xAxis = xAxis;
            this.yAxis = yAxis;
            this.zAxis = zAxis;
            origin = position;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AffineTransformation" /> struct, which
        /// represents an affine transformation with the upper 3x4 matrix.
        /// </summary>
        /// <remarks>
        /// The bottom row of the matrix is ignored.
        /// </remarks>
        public AffineTransformation(Matrix4x4 matrix)
        {
            xAxis = matrix.GetColumn(0);
            yAxis = matrix.GetColumn(1);
            zAxis = matrix.GetColumn(2);
            origin = matrix.GetColumn(3);
        }

        /// <summary>
        /// Gets the identity transformation.
        /// </summary>
        public static AffineTransformation identity => new AffineTransformation(Vector3.right,
                                                                                Vector3.up,
                                                                                Vector3.forward,
                                                                                Vector3.zero);

        /// <summary>
        /// Gets the magnitudes of the three axes that define this linear transformation.
        /// </summary>
        public Vector3 axesMagnitudes => new Vector3(xAxis.magnitude,
                                                     yAxis.magnitude,
                                                     zAxis.magnitude);

        /// <inheritdoc cref="ITransformation.inverse" />
        public AffineTransformation inverse => new AffineTransformation(matrix.inverse);

        /// <inheritdoc cref="ITransformation.matrix" />
        public Matrix4x4 matrix =>
            new Matrix4x4(xAxis, yAxis, zAxis, new Vector4(origin.x, origin.y, origin.z, 1));

        /// <inheritdoc cref="ITransformation.inverseMatrix" />
        public Matrix4x4 inverseMatrix => inverse.matrix;

        public static implicit operator Matrix4x4(AffineTransformation transformation)
        {
            return transformation.matrix;
        }

        public static implicit operator AffineTransformation(LinearTransformation transformation)
        {
            return new AffineTransformation(transformation.xAxis,
                                            transformation.yAxis,
                                            transformation.zAxis,
                                            Vector3.zero);
        }

        public static implicit operator AffineTransformation(Transformation transformation)
        {
            return new AffineTransformation(transformation.matrix);
        }

        public static implicit operator AffineTransformation(
            UniformScaleTransformation transformation)
        {
            return new AffineTransformation(transformation.matrix);
        }

        public static implicit operator AffineTransformation(UnitScaleTransformation transformation)
        {
            return new AffineTransformation(transformation.matrix);
        }

        public static AffineTransformation operator *(AffineTransformation a,
                                                      AffineTransformation b)
        {
            return new AffineTransformation(a.matrix * b.matrix);
        }

        /// <inheritdoc cref="ITransformation.TransformPoint" />
        public Vector3 TransformPoint(Vector3 point)
        {
            return point.x * xAxis
                 + point.y * yAxis
                 + point.z * zAxis
                 + origin;
        }

        /// <inheritdoc cref="ITransformation.InverseTransformPoint" />
        public Vector3 InverseTransformPoint(Vector3 point)
        {
            return inverse.TransformPoint(point);
        }

        /// <inheritdoc cref="ITransformation.TransformDirection" />
        public Vector3 TransformDirection(Vector3 direction)
        {
            return direction.x * xAxis
                 + direction.y * yAxis
                 + direction.z * zAxis;
        }

        /// <inheritdoc cref="ITransformation.InverseTransformDirection" />
        public Vector3 InverseTransformDirection(Vector3 direction)
        {
            return inverse.TransformDirection(direction);
        }

        /// <inheritdoc />
        public bool Equals(AffineTransformation other)
        {
            return xAxis.Equals(other.xAxis) &&
                   yAxis.Equals(other.yAxis) &&
                   zAxis.Equals(other.zAxis) &&
                   origin.Equals(other.origin);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = xAxis.GetHashCode();
                hashCode = (hashCode * 397) ^ yAxis.GetHashCode();
                hashCode = (hashCode * 397) ^ zAxis.GetHashCode();
                hashCode = (hashCode * 397) ^ origin.GetHashCode();
                return hashCode;
            }
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is AffineTransformation other && Equals(other);
        }

        public static bool operator ==(AffineTransformation left, AffineTransformation right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(AffineTransformation left, AffineTransformation right)
        {
            return !(left == right);
        }
    }
}