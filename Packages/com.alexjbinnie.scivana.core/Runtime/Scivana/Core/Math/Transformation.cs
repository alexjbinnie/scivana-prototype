﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace Scivana.Core.Math
{
    /// <summary>
    /// A transformation consisting of a scaling, rotation and translation in that order.
    /// </summary>
    public struct Transformation : ITransformation, ITranslationRotationScaling
    {
        /// <inheritdoc />
        Vector3 ITranslationRotationScaling.position => position;

        /// <inheritdoc />
        Quaternion ITranslationRotationScaling.rotation => rotation;

        /// <inheritdoc />
        Vector3 ITranslationRotationScaling.scaling => scale;

        /// <inheritdoc />
        ITransformation ITransformation.inverse => new AffineTransformation(matrix.inverse);

        /// <inheritdoc />
        Matrix4x4 ITransformation.inverseMatrix => matrix.inverse;

        /// <inheritdoc />
        Vector3 ITransformation.TransformPoint(Vector3 point) => matrix.TransformPoint(point);

        /// <inheritdoc />
        Vector3 ITransformation.InverseTransformPoint(Vector3 point) =>
            matrix.InverseTransformPoint(point);

        /// <inheritdoc />
        Vector3 ITransformation.TransformDirection(Vector3 point) =>
            matrix.TransformDirection(point);

        /// <inheritdoc />
        Vector3 ITransformation.InverseTransformDirection(Vector3 point) =>
            matrix.InverseTransformDirection(point);

        /// <summary>
        /// Construct a transformation from the translation, rotation, and
        /// scale of a Unity <see cref="Transform" /> relative to world space.
        /// </summary>
        /// <remarks>
        /// The scale is inherently lossy, as the composition of multiple
        /// transforms is not necessarily a transform.
        /// </remarks>
        public static Transformation FromTransformRelativeToWorld(Transform transform)
        {
            return new Transformation(transform.position,
                                      transform.rotation,
                                      transform.lossyScale);
        }

        /// <summary>
        /// Construct a transformation from the translation, rotation, and
        /// scale of a Unity <see cref="Transform" /> relative to world space.
        /// </summary>
        public static Transformation FromTransformRelativeToParent(Transform transform)
        {
            return new Transformation(transform.localPosition,
                                      transform.localRotation,
                                      transform.localScale);
        }

        /// <summary>
        /// The identity transformation.
        /// </summary>
        public static Transformation identity =>
            new Transformation(Vector3.zero, Quaternion.identity, Vector3.one);

        /// <summary>
        /// Position of this transformation.
        /// </summary>
        public Vector3 position;

        /// <summary>
        /// Rotation of this transformation.
        /// </summary>
        public Quaternion rotation;

        /// <summary>
        /// Scale of this transformation.
        /// </summary>
        public Vector3 scale;

        /// <summary>
        /// <see cref="Matrix4x4" /> representation of this transformation.
        /// </summary>
        public Matrix4x4 matrix => Matrix4x4.TRS(position, rotation, scale);

        /// <summary>
        /// The <see cref="Matrix4x4" /> representation of the inverse of this
        /// transformation. Note that the inverse itself cannot be necessarily represented
        /// as a single <see cref="Transformation" />.
        /// </summary>
        public Matrix4x4 InverseMatrix => matrix.inverse;

        /// <summary>
        /// The inverse transpose of the matrix representation of this transformation. This
        /// is equivalent to the transformation with the same position and rotation, but
        /// with inverted scales.
        /// </summary>
        /// <remarks>
        /// Some shaders require the inverse transpose, as normal vectors transform using
        /// the inverse transpose of the transformation the positions undergo.
        /// </remarks>
        public Matrix4x4 InverseTransposeMatrix => Matrix4x4.TRS(position,
                                                                 rotation,
                                                                 new Vector3(1f / scale.x,
                                                                             1f / scale.y,
                                                                             1f / scale.z));

        /// <summary>
        /// Initializes a new instance of the <see cref="Transformation" /> struct, which represents
        /// a transformation obtained by scaling by <paramref name="scale" />, rotating by
        /// <paramref name="rotation" /> and then translating by <paramref name="position" />.
        /// </summary>
        public Transformation(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            this.position = position;
            this.rotation = rotation.normalized;
            this.scale = scale;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var pos = position;
            var rot = rotation.eulerAngles;
            var scale = this.scale;
            return
                $"Transformation(Position: ({pos.x}, {pos.y}, {pos.z}), Rotation: ({rot.x}, {rot.y}, {rot.z}), Scale: ({scale.x}, {scale.y}, {scale.z}))";
        }

        /// <summary>
        /// Convert to a transformation with unit scale, discarding any scaling associated
        /// with this transformation.
        /// </summary>
        public UnitScaleTransformation AsUnitTransformWithoutScale()
        {
            return new UnitScaleTransformation(position, rotation);
        }

        public static Transformation operator *(Quaternion rotation, Transformation transformation)
        {
            return new Transformation(rotation * transformation.position,
                                      rotation * transformation.rotation,
                                      transformation.scale);
        }

        public static Transformation operator *(float scale, Transformation transformation)
        {
            return new Transformation(scale * transformation.position,
                                      transformation.rotation,
                                      scale * transformation.scale);
        }

        public static Transformation operator *(UnitScaleTransformation unit,
                                                Transformation transformation)
        {
            return new Transformation(unit.position + unit.rotation * transformation.position,
                                      unit.rotation * transformation.rotation,
                                      transformation.scale);
        }

        public static Transformation operator *(UniformScaleTransformation uniform,
                                                Transformation transformation)
        {
            return new Transformation(
                uniform.position + uniform.scale * (uniform.rotation * transformation.position),
                uniform.rotation * transformation.rotation,
                uniform.scale * transformation.scale);
        }
    }
}