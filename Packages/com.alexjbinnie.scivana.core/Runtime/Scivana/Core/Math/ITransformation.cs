/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace Scivana.Core.Math
{
    /// <summary>
    /// A transformation that can be represented as a 4x4 matrix, used for transforming points.
    /// </summary>
    public interface ITransformation
    {
        /// <summary>
        /// The inverse of this transformation, which undoes this transformation.
        /// </summary>
        ITransformation inverse { get; }

        /// <summary>
        /// The 4x4 augmented matrix representing this transformation as it acts upon
        /// vectors and directions in homogeneous coordinates.
        /// </summary>
        Matrix4x4 matrix { get; }

        /// <summary>
        /// The 4x4 augmented matrix representing the inverse of this transformation as it
        /// acts upon vectors and directions in homogeneous coordinates.
        /// </summary>
        Matrix4x4 inverseMatrix { get; }

        /// <summary>
        /// Transform a point in space using this transformation. When considered as a
        /// transformation from an object's local space to world space, this takes points
        /// in the object's local space to world space.
        /// </summary>
        Vector3 TransformPoint(Vector3 point);

        /// <summary>
        /// Transform a point in space using the inverse of this transformation. When
        /// considered as a transformation from an object's local space to world space,
        /// this takes points in world space to the object's local space.
        /// </summary>
        Vector3 InverseTransformPoint(Vector3 point);

        /// <summary>
        /// Transform a direction in space using this transformation. When considered as a
        /// transformation from an object's local space to world space, this takes
        /// directions in the object's local space to world space.
        /// </summary>
        Vector3 TransformDirection(Vector3 direction);

        /// <summary>
        /// Transform a direction in space using the inverse of this transformation. When
        /// considered as a transformation from an object's local space to world space,
        /// this takes directions in world space to the object's local space.
        /// </summary>
        Vector3 InverseTransformDirection(Vector3 direction);
    }
}