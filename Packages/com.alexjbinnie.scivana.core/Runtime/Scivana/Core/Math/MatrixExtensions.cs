﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace Scivana.Core.Math
{
    /// <summary>
    /// Extension methods for Unity's <see cref="Matrix4x4" />
    /// </summary>
    public static class MatrixExtensions
    {
        /// <summary>
        /// Extract the translation component of the given TRS matrix. This is
        /// the worldspace origin of matrix's coordinate space.
        /// </summary>
        public static Vector3 GetTranslation(this Matrix4x4 matrix)
        {
            return matrix.GetColumn(3);
        }

        /// <summary>
        /// Extract the rotation component of the given TRS matrix. This is
        /// the quaternion that rotates worldspace forward, up, right vectors
        /// into the matrix's coordinate space.
        /// </summary>
        public static Quaternion GetRotation(this Matrix4x4 matrix)
        {
            return matrix.rotation;
        }

        /// <summary>
        /// Extract the scale component of the given TRS matrix, assuming it is orthogonal.
        /// </summary>
        public static Vector3 GetScale(this Matrix4x4 matrix)
        {
            return matrix.lossyScale;
        }

        /// <summary>
        /// Get the matrix that transforms from this matrix to another.
        /// </summary>
        /// <remarks>
        /// In Unity transformations are from local-space to world-space, so
        /// the transformation is multiplied on the right-hand side.
        /// </remarks>
        public static Matrix4x4 GetTransformationTo(this Matrix4x4 from, Matrix4x4 to)
        {
            return from.inverse * to;
        }

        /// <summary>
        /// Return this matrix transformed by the given transformation matrix.
        /// </summary>
        /// <remarks>
        /// In Unity transformations are from local-space to world-space, so
        /// the transformation is multiplied on the right-hand side.
        /// </remarks>
        public static Matrix4x4 TransformedBy(this Matrix4x4 matrix, Matrix4x4 transformation)
        {
            return matrix * transformation;
        }

        /// <inheritdoc cref="ITransformation.TransformPoint" />
        public static Vector3 TransformPoint(this Matrix4x4 matrix, Vector3 point)
        {
            return matrix.MultiplyPoint3x4(point);
        }

        /// <inheritdoc cref="ITransformation.InverseTransformPoint" />
        public static Vector3 InverseTransformPoint(this Matrix4x4 matrix, Vector3 point)
        {
            return matrix.inverse.MultiplyPoint3x4(point);
        }

        /// <inheritdoc cref="ITransformation.TransformDirection" />
        public static Vector3 TransformDirection(this Matrix4x4 matrix, Vector3 point)
        {
            return matrix.MultiplyVector(point);
        }

        /// <inheritdoc cref="ITransformation.InverseTransformDirection" />
        public static Vector3 InverseTransformDirection(this Matrix4x4 matrix, Vector3 point)
        {
            return matrix.inverse.MultiplyVector(point);
        }

        /// <summary>
        /// Set the transform's position, rotation and scale relative to its parent from
        /// this transformation.
        /// </summary>
        public static void CopyToTransformRelativeToParent<TTransformation>(
            this TTransformation transformation,
            Transform transform)
            where TTransformation : ITranslationRotationScaling
        {
            transform.localPosition = transformation.position;
            transform.localRotation = transformation.rotation;
            transform.localScale = transformation.scaling;
        }

        /// <summary>
        /// Set the transform's position, rotation and scale relative to the world space
        /// from this transformation.
        /// </summary>
        public static void CopyToTransformRelativeToWorld<TTransformation>(
            this TTransformation transformation,
            Transform transform)
            where TTransformation : ITranslationRotationScaling
        {
            // we are not allowed to set global scale directly in Unity, so
            // instead we unparent the object, make local changes, then reparent
            var parent = transform.parent;

            transform.parent = null;

            transform.localPosition = transformation.position;
            transform.localRotation = transformation.rotation;
            transform.localScale = transformation.scaling;

            transform.parent = parent;
        }
    }
}