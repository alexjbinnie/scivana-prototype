/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace Scivana.Core.Math
{
    /// <summary>
    /// Binary search algorithms for quickly finding integers in sorted lists.
    /// </summary>
    public static class SearchAlgorithms
    {
        /// <summary>
        /// Binary search to find an integer in a set of unique ordered integers.
        /// </summary>
        /// <param name="value">The value that is being searched for.</param>
        /// <param name="set">A set of integers ordered low to high.</param>
        public static bool BinarySearch(int value, IReadOnlyList<int> set)
        {
            var leftIndex = 0;
            var rightIndex = set.Count - 1;
            while (leftIndex <= rightIndex)
            {
                var midpointIndex = (leftIndex + rightIndex) / 2;
                var valueAtMidpoint = set[midpointIndex];
                if (valueAtMidpoint < value)
                    leftIndex = midpointIndex + 1;
                else if (valueAtMidpoint > value)
                    rightIndex = midpointIndex - 1;
                else
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Binary search to find the index of an integer in a set of unique ordered integers.
        /// </summary>
        /// <param name="value">The value that is being searched for.</param>
        /// <param name="set">A set of integers ordered low to high.</param>
        /// <returns>
        /// The index of <paramref name="value" /> in <paramref name="set" />, or
        /// -1 if value is not present.
        /// </returns>
        public static int BinarySearchIndex(int value, IReadOnlyList<int> set)
        {
            var leftIndex = 0;
            var rightIndex = set.Count - 1;
            while (leftIndex <= rightIndex)
            {
                var midpointIndex = (leftIndex + rightIndex) / 2;
                var valueAtMidpoint = set[midpointIndex];
                if (valueAtMidpoint < value)
                    leftIndex = midpointIndex + 1;
                else if (valueAtMidpoint > value)
                    rightIndex = midpointIndex - 1;
                else
                    return midpointIndex;
            }

            return -1;
        }
    }
}