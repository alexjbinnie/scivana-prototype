/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Runtime.Serialization;
using UnityEngine;

namespace Scivana.Core.Math
{
    /// <summary>
    /// A transformation consisting of a scaling by a uniform factor, a rotation and
    /// then a translation.
    /// </summary>
    /// <remarks>
    /// <see cref="UniformScaleTransformation" />s are closed under composition
    /// (combining two of them yields a third). These transformations preserve angles.
    /// </remarks>
    [DataContract]
    public struct UniformScaleTransformation : ITransformation, ITranslationRotationScaling
    {
        /// <inheritdoc />
        Vector3 ITranslationRotationScaling.position => position;

        /// <inheritdoc />
        Quaternion ITranslationRotationScaling.rotation => rotation;

        /// <inheritdoc />
        Vector3 ITranslationRotationScaling.scaling => scale * Vector3.one;

        /// <inheritdoc cref="ITransformation.inverse" />
        ITransformation ITransformation.inverse => inverse;

        /// <summary>
        /// The translation this transformation applies. When considered as a
        /// transformation from an object's local space to world space, describes the
        /// position of the object.
        /// </summary>
        [DataMember]
        public Vector3 position;

        /// <summary>
        /// The rotation this transformation applies. When considered as a transformation
        /// from an object's local space to world space, describes the rotation of the
        /// object.
        /// </summary>
        [DataMember]
        public Quaternion rotation;

        /// <summary>
        /// The uniform scaling this transformation applies. When considered as a
        /// transformation from an object's local space to world space, describes the scale
        /// of the object.
        /// </summary>
        [DataMember]
        public float scale;

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformScaleTransformation" /> struct,
        /// which represents a transformation obtained by scaling uniformly by
        /// <paramref name="scale" />, rotating by <paramref name="rotation" /> and then translating
        /// by <paramref name="position" />.
        /// </summary>
        public UniformScaleTransformation(Vector3 position, Quaternion rotation, float scale)
        {
            this.position = position;
            this.rotation = rotation.normalized;
            this.scale = scale;
        }

        /// <summary>
        /// The identity transformation.
        /// </summary>
        public static UniformScaleTransformation identity =>
            new UniformScaleTransformation(Vector3.zero, Quaternion.identity, 1);


        /// <inheritdoc cref="ITransformation.inverse" />
        public UniformScaleTransformation inverse
        {
            get
            {
                var inverseRotation = Quaternion.Inverse(rotation);
                return new UniformScaleTransformation(
                    inverseRotation * (-(1f / scale) * position),
                    inverseRotation,
                    1 / scale);
            }
        }

        /// <inheritdoc cref="ITransformation.matrix" />
        public Matrix4x4 matrix => Matrix4x4.TRS(position, rotation, Vector3.one * scale);

        /// <inheritdoc cref="ITransformation.inverseMatrix" />
        public Matrix4x4 inverseMatrix => inverse.matrix;

        public static implicit operator Matrix4x4(UniformScaleTransformation transformation)
        {
            return transformation.matrix;
        }

        public static implicit operator Transformation(UniformScaleTransformation transformation)
        {
            return new Transformation(transformation.position,
                                      transformation.rotation,
                                      transformation.scale * Vector3.one);
        }

        public static UniformScaleTransformation operator *(UniformScaleTransformation a,
                                                            UniformScaleTransformation b)
        {
            return new UniformScaleTransformation(a.position + a.scale * (a.rotation * b.position),
                                                  a.rotation * b.rotation,
                                                  a.scale * b.scale);
        }

        /// <inheritdoc cref="ITransformation.TransformPoint" />
        public Vector3 TransformPoint(Vector3 pt)
        {
            return rotation * (scale * pt) + position;
        }

        /// <inheritdoc cref="ITransformation.InverseTransformPoint" />
        public Vector3 InverseTransformPoint(Vector3 pt)
        {
            return inverse.TransformPoint(pt);
        }

        /// <inheritdoc cref="ITransformation.TransformDirection" />
        public Vector3 TransformDirection(Vector3 direction)
        {
            return rotation * (scale * direction);
        }

        /// <inheritdoc cref="ITransformation.InverseTransformDirection" />
        public Vector3 InverseTransformDirection(Vector3 pt)
        {
            return inverse.TransformDirection(pt);
        }

        /// <summary>
        /// Get the transformation matrix that takes this transformation to the one
        /// provided.
        /// </summary>
        public Transformation TransformationTo(Transformation to)
        {
            var inverseRotation = Quaternion.Inverse(rotation);
            return new Transformation(inverseRotation * (to.position - position) / scale,
                                      inverseRotation * to.rotation,
                                      to.scale / scale);
        }

        /// <summary>
        /// Right multiply by the provided transformation matrix to give another.
        /// </summary>
        public Transformation TransformBy(Transformation trans)
        {
            return this * trans;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var pos = position;
            var rot = rotation.eulerAngles;
            return
                $"UniformTransformation(Position: ({pos.x}, {pos.y}, {pos.z}), Rotation: ({rot.x}, {rot.y}, {rot.z}), Scale: ({scale}))";
        }
    }
}