/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Scivana.Core.Properties
{
    /// <summary>
    /// An property (see <see cref="IReadOnlyProperty" />) which can have its
    /// value altered.
    /// </summary>
    public interface IProperty : IReadOnlyProperty
    {
        /// <summary>
        /// Remove the value from this property.
        /// </summary>
        void UndefineValue();

        /// <summary>
        /// Attempt to set the value without knowing the types involved.
        /// </summary>
        void TrySetValue(object value);

        /// <summary>
        /// Is this property linked to another?
        /// </summary>
        bool HasLinkedProperty { get; }

        /// <summary>
        /// Attempt to set the linked property without knowing the types involved.
        /// </summary>
        void TrySetLinkedProperty(object property);

        /// <summary>
        /// Linked property that will override this value.
        /// </summary>
        IReadOnlyProperty LinkedProperty { get; }
    }

    /// <inheritdoc cref="IProperty" />
    public interface IProperty<TValue> : IReadOnlyProperty<TValue>, IProperty
    {
        /// <inheritdoc cref="IProperty.Value" />
        new TValue Value { get; set; }

        /// <inheritdoc cref="IProperty.Value" />
        new IReadOnlyProperty<TValue> LinkedProperty { get; set; }
    }
}