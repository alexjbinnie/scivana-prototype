/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace Scivana.Core.Properties
{
    /// <summary>
    /// Abstract base class for an implementation of <see cref="IProperty" />.
    /// </summary>
    [Serializable]
    public abstract class Property : IDirtyProperty
    {
        IReadOnlyProperty IProperty.LinkedProperty => NonGenericLinkedProperty;

        object IReadOnlyProperty.Value => NonGenericValue;

        protected abstract IReadOnlyProperty NonGenericLinkedProperty { get; }

        protected abstract object NonGenericValue { get; }

        /// <summary>
        /// Callback for when the value is changed or undefined.
        /// </summary>
        public event Action ValueChanged;

        /// <summary>
        /// Internal method, called when the value is changed or undefined.
        /// </summary>
        protected void OnValueChanged()
        {
            ValueChanged?.Invoke();
        }

        /// <inheritdoc cref="IReadOnlyProperty.HasValue" />
        public abstract bool HasValue { get; }

        /// <inheritdoc cref="IProperty.UndefineValue" />
        public abstract void UndefineValue();

        /// <inheritdoc cref="IProperty.PropertyType" />
        public abstract Type PropertyType { get; }

        /// <inheritdoc />
        public bool IsDirty { get; set; } = true;

        /// <summary>
        /// Mark the value as having changed.
        /// </summary>
        public virtual void MarkValueAsChanged()
        {
            IsDirty = true;
        }

        /// <inheritdoc cref="IProperty.HasLinkedProperty" />
        public abstract bool HasLinkedProperty { get; }

        /// <inheritdoc cref="IProperty.TrySetValue" />
        public abstract void TrySetValue(object value);

        /// <inheritdoc cref="IProperty.TrySetLinkedProperty" />
        public abstract void TrySetLinkedProperty(object property);
    }
}