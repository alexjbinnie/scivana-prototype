﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Scivana.Core.Properties
{
    public class FlattenProperty<TType> : IReadOnlyProperty<TType[]>, IPropertyMutation
    {
        private IReadOnlyProperty<IReadOnlyList<IReadOnlyList<TType>>> input;

        public FlattenProperty(IReadOnlyProperty<IReadOnlyList<IReadOnlyList<TType>>> input)
        {
            this.input = input;
            GenerateValue();
            input.ValueChanged += GenerateValue;
        }

        private void GenerateValue()
        {
            if (input.HasValue)
            {
                var arr = new TType[input.Value.Sum(list => list.Count)];
                var i = 0;
                foreach (var a in input.Value)
                {
                    foreach (var b in a)
                    {
                        arr[i++] = b;
                    }
                }

                Value = arr;
            }

            ValueChanged?.Invoke();
        }

        object IReadOnlyProperty.Value => Value;

        public TType[] Value { get; private set; }

        public bool HasValue => input.HasValue;

        public event Action ValueChanged;

        public Type PropertyType => typeof(TType[]);

        public IReadOnlyProperty OriginalProperty => input;
    }
}