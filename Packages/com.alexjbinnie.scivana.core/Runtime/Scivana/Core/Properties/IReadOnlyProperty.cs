/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace Scivana.Core.Properties
{
    /// <summary>
    /// A property is a wrapper around a value which may or not be present, and possesses a callback for
    /// when the value is altered. Read-only properties are value providers, whilst
    /// <see cref="IProperty" /> can also be linked to other properties to obtain their values.
    /// </summary>
    public interface IReadOnlyProperty
    {
        /// <summary>
        /// The current value of the property. This should only be called if
        /// <see cref="HasValue" /> has been ensured to be true.
        /// </summary>
        /// <exception cref="InvalidOperationException">The property does not currently have a value.</exception>
        object Value { get; }

        /// <summary>
        /// Does the property currently have a value? If true, then accessing
        /// <see cref="Value" /> should be valid.
        /// </summary>
        bool HasValue { get; }

        /// <summary>
        /// Callback for when the value is changed. This includes if there is now not a value, so
        /// <see cref="HasValue" /> should still be checked.
        /// </summary>
        event Action ValueChanged;

        /// <summary>
        /// The <see cref="Type" /> that this property wraps.
        /// </summary>
        Type PropertyType { get; }
    }

    /// <inheritdoc cref="IReadOnlyProperty" />
    public interface IReadOnlyProperty<out TValue> : IReadOnlyProperty
    {
        /// <inheritdoc cref="IReadOnlyProperty.Value" />
        new TValue Value { get; }
    }
}