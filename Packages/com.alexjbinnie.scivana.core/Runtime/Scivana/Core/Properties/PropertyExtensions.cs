/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;

namespace Scivana.Core.Properties
{
    public static class PropertyExtensions
    {
        /// <summary>
        /// Does the property have a value which is not null?
        /// </summary>
        public static bool HasNonNullValue<TValue>(this IReadOnlyProperty<TValue> property)
        {
            return property.HasValue && property.Value != null;
        }

        /// <summary>
        /// Does the property have a value which is null?
        /// </summary>
        public static bool HasNullValue<TValue>(this IReadOnlyProperty<TValue> property)
        {
            return property.HasValue && property.Value == null;
        }

        /// <summary>
        /// Does the property have no value or a null value?
        /// </summary>
        public static bool HasNullOrNoValue<TValue>(this IReadOnlyProperty<TValue> property)
        {
            return !property.HasValue || property.Value == null;
        }

        /// <summary>
        /// Does this property have a value which is non empty?
        /// </summary>
        public static bool HasNonEmptyValue<TValue>(
            this IReadOnlyProperty<IEnumerable<TValue>> property)
        {
            return property.HasNonNullValue() && property.Value.Any();
        }

        /// <summary>
        /// Does this property have a value which is empty (but not null)?
        /// </summary>
        public static bool HasEmptyValue<TValue>(
            this IReadOnlyProperty<IEnumerable<TValue>> property)
        {
            return property.HasNonNullValue() && !property.Value.Any();
        }

        /// <summary>
        /// Get a property representing the count of a enumerable property.
        /// </summary>
        public static IReadOnlyProperty<int> Count<TValue>(
            this IReadOnlyProperty<ICollection<TValue>> property)
        {
            return new CountProperty<TValue>(property);
        }

        public static IReadOnlyProperty<TType[]> Flatten<TType>(
            this IReadOnlyProperty<IReadOnlyList<IReadOnlyList<TType>>> property)
        {
            return new FlattenProperty<TType>(property);
        }
    }
}