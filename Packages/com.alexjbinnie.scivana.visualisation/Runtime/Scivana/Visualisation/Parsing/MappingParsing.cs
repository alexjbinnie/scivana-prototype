﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using Scivana.Core;
using Scivana.Core.Science;
using Scivana.Visualisation.Data;
using Scivana.Visualisation.Node.Protein;
using UnityEngine;

namespace Scivana.Visualisation.Parsing
{
    public static class MappingParsing
    {
        private static Type GetImplementation(Type type1, Type type2)
        {
            if (type1 == typeof(string) && type2 == typeof(Color))
                return typeof(StringColorMapping);
            if (type1 == typeof(Element) && type2 == typeof(Color))
                return typeof(ElementColorMapping);
            if (type1 == typeof(SecondaryStructureAssignment) && type2 == typeof(Color))
                return typeof(SecondaryStructureColorMapping);
            return null;
        }

        public static Parse<object> GetParser(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(IMapping<,>))
            {
                var type1 = type.GetGenericArguments()[0];
                var type2 = type.GetGenericArguments()[1];

                var parser1 = VisualisationParser.GetParser(type1);
                var parser2 = VisualisationParser.GetParser(type2);

                var implementation = GetImplementation(type1, type2);

                var method = typeof(MappingParsing).GetMethod(nameof(TryParse),
                                                              BindingFlags.Static
                                                            | BindingFlags.Public)
                                                   ?.MakeGenericMethod(
                                                       type1, type2);

                if (method == null)
                    return null;

                object InternalTryParse(object value)
                {
                    return method.Invoke(null, new[]
                    {
                        value, parser1, parser2, implementation
                    });
                }

                return InternalTryParse;
            }

            return null;
        }

        public static IMapping<TKey, TValue> TryParse<TKey, TValue>(object value,
                                                                    Parse<object> keyParser,
                                                                    Parse<object> valueParser,
                                                                    Type implementation = null)
        {
            switch (value)
            {
                // Object is already a mapping of strings to colors
                case IMapping<TKey, TValue> actualValue:
                    return actualValue;

                // Object is a string name of a predefined mapping
                case string str:
                    if (implementation == null)
                        return null;
                    return Resources.Load($"Visualiser/Mapping/{str}", implementation) as
                               IMapping<TKey, TValue>;

                // Object is a dictionary, to be interpreted as element symbols to colors
                case Dictionary<string, object> dict:
                {
                    var mappingDictionary = new Dictionary<TKey, TValue>();
                    foreach (var (key, val) in dict)
                        if (keyParser(key) is TKey correctKey
                         && valueParser(val) is TValue correctVal)
                            mappingDictionary.Add(correctKey, correctVal);
                        else
                            return null;

                    return mappingDictionary.AsMapping();
                }

                default:
                    return null;
            }
        }
    }
}