/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Scivana.Core;
using Scivana.Core.Science;
using Scivana.Visualisation.Data;
using UnityEngine;

namespace Scivana.Visualisation.Parsing
{
    public delegate T Parse<out T>(object value);

    public static class VisualisationParser
    {
        /// <summary>
        /// Parse a gradient from a generic C# object. Possible values include a Unity
        /// gradient or a list of items which can be interpreted as colors using
        /// <see cref="ParseColor" />.
        /// </summary>
        public static Gradient ParseGradient(object value)
        {
            switch (value)
            {
                // Object is already a Gradient
                case Gradient g:
                    return g;

                // Object is a list of colors
                case IReadOnlyList<object> list:
                {
                    var colors = new List<Color>();

                    // Parse each item of the list as a color
                    foreach (var item in list)
                    {
                        var color = ParseColor(item);
                        if (!color.HasValue)
                            return null;
                        colors.Add(color.Value);
                    }

                    // Construct a gradient from two or more colors
                    if (colors.Count >= 2)
                    {
                        var gradient = new Gradient();
                        gradient.SetKeys(
                            colors.Select((c, i) => new GradientColorKey(
                                              c, (float) i / (colors.Count - 1)))
                                  .ToArray(),
                            new[]
                            {
                                new GradientAlphaKey(1, 0), new GradientAlphaKey(1, 1)
                            });
                        return gradient;
                    }

                    break;
                }
            }

            return null;
        }

        /// <summary>
        /// Parse an <see cref="IMapping{TFrom,TTo}" /> from a C# object.
        /// </summary>
        /// <summary>
        /// Parse a <see cref="Mesh" /> from a C# object.
        /// </summary>
        public static Mesh ParseMesh(object value)
        {
            switch (value)
            {
                // Object is already a mapping of elements to colors
                case Mesh actualValue:
                    return actualValue;

                // Object is a string name of a predefined mapping
                case string str:
                    return Resources.LoadAll<DefinedMesh>($"Visualiser/Meshes")
                                    .FirstOrDefault(d => d.ID == str)
                                    ?.Mesh;
            }

            return null;
        }

        /// <summary>
        /// Parse a <see cref="Material" /> from a C# object.
        /// </summary>
        public static Material ParseMaterial(object value)
        {
            switch (value)
            {
                // Object is already a mapping of elements to colors
                case Material actualValue:
                    return actualValue;

                // Object is a string name of a predefined mapping
                case string str:
                    var material = Resources.LoadAll<DefinedMaterial>($"Visualiser/Materials")
                                            .FirstOrDefault(d => d.ID == str)
                                            ?.Material;
                    if (material == null)
                        Debug.LogWarning($"Failed to find material with id '{str}'");
                    return material;
            }

            return null;
        }

        /// <summary>
        /// Parse an element from either a string symbol or an integer atomic number.
        /// </summary>
        public static Element? ParseElement(object value)
        {
            switch (value)
            {
                // Object is potentially an atomic symbol
                case string str:
                    return ElementSymbols.GetFromSymbol(str);

                // Object is an atomic number
                case int atomicNumber:
                    if (atomicNumber >= 1 && atomicNumber <= 118)
                    {
                        return (Element) atomicNumber;
                    }

                    break;

                // Object is an atomic number, but in float form
                case double d:
                {
                    var atomicNumber = (int) d;
                    if (atomicNumber >= 1 && atomicNumber <= 118)
                    {
                        return (Element) atomicNumber;
                    }

                    break;
                }
            }

            return null;
        }

        /// <summary>
        /// Regex for parsing strings such as E4F924, #24fac8 and 0x2Ef9e2
        /// </summary>
        private const string RegexRgbHex =
            @"^(?:#|0x)?([A-Fa-f0-9][A-Fa-f0-9])([A-Fa-f0-9][A-Fa-f0-9])([A-Fa-f0-9][A-Fa-f0-9])$";

        /// <summary>
        /// Attempt to parse a color, from a name, hex code or array of rgba values.
        /// </summary>
        public static Color? ParseColor(object value)
        {
            switch (value)
            {
                // Object is already a color
                case Color c:
                    return c;

                // Object could be either a predefined color or a hex string
                case string str:
                {
                    if (Colors.GetColorFromName(str) is {} c)
                    {
                        return c;
                    }

                    // Match hex color
                    var match = Regex.Match(str, RegexRgbHex);

                    if (match.Success)
                    {
                        var r = int.Parse(match.Groups[1].Value,
                                          NumberStyles.HexNumber,
                                          CultureInfo.InvariantCulture);
                        var g = int.Parse(match.Groups[2].Value,
                                          NumberStyles.HexNumber,
                                          CultureInfo.InvariantCulture);
                        var b = int.Parse(match.Groups[3].Value,
                                          NumberStyles.HexNumber,
                                          CultureInfo.InvariantCulture);
                        return new Color(r / 255f,
                                         g / 255f,
                                         b / 255f,
                                         1);
                    }

                    break;
                }

                // Object could be a list of RGB values
                case IReadOnlyList<object> list
                    when list.Count == 3 && list.All(item => item is double
                                                          || item is int
                                                          || item is float
                                                          || item is long):
                    return new Color((float) (double) list[0],
                                     (float) (double) list[1],
                                     (float) (double) list[2]);

                // Object could be a list of RGBA values
                case IReadOnlyList<object> list
                    when list.Count == 4 && list.All(item => item is double
                                                          || item is int
                                                          || item is float
                                                          || item is long):
                    return new Color((float) (double) list[0],
                                     (float) (double) list[1],
                                     (float) (double) list[2],
                                     (float) (double) list[3]);
            }

            return null;
        }

        /// <summary>
        /// Attempt to parse a float from a generic object.
        /// </summary>
        public static float? ParseFloat(object value)
        {
            switch (value)
            {
                // Object is a float
                case float flt:
                    return flt;

                // Object is a double
                case double dbl:
                    return (float) dbl;

                // Object is an int
                case int it:
                    return it;

                case long lng:
                    return lng;
            }

            return null;
        }

        /// <summary>
        /// Attempt to parse a string from a generic object.
        /// </summary>
        public static string ParseString(object value)
        {
            return value as string;
        }

        private static Func<Type, Parse<object>> WrapValueParser<T>(Func<object, T?> parser) where T : struct
        {
            return (type) =>
            {
                if (type == typeof(T))
                {
                    object Wrapped(object value)
                    {
                        return parser(value);
                    }

                    return Wrapped;
                }

                return null;
            };
        }

        private static Func<Type, Parse<object>> WrapReferenceParser<T>(Parse<T> parser) where T : class
        {
            return (type) =>
            {
                if (type == typeof(T))
                {
                    return parser;
                }

                return null;
            };
        }

        public static object Parse(object obj, Type type)
        {
            var parser = GetParser(type);
            return parser?.Invoke(obj);
        }

        public static Parse<object> GetParser(Type type)
        {
            if (ParserPerType.ContainsKey(type))
                return ParserPerType[type];
            foreach (var parserGenerator in GetParsers())
            {
                if (parserGenerator(type) is {} parser)
                {
                    ParserPerType[type] = parser;
                    break;
                }
            }

            if (ParserPerType.ContainsKey(type))
                return ParserPerType[type];
            return null;
        }


        private static Dictionary<Type, Parse<object>> ParserPerType = new Dictionary<Type, Parse<object>>();

        private static IEnumerable<Func<Type, Parse<object>>> GetParsers()
        {
            yield return MappingParsing.GetParser;
            yield return ArrayParsing.GetParser;
            yield return WrapValueParser(ParseFloat);
            yield return WrapValueParser(ParseColor);
            yield return WrapValueParser(ParseElement);
            yield return WrapReferenceParser(ParseGradient);
            yield return WrapReferenceParser(ParseMaterial);
            yield return WrapReferenceParser(ParseString);
            yield return WrapReferenceParser(ParseMesh);
        }
    }
}