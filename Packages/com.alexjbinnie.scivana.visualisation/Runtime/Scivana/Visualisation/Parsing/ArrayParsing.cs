﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Scivana.Visualisation.Data;
using UnityEngine;

namespace Scivana.Visualisation.Parsing
{
    public static class ArrayParsing
    {
        private static Type GetImplementation(Type type)
        {
            if (type == typeof(Color))
                return typeof(ColorSet);
            return null;
        }

        public static Parse<object> GetParser(Type type)
        {
            if (type.IsArray)
            {
                var type1 = type.GetElementType();

                var parser1 = VisualisationParser.GetParser(type1);
                if (parser1 == null)
                    return null;

                var implementation = GetImplementation(type1);

                var method = typeof(MappingParsing).GetMethod(nameof(TryParse),
                                                              BindingFlags.Static
                                                            | BindingFlags.Public)
                                                   ?.MakeGenericMethod(
                                                       type1,
                                                       implementation);

                if (method == null)
                    return null;

                object InternalTryParse(object value)
                {
                    return method.Invoke(null, new[]
                    {
                        value, parser1, implementation
                    });
                }

                return InternalTryParse;
            }

            return null;
        }

        public static TValue[] TryParse<TValue>(object value,
                                                Parse<TValue> valueParser,
                                                Type implementation = null)
        {
            switch (value)
            {
                // Object is already a mapping of strings to colors
                case TValue[] actualValue:
                    return actualValue;

                // Object is a string name of a predefined mapping
                case string str:
                    if (implementation == null)
                        return null;
                    var list =
                        Resources.Load($"Visualiser/Set/{str}", implementation) as
                            IReadOnlyList<TValue>;
                    return list?.ToArray();

                // Object is a dictionary, to be interpreted as element symbols to colors
                case object[] objectArray:
                {
                    var array = new TValue[objectArray.Length];
                    for (var i = 0; i < array.Length; i++)
                        if (valueParser(objectArray[i]) is {} newValue)
                            array[i] = newValue;
                        else
                            return null;

                    return array;
                }
            }

            return null;
        }
    }
}