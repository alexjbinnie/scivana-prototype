﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using JetBrains.Annotations;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using UnityEngine;

namespace Scivana.Visualisation
{
    /// <summary>
    /// Interface between a <see cref="ITrajectorySnapshot" /> and Unity. Frontend
    /// specific tasks such as rendering should utilise this to track the frame, as it
    /// delays frame updating to the main thread.
    /// </summary>
    [DisallowMultipleComponent]
    public class SynchronisedFrameSource : MonoBehaviour,
                                           ITrajectorySnapshot
    {
        /// <inheritdoc cref="ITrajectorySnapshot.CurrentFrame" />
        public Frame CurrentFrame => snapshot?.CurrentFrame;

        /// <inheritdoc cref="ITrajectorySnapshot.FrameChanged" />
        public event FrameChanged FrameChanged;

        private ITrajectorySnapshot snapshot;

        /// <summary>
        /// Source for the frames to be displayed.
        /// </summary>
        public ITrajectorySnapshot FrameSource
        {
            set
            {
                if (snapshot != null)
                    snapshot.FrameChanged -= SnapshotOnFrameChanged;
                snapshot = value;
                if (snapshot != null)
                {
                    snapshot.FrameChanged += SnapshotOnFrameChanged;
                    changes = FrameChanges.All;
                }
            }
        }

        /// <summary>
        /// Callback when a frame is updated, which can happen outwith the main Unity
        /// thread.
        /// </summary>
        private void SnapshotOnFrameChanged(Frame frame, FrameChanges changes)
        {
            this.changes.MergeChanges(changes);
        }

        [NotNull]
        private FrameChanges changes = FrameChanges.None;

        private void Update()
        {
            FlushChanges();
        }

        /// <summary>
        /// Raise <see cref="FrameChanged" /> if necessary then clear state to
        /// unchanged.
        /// </summary>
        private void FlushChanges()
        {
            if (changes.HasAnythingChanged)
            {
                FrameChanged?.Invoke(snapshot.CurrentFrame, changes);
                changes = FrameChanges.None;
            }
        }
    }
}