/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Node;
using Scivana.Visualisation.Node.Adaptor;
using Scivana.Visualisation.Node.Calculator;
using Scivana.Visualisation.Node.Protein;
using Scivana.Visualisation.Parsing;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation
{
    /// <summary>
    /// Construction methods for creating visualisers from nested data structures.
    /// </summary>
    public partial class VisualiserFactory
    {
        /// <summary>
        /// Construct a visualiser from the provided arbitrary C# data.
        /// </summary>
        public static Visualiser ConstructVisualiser(object data)
        {
            var gameObject = new GameObject();
            gameObject.name = "Visualiser";
            var subgraphs = new List<VisualisationNode>();
            if (data is string visName)
            {
                // A renderer specified by a single string is assumed
                // to be a predefined visualiser
                subgraphs = CreateVisualiser(visName);
            }

            if (data is Dictionary<string, object> dict)
            {
                // A dictionary indicates the visualiser should be created from
                // fields in dict
                subgraphs = CreateVisualiser(dict);
            }

            if (subgraphs.Count == 0)
                return null;

            var visualiser = gameObject.AddComponent<Visualiser>();
            visualiser.Setup(subgraphs.ToArray());

            return visualiser;
        }

        public static List<VisualisationNode> CreateVisualiser(string preset)
        {
            var factory = new VisualiserFactory(GetPredefinedVisualiser(preset));
            return factory.subgraphs;
        }

        public static List<VisualisationNode> CreateVisualiser(Dictionary<string, object> dict)
        {
            var factory = new VisualiserFactory(dict);
            return factory.subgraphs;
        }

        public static VisualisationNode[] CreateVisualiser(params VisualisationNode[] nodes)
        {
            var factory = new VisualiserFactory(nodes);
            return factory.subgraphs.ToArray();
        }

        private void SetupAdaptors()
        {
            baseAdaptor = new ParentedAdaptorNode();
            filterAdaptor = new ParticleFilteredAdaptorNode();

            subgraphs.Add(baseAdaptor);
            subgraphs.Add(filterAdaptor);
        }

        private VisualiserFactory(Dictionary<string, object> dict)
        {
            SetupAdaptors();

            rootParameters = dict;

            FindAllSubgraphsInRootDictionary();

            CheckIfSecondaryStructureIsRequired();

            ResolveSubgraphAdaptors();

            ResolveSubgraphConnections();
        }

        private VisualiserFactory(params VisualisationNode[] nodes)
        {
            SetupAdaptors();

            foreach (var node in nodes)
                subgraphs.Add(node);

            CheckIfSecondaryStructureIsRequired();

            ResolveSubgraphAdaptors();

            ResolveSubgraphConnections();
        }

        private Dictionary<string, object> rootParameters;

        private Dictionary<VisualisationNode, Dictionary<string, object>> subgraphParameters =
            new Dictionary<VisualisationNode, Dictionary<string, object>>();

        private List<VisualisationNode> subgraphs = new List<VisualisationNode>();

        private ParentedAdaptorNode baseAdaptor;
        private ParticleFilteredAdaptorNode filterAdaptor;

        public static readonly (string Key, Type Type) ResidueSecondaryStructure
            = ("residue.secondarystructures", typeof(SecondaryStructureAssignment[]));

        private void CheckIfSecondaryStructureIsRequired()
        {
            // If a subgraph requires secondary structure, then a secondary structure adaptor is inserted into the chain before the particle filter.
            if (subgraphs.Any(subgraph =>
                                  subgraph.HasInputProperty(
                                      StandardProperties.ResidueSecondaryStructures)))
            {
                var calculator = new CalculateSecondaryStructure();
                subgraphs.Insert(0, calculator);
                calculator.LinkToAdaptor(baseAdaptor);
            }

            if (subgraphs.Any(subgraph =>
                                  subgraph.GetInputProperty(StandardProperties.Cycles) is { } property
                               && !property.HasLinkedProperty))
            {
                subgraphs.Insert(subgraphs.IndexOf(filterAdaptor) + 1, new CalculateCycles());
            }
        }

        /// <summary>
        /// Go through each input node of each subgraph, attempting to resolve the value of
        /// the input node.
        /// </summary>
        private void ResolveSubgraphConnections()
        {
            foreach (var subgraph in subgraphs)
            {
                foreach (var (name, property, attributes) in subgraph.GetInputProperties())
                {
                    ResolveSubgraphConnections(name, property, attributes, subgraph);
                }
            }
        }


        /// <summary>
        /// Resolve all adaptors that appear.
        /// </summary>
        private void ResolveSubgraphAdaptors()
        {
            IDynamicPropertyProvider adaptor = baseAdaptor;
            foreach (var subgraph in subgraphs)
            {
                if (subgraph == adaptor)
                    continue;

                if (subgraph.GetDynamicInputProperty() is {} prop)
                {
                    prop.Value = adaptor;
                }

                if (subgraph.GetDynamicPropertyProvider() is {} provider)
                {
                    adaptor = provider;
                }
            }
        }


        private void ResolveSubgraphConnections(string name,
                                                IProperty input,
                                                PropertyAttributes attributes,
                                                VisualisationNode subgraph)
        {
            // If it is already linked, we should not interfere
            if (input.HasLinkedProperty)
                return;

            var subgraphIndex = subgraphs.IndexOf(subgraph);

            // If for an input "some.input" there's a provided "some.input": "#new.input", change
            // the input name to "new.input"
            if (subgraphParameters.ContainsKey(subgraph) &&
                subgraphParameters[subgraph].ContainsKey(name) &&
                subgraphParameters[subgraph][name] is string replacement &&
                replacement.StartsWith("#", StringComparison.InvariantCultureIgnoreCase))
                name = replacement.Substring(1);

            // Is the value provided specifically for this subgraph in its parameters
            if (subgraphParameters.ContainsKey(subgraph)
             && FindParameterAndSetInputNode(name, input, subgraphParameters[subgraph]))
                return;

            // It there a parameter in the root space which can provide a value
            if (FindParameterAndSetInputNode(name, input, rootParameters))
                return;

            if (!attributes.SearchInPreviousSubgraphs)
                return;

            // Is there an output node in a preceding graph which is relevant
            for (var i = subgraphIndex - 1; i >= 0; i--)
            {
                var precedingSubgraph = subgraphs[i];

                if (precedingSubgraph.GetOutputProperty(name) is { } output)
                {
                    input.TrySetLinkedProperty(output);
                    return;
                }
            }

            // If there's a default value, that's okay
            if (input.HasValue)
                return;

            // Look for adaptors further up the chain
            for (var i = subgraphIndex - 1; i >= 0; i--)
            {
                var precedingSubgraph = subgraphs[i];

                if (precedingSubgraph.GetDynamicPropertyProvider() is { } adaptor)
                {
                    var outputProperty = adaptor.GetOrCreateProperty(name, input.PropertyType);
                    input.TrySetLinkedProperty(outputProperty);
                    return;
                }
            }

            // Search for the key in the frame
            input.TrySetLinkedProperty(filterAdaptor.GetOrCreateProperty(name,
                                                                         input.PropertyType));
        }

        /// <summary>
        /// For a property with a given <paramref name="name" />, look for a parameter in the
        /// dictionary <paramref name="parameters" /> with the key corresponding to the
        /// name of the node, and if present attempt to set the value of the input node to
        /// the value of the parameter. Returns true if this is successful, and false if
        /// there is no key in <paramref name="parameters" /> or the parameter value cannot
        /// be parsed.
        /// </summary>
        private static bool FindParameterAndSetInputNode(string name,
                                                         IProperty property,
                                                         IReadOnlyDictionary<string, object>
                                                             parameters)
        {
            if (parameters == null)
                return false;

            if (!parameters.TryGetValue(name, out var parameter))
                return false;

            return SetPropertyValue(property, parameter);
        }

        public static bool SetPropertyValue(IProperty property, object value)
        {
            var parsedValue = VisualisationParser.Parse(value, property.PropertyType);
            if (parsedValue != null)
            {
                property.TrySetValue(parsedValue);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Key in a visualiser subgraph that indicates the subgraph to use.
        /// </summary>
        private static string TypeKeyword = "type";

        /// <summary>
        /// Key in the root visualiser dictionary that indicates a sequence calculator.
        /// Will default to <see cref="DefaultSequenceSubgraph" /> if not provided and a
        /// sequence is required.
        /// </summary>
        private const string SequenceKeyword = "sequence";

        /// <summary>
        /// Key in the root visualiser that can indicate either a subgraph (as a name or a
        /// dict with a type field) or an actual color.
        /// </summary>
        private const string ColorKeyword = "color";

        /// <summary>
        /// Key in the root visualiser that can indicate either a subgraph (as a name or a
        /// dict with a type field) or an actual scale.
        /// </summary>
        private const string ScaleKeyword = "scale";

        /// <summary>
        /// Key in the root visualiser that can indicate either a subgraph (as a name or a
        /// dict with a type field) or an actual width.
        /// </summary>
        private const string WidthKeyword = "width";

        /// <summary>
        /// Key in the root visualiser that can indicate either a subgraph (as a name or a
        /// dict with a type field) or an actual color.
        /// </summary>
        private const string RenderKeyword = "render";

        /// <summary>
        /// The default render subgraph to use if one is not provided.
        /// </summary>
        private const string DefaultRenderSubgraph = "ball and stick";

        /// <summary>
        /// The key for sequence lengths. This indicates that a sequence subgraph is
        /// required earlier in the chain to provide this.
        /// </summary>
        public const string SequenceLengthsKey = "sequence.lengths";

        /// <summary>
        /// The default subgraph for generating sequences.
        /// </summary>
        private const string DefaultSequenceSubgraph = "entities";
    }
}