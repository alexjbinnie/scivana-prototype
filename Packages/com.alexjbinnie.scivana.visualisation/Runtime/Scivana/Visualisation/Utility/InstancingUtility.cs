/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Runtime.InteropServices;
using Scivana.Core.Math;
using Scivana.Trajectory;
using UnityEngine;

namespace Scivana.Visualisation.Utility
{
    /// <summary>
    /// Utility methods for setting up Narupa-specific instancing parameters
    /// for shaders.
    /// </summary>
    public static class InstancingUtility
    {
        /// <summary>
        /// Enable an arbitrary keyword on the shader and set some array.
        /// </summary>
        public static void SetArray(IndirectMeshDrawCommand command, string keyword, string bufferName, Array data)
        {
            command.SetKeyword(keyword);
            command.SetDataBuffer(bufferName, data);
        }

        /// <summary>
        /// Enable the position array in the shader and set the position values.
        /// </summary>
        public static void SetPositions(IndirectMeshDrawCommand command, Vector3[] positions)
        {
            command.SetKeyword("POSITION_ARRAY");
            command.SetDataBuffer("PositionArray", positions);
        }

        /// <summary>
        /// Enable the color array in the shader and set the color values.
        /// </summary>
        public static void SetColors(IndirectMeshDrawCommand command, Color[] colors)
        {
            command.SetKeyword("COLOR_ARRAY");
            command.SetDataBuffer("ColorArray", colors);
        }

        /// <summary>
        /// Enable the scales array in the shader and set the scale values.
        /// </summary>
        public static void SetScales(IndirectMeshDrawCommand command, float[] scales)
        {
            command.SetKeyword("SCALE_ARRAY");
            command.SetDataBuffer("ScaleArray", scales);
        }

        /// <summary>
        /// Enable the edge array in the shader and set the edge values.
        /// </summary>
        public static void SetEdges(IndirectMeshDrawCommand command, BondPair[] edges)
        {
            command.SetKeyword("EDGE_ARRAY");
            command.SetDataBuffer("EdgeArray", edges);
        }

        /// <summary>
        /// Enable the edge count array in the shader and set the edge count values.
        /// </summary>
        public static void SetEdgeCounts(IndirectMeshDrawCommand command, int[] edgeCounts)
        {
            command.SetKeyword("EDGE_COUNT_ARRAY");
            command.SetDataBuffer("EdgeCountArray", edgeCounts);
        }

        /// <summary>
        /// Copy the transform's world to object and object to world transform
        /// matrixes into the shader.
        /// </summary>
        public static void SetTransform(IndirectMeshDrawCommand command, Transform transform)
        {
            var trs = Transformation.FromTransformRelativeToWorld(transform);
            command.SetMatrix("WorldToObject", trs.InverseMatrix);
            command.SetMatrix("ObjectToWorld", trs.matrix);
            command.SetMatrix("ObjectToWorldInverseTranspose", trs.InverseTransposeMatrix);
        }

        public static ComputeBuffer CreateBuffer<T>(T[] array)
        {
            var buffer = new ComputeBuffer(array.Length,
                                           Marshal.SizeOf<T>(),
                                           ComputeBufferType.Default);
            buffer.SetData(array);
            return buffer;
        }
    }
}