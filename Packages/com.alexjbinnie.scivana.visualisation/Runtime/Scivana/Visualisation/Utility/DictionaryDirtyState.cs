/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Scivana.Visualisation.Utility
{
    /// <summary>
    /// Represents the dirty state of an <see cref="IDictionary{TKey,TValue}" />,
    /// keeping track of the keys of items
    /// which have been modified.
    /// </summary>
    public class DictionaryDirtyState<TKey, TValue> : CollectionDirtyState<TKey>
    {
        private readonly IDictionary<TKey, TValue> dictionary;

        public DictionaryDirtyState(IDictionary<TKey, TValue> dictionary) : base(dictionary.Keys)
        {
            this.dictionary = dictionary;
            if (dictionary is INotifyCollectionChanged notify)
                notify.CollectionChanged += OnCollectionChanged;
        }

        /// <summary>
        /// Collection of dirty keys of the dictionary
        /// </summary>
        public IEnumerable<TKey> DirtyKeys => DirtyItems;

        /// <summary>
        /// Collection of dirty values of the dictionary
        /// </summary>
        public IEnumerable<TValue> DirtyValues => DirtyKeys.Select(key => dictionary[key]);

        /// <summary>
        /// Collection of dirty key-value pairs in the dictionary
        /// </summary>
        public IEnumerable<KeyValuePair<TKey, TValue>> DirtyKeyValuePairs =>
            dictionary.Where(kvp => IsDirty(kvp.Key));
    }
}