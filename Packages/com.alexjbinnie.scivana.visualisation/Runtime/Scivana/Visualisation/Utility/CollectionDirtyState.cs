/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using JetBrains.Annotations;

namespace Scivana.Visualisation.Utility
{
    /// <summary>
    /// Tracks the dirty state of an <see cref="IEnumerable" />, by tracking items
    /// which have changed since the last
    /// time the dirty state of this object was reset. If the provided collection
    /// implements
    /// <see cref="INotifyCollectionChanged" />, it will automatically use
    /// <see cref="INotifyCollectionChanged.CollectionChanged" /> to update the dirty
    /// state.
    /// </summary>
    /// <remarks>
    /// The dirty items are stored in no particular order.
    /// </remarks>
    public class CollectionDirtyState<T> : IReadOnlyDictionary<T, bool>
    {
        private readonly HashSet<T> dirtyItems = new HashSet<T>();

        private readonly ICollection<T> collection;

        public CollectionDirtyState([NotNull] ICollection<T> collection)
        {
            this.collection = collection;

            foreach (var item in collection)
                MarkDirty(item);

            if (collection is INotifyCollectionChanged notify)
                notify.CollectionChanged += OnCollectionChanged;
        }

        /// <summary>
        /// Collection of dirty items.
        /// </summary>
        public IReadOnlyCollection<T> DirtyItems => dirtyItems;

        /// <inheritdoc cref="IEnumerable{T}" />
        public IEnumerator<KeyValuePair<T, bool>> GetEnumerator()
        {
            return collection.Select(item => new KeyValuePair<T, bool>(item, IsDirty(item)))
                             .GetEnumerator();
        }

        /// <inheritdoc cref="IEnumerable" />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public int Count => collection.Count;

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool ContainsKey(T key)
        {
            return Keys.Contains(key);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool TryGetValue(T key, out bool value)
        {
            value = IsDirty(key);
            return ContainsKey(key);
        }

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public bool this[T key] => IsDirty(key);

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public IEnumerable<T> Keys => collection;

        /// <inheritdoc cref="IDictionary{TKey,TValue}" />
        public IEnumerable<bool> Values => Keys.Select(IsDirty);

        /// <summary>
        /// Handler for an <see cref="INotifyCollectionChanged" />, where additions,
        /// removals, replacements and
        /// resets all result in the dirty state being updated.
        /// </summary>
        protected void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Replace:
                    if (e.OldItems != null)
                        foreach (var key in e.OldItems)
                            ClearDirty((T) key);
                    if (e.NewItems != null)
                        foreach (var key in e.NewItems)
                            MarkDirty((T) key);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    ClearAllDirty();
                    foreach (var item in collection)
                        MarkDirty(item);
                    break;
            }
        }

        /// <summary>
        /// Has <paramref name="item" /> been altered since the last time its dirty state
        /// was reset.
        /// </summary>
        public bool IsDirty(T item)
        {
            return dirtyItems.Contains(item);
        }

        /// <summary>
        /// Marks <paramref name="item" /> as being dirty.
        /// </summary>
        public void MarkDirty(T item)
        {
            dirtyItems.Add(item);
        }

        /// <summary>
        /// Set the dirty state of <paramref name="item" /> to be <paramref name="dirty" />
        /// .
        /// </summary>
        public void SetDirty(T item, bool dirty)
        {
            if (dirty)
                MarkDirty(item);
            else
                ClearDirty(item);
        }

        /// <summary>
        /// Reset the dirty state of <paramref name="item" />.
        /// </summary>
        public void ClearDirty(T item)
        {
            dirtyItems.Remove(item);
        }

        /// <summary>
        /// Reset the dirty state of all items.
        /// </summary>
        public void ClearAllDirty()
        {
            dirtyItems.Clear();
        }
    }
}