/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace Scivana.Visualisation.Utility
{
    /// <summary>
    /// A Unity <see cref="UnityEngine.Material" /> represented as an abstract GPU
    /// program.
    /// </summary>
    public class MaterialShaderProgram : IGpuRenderProgram
    {
        public Material Material { get; }

        public MaterialShaderProgram(Material material)
        {
            Material = material;
        }

        /// <inheritdoc cref="IGpuProgram.SetBuffer" />
        public void SetBuffer(string id, ComputeBuffer buffer)
        {
            Material.SetBuffer(id, buffer);
        }

        /// <inheritdoc cref="IGpuRenderProgram.SetBuffer" />
        public void SetKeyword(string keyword, bool active = true)
        {
            if (active)
                Material.EnableKeyword(keyword);
            else
                Material.DisableKeyword(keyword);
        }

        /// <summary>
        /// Set the float value for a shader parameter in the Unity material.
        /// </summary>
        public void SetFloat(string id, float value)
        {
            Material.SetFloat(id, value);
        }

        /// <summary>
        /// Set the matrix value for a shader parameter in the Unity material.
        /// </summary>
        public void SetMatrix(string id, Matrix4x4 value)
        {
            Material.SetMatrix(id, value);
        }

        /// <summary>
        /// Set the color value for a shader parameter in the Unity material.
        /// </summary>
        public void SetColor(string id, Color value)
        {
            Material.SetColor(id, value);
        }
    }
}