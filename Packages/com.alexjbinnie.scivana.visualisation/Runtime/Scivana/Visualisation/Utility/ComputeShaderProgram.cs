/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

#endif

namespace Scivana.Visualisation.Utility
{
    /// <summary>
    /// A specific kernel (executable program) of a <see cref="ComputeShader" />, which
    /// is an example of an abstract
    /// GPU program which can have data passed to it from the CPU using
    /// <see cref="ComputeBuffer" />'s.
    /// </summary>
    public class ComputeShaderProgram : IGpuProgram
    {
        private readonly int kernelIndex;

        private readonly ComputeShader shader;

        /// <summary>
        /// Create a <see cref="ComputeShaderProgram" /> that represents the kernel
        /// (executable program) of the
        /// <see cref="ComputeShader" /> <paramref name="shader" /> with the provided
        /// <paramref name="kernelIndex" />
        /// </summary>
        public ComputeShaderProgram(ComputeShader shader, int kernelIndex)
        {
            this.shader = shader;
            this.kernelIndex = kernelIndex;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Path to the asset of the <see cref="ComputeShader" /> containing this program
        /// </summary>
        public string ShaderPath => AssetDatabase.GetAssetPath(shader);
#endif

        /// <inheritdoc cref="IGpuProgram.SetBuffer" />
        public void SetBuffer(string id, ComputeBuffer buffer)
        {
            shader.SetBuffer(kernelIndex, id, buffer);
        }

        /// <summary>
        /// Execute this GPU program by dispatching the corresponding kernel of the
        /// <see cref="ComputeShader" />
        /// </summary>
        public void Dispatch(int threadGroupsX, int threadGroupsY, int threadGroupsZ)
        {
            shader.Dispatch(kernelIndex, threadGroupsX, threadGroupsY, threadGroupsZ);
        }
    }
}