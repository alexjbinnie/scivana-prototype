/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Scivana.Core;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node;
using UnityEngine.Assertions;

namespace Scivana.Visualisation
{
    public partial class VisualiserFactory
    {
        /// <summary>
        /// Find all the subgraphs that are defined in the root dictionary.
        /// </summary>
        private void FindAllSubgraphsInRootDictionary()
        {
            var sequenceSubgraph =
                FindSubgraphInRootDictionary(SequenceKeyword, GetSequenceSubgraph);

            FindSubgraphInRootDictionary(ColorKeyword, GetColorSubgraph);

            FindSubgraphInRootDictionary(WidthKeyword, GetWidthSubgraph);

            FindSubgraphInRootDictionary(ScaleKeyword, GetScaleSubgraph);

            var colorPulser = GetColorSubgraph("color pulser");
            Assert.IsNotNull(colorPulser);
            subgraphs.Add(colorPulser);

            var renderSubgraph = FindSubgraphInRootDictionary(RenderKeyword, GetRenderSubgraph);
            if (renderSubgraph == null)
                subgraphs.Add(GetRenderSubgraph(DefaultRenderSubgraph));

            // If a subgraph requires a set of sequence lengths, a sequence provider is required.
            // If one hasn't already been provided, the default is one that generates sequences
            // based on entities.
            if (sequenceSubgraph == null
             && subgraphs.Any(subgraph =>
                                  subgraph.HasInputProperty(StandardProperties.SequenceParticles)))
            {
                var subgraph = GetSequenceSubgraph(DefaultSequenceSubgraph);
                subgraphs.Insert(0, subgraph);
            }
        }

        /// <summary>
        /// Look in <paramref name="dict" /> to see if it contains key. If it does and its
        /// a string, see if a subgraph with that name exists (using <paramref name="findSubgraph" />
        /// and return it. If it's a dictionary with a 'type' key, look up a subgraph with this
        /// id, and treat the rest of the dictionary as parameters.
        /// </summary>
        /// <param name="dict">The root dictionary in which the key could be found</param>
        /// <param name="key">The key that defines this kind of subgraph</param>
        /// <param name="findSubgraph">A function to find a subgraph with the given name</param>
        private static (VisualisationNode subgraph, Dictionary<string, object> parameters)
            GetSubgraph(Dictionary<string, object> dict,
                        string key,
                        Func<string, VisualisationNode> findSubgraph)
        {
            if (dict.TryGetValue<Dictionary<string, object>>(key, out var strut))
            {
                if (strut.TryGetValue<string>(TypeKeyword, out var type))
                {
                    var subgraph = findSubgraph(type);
                    if (subgraph != null)
                    {
                        return (subgraph, strut);
                    }
                }
            }
            else if (dict.TryGetValue<string>(key, out var t))
            {
                var subgraph = findSubgraph(t);
                if (subgraph != null)
                {
                    return (subgraph, null);
                }
            }

            return (null, null);
        }

        /// <summary>
        /// Add the subgraph and its parameters to the local list of them
        /// </summary>
        private VisualisationNode AddSubgraph(VisualisationNode subgraph,
                                              Dictionary<string, object> parameters)
        {
            subgraphs.Add(subgraph);
            if (parameters != null)
                subgraphParameters.Add(subgraph, parameters);
            return subgraph;
        }

        /// <summary>
        /// Find a subgraph with the given key, and if found add it to our list of
        /// subgraphs.
        /// </summary>
        private VisualisationNode FindSubgraphInRootDictionary(string key,
                                                               Func<string, VisualisationNode> findSubgraph)
        {
            var (subgraph, parameters) = GetSubgraph(rootParameters, key, findSubgraph);
            return subgraph == null ? null : AddSubgraph(subgraph, parameters);
        }

        private static Dictionary<string, Type> GetSubgraphsOfType<TSubgraphAttribute>()
            where TSubgraphAttribute : SubgraphAttribute
        {
            var subgraphTypes = new Dictionary<string, Type>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            foreach (var type in assembly.GetTypes())
                if (type.GetCustomAttribute<TSubgraphAttribute>() is {} subgraphAttr)
                    foreach (var id in subgraphAttr.Ids)
                        subgraphTypes[id] = type;
            return subgraphTypes;
        }

        private static VisualisationNode CreateSubgraphOfType(Dictionary<string, Type> subgraphTypes, string name)
        {
            if (!subgraphTypes.ContainsKey(name))
                return null;

            return Activator.CreateInstance(subgraphTypes[name]) as VisualisationNode;
        }

        private static Dictionary<string, Type> colorSubgraphTypes;
        private static Dictionary<string, Type> renderSubgraphTypes;
        private static Dictionary<string, Type> sequenceSubgraphTypes;
        private static Dictionary<string, Type> scaleSubgraphTypes;

        /// <summary>
        /// Get a visualisation subgraph which is responsible for rendering information.
        /// </summary>
        private static VisualisationNode GetRenderSubgraph(string name)
        {
            renderSubgraphTypes ??= GetSubgraphsOfType<RenderSubgraphAttribute>();
            return CreateSubgraphOfType(renderSubgraphTypes, name);
        }

        /// <summary>
        /// Get a visualisation subgraph which is responsible for coloring particles.
        /// </summary>
        private static VisualisationNode GetColorSubgraph(string name)
        {
            colorSubgraphTypes ??= GetSubgraphsOfType<ColorSubgraphAttribute>();
            return CreateSubgraphOfType(colorSubgraphTypes, name);
        }

        /// <summary>
        /// Get a visualisation subgraph which is responsible for the scale of particles.
        /// </summary>
        private static VisualisationNode GetScaleSubgraph(string name)
        {
            scaleSubgraphTypes ??= GetSubgraphsOfType<ScaleSubgraphAttribute>();
            return CreateSubgraphOfType(scaleSubgraphTypes, name);
        }

        /// <summary>
        /// Get a visualisation subgraph which is responsible for the width of particles in
        /// splines.
        /// </summary>
        private static VisualisationNode GetWidthSubgraph(string name)
        {
            scaleSubgraphTypes ??= GetSubgraphsOfType<ScaleSubgraphAttribute>();
            return CreateSubgraphOfType(scaleSubgraphTypes, name);
        }

        /// <summary>
        /// Get a visualisation subgraph which is responsible for calculating sequences.
        /// </summary>
        private static VisualisationNode GetSequenceSubgraph(string name)
        {
            sequenceSubgraphTypes ??= GetSubgraphsOfType<SequenceSubgraphAttribute>();
            return CreateSubgraphOfType(sequenceSubgraphTypes, name);
        }
    }
}