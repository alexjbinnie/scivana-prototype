﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation
{
    [ExecuteAlways]
    public class CommandBufferRenderer : MonoBehaviour
    {
        protected void OnEnable()
        {
            Camera.onPreCull += Render;
        }

        protected void OnDisable()
        {
            Camera.onPreCull -= Render;
            ClearAllBuffers();
        }

        private void ClearAllBuffers()
        {
            buffers.Clear();
            foreach (var state in cameraBuffers.Values)
                state.RemoveAll();
        }

        private class CameraState
        {
            private Camera camera;

            private List<(CameraEvent, CommandBuffer)> currentBuffers =
                new List<(CameraEvent, CommandBuffer)>();

            private List<(CameraEvent, CommandBuffer)> pendingRemovals =
                new List<(CameraEvent, CommandBuffer)>();

            private List<(CameraEvent, CommandBuffer)> pendingAdditions =
                new List<(CameraEvent, CommandBuffer)>();

            public CameraState(Camera camera)
            {
                this.camera = camera;
            }

            public void AddBuffer(CameraEvent evnt, CommandBuffer buffer)
            {
                pendingAdditions.Add((evnt, buffer));
            }

            public void AddBuffers(IEnumerable<(CameraEvent evnt, CommandBuffer buffer)> buffers)
            {
                pendingAdditions.AddRange(buffers);
            }

            public void RemoveBuffer(CameraEvent evnt, CommandBuffer buffer)
            {
                pendingRemovals.Add((evnt, buffer));
            }

            public void UpdateCamera()
            {
                if (camera == null)
                    return;

                foreach (var (cameraEvent, commandBuffer) in pendingRemovals)
                {
                    currentBuffers.Remove((cameraEvent, commandBuffer));
                    camera.RemoveCommandBuffer(cameraEvent, commandBuffer);
                }

                foreach (var (cameraEvent, commandBuffer) in pendingAdditions)
                {
                    currentBuffers.Add((cameraEvent, commandBuffer));
                    camera.AddCommandBuffer(cameraEvent, commandBuffer);
                }

                pendingAdditions.Clear();
                pendingRemovals.Clear();
            }

            public void RemoveAll()
            {
                pendingAdditions.Clear();
                pendingRemovals.Clear();
                pendingRemovals.AddRange(currentBuffers);
                UpdateCamera();
            }
        }

        private List<(CameraEvent, CommandBuffer)> buffers =
            new List<(CameraEvent, CommandBuffer)>();

        private Dictionary<Camera, CameraState> cameraBuffers =
            new Dictionary<Camera, CameraState>();

        protected void Render(Camera camera)
        {
            if (!cameraBuffers.ContainsKey(camera))
            {
                cameraBuffers.Add(camera, new CameraState(camera));
                cameraBuffers[camera].AddBuffers(buffers);
            }

            cameraBuffers[camera].UpdateCamera();
        }

        public void AddCommandBuffer(CameraEvent evnt, CommandBuffer buffer)
        {
            buffers.Add((evnt, buffer));
            foreach (var state in cameraBuffers.Values)
                state.AddBuffer(evnt, buffer);
        }

        public void RemoveCommandBuffer(CameraEvent evnt, CommandBuffer buffer)
        {
            buffers.Remove((evnt, buffer));
            foreach (var state in cameraBuffers.Values)
                state.RemoveBuffer(evnt, buffer);
        }
    }
}