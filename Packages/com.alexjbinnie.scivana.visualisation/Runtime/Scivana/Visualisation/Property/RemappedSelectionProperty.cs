/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Math;
using Scivana.Core.Properties;

namespace Scivana.Visualisation.Property
{
    /// <summary>
    /// Takes a set of selections of particle indices, and a list of the new set of indices.
    /// </summary>
    public class RemappedSelectionProperty : IReadOnlyProperty<IReadOnlyList<int>[]>, IFilteredProperty
    {
        IReadOnlyProperty IFilteredProperty.SourceProperty => property;

        IReadOnlyProperty<int[]> IFilteredProperty.FilterProperty => filter;

        private readonly IReadOnlyProperty<IReadOnlyList<int>[]> property;

        private readonly IReadOnlyProperty<int[]> filter;

        public RemappedSelectionProperty(IReadOnlyProperty<IReadOnlyList<int>[]> property,
                                         IReadOnlyProperty<int[]> filter)
        {
            this.property = property;
            this.filter = filter;
            this.property.ValueChanged += Update;
            this.filter.ValueChanged += Update;
            Update();
        }

        private bool hasValue;
        private bool hasFilter;

        private IReadOnlyList<int>[] filteredValues = new IReadOnlyList<int>[0];

        public void Update()
        {
            hasValue = property.HasValue;
            hasFilter = filter.HasValue;

            if (hasFilter && hasValue)
            {
                Array.Resize(ref filteredValues, property.Value.Length);
                var j = 0;
                foreach (var selection in property.Value)
                {
                    var newSelection = new int[selection.Count];
                    var i = 0;
                    foreach (var f in selection)
                    {
                        var indexInFilter = SearchAlgorithms.BinarySearchIndex(f, filter.Value);
                        if (indexInFilter >= 0)
                            newSelection[i++] = indexInFilter;
                    }

                    filteredValues[j++] = newSelection;
                }
            }

            ValueChanged?.Invoke();
        }

        public bool HasValue => hasValue;

        public event Action ValueChanged;

        public Type PropertyType => typeof(int[]);

        object IReadOnlyProperty.Value => Value;

        public IReadOnlyList<int>[] Value
        {
            get
            {
                if (!hasValue)
                    throw new InvalidOperationException(
                        "Tried accessing value of property when it is not defined");
                if (!hasFilter)
                    return property.Value;
                return filteredValues;
            }
        }
    }
}