/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;

namespace Scivana.Visualisation.Property
{
    public interface IDynamicPropertyProvider : IPropertyProvider
    {
        /// <summary>
        /// Get an existing property, or attempt to dynamically create one with the given
        /// type.
        /// </summary>
        IReadOnlyProperty<T> GetOrCreateProperty<T>(string name);

        /// <summary>
        /// Get a list of potential properties, some of which may already exist.
        /// </summary>
        /// <remarks>
        /// A returned item of this method indicates that
        /// <see cref="IDynamicPropertyProvider.GetOrCreateProperty{T}" /> will be
        /// successful with the given name
        /// and type. However, that method may also support arbitary names/types
        /// depending on the implementation.
        /// </remarks>
        IEnumerable<(string name, Type type)> GetPotentialProperties();

        /// <summary>
        /// Could this provider give a property on a call to
        /// <see cref="IDynamicPropertyProvider.GetOrCreateProperty{T}" />?.
        /// </summary>
        bool CanDynamicallyProvideProperty<T>(string name);
    }
}