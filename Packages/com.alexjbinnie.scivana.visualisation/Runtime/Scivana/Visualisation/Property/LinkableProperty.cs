/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using JetBrains.Annotations;
using Scivana.Core.Properties;

namespace Scivana.Visualisation.Property
{
    /// <summary>
    /// Implementation of a property which does not define if it itself provides a
    /// value, but allows linking to other properties.
    /// </summary>
    public abstract class LinkableProperty<TValue> : Core.Properties.Property, IProperty<TValue>
    {
        /// <summary>
        /// A linked <see cref="Property" /> which can provide a value.
        /// </summary>
        [CanBeNull]
        private IReadOnlyProperty<TValue> linkedProperty;

        protected override IReadOnlyProperty NonGenericLinkedProperty => LinkedProperty;

        protected override object NonGenericValue => Value;

        /// <inheritdoc cref="IProperty{TValue}.HasValue" />
        public override bool HasValue
        {
            get
            {
                EnsureCacheValid();
                return cachedHasValue;
            }
        }

        private bool isCacheValid;
        private bool cachedHasValue;
        private TValue cachedValue;

        private void ResetCache()
        {
            isCacheValid = false;
            cachedValue = default;
            cachedHasValue = false;
        }

        private void EnsureCacheValid()
        {
            if (!isCacheValid)
            {
                cachedHasValue = GetHasValueInternal();
                if (cachedHasValue)
                    cachedValue = GetValueInternal();
                isCacheValid = true;
            }
        }

        private bool GetHasValueInternal()
        {
            if (HasLinkedProperty)
                return LinkedProperty.HasValue;
            if (HasProvidedValue)
                return true;
            return false;
        }

        /// <inheritdoc cref="Property.MarkValueAsChanged" />
        public override void MarkValueAsChanged()
        {
            ResetCache();
            base.MarkValueAsChanged();
            OnValueChanged();
        }

        protected abstract TValue ProvidedValue { get; set; }

        protected abstract bool HasProvidedValue { get; }

        /// <inheritdoc cref="IProperty{TValue}.Value" />
        public virtual TValue Value
        {
            get
            {
                EnsureCacheValid();
                if (!cachedHasValue)
                    throw new InvalidOperationException(
                        "Cannot called Value on a property without a value. Check first using HasValue.");
                return cachedValue;
            }
            set
            {
                LinkedProperty = null;
                ProvidedValue = value;
                MarkValueAsChanged();
            }
        }

        private TValue GetValueInternal()
        {
            if (HasLinkedProperty)
                return LinkedProperty.Value;
            if (HasProvidedValue)
                return ProvidedValue;
            throw new InvalidOperationException(
                "Tried accessing value of property when it is not defined");
        }

        /// <inheritdoc cref="IProperty{TValue}.HasLinkedProperty" />
        public override bool HasLinkedProperty => LinkedProperty != null;

        /// <inheritdoc cref="IProperty{TValue}.LinkedProperty" />
        [CanBeNull]
        public IReadOnlyProperty<TValue> LinkedProperty
        {
            get => linkedProperty;
            set
            {
                if (linkedProperty == value)
                    return;

                if (value == this)
                    throw new ArgumentException("Cannot link property to itself!");

                // Check no cyclic linked properties will occur
                var linked = value is SerializableProperty<TValue> linkable
                                 ? linkable.LinkedProperty
                                 : null;
                while (linked != null)
                {
                    if (linked == this)
                        throw new ArgumentException("Cyclic link detected!");
                    linked = linked is SerializableProperty<TValue> linkable2
                                 ? linkable2.LinkedProperty
                                 : null;
                }


                if (linkedProperty != null)
                    linkedProperty.ValueChanged -= MarkValueAsChanged;
                linkedProperty = value;
                if (linkedProperty != null)
                    linkedProperty.ValueChanged += MarkValueAsChanged;

                MarkValueAsChanged();
            }
        }

        /// <inheritdoc cref="IProperty{TValue}.UndefineValue" />
        public override void UndefineValue()
        {
            LinkedProperty = null;
            MarkValueAsChanged();
        }

        /// <summary>
        /// Implicit conversion of the property to its value
        /// </summary>
        public static implicit operator TValue(LinkableProperty<TValue> property)
        {
            return property.Value;
        }

        /// <inheritdoc cref="Property.PropertyType" />
        public override Type PropertyType => typeof(TValue);

        /// <inheritdoc cref="Property.TrySetValue" />
        public override void TrySetValue(object value)
        {
            if (value is TValue validValue)
                Value = validValue;
            else if (value == default)
                Value = default;
            else
                throw new ArgumentException(
                    $"Tried to set property of type {PropertyType} to {value}.");
        }

        /// <inheritdoc cref="Property.TrySetLinkedProperty" />
        public override void TrySetLinkedProperty(object value)
        {
            if (value is IReadOnlyProperty<TValue> validValue)
                LinkedProperty = validValue;
            else if (value == null)
                LinkedProperty = null;
            else
                throw new ArgumentException($"Cannot set linked property {value} for {this}");
        }
    }
}