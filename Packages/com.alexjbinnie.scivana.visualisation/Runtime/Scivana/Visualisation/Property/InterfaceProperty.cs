/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Scivana.Visualisation.Property
{
    /// <summary>
    /// A interface-based property which is serialised as a Unity Object.
    /// </summary>
    [Serializable]
    public class InterfaceProperty<TValue> : LinkableProperty<TValue>,
                                             ISerializationCallbackReceiver
    {
        /// <summary>
        /// Unity object which could implement the interface.
        /// </summary>
        [SerializeField]
        private Object unityObject;

        private TValue value;

        /// <summary>
        /// Override for indicating that the value is null. Unity does not serialize
        /// nullable types, so this is required.
        /// </summary>
        [SerializeField]
        private bool isValueProvided;

        protected override TValue ProvidedValue
        {
            get => value;
            set

            {
                this.value = value;
                if (value is Object obj)
                    unityObject = obj;
                else
                    unityObject = null;
                isValueProvided = true;
            }
        }

        protected override bool HasProvidedValue => isValueProvided;

        public override void UndefineValue()
        {
            if (HasProvidedValue)
            {
                isValueProvided = false;
                unityObject = null;
                MarkValueAsChanged();
            }

            base.UndefineValue();
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            if (unityObject is TValue val)
            {
                value = val;
            }

            MarkValueAsChanged();
        }
    }
}