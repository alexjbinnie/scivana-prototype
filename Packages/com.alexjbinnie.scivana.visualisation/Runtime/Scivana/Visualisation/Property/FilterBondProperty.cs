/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Math;
using Scivana.Core.Properties;
using Scivana.Trajectory;

namespace Scivana.Visualisation.Property
{
    /// <summary>
    /// Filters a list of bonds by some index property.
    /// </summary>
    public class FilterBondProperty : IReadOnlyProperty<BondPair[]>
    {
        private readonly IReadOnlyProperty<BondPair[]> property;

        private readonly IReadOnlyProperty<int[]> filter;

        public FilterBondProperty(IReadOnlyProperty<BondPair[]> property,
                                  IReadOnlyProperty<int[]> filter)
        {
            this.property = property;
            this.filter = filter;
            this.property.ValueChanged += Update;
            this.filter.ValueChanged += Update;
            Update();
        }

        private bool hasValue;
        private bool hasFilter;

        private BondPair[] filteredValues = new BondPair[0];

        public void Update()
        {
            hasValue = property.HasValue;
            hasFilter = filter.HasNonNullValue();

            if (hasFilter && hasValue)
            {
                Array.Resize(ref filteredValues, property.Value.Length);
                var j = 0;
                foreach (var bond in property.Value)
                {
                    var i1 = SearchAlgorithms.BinarySearchIndex(bond.A, filter.Value);
                    if (i1 < 0)
                        continue;

                    var i2 = SearchAlgorithms.BinarySearchIndex(bond.B, filter.Value);
                    if (i2 < 0)
                        continue;

                    filteredValues[j] = new BondPair(i1, i2);
                    j++;
                }

                Array.Resize(ref filteredValues, j);
            }

            ValueChanged?.Invoke();
        }

        public bool HasValue => hasValue;

        public event Action ValueChanged;
        public Type PropertyType => typeof(BondPair[]);
        object IReadOnlyProperty.Value => Value;

        public BondPair[] Value
        {
            get
            {
                if (!hasValue)
                    throw new InvalidOperationException(
                        "Tried accessing value of property when it is not defined");
                if (!hasFilter)
                    return property.Value;
                return filteredValues;
            }
        }
    }
}