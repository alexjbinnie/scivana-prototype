/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core;
using UnityEngine;

namespace Scivana.Visualisation.Data
{
    /// <summary>
    /// Stores key-value pair of atomic number and color, for defining CPK style
    /// coloring.
    /// </summary>
    [CreateAssetMenu(menuName = "Definition/String-Color Mapping")]
    public class StringColorMapping : ScriptableObject, IMapping<string, Color>
    {
#pragma warning disable 0649
        /// <summary>
        /// Default color used when a color is not found.
        /// </summary>
        [SerializeField]
        private Color defaultColor;

        /// <summary>
        /// List of assignments, acting as a dictionary so Unity can serialize.
        /// </summary>
        [SerializeField]
        private List<StringColorAssignment> dictionary;
#pragma warning restore 0649

        /// <summary>
        /// Get the color for the given atomic element, returning a default color if the
        /// element is not defined.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public Color Map(string name)
        {
            foreach (var item in dictionary)
                if (item.value.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    return item.color;
            return defaultColor;
        }

        /// <summary>
        /// Key-value pair for atomic element to color mappings for serialisation in Unity
        /// </summary>
        [Serializable]
        public class StringColorAssignment
        {
            public string value;
            public Color color;
        }
    }
}