/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Shader for drawing a cylinder along the y axis using raycasting. It can use procedural instancing
// to draw multiple cylinders between different points

// Width of the gradient
float _GradientWidth;

// The scales to apply to the particles
float _ParticleScale;

// The scales to apply to the edges
float _EdgeScale;

// Color multiplier
float4 _Color;

// Diffuse factor (0 for flat, 1 for full diffuse)
float _Diffuse;

#include "UnityCG.cginc"
#include "../Instancing.cginc"
#include "../Transformation.cginc"
#include "../Intersection.cginc"
#include "../Depth.cginc"
#include "../Lighting.cginc"

#include "dashed_line.cginc"

void setup() {
    
}

struct appdata
{
    float4 vertex : POSITION;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct v2f
{
    float4 vertex : SV_POSITION;
    float4 q : TEXCOORD0;
    float4 d : TEXCOORD1;
    fixed4 color1 : TEXCOORD2;
    fixed4 color2 : TEXCOORD3;
    float3 a : TEXCOORD4;
};

v2f vert (appdata i)
{
    v2f o;
    UNITY_SETUP_INSTANCE_ID(i);
    float overall_scale = length(ObjectToWorld._11_21_31);
    
    #if !defined(PROCEDURAL_INSTANCING_ON)
        float3 p = unity_ObjectToWorld._14_24_34;
         o.a = unity_ObjectToWorld._13_23_33 * 0.5;
         float scale = min(length(unity_ObjectToWorld._11_21_31), length(unity_ObjectToWorld._12_22_32));
    #else
        
        float3 p1 = edge_position(0);
        float3 p2 = edge_position(1);
        
        float scale = _EdgeScale;
        float3 off = normalize(p2 - p1);
    
       float rad1 = edge_scale(0) * _ParticleScale;
       float rad2 = edge_scale(1) * _ParticleScale;
    
       scale = min(min(rad1, rad2), scale) - 0.0001;
    
       float o1 = sqrt(rad1*rad1-scale*scale)/2;
       float o2 = sqrt(rad2*rad2-scale*scale)/2;
    
       p1 += off * o1;
       p2 -= off * o2;
       
        setup_isotropic_edge_transformation(p1, p2, scale);
        
        float3 p = 0.5 * (p1 + p2);
        
        o.a = 0.5 * (p2 - p1);
        
        p = mul(ObjectToWorld, float4(p, 1)).xyz;
        
        o.a = mul(ObjectToWorld, float4(o.a, 0)).xyz;
    #endif
    
    
    o.vertex = UnityObjectToClipPos(i.vertex);
    
    float3 v = mul(unity_ObjectToWorld, i.vertex);
    float3 c = _WorldSpaceCameraPos.xyz;
    
    float s = scale * 0.5 * overall_scale;
    
    o.q = float4(p, s);
    o.d = float4(v - c, 0);
   
    o.color1 = _Color * edge_color(0);
    o.color2 = _Color * edge_color(1);
    return o;
}


struct fout {
    fixed4 color : SV_Target;
    float depth : SV_Depth;
};

fout frag (v2f i)
{
    float3 p = _WorldSpaceCameraPos.xyz;
    float3 c = i.q.xyz;
    float3 d = i.d.xyz;
    float s = i.q.w;
    float3 a = i.a.xyz;

    fout o;
    
    float3 position = 0;
    float3 normal = 0;
    float relative = 0;
    intersect_dashed_line(p, d, c - a, 2*a, s, 3 * s, 5 * s, position, normal, relative);
    
    float3 l = normalize(_WorldSpaceLightPos0.xyz);
    
    float lerpt = clamp((relative - 0.5) / (_GradientWidth + 0.0001) + 0.5, 0, 1);
    o.color = DIFFUSE(lerp(i.color1, i.color2, lerpt), normal, l, _Diffuse);
    
    OUTPUT_FRAG_DEPTH(o, position);
    return o;
}