﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Shader "Scivana/Dashed Line"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            
            #include "../Depth.cginc"
            #include "../Lighting.cginc"
            #include "dashed_line.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 p: TEXCOORD0;
                float3 d: TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                float3 worldPos = mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1));
                
                float3 cameraPos = _WorldSpaceCameraPos;
                
                o.p = cameraPos;
                o.d = worldPos - cameraPos;
                
                return o;
            }
            
            struct fout {
                fixed4 color : SV_Target;
                float depth : SV_Depth;
            };

            fout frag (v2f i)
            {
                float3 position = 0;
                float3 normal = 0;
                float relative = 0;
                intersect_dashed_line(i.p, i.d, float3(0, 0, 0), float3(3, 0, 0), 0.07, 0.15, 0.25, position, normal, relative);
                
                fout o;
                float3 l = normalize(_WorldSpaceLightPos0.xyz);
                o.color = DIFFUSE(fixed4(1,1,1,1), normal, l, 0.5);
                OUTPUT_FRAG_DEPTH(o, position);
                return o;
            }
            ENDCG
        }
    }
}
