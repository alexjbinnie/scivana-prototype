/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Shader code to render quads that are billboarded, but aligned along a given axis

float _EdgeScale;
float4 _Color;

// Width of the gradient
float _GradientWidth;

#include "UnityCG.cginc"

#include "../Instancing.cginc"
#include "../Transformation.cginc"

void setup() {
    
}

struct appdata
{
    float4 vertex : POSITION;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct v2f
{
    float4 vertex : SV_POSITION;
    fixed4 color1 : TEXCOORD2;
    fixed4 color2 : TEXCOORD3;
    
    // Lerp value from 0 (start) to 1 (end)
    float lerp :TEXCOORD1;
};

v2f vert (appdata i)
{
    v2f o;
    UNITY_SETUP_INSTANCE_ID(i);
    
    #if defined(PROCEDURAL_INSTANCING_ON)
        // Use instanced position and scale
        float3 startPosition = edge_position(0);
        float3 endPosition = edge_position(1);
        float scale = _EdgeScale;
        setup_billboard_edge_transformation_y(startPosition, endPosition, scale);
    #else
        // Use object position and scale
        float3 position = unity_ObjectToWorld._14_24_34;
        float3 a = 0.5 * unity_ObjectToWorld._12_22_32;
        
        float scale = length(unity_ObjectToWorld._11_21_31);
        
        setup_billboard_edge_transformation_y(position - a, position + a, scale);
         
    #endif
    
    o.vertex = UnityObjectToClipPos(i.vertex);

    o.color1 = _Color * edge_color(0);
    o.color2 = _Color * edge_color(1);
    
    o.lerp = 0.5 + 0.5 * i.vertex.y;
    
    return o;
}

struct fout {
    fixed4 color : SV_Target;
};

fout frag (v2f i)
{
    fout o;
    
    float lerpt = clamp((i.lerp - 0.5) / (_GradientWidth + 0.0001) + 0.5, 0, 1);

    o.color = lerp(i.color1, i.color2, lerpt);
    return o;
}