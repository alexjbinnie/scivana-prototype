﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

void compute_sphere_chain_intersection(out float t, out float3 normal, float3 q, float3 d, float3 a, float R, int min_N, int max_N) {
    t = 1000;
    normal = 0;

    float3 qxaxd = cross(q, cross(a, d));
    float3 axd = cross(a, d);
    
    float discrim_n = dot(qxaxd, qxaxd) - dot(axd, axd) * (dot(q, q) - R*R);
    
    if(discrim_n < 0)
        return;
        
    float i_n = -dot(qxaxd, d) / dot(axd, axd);
    float j_n = sqrt(dot(d, d) * discrim_n) / dot(axd, axd);
        
    float n_min = ceil(i_n - j_n);
    float n_max = floor(i_n + j_n);
    
    if(n_min < min_N)
        n_min = min_N;
        
    if(n_max > max_N)
        n_max = max_N;
    
    if(n_max < n_min)
        return;
    
    float n = n_min;
    
    if(dot(a, d) < 0)
        n = n_max;
    
    float b2a = (dot(q, d) - n * dot(a, d)) / dot(d, d);
    float ca = (dot(q, q) - 2 * n * dot(q, a) + n * n * dot(a, a) - R * R) / dot(d, d);
    
    float discrim = b2a * b2a - ca;
    
    if(discrim < 0)
        discard;
        
    t = -b2a - sqrt(discrim);
        
    normal = normalize(q - a * n + d * t);
}

void compute_sphere_intersection(float3 q, float3 d, float R, out float t, out float normal) {
    
    t = 1000;
    normal = 0;
    
    float b2a = dot(q, d) / dot(d, d);
    
    float ca = (dot(q, q) - R * R) / dot(d, d);
    
    float discrim = b2a * b2a - ca;
    
    if(discrim < 0)
        return;
    
    t = -b2a - sqrt(discrim);
    
    normal = normalize(q + d * t);
}

void compute_cylinder_shell_intersection(float3 q, float3 d, float3 a, float R, out float t, out float rel, out float3 normal) {
    t = 1000;
    normal = 0;
    rel = 0;
    
    float qs = dot(q, a) / dot(a, a);
    float ds = dot(d, a) / dot(a, a);
    
    q = q - qs * a;
    d = d - ds * a;
    
    float b2a = dot(q, d) / dot(d, d);
    
    float ca = (dot(q, q) - R * R) / dot(d, d);
    
    float discrim = b2a * b2a - ca;
    
    if(discrim < 0)
        return;
    
    t = -b2a - sqrt(discrim);
    
    rel = qs + ds * t;
    
    normal = normalize(q + d * t);
}

void intersect_dashed_line(float3 p, float3 d, float3 c, float3 a, float R, float segmentLength, float spaceLength, out float3 position, out float3 normal, out float relative) {

    float percentage = segmentLength / (segmentLength + spaceLength);
    
    float numberOfFullDashes = length(a) / (segmentLength + spaceLength); 
            
    float3 q = p - c;
    
    float t;
    normal;
    
    float t2;
    float3 normal2;
    
    float t_cyl;
    float rel;
    float3 normal_cyl;
    
    float shift = frac(-_Time.x * 10);
    
    compute_cylinder_shell_intersection(q, d, a, R, t_cyl, rel, normal_cyl);
    
    if(rel < 0 || rel > 1)
        discard;
    
    if(percentage < (rel * numberOfFullDashes + shift) - floor(rel * numberOfFullDashes + shift))
        t_cyl = 1000;
        
    float3 physicalShift = normalize(a) * (segmentLength + spaceLength) * shift;
    
    compute_sphere_chain_intersection(t, normal, q + physicalShift, d, a / numberOfFullDashes, R, 0, ceil(numberOfFullDashes) + 1);
    
    compute_sphere_chain_intersection(t2, normal2, q - normalize(a) * segmentLength + physicalShift, d, a / numberOfFullDashes, R, 0, ceil(numberOfFullDashes) + 1);
    
    if(t2 < t) {
        t = t2;
        normal = normal2;
    }
    
    if(t_cyl < t) {
        t = t_cyl;
        normal = normal_cyl;
    }
   
    if(t > 900)
        discard;
        
    position = p + d * t;
    
    relative = dot(q + d * t, a) / dot(a, a);
}