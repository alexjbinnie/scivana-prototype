﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
Shader "Unlit/Capped Cone"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "../Depth.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 q : TEXCOORD0;
                float3 d : TEXCOORD1;
                float3 a : TEXCOORD2;
            };
 
            fixed4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.a = unity_ObjectToWorld._13_23_33;
                //o.q.xyz = _WorldSpaceCameraPos.xyz - unity_ObjectToWorld._14_24_34 - o.a * 0.5;
                //o.d = mul(unity_ObjectToWorld, v.vertex).xyz - _WorldSpaceCameraPos.xyz;
                //o.q.w = min(length(unity_ObjectToWorld._11_21_31), length(unity_ObjectToWorld._12_22_32)) * 0.5;
                
                o.a = float3(0, 0, 1);
                o.q.xyz = _WorldSpaceCameraPos.xyz;
                o.q.w = 0.5;
                o.d = mul(unity_ObjectToWorld, v.vertex).xyz - _WorldSpaceCameraPos.xyz;
                
                return o;
            }
            
            struct fout {
                fixed4 color : SV_Target;
                float depth : SV_Depth;
            };

            fout frag (v2f i)
            {
                float3 q = i.q;
                float3 d = i.d;
                float R = i.q.w;
                float3 a = i.a;
                
                float qp = dot(q, a) / dot(a, a);
                float dp = dot(d, a) / dot(a, a);
                
                float3 qt = q - qp * a;
                float3 dt = d - dp * a;
                
                float t_cap = - qp / dp;
                
                  
                float qq = dot(qt, qt) - R * R * (1 - qp) * (1 - qp);
                float dd = dot(dt, dt) - R * R * dp * dp;
                float qd = dot(qt, dt) + R * R * (1 - qp) * dp;
                
                float b2a = qd / dd;
                float ca = qq / dd;
                
                float discrim = b2a * b2a - ca;
                
                if(discrim < 0)
                    discard;
                    
                float t = -b2a - sqrt(discrim);
                
                if(qp + dp * t < 0 || qp + dp * t > 1) {
                    //if(length(qt + dt * t_cap) < R && dot(a, d) > 0)
                    //    t = t_cap;
                    //else
                        discard;
                }
                
                
                fout o;
                o.color = _Color;
                OUTPUT_FRAG_DEPTH(o, _WorldSpaceCameraPos.xyz + d * t);
                return o;
            }
            ENDCG
        }
    }
}
