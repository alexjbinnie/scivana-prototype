/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Shader for drawing spherical sector

float _Scale;
float4 _Color;
float _Diffuse;
float _CosTheta;

#include "UnityCG.cginc"

#include "../Instancing.cginc"
#include "../Transformation.cginc"
#include "../Intersection.cginc"
#include "../Depth.cginc"
#include "../Lighting.cginc"

void setup() {
    setup_isotropic_transformation(instance_position(), instance_scale() * _Scale);
}

struct appdata
{
    float4 vertex : POSITION;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct v2f
{
    float4 vertex : SV_POSITION;
    float4 q : TEXCOORD0;
    float4 d : TEXCOORD1;
    fixed4 color : TEXCOORD2;
    float4 k : TEXCOORD3;
};

float3 direction(float4 q) {
    return float3(2 * (q.x * q.z + q.y * q.w), 2 * (q.y * q.z - q.x * q.w), 1 - 2 * (q.x * q.x + q.y * q.y));
}

v2f vert (appdata i)
{
    v2f o;
    UNITY_SETUP_INSTANCE_ID(i);
    
    o.vertex = UnityObjectToClipPos(i.vertex);
    
    float3 v = mul(unity_ObjectToWorld, i.vertex);
    float3 c = _WorldSpaceCameraPos.xyz;
    
    #if !defined(PROCEDURAL_INSTANCING_ON)
        float3 p = unity_ObjectToWorld._14_24_34;
        float s = 0.5 * min(min(length(unity_ObjectToWorld._11_21_31), length(unity_ObjectToWorld._12_22_32)), length(unity_ObjectToWorld._13_23_33));
        float3 k = direction(instance_rotation());
        k = mul(unity_ObjectToWorld, float4(k, 0)).xyz;
    #else
        float3 p = mul(ObjectToWorld, float4(instance_position(), 1)).xyz;
        float s = length(ObjectToWorld._11_21_31) * 0.5 * instance_scale() * _Scale;
        float3 k = direction(instance_rotation());
        k = mul(ObjectToWorld, float4(k, 0)).xyz;
    #endif
    
    o.q = float4(c - p, s);
    o.d = float4(v - c, 0);
    o.k = float4(k.xyz, _CosTheta);
    o.color = _Color * instance_color();
    
    return o;
}

struct fout {
    fixed4 color : SV_Target;
    float depth : SV_Depth;
};

int intersect_sphere(float3 q, float3 d, float s, float3 k, float costheta, out float t, out float3 r, out float3 n) {

    t = 0;
    r = 0;
    n = 0;

    float qq = dot(q, q);
    float dd = dot(d, d);
    float qd = dot(q, d);
    
    float b2a = (qd) / (dd);
    float ca = (qq - s * s) / (dd);

    float discrim = b2a * b2a - ca;
    
    if(discrim < 0)
        return 0;
    
    t = -b2a - sqrt(discrim);
    
    r = q + t * d;
    
    if(dot(normalize(r), k) < costheta)
        return 0;
    
    n = normalize(r);
    
    return 1;
}

int intersect_sector(float3 q, float3 d, float s, float3 k, float costheta, out float t, out float3 r, out float3 n) {
    
    t = 0;
    r = 0;
    n = 0;

    float qk = dot(q, k);
    float qq = dot(q, q);
    float dd = dot(d, d);
    float dk = dot(d, k);
    float qd = dot(q, d);
    
    float b2a = (qk * dk - costheta * costheta * qd) / (dk * dk - costheta * costheta * dd);
    float ca = (qk * qk - costheta * costheta * qq) / (dk * dk - costheta * costheta * dd);
    
    float discrim = b2a * b2a - ca;
    
    if(discrim < 0)
        return 0;
    
    t = -b2a - sqrt(discrim);
    if(dk > costheta)
        t = -b2a + sqrt(discrim);
    
    r = q + t * d;
    
    if(dot(r, r) > s * s)
        return 0;
        
    if(dot(r, k) < 0)
        return 0;
    
    n = -normalize((qk + t * dk) * k - costheta * r);

    return 1;
}

int closest(int a, float at, float3 ar, float3 an, int b, float bt, float3 br, float3 bn, out float t, out float3 r, out float3 n) {
    t = 0;
    r = 0;
    n = 0;

    if(a < 1 && b < 1)
        return 0;
        
    if(a > 0 && b > 0) {
        if(at < bt) {
            t = at;
            r = ar;
            n = an;
            return 1;
        }
        else {
            t = bt;
            r = br;
            n = bn;
            return 1;
        }
    }
    
    if(a > 0) {
        t = at;
        r = ar;
        n = an;
        return 1;
    }
    else {
        t = bt;
        r = br;
        n = bn;
        return 1;
    }
}

fout frag (v2f i)
{
    fout o;
    
    float3 q = i.q.xyz;
    float3 d = normalize(i.d.xyz);
    float s = i.q.w;
    
    float3 k = normalize(i.k.xyz);
    float costheta = i.k.w;
    
    float t_sector = 0;
    float3 r_sector = 0;
    float3 n_sector = 0;
    float t_sphere = 0;
    float3 r_sphere = 0;
    float3 n_sphere = 0;
    int sphere = intersect_sphere(q, d, s, k, costheta, t_sphere, r_sphere, n_sphere);
    int sector = intersect_sector(q, d, s, k, costheta, t_sector, r_sector, n_sector);
    
    float t = 0;
    float3 r = 0;
    float3 n = 0;
    int combined = closest(sphere, t_sphere, r_sphere, n_sphere, sector, t_sector, r_sector, n_sector, t, r, n);
    if(combined < 1)
        discard;
   
    float3 l = normalize(_WorldSpaceLightPos0.xyz);
    float3 c = _WorldSpaceCameraPos.xyz;
    
    o.color = DIFFUSE(i.color, n, l, _Diffuse);
    OUTPUT_FRAG_DEPTH(o, c + d * t);
    return o;
}