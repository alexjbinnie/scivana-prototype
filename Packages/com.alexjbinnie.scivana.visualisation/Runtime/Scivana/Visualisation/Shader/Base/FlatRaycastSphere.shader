﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
Shader "Unlit/FlatRaycastSphere"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 q : TEXCOORD0;
                float3 d : TEXCOORD1;
            };
 

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.q = _WorldSpaceCameraPos.xyz;
                o.d = mul(unity_ObjectToWorld, v.vertex).xyz - o.q;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 q = i.q;
                float3 d = i.d;
                float R = 0.5;
                
                float qq = dot(q, q);
                float dd = dot(d, d);
                float qd = dot(q, d);
                
                float b2a = qd / dd;
                float ca = (qq  - R*R) / dd;
                
                float discrim = b2a * b2a - ca;
                
                if(discrim < 0)
                    discard;
                    
                float t = -b2a - sqrt(discrim);
                
                float3 r = q + d * t;
                    
                float3 camUp = mul((float3x3)unity_CameraToWorld, float3(0,1,0));
                
                float3 camRight = mul((float3x3)unity_CameraToWorld, float3(1,0,0));
                
                float3 camForward = normalize(-_WorldSpaceCameraPos.xyz);
                
                float3 reject = r - dot(r, camForward) / dot(camForward, camForward) * camForward;
                   
                float rad = 0.07;
                   
                float rho = sqrt(dot(reject, reject));
               
                float3 axel = normalize(-0.5 * camRight + 0.5 * camUp);
                
                float angle = acos(dot(axel, normalize(reject)));
                
                float cutoff = 0.7853 + sqrt(- (rho-R) * (rho - R) - 2 * (rho - R) * rad) / rho;
                
                if(rho > R - 2 * rad && angle < cutoff)
                    return fixed4(1,1,1,1);
                   
                return fixed4(0.6,0.6,0.6,1);
            }
            ENDCG
        }
    }
}
