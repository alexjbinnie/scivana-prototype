/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
/// Contains methods for dealing with splines

#ifndef SPLINE_CGINC_INCLUDED

    #define SPLINE_CGINC_INCLUDED
    
    struct SplineSegment {
        float3 startPoint;
        float3 endPoint;
        float3 startTangent;
        float3 endTangent;
        float3 startNormal;
        float3 endNormal;
        fixed4 startColor;
        fixed4 endColor;
        float3 startScale;
        float3 endScale;
    };
    
    StructuredBuffer<SplineSegment> SplineArray;

    SplineSegment instance_spline() {
        return SplineArray[instance_id];
    }
    
    float3 GetHermitePoint(float t, float3 startPoint, float3 startTangent, float3 endPoint, float3 endTangent)
    {
        return (2 * t * t * t - 3 * t * t + 1) * startPoint + (t * t * t - 2 * t * t + t) * startTangent +
               (-2 * t * t * t + 3 * t * t) * endPoint + (t * t * t - t * t) * endTangent;
    }

    float3 GetHermiteTangent(float t, float3 startPoint, float3 startTangent, float3 endPoint, float3 endTangent)
    {
        return (6 * t * t - 6 * t) * startPoint + (3 * t * t - 4 * t + 1) * startTangent +
               (-6 * t * t + 6 * t) * endPoint + (3 * t * t - 2 * t) * endTangent;
    }
    
    float3 GetHermiteSecondDerivative(float t, float3 startPoint, float3 startTangent, float3 endPoint, float3 endTangent)
    {
        return (12 * t - 6) * startPoint + (6 * t - 4) * startTangent +
               (-12 * t + 6) * endPoint + (6 * t - 2) * endTangent;
    }

#endif
