﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// Example of a simple raytraced sphere. This shader draws a sphere in object space with a solid color without instancing, 
/// shadow support or depth

Shader "NarupaXR/Example/Raytrace Sphere"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            fixed4 _Color;
            
            struct Ray {
                float4 origin;
                float4 direction;
            };
            
            struct Sphere {
                float4 centre;
                float radius;
            };
            
            /// Calculate the intersection of a ray and a sphere, returning the number of intersections
            int get_sphere_intersections(Sphere sphere, Ray ray, out float2 intersection_offsets) {
            
                float4 q = ray.origin - sphere.centre;
                
                float qq = dot(q, q);
                float dq = dot(ray.direction, q);
                
                float determinant = dq * dq - qq + sphere.radius * sphere.radius;
                
                if(determinant < 0)
                    return 0;
                    
                intersection_offsets.x = -dq - sqrt(determinant);
                intersection_offsets.y = -dq + sqrt(determinant);
                
                return 2;
            };

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 clippos : SV_POSITION;
                float4 vertex : TEXCOORD0;
                float4 viewdir : TEXCOORD1;
            };
            
            struct fout
            {
                fixed4 color : SV_Target;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.clippos = UnityObjectToClipPos(v.vertex);
                o.vertex = v.vertex;
                o.viewdir = v.vertex - mul(unity_WorldToObject, float4(_WorldSpaceCameraPos.xyz, 1));
                return o;
            }

            fout frag (v2f i) : SV_Target
            {
                fout o;
                
                Ray ray;
                ray.origin = i.vertex;
                ray.direction = normalize(i.viewdir);
                
                ray.origin.w = 1;
                ray.direction.w = 0;
                
                Sphere sphere;
                sphere.centre = float4(0,0,0,1);
                sphere.radius = 0.5;
                
                float2 intersection;
                if(!get_sphere_intersections(sphere, ray, intersection))
                    discard;
                
                o.color = _Color;
                return o;
            }
            
            ENDCG
        }
    }
}
