/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
/// Contains methods for computing intersections

#ifndef INTERSECTION_CGINC_INCLUDED

    #define INTERSECTION_CGINC_INCLUDED
    
    // For a ray of the form r = q + d t, solve to find the first point where the ray has a length s from the origin by solving
    //
    //   r.r = s^2    =>    (q + d t).(q + d t) = s^2
    //
    // This returns the first intersection (using q + d t) and the corresponding t.
    // If this never occurs, this function discards the pixel.
    float4 solve_ray_intersection(float3 q, float3 d, float s) {
        float qq = dot(q, q) - s*s;
        float dq = dot(d, q);
        float dd = dot(d, d);
        
        float b2a = dq / dd;
        float ca = qq / dd;
        
        float det = b2a * b2a - ca;
        
        if(det < 0)
            discard;
            
        float t = -b2a - sqrt(det);
        
        float3 r = q + d * t;
        
        return float4(r, t);
    };
    
    // For a ray of the form r = q + d t, solve to find the first point where the ray has a length s from the origin by solving
    //
    //   r.r = s^2    =>    (q + d t).(q + d t) = s^2
    //
    // This returns the first intersection (using q + d t) and the corresponding t.
    // If this never occurs, this function discards the pixel.
    int is_ray_intersection(float3 q, float3 d, float s, out float t) {
        float qq = dot(q, q) - s*s;
        float dq = dot(d, q);
        float dd = dot(d, d);
        
        float b2a = dq / dd;
        float ca = qq / dd;
        
        float det = b2a * b2a - ca;
        
        if(det < 0) {
            t = 0;
            return 0;
        }
            
        t = -b2a - sqrt(det);
        
        return 1;
    };
    
    // Reject a given vector from an axis, giving the pependicular distance from the axis
    float3 reject(float3 v, float3 axis) {
        return v - dot(v, axis) / dot(axis, axis) * axis;
    };

#endif
