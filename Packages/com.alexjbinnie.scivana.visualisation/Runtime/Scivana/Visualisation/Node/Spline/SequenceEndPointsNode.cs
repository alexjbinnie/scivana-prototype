/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Spline
{
    /// <summary>
    /// Get the indices of the end points of a set of sequences.
    /// </summary>
    [Serializable]
    public class SequenceEndPointsNode : GenericOutputNode
    {
        /// <summary>
        /// A set of sequences.
        /// </summary>
        [SerializeField]
        private ArrayProperty<IReadOnlyList<int>> sequences
            = new ArrayProperty<IReadOnlyList<int>>();

        /// <summary>
        /// The indices of the start and end of each sequence.
        /// </summary>
        private readonly ArrayProperty<int> filters = new ArrayProperty<int>();

        /// <inheritdoc cref="GenericOutputNode.IsInputValid" />
        protected override bool IsInputValid => sequences.HasValue;

        /// <inheritdoc cref="GenericOutputNode.IsInputDirty" />
        protected override bool IsInputDirty => sequences.IsDirty;

        /// <inheritdoc cref="GenericOutputNode.ClearDirty" />
        protected override void ClearDirty()
        {
            sequences.IsDirty = false;
        }

        /// <inheritdoc cref="GenericOutputNode.UpdateOutput" />
        protected override void UpdateOutput()
        {
            var list = new List<int>();
            foreach (var sequence in sequences.Value)
            {
                list.Add(sequence[0]);
                if (sequence.Count > 1)
                    list.Add(sequence[sequence.Count - 1]);
            }

            filters.Value = list.ToArray();
        }

        /// <inheritdoc cref="GenericOutputNode.ClearOutput" />
        protected override void ClearOutput()
        {
            filters.UndefineValue();
        }
    }
}