/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Spline
{
    /// <summary>
    /// Rotates normals so within each sequence, the normals only rotate by up to 90
    /// degrees.
    /// </summary>
    [Serializable]
    public class OrientCurveNormals : GenericOutputNode
    {
        [SerializeField]
        private SerializableProperty<CurvePoint[][]> inputCurves = new SerializableProperty<CurvePoint[][]>();

        public IProperty<CurvePoint[][]> InputCurves => inputCurves;

        private ArrayProperty<CurvePoint[]> outputCurves = new ArrayProperty<CurvePoint[]>();

        public IReadOnlyProperty<CurvePoint[][]> OutputCurves => inputCurves;

        /// <summary>
        /// Calculate rotated normals from an existing set of normals and tangents.
        /// </summary>
        public CurvePoint[] RotateNormals(IReadOnlyList<CurvePoint> curve)
        {
            var newCurve = new CurvePoint[curve.Count];

            newCurve[0] = curve[0];

            for (var j = 1; j < curve.Count; j++)
            {
                var prev = newCurve[j - 1];
                var current = curve[j];
                if (Vector3.Dot(prev.Normal, current.Normal) < 0)
                    newCurve[j] = new CurvePoint
                    {
                        Position = current.Position,
                        Normal = -current.Normal,
                        Tangent = current.Tangent
                    };
                else
                    newCurve[j] = current;
            }

            return newCurve;
        }

        /// <inheritdoc cref="GenericOutputNode.IsInputValid" />
        protected override bool IsInputValid => inputCurves.HasNonNullValue();

        /// <inheritdoc cref="GenericOutputNode.IsInputDirty" />
        protected override bool IsInputDirty => inputCurves.IsDirty;

        /// <inheritdoc cref="GenericOutputNode.ClearDirty" />
        protected override void ClearDirty()
        {
            inputCurves.IsDirty = false;
        }

        /// <inheritdoc cref="GenericOutputNode.UpdateOutput" />
        protected override void UpdateOutput()
        {
            outputCurves.Resize(inputCurves.Value.Length);
            for (var i = 0; i < inputCurves.Value.Length; i++)
            {
                outputCurves.Value[i] = RotateNormals(inputCurves.Value[i]);
            }

            outputCurves.MarkValueAsChanged();
        }

        /// <inheritdoc cref="GenericOutputNode.ClearOutput" />
        protected override void ClearOutput()
        {
            outputCurves.UndefineValue();
        }
    }
}