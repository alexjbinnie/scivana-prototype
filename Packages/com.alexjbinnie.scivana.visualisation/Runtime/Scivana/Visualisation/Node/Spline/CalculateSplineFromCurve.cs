﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Spline
{
    /// <summary>
    /// Generate a set of <see cref="SplineSegment" />s from a set of positions.
    /// </summary>
    [Serializable]
    public class CalculateSplineFromCurve : GenericOutputNode
    {
        [SerializeField]
        private SerializableProperty<CurvePoint[][]> curves = new ArrayProperty<CurvePoint[]>();

        public IProperty<CurvePoint[][]> Curves => curves;

        [SerializeField]
        private SerializableProperty<UnityEngine.Color[][]> curveColors = new ArrayProperty<UnityEngine.Color[]>();

        public IProperty<UnityEngine.Color[][]> CurveColors => curveColors;

        [SerializeField]
        private SerializableProperty<float[][]> curveScales = new ArrayProperty<float[]>();

        public IProperty<float[][]> CurveScales => curveScales;

        [SerializeField]
        private SerializableProperty<UnityEngine.Color> color
            = new SerializableProperty<UnityEngine.Color>
            {
                Value = UnityEngine.Color.white
            };

        [SerializeField]
        private SerializableProperty<float> radius = new SerializableProperty<float>
        {
            Value = 1f
        };

        private ArrayProperty<SplineSegment> splineSegments = new ArrayProperty<SplineSegment>();

        public IReadOnlyProperty<SplineSegment[]> SplineSegments => splineSegments;

        /// <inheritdoc cref="GenericOutputNode.IsInputValid" />
        protected override bool IsInputValid => curves.HasNonNullValue()
                                             && color.HasNonNullValue()
                                             && radius.HasNonNullValue();

        /// <inheritdoc cref="GenericOutputNode.IsInputDirty" />
        protected override bool IsInputDirty => curves.IsDirty
                                             || curveColors.IsDirty
                                             || curveScales.IsDirty
                                             || color.IsDirty
                                             || radius.IsDirty;

        /// <inheritdoc cref="GenericOutputNode.ClearDirty" />
        protected override void ClearDirty()
        {
            curves.IsDirty = false;
            curveColors.IsDirty = false;
            curveScales.IsDirty = false;
            color.IsDirty = false;
            radius.IsDirty = false;
        }

        /// <inheritdoc cref="GenericOutputNode.UpdateOutput" />
        protected override void UpdateOutput()
        {
            var segmentCount = curves.Value.Sum(s => s.Length - 1);

            this.splineSegments.Resize(segmentCount);

            var splineSegments = this.splineSegments.Value;

            var segOffset = 0;

            for (var i = 0; i < curves.Value.Length; i++)
            {
                var points = curves.Value[i];
                var curveColors = this.curveColors.HasValue ? this.curveColors.Value[i] : null;
                var curveScales = this.curveScales.HasValue ? this.curveScales.Value[i] : null;

                if (points.Length <= 1)
                    continue;

                var startPoint = points[0];
                var startColor = curveColors?[0] ?? color.Value;
                var startScale = curveScales?[0] ?? radius.Value;

                for (var j = 1; j < points.Length; j++)
                {
                    var endPoint = points[j];
                    var endColor = curveColors?[j] ?? color.Value;
                    var endScale = curveScales?[j] ?? radius.Value;

                    splineSegments[segOffset++] = new SplineSegment
                    {
                        StartPoint = startPoint.Position,
                        StartNormal = startPoint.Normal,
                        StartTangent = startPoint.Tangent,
                        StartColor = startColor,
                        StartScale = Vector3.one * startScale,
                        EndPoint = endPoint.Position,
                        EndNormal = endPoint.Normal,
                        EndTangent = endPoint.Tangent,
                        EndColor = endColor,
                        EndScale = Vector3.one * endScale
                    };

                    startPoint = endPoint;
                    startColor = endColor;
                    startScale = endScale;
                }
            }

            this.splineSegments.Value = splineSegments;
        }

        /// <inheritdoc cref="GenericOutputNode.ClearOutput" />
        protected override void ClearOutput()
        {
            splineSegments.UndefineValue();
        }
    }
}