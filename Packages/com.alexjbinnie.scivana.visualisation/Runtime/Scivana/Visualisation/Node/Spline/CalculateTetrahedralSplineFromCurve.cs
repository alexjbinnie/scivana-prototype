﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Spline
{
    [Serializable]
    public class CalculateTetrahedralSplineFromCurve : GenericOutputNode
    {
        [SerializeField]
        private SerializableProperty<CurvePoint[][]> curves = new ArrayProperty<CurvePoint[]>();

        public IProperty<CurvePoint[][]> Curves => curves;

        [SerializeField]
        private SerializableProperty<UnityEngine.Color[][]> curveColors = new ArrayProperty<UnityEngine.Color[]>();

        public IProperty<UnityEngine.Color[][]> CurveColors => curveColors;

        [SerializeField]
        private SerializableProperty<float[][]> curveWidths = new ArrayProperty<float[]>();

        public IProperty<float[][]> CurveWidths => curveWidths;

        [SerializeField]
        private SerializableProperty<UnityEngine.Color> color
            = new SerializableProperty<UnityEngine.Color>
            {
                Value = UnityEngine.Color.white
            };

        [SerializeField]
        private SerializableProperty<float> width = new SerializableProperty<float>
        {
            Value = 0.1f
        };

        private ArrayProperty<Vector3> outputPositions = new ArrayProperty<Vector3>();

        public IReadOnlyProperty<Vector3[]> OutputPositions => outputPositions;

        private ArrayProperty<BondPair> outputBonds = new ArrayProperty<BondPair>();

        public IReadOnlyProperty<BondPair[]> OutputBonds => outputBonds;

        private ArrayProperty<IReadOnlyList<int>> outputFaces
            = new ArrayProperty<IReadOnlyList<int>>();

        public IReadOnlyProperty<IReadOnlyList<int>[]> OutputFaces => outputFaces;

        private ArrayProperty<UnityEngine.Color> outputColors
            = new ArrayProperty<UnityEngine.Color>();

        public IReadOnlyProperty<UnityEngine.Color[]> OutputColors => outputColors;

        public IProperty<float> Width => width;

        /// <inheritdoc cref="GenericOutputNode.IsInputValid" />
        protected override bool IsInputValid => curves.HasNonNullValue()
                                             && color.HasNonNullValue()
                                             && width.HasNonNullValue();

        /// <inheritdoc cref="GenericOutputNode.IsInputDirty" />
        protected override bool IsInputDirty => curves.IsDirty
                                             || curveColors.IsDirty
                                             || curveWidths.IsDirty
                                             || color.IsDirty
                                             || width.IsDirty;

        /// <inheritdoc cref="GenericOutputNode.ClearDirty" />
        protected override void ClearDirty()
        {
            curves.IsDirty = false;
            curveColors.IsDirty = false;
            curveWidths.IsDirty = false;
            color.IsDirty = false;
            width.IsDirty = false;
        }

        private int[] cachedLengths = new int[0];

        /// <inheritdoc cref="GenericOutputNode.UpdateOutput" />
        protected override void UpdateOutput()
        {
            var pointCount = curves.Value.Sum(i => i.Length);

            var pointsDirty = cachedLengths.Length != curves.Value.Length;

            if (!pointsDirty)
            {
                for (var i = 0; i < curves.Value.Length; i++)
                    if (cachedLengths[i] != curves.Value[i].Length)
                        pointsDirty = true;
            }

            if (pointsDirty)
            {
                Array.Resize(ref cachedLengths, curves.Value.Length);
                var segmentCount = curves.Value.Sum(i => i.Length - 1);
                var bondcount = pointCount + 4 * segmentCount;

                this.outputPositions.Resize(pointCount * 2);
                this.outputBonds.Resize(bondcount);
                this.outputColors.Resize(pointCount * 2);
                this.outputFaces.Resize(segmentCount * 4);

                var outputBonds = this.outputBonds.Value;
                var outputFaces = this.outputFaces.Value;

                // Generate bonds between each pair of points representing a point of the spline.
                var bondIndex = 0;
                var curveIndex = 0;
                foreach (var curve in curves.Value)
                {
                    cachedLengths[curveIndex] = curve.Length;
                    for (var i = 0; i < curve.Length; i++)
                    {
                        outputBonds[bondIndex] = new BondPair(bondIndex * 2, bondIndex * 2 + 1);
                        bondIndex++;
                    }

                    curveIndex++;
                }

                var pointIndex = 0;
                var faceIndex = 0;
                foreach (var curve in curves.Value)
                {
                    for (var segment = 0; segment < curve.Length - 1; segment++)
                    {
                        outputBonds[bondIndex] = new BondPair(pointIndex, pointIndex + 2);
                        outputBonds[bondIndex + 1] = new BondPair(pointIndex, pointIndex + 3);
                        outputBonds[bondIndex + 2] = new BondPair(pointIndex + 1, pointIndex + 2);
                        outputBonds[bondIndex + 3] = new BondPair(pointIndex + 1, pointIndex + 3);

                        outputFaces[faceIndex] = new[]
                        {
                            pointIndex, pointIndex + 1, pointIndex + 2
                        };
                        outputFaces[faceIndex + 1] = new[]
                        {
                            pointIndex, pointIndex + 1, pointIndex + 3
                        };
                        outputFaces[faceIndex + 2] = new[]
                        {
                            pointIndex, pointIndex + 2, pointIndex + 3
                        };
                        outputFaces[faceIndex + 3] = new[]
                        {
                            pointIndex + 1, pointIndex + 2, pointIndex + 3
                        };

                        bondIndex += 4;
                        faceIndex += 4;
                        pointIndex += 2;
                    }

                    pointIndex += 2;
                }

                this.outputBonds.Value = outputBonds;
                this.outputFaces.Value = outputFaces;
            }

            var outputPositions = this.outputPositions.Value;
            var outputColors = this.outputColors.Value;
            var curvePoints = curves.Value;

            var index = 0;
            var curveI = 0;
            foreach (var curve in curvePoints)
            {
                for (var vertexIndex = 0; vertexIndex < curve.Length; vertexIndex++)
                {
                    var vertex = curvePoints[curveI][vertexIndex];
                    var binormal = Vector3.Cross(vertex.Tangent, vertex.Normal);
                    var color = (curveColors.HasValue
                                     ? curveColors.Value[curveI][vertexIndex]
                                     : this.color.Value);
                    var width = (curveWidths.HasValue
                                     ? curveWidths.Value[curveI][vertexIndex]
                                     : this.width.Value);

                    outputPositions[index * 2] = vertex.Position + 0.5f * width * binormal;
                    outputPositions[index * 2 + 1] = vertex.Position - 0.5f * width * binormal;
                    outputColors[index * 2] = color;
                    outputColors[index * 2 + 1] = color;
                    index++;
                }

                curveI++;
            }

            this.outputPositions.Value = outputPositions;
            this.outputColors.Value = outputColors;
        }

        /// <inheritdoc cref="GenericOutputNode.ClearOutput" />
        protected override void ClearOutput()
        {
            outputBonds.UndefineValue();
            outputColors.UndefineValue();
            outputFaces.UndefineValue();
            outputPositions.UndefineValue();
        }
    }
}