﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Spline
{
    /// <summary>
    /// From an array of data and a list of selections of subsets of this data, give a 2D dimensional
    /// array of the input for each subset.
    /// </summary>
    /// <typeparam name="TType"></typeparam>
    [Serializable]
    public class CalculatePerSelectionValue<TType> : GenericOutputNode
    {
        [SerializeField]
        private SerializableProperty<TType[]> input = new ArrayProperty<TType>();

        public IProperty<TType[]> Input => input;

        private ArrayProperty<TType[]> output = new ArrayProperty<TType[]>();

        public IReadOnlyProperty<TType[][]> Output => output;

        [SerializeField]
        private SerializableProperty<IReadOnlyList<int>[]> selections = new ArrayProperty<IReadOnlyList<int>>();

        public IProperty<IReadOnlyList<int>[]> Selections => selections;

        protected override bool IsInputValid => input.HasNonNullValue()
                                             && selections.HasNonNullValue();

        protected override bool IsInputDirty => input.IsDirty
                                             || selections.IsDirty;

        protected override void ClearDirty()
        {
            input.IsDirty = false;
            selections.IsDirty = false;
        }

        protected override void UpdateOutput()
        {
            var count = selections.Value.Sum(sel => sel.Count);
            output.Resize(count);
            var i = 0;
            foreach (var selection in selections.Value)
            {
                var arr = new TType[selection.Count];
                var j = 0;
                foreach (var index in selection)
                {
                    arr[j++] = input.Value[index];
                }

                output.Value[i++] = arr;
            }

            output.MarkValueAsChanged();
        }

        protected override void ClearOutput()
        {
            output.UndefineValue();
        }
    }
}