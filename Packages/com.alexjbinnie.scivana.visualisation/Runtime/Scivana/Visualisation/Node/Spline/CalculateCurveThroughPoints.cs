﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Spline
{
    /// <summary>
    /// Calculates normals and tangents for splines going through a set of points,
    /// assuming each segment is a hermite spline.
    /// </summary>
    [Serializable]
    public class CalculateCurveThroughPoints : GenericOutputNode
    {
        /// <summary>
        /// Positions to sample curve vertex positions.
        /// </summary>
        [SerializeField]
        private ArrayProperty<Vector3> positions = new ArrayProperty<Vector3>();

        public IProperty<Vector3[]> Positions => positions;

        /// <summary>
        /// Groups of indices of the Positions array which form sequences.
        /// </summary>
        [SerializeField]
        private ArrayProperty<IReadOnlyList<int>> sequences = new ArrayProperty<IReadOnlyList<int>>();

        public IProperty<IReadOnlyList<int>[]> Sequences => sequences;

        private SerializableProperty<CurvePoint[][]> curves = new SerializableProperty<CurvePoint[][]>();

        public IReadOnlyProperty<CurvePoint[][]> Curves => curves;

        /// <summary>
        /// Factor which decides the weighting of the tangents.
        /// </summary>
        [SerializeField]
        private SerializableProperty<float> shape = new SerializableProperty<float>
        {
            Value = 0.5f
        };

        /// <summary>
        /// Calculate the normals and tangents for a certain sequence.
        /// </summary>
        private static CurvePoint[] CalculateCurve(Vector3[] positions,
                                                   float shape)
        {
            var length = positions.Length;
            var curve = new CurvePoint[length];

            var tangents = new Vector3[length];
            var normals = new Vector3[length];

            // Calculate tangents based offsets between positions.
            for (var j = 1; j < positions.Length - 1; j++)
                tangents[j] = shape * (positions[j + 1] - positions[j - 1]);

            // Initial and final tangents are simply the tangents of the second and second from last tangents
            tangents[0] = tangents[1];
            tangents[length - 1] = tangents[length - 2];

            // Compute normals by rejection of second derivative of curve from tangent
            for (var i = 0; i < length - 1; i++)
            {
                var p1 = positions[i];
                var p2 = positions[i + 1];

                var m1 = tangents[i];
                var m2 = tangents[i + 1];

                var n0 = -6 * p1 - 4 * m1 + 6 * p2 - 2 * m2;
                var n1 = 6 * p1 + 2 * m1 - 6 * p2 + 4 * m2;

                n0 -= Vector3.Project(n0, m1);
                n1 -= Vector3.Project(n1, m2);

                normals[i] += n0;
                normals[i + 1] += n1;
            }

            // Normalize all normals
            for (var i = 0; i < length; i++)
            {
                normals[i] = normals[i].normalized;
            }

            // Set the first normal
            normals[0] = (2 * normals[1] - normals[2]).normalized;


            for (var i = 0; i < length; i++)
            {
                curve[i] = new CurvePoint
                {
                    Position = positions[i],
                    Normal = normals[i],
                    Tangent = tangents[i]
                };
            }

            return curve;
        }

        /// <inheritdoc cref="GenericOutputNode.IsInputValid" />
        protected override bool IsInputValid => sequences.HasNonNullValue()
                                             && positions.HasNonNullValue()
                                             && shape.HasNonNullValue();

        /// <inheritdoc cref="GenericOutputNode.IsInputDirty" />
        protected override bool IsInputDirty => sequences.IsDirty
                                             || positions.IsDirty
                                             || shape.IsDirty;

        /// <inheritdoc cref="GenericOutputNode.ClearDirty" />
        protected override void ClearDirty()
        {
            sequences.IsDirty = false;
            positions.IsDirty = false;
            shape.IsDirty = false;
        }

        /// <inheritdoc cref="GenericOutputNode.UpdateOutput" />
        protected override void UpdateOutput()
        {
            if (sequences.IsDirty)
            {
                curves.Value = new CurvePoint[sequences.Value.Length][];
                sequences.IsDirty = false;
                positions.IsDirty = true;
            }

            if (positions.IsDirty || shape.IsDirty)
            {
                var i = 0;
                foreach (var sequence in sequences.Value)
                {
                    curves.Value[i++] = CalculateCurve(sequence.Select(j => positions.Value[j]).ToArray(),
                                                       shape);
                }

                curves.MarkValueAsChanged();
            }
        }

        /// <inheritdoc cref="GenericOutputNode.ClearOutput" />
        protected override void ClearOutput()
        {
            curves.UndefineValue();
        }
    }
}