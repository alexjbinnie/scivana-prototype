/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Spline
{
    /// <summary>
    /// Calculate spline segments for every bonds, curved based on the bonded structure.
    /// </summary>
    [Serializable]
    public class CalculateCurvedBonds : GenericOutputNode
    {
        [SerializeField]
        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IProperty<Vector3[]> ParticlePositions => particlePositions;

        [SerializeField]
        private ArrayProperty<UnityEngine.Color> particleColors = new ArrayProperty<UnityEngine.Color>();

        [InputProperty(StandardProperties.ParticleColors)]
        public IProperty<UnityEngine.Color[]> ParticleColors => particleColors;

        [SerializeField]
        private ArrayProperty<float> particleScales = new ArrayProperty<float>();

        [InputProperty(StandardProperties.ParticleScales)]
        public IProperty<float[]> ParticleScales => particleScales;

        [SerializeField]
        private ArrayProperty<BondPair> bondPairs = new ArrayProperty<BondPair>();

        [InputProperty(StandardProperties.BondPairs)]
        public IProperty<BondPair[]> BondPairs => bondPairs;

        private ArrayProperty<SplineSegment> splineSegment = new ArrayProperty<SplineSegment>();

        [InputProperty(StandardProperties.SplineSegments)]
        public IProperty<SplineSegment[]> SplineSegments => splineSegment;

        protected override bool IsInputValid => particlePositions.HasValue
                                             && bondPairs.HasValue;

        protected override bool IsInputDirty => particlePositions.IsDirty
                                             || bondPairs.IsDirty
                                             || particleColors.IsDirty
                                             || particleScales.IsDirty;

        private Vector3[] offsetArray = new Vector3[0];
        private int[] offsetCount = new int[0];

        protected override void ClearDirty()
        {
            particlePositions.IsDirty = false;
            bondPairs.IsDirty = false;
            particleColors.IsDirty = false;
            particleScales.IsDirty = false;
        }

        protected override void UpdateOutput()
        {
            var particlePositions = this.particlePositions.Value;

            if (bondPairs.IsDirty)
            {
                Array.Resize(ref offsetArray, particlePositions.Length);
                Array.Resize(ref offsetCount, particlePositions.Length);
            }

            var bondcount = bondPairs.Value.Length;
            var particleCount = particlePositions.Length;

            for (var i = 0; i < particleCount; i++)
            {
                offsetArray[i] = Vector3.zero;
                offsetCount[i] = 0;
            }

            foreach (var bond in bondPairs)
            {
                var a = bond.A;
                var b = bond.B;
                var dir = particlePositions[b] - particlePositions[a];
                offsetArray[a] += dir;
                offsetArray[b] -= dir;
                offsetCount[a]++;
                offsetCount[b]++;
            }

            this.splineSegment.Resize(bondcount);
            var splineSegment = this.splineSegment.Value;

            var hasColors = this.particleColors.HasValue;
            var hasScales = this.particleScales.HasValue;

            var particleColors = hasColors ? this.particleColors.Value : null;
            var particleScales = hasScales ? this.particleScales.Value : null;

            var l = 0;
            foreach (var bond in bondPairs)
            {
                var offset1 = offsetArray[bond.A];
                var offset2 = offsetArray[bond.B];
                var offsetC1 = offsetCount[bond.A];
                var offsetC2 = offsetCount[bond.B];

                var pos1 = particlePositions[bond.A];
                var pos2 = particlePositions[bond.B];
                var dir = pos2 - pos1;

                var tan1 = dir;
                var tan2 = dir;

                if (offsetC1 > 1)
                {
                    var mag = offset1.magnitude / offsetC1;
                    tan1 -= Mathf.Clamp01(15f * mag) * Vector3.Project(tan1, offset1 / offsetC1);
                }

                if (offsetC2 > 1)
                {
                    var mag = offset2.magnitude / offsetC2;
                    tan2 -= Mathf.Clamp01(15f * mag) * Vector3.Project(tan2, offset2 / offsetC2);
                }

                var norm1 = Vector3.Cross(Vector3.up, tan1).normalized;
                var norm2 = Vector3.Cross(Vector3.up, tan2).normalized;

                var color1 = hasColors
                                 ? particleColors[bond.A]
                                 : UnityEngine.Color.white;

                var color2 = hasColors
                                 ? particleColors[bond.B]
                                 : UnityEngine.Color.white;

                var scale1 = hasScales
                                 ? particleScales[bond.A] * Vector3.one
                                 : Vector3.one;

                var scale2 = hasScales
                                 ? particleScales[bond.B] * Vector3.one
                                 : Vector3.one;

                splineSegment[l] = new SplineSegment
                {
                    StartPoint = pos1,
                    EndPoint = pos2,
                    StartTangent = tan1,
                    EndTangent = tan2,
                    StartNormal = norm1,
                    EndNormal = norm2,
                    StartColor = color1,
                    EndColor = color2,
                    StartScale = scale1,
                    EndScale = scale2
                };

                l++;
            }

            this.splineSegment.Value = splineSegment;
        }

        protected override void ClearOutput()
        {
            splineSegment.UndefineValue();
        }
    }
}