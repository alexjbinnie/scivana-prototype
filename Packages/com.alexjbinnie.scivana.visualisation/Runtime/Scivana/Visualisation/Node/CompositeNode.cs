﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node
{
    public abstract class CompositeNode : VisualisationNode, IDisposable
    {
        public abstract VisualisationNode[] Nodes { get; }

        public override void Refresh()
        {
            foreach (var node in Nodes)
                node.Refresh();
        }

        /// <inheritdoc />
        public override IEnumerable<(string Name, IProperty Property, PropertyAttributes Attributes)>
            GetInputProperties()
        {
            foreach (var a in base.GetInputProperties())
                yield return a;
            foreach (var node in Nodes)
            {
                foreach (var a in node.GetInputProperties())
                    yield return a;
            }
        }

        /// <inheritdoc />
        public override IEnumerable<(string Name, IReadOnlyProperty Property)> GetOutputProperties(
            bool includeDynamic = false)
        {
            foreach (var (name, prop) in base.GetOutputProperties(includeDynamic))
                yield return (name, prop);

            // Only get outputs from children if there isn't a dynamic property provider
            if (GetDynamicPropertyProvider() == null || includeDynamic)
            {
                foreach (var node in Nodes.Reverse())
                {
                    foreach (var (name, prop) in node.GetOutputProperties(includeDynamic))
                        yield return (name, prop);
                }
            }
        }

        public override IEnumerable<(CameraEvent, CommandBuffer)> GetCommandBuffers()
        {
            return Nodes.SelectMany(node => node.GetCommandBuffers());
        }

        public override Transform Transform
        {
            set
            {
                foreach (var node in Nodes)
                    node.Transform = value;
            }
        }

        public override IDynamicPropertyProvider GetDynamicPropertyProvider()
        {
            var nodes = Nodes;
            for (var i = nodes.Length - 1; i >= 0; i--)
            {
                if (nodes[i] is IDynamicPropertyProvider provider)
                    return provider;
            }

            return null;
        }

        public void Dispose()
        {
            foreach (var node in Nodes)
            {
                if (node is IDisposable disposable)
                    disposable.Dispose();
            }
        }
    }
}