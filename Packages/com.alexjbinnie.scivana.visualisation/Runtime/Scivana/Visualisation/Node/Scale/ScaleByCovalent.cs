/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Scale
{
    /// <summary>
    /// Visualiser node which scales each particle by its covalent radius
    /// </summary>
    [Serializable]
    [ScaleSubgraph("covalent")]
    public class ScaleByCovalent : ScaleByElement
    {
        /// <summary>
        /// Multiplier for each radius.
        /// </summary>
        [InputProperty(StandardProperties.Scale)]
        public IProperty<float> Scale => scale;

        [SerializeField]
        private SerializableProperty<float> scale = new SerializableProperty<float>
        {
            Value = 1f
        };

        /// <inheritdoc cref="ScaleByElement.ClearDirty" />
        protected override void ClearDirty()
        {
            base.ClearDirty();
            scale.IsDirty = false;
        }

        /// <inheritdoc cref="ScaleByElement.IsInputDirty" />
        protected override bool IsInputDirty => base.IsInputDirty || scale.IsDirty;

        /// <inheritdoc cref="ScaleByElement.IsInputValid" />
        protected override bool IsInputValid => base.IsInputValid && scale.HasNonNullValue();

        /// <summary>
        /// Get the scale of the provided atomic element.
        /// </summary>
        protected override float GetScale(Element element)
        {
            return (element.GetCovalentRadius() ?? 1f) * scale.Value;
        }
    }
}