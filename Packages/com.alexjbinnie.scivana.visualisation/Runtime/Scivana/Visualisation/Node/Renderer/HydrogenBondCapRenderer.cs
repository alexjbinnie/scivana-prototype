﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Calculator;
using Scivana.Visualisation.Property;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node.Renderer
{
    [Serializable]
    [RenderSubgraph("hydrogen cap")]
    public class HydrogenBondCapRenderer : CompositeNode
    {
        private ParticleRenderer acceptorRenderer = new ParticleRenderer();
        private ParticleRenderer donorRenderer = new ParticleRenderer();

        private SerializableProperty<float> acceptorScale = new SerializableProperty<float>()
        {
            Value = 0.25f
        };

        [InputProperty("acceptor.scale")]
        public IProperty<float> AcceptorScale => donorScale;

        private SerializableProperty<float> donorScale = new SerializableProperty<float>()
        {
            Value = 0.3f
        };

        [InputProperty("donor.scale")]
        public IProperty<float> DonorScale => donorScale;

        private SerializableProperty<UnityEngine.Color> acceptorColor = new SerializableProperty<UnityEngine.Color>()
        {
            Value = new UnityEngine.Color(1f, 0.35f, 0.35f)
        };

        [InputProperty("acceptor.color")]
        public IProperty<UnityEngine.Color> AcceptorColor => donorColor;

        private SerializableProperty<UnityEngine.Color> donorColor = new SerializableProperty<UnityEngine.Color>()
        {
            Value = new UnityEngine.Color(0.25f, 0.65f, 1f)
        };

        [InputProperty("donor.color")]
        public IProperty<UnityEngine.Color> DonorColor => donorColor;


        private SerializableProperty<Material> donorMaterial =
            new SerializableProperty<Material>();

        [DefaultPropertyValue("donor hydrogen cap")]
        public IProperty<Material> DonorMaterial => donorMaterial;

        private SerializableProperty<Material> acceptorMaterial = new SerializableProperty<Material>();

        [DefaultPropertyValue("acceptor hydrogen cap")]
        public IProperty<Material> AcceptorMaterial => acceptorMaterial;

        private SerializableProperty<Mesh> mesh =
            new SerializableProperty<Mesh>();

        [DefaultPropertyValue("cube")]
        public IProperty<Mesh> Mesh => mesh;

        private FindHydrogenBondDonors donorCalculator = new FindHydrogenBondDonors();

        private FindHydrogenBondAcceptors acceptorCalculator = new FindHydrogenBondAcceptors();

        public HydrogenBondCapRenderer()
        {
            Debug.Log("Start Cap Renderer");
            donorRenderer.RendererColor.Value = donorColor;
            donorRenderer.Material.Value = donorMaterial;
            donorRenderer.AddCustomBuffer("particle.rotations",
                                          "ROTATION_ARRAY",
                                          "RotationArray",
                                          donorCalculator.DonorRotations);
            donorRenderer.ParticlePositions.LinkedProperty = donorCalculator.DonorPositions;
            donorRenderer.RendererScale.Value = donorScale;

            acceptorRenderer.RendererColor.Value = acceptorColor;
            acceptorRenderer.Material.Value = acceptorMaterial;
            acceptorRenderer.AddCustomBuffer("particle.rotations",
                                             "ROTATION_ARRAY",
                                             "RotationArray",
                                             acceptorCalculator.AcceptorRotations);
            acceptorRenderer.ParticlePositions.LinkedProperty = acceptorCalculator.AcceptorPositions;
            acceptorRenderer.RendererScale.Value = acceptorScale;
        }

        public override VisualisationNode[] Nodes => new VisualisationNode[]
        {
            donorCalculator, acceptorCalculator, donorRenderer, acceptorRenderer
        };

        public Transform Transform
        {
            set
            {
                donorRenderer.Transform = value;
                acceptorRenderer.Transform = value;
            }
        }

        public IEnumerable<CommandBuffer> CommandBuffers
        {
            get
            {
                yield return donorRenderer.CommandBuffer;
                yield return acceptorRenderer.CommandBuffer;
            }
        }
    }
}