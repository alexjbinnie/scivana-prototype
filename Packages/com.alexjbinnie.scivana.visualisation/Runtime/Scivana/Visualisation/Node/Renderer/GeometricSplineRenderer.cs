﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Renderer.Primitives;
using Scivana.Visualisation.Node.Spline;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Renderer
{
    [Serializable]
    [RenderSubgraph("tetrahedral spline", "geometric spline")]
    public class GeometricSplineRenderer : CompositeNode
    {
        private CalculateCurveThroughPoints calculateCurve = new CalculateCurveThroughPoints();

        private CalculatePerSelectionValue<UnityEngine.Color> calculateCurveColors =
            new CalculatePerSelectionValue<UnityEngine.Color>();

        private CalculatePerSelectionValue<float> calculateCurveWidths =
            new CalculatePerSelectionValue<float>();

        private CalculateTetrahedralSplineFromCurve calculateSpline = new CalculateTetrahedralSplineFromCurve();

        private ParticleRenderer sphereRenderer = new ParticleRenderer();

        private BondRenderer bondRenderer = new BondRenderer();

        private CycleRendererNode cyclesRenderer = new CycleRendererNode();


        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        private ArrayProperty<IReadOnlyList<int>> sequences = new ArrayProperty<IReadOnlyList<int>>();

        private ArrayProperty<UnityEngine.Color> colors = new ArrayProperty<UnityEngine.Color>();

        private ArrayProperty<float> scales = new ArrayProperty<float>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IReadOnlyProperty<Vector3[]> ParticlePositions => particlePositions;

        [InputProperty(StandardProperties.SequenceParticles)]
        public IReadOnlyProperty<IReadOnlyList<int>[]> Sequences => sequences;

        [InputProperty(StandardProperties.ParticleColors)]
        public IReadOnlyProperty<UnityEngine.Color[]> ParticleColors => colors;

        [InputProperty(StandardProperties.ParticleWidths)]
        public IReadOnlyProperty<float[]> ParticleWidths => scales;

        private SerializableProperty<float> scale = new SerializableProperty<float>
        {
            Value = 0.1f
        };

        private SerializableProperty<float> width = new SerializableProperty<float>
        {
            Value = 0.1f
        };

        [InputProperty(StandardProperties.Scale)]
        public IReadOnlyProperty<float> Scale => scale;

        [InputProperty(StandardProperties.Width)]
        public IReadOnlyProperty<float> Width => width;

        public GeometricSplineRenderer()
        {
            sphereRenderer.RendererScale.LinkedProperty = scale;
            bondRenderer.BondScale.LinkedProperty = scale;
            bondRenderer.ParticleScale.LinkedProperty = scale;
            cyclesRenderer.Scale.LinkedProperty = scale;

            calculateSpline.Width.LinkedProperty = width;

            calculateCurve.Positions.LinkedProperty = particlePositions;
            calculateCurve.Sequences.LinkedProperty = sequences;

            calculateCurveColors.Input.LinkedProperty = colors;
            calculateCurveColors.Selections.LinkedProperty = sequences;

            calculateCurveWidths.Input.LinkedProperty = scales;
            calculateCurveWidths.Selections.LinkedProperty = sequences;

            calculateSpline.Curves.LinkedProperty = calculateCurve.Curves;
            calculateSpline.CurveColors.LinkedProperty = calculateCurveColors.Output;
            calculateSpline.CurveWidths.LinkedProperty = calculateCurveWidths.Output;

            sphereRenderer.ParticlePositions.LinkedProperty = calculateSpline.OutputPositions;
            sphereRenderer.ParticleColors.LinkedProperty = calculateSpline.OutputColors;

            bondRenderer.ParticlePositions.LinkedProperty = calculateSpline.OutputPositions;
            bondRenderer.ParticleColors.LinkedProperty = calculateSpline.OutputColors;
            bondRenderer.BondPairs.LinkedProperty = calculateSpline.OutputBonds;

            cyclesRenderer.ParticlePositions.LinkedProperty = calculateSpline.OutputPositions;
            cyclesRenderer.Cycles.LinkedProperty = calculateSpline.OutputFaces;
            cyclesRenderer.ParticleColors.LinkedProperty = calculateSpline.OutputColors;

            bondRenderer.EdgeSharpness.Value = 0;
        }

        public override VisualisationNode[] Nodes => new VisualisationNode[]
        {
            calculateCurve, calculateCurveColors, calculateCurveWidths,
            calculateSpline, sphereRenderer, bondRenderer, cyclesRenderer
        };

        public override Transform Transform
        {
            set
            {
                sphereRenderer.Transform = value;
                bondRenderer.Transform = value;
                cyclesRenderer.Transform = value;
            }
        }
    }
}