﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Protein;
using Scivana.Visualisation.Node.Renderer.Primitives;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Renderer
{
    [Serializable]
    [RenderSubgraph("peptide planes")]
    public class PeptidePlaneRenderer : CompositeNode
    {
        private CalculatePeptidePlane calculateIndices = new CalculatePeptidePlane();

        private CalculateSolidPeptideBlocks calculateBlocks = new CalculateSolidPeptideBlocks();

        private ParticleRenderer sphereRenderer = new ParticleRenderer();

        private BondRenderer bondRenderer = new BondRenderer();

        private CycleRendererNode cyclesRenderer = new CycleRendererNode();

        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        private ArrayProperty<UnityEngine.Color> colors = new ArrayProperty<UnityEngine.Color>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IReadOnlyProperty<Vector3[]> ParticlePositions => particlePositions;

        [InputProperty(StandardProperties.ParticleColors)]
        public IReadOnlyProperty<UnityEngine.Color[]> ParticleColors => colors;

        private SerializableProperty<float> scale = new SerializableProperty<float>
        {
            Value = 0.1f
        };

        [InputProperty(StandardProperties.Scale)]
        public IReadOnlyProperty<float> Scale => scale;

        public PeptidePlaneRenderer()
        {
            sphereRenderer.RendererScale.LinkedProperty = scale;
            bondRenderer.BondScale.LinkedProperty = scale;
            bondRenderer.ParticleScale.LinkedProperty = scale;
            cyclesRenderer.Scale.LinkedProperty = scale;

            calculateBlocks.ResidueProteinIndices.LinkedProperty =
                calculateIndices.ResidueProteinIndices;

            sphereRenderer.ParticlePositions.LinkedProperty = calculateBlocks.OutputPositions;
            sphereRenderer.ParticleColors.LinkedProperty = calculateBlocks.OutputColors;

            bondRenderer.ParticlePositions.LinkedProperty = calculateBlocks.OutputPositions;
            bondRenderer.ParticleColors.LinkedProperty = calculateBlocks.OutputColors;
            bondRenderer.BondPairs.LinkedProperty = calculateBlocks.OutputBonds;

            cyclesRenderer.ParticlePositions.LinkedProperty = calculateBlocks.OutputPositions;
            cyclesRenderer.Cycles.LinkedProperty = calculateBlocks.OutputFaces;
            cyclesRenderer.ParticleColors.LinkedProperty = calculateBlocks.OutputColors;

            bondRenderer.EdgeSharpness.Value = 0;
        }

        public override VisualisationNode[] Nodes => new VisualisationNode[]
        {
            calculateIndices, calculateBlocks, sphereRenderer, bondRenderer, cyclesRenderer
        };

        public override Transform Transform
        {
            set
            {
                sphereRenderer.Transform = value;
                bondRenderer.Transform = value;
                cyclesRenderer.Transform = value;
            }
        }
    }
}