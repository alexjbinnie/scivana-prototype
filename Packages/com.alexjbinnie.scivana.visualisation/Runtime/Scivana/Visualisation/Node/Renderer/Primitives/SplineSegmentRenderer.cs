/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Spline;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using Scivana.Visualisation.Utility;
using UnityEngine;

namespace Scivana.Visualisation.Node.Renderer.Primitives
{
    [Serializable]
    public class SplineSegmentRenderer : IndirectMeshRenderer, IDisposable
    {
        [SerializeField]
        private bool useBox = false;

        [SerializeField]
        private ArrayProperty<SplineSegment> splineSegments = new ArrayProperty<SplineSegment>();

        [InputProperty(StandardProperties.SplineSegments)]
        public IProperty<SplineSegment[]> SplineSegments => splineSegments;

        [SerializeField]
        private SerializableProperty<UnityEngine.Color> rendererColor
            = new SerializableProperty<UnityEngine.Color>
            {
                Value = UnityEngine.Color.white
            };

        [InputProperty(StandardProperties.Color)]
        public IProperty<UnityEngine.Color> RendererColor => rendererColor;

        [SerializeField]
        private int segments = 32;

        [SerializeField]
        private int sides = 32;

        [SerializeField]
        private SerializableProperty<Material> material = new SerializableProperty<Material>();

        [InputProperty(StandardProperties.Material)]
        [DefaultPropertyValue("hermite spline")]
        public IProperty<Material> Material => material;

        private Mesh mesh;

        [SerializeField]
        private SerializableProperty<float> splineRadius = new SerializableProperty<float>
        {
            Value = 1f
        };

        [InputProperty(StandardProperties.Scale)]
        public IProperty<float> Scale => splineRadius;

        public override bool ShouldRender => splineSegments.HasNonEmptyValue()
                                          && material.HasNonNullValue();

        public override bool IsInputDirty => splineSegments.IsDirty
                                          || material.IsDirty;

        public override bool DoesCommandBufferNeedRefresh => material.IsDirty;

        protected override void UpdateInput()
        {
            if (mesh == null)
                mesh = GenerateMesh();

            var count = splineSegments.Value.Length;

            DrawCommand.SetInstanceCount(count);

            if (material.IsDirty)
                DrawCommand.SetMaterial(material);

            DrawCommand.SetMesh(mesh);
            InstancingUtility.SetTransform(DrawCommand, Transform);

            DrawCommand.SetDataBuffer("SplineArray", splineSegments.Value);
            DrawCommand.SetColor(
                "_Color",
                rendererColor.HasValue ? rendererColor.Value : UnityEngine.Color.white);
            DrawCommand.SetFloat("_Radius", splineRadius.HasValue ? splineRadius.Value : 1f);

            splineSegments.IsDirty = false;
            material.IsDirty = false;
        }

        private Mesh GenerateMesh()
        {
            var vertices = new List<Vector3>();
            var normals = new List<Vector3>();
            var triangles = new List<int>();

            if (useBox)
                GenerateBox(vertices, normals, triangles);
            else
                GenerateCylinder(vertices, normals, triangles);


            var mesh = new Mesh();
            mesh.SetVertices(vertices);
            mesh.SetNormals(normals);
            mesh.SetTriangles(triangles, 0);

            return mesh;
        }

        private void GenerateBox(List<Vector3> vertices, List<Vector3> normals, List<int> triangles)
        {
            var height = 1f;
            var radius = 0.5f;

            var i = 0;
            for (var level = 0; level <= segments; level++)
            {
                var z = height * level / segments;
                normals.Add(-Vector3.forward);
                normals.Add(-Vector3.forward);
                normals.Add(Vector3.right);
                normals.Add(Vector3.right);
                normals.Add(Vector3.forward);
                normals.Add(Vector3.forward);
                normals.Add(-Vector3.right);
                normals.Add(-Vector3.right);

                vertices.Add(new Vector3(-0.5f, z, -0.5f));
                vertices.Add(new Vector3(0.5f, z, -0.5f));
                vertices.Add(new Vector3(0.5f, z, -0.5f));
                vertices.Add(new Vector3(0.5f, z, 0.5f));
                vertices.Add(new Vector3(0.5f, z, 0.5f));
                vertices.Add(new Vector3(-0.5f, z, 0.5f));
                vertices.Add(new Vector3(-0.5f, z, 0.5f));
                vertices.Add(new Vector3(-0.5f, z, -0.5f));
            }

            triangles.AddRange(new[]
            {
                0, 2, 6
            });
            triangles.AddRange(new[]
            {
                2, 4, 6
            });

            // Bottom

            for (var l = 0; l < segments; l++)
            {
                var lvl = l * 8;
                var lvl2 = l * 8 + 8;
                triangles.AddRange(new[]
                {
                    lvl + 0, lvl2 + 0, lvl + 1
                });
                triangles.AddRange(new[]
                {
                    lvl + 1, lvl2 + 0, lvl2 + 1
                });

                triangles.AddRange(new[]
                {
                    lvl + 2, lvl2 + 2, lvl + 3
                });
                triangles.AddRange(new[]
                {
                    lvl + 3, lvl2 + 2, lvl2 + 3
                });

                triangles.AddRange(new[]
                {
                    lvl + 4, lvl2 + 4, lvl + 5
                });
                triangles.AddRange(new[]
                {
                    lvl + 5, lvl2 + 4, lvl2 + 5
                });

                triangles.AddRange(new[]
                {
                    lvl + 6, lvl2 + 6, lvl + 7
                });
                triangles.AddRange(new[]
                {
                    lvl + 7, lvl2 + 6, lvl2 + 7
                });
            }
        }

        private void GenerateCylinder(List<Vector3> vertices,
                                      List<Vector3> normals,
                                      List<int> triangles)
        {
            var height = 1f;
            var radius = 0.5f;

            var i = 0;
            for (var level = 0; level <= segments; level++)
            {
                var z = height * level / segments;
                for (var side = 0; side < sides; side++)
                {
                    var ang = side / (float) sides * Mathf.PI * 2f;
                    var normal = new Vector3(Mathf.Sin(ang), 0, Mathf.Cos(ang));
                    normals.Add(normal);
                    vertices.Add(radius * normal + Vector3.up * z);
                    i++;
                }
            }

            i = 0;

            for (var l = 0; l < segments; l++)
            for (var s = 0; s < sides; s++)
            {
                triangles.AddRange(new[]
                {
                    l * sides + (s) % sides, l * sides + (s + 1) % sides,
                    (l + 1) * sides + (s) % sides
                });
                triangles.AddRange(new[]
                {
                    l * sides + (s + 1) % sides, (l + 1) * sides + (s + 1) % sides,
                    (l + 1) * sides + (s) % sides
                });
            }
        }
    }
}