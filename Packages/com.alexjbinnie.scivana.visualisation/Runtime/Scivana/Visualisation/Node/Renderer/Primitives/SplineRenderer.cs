﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Spline;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Renderer.Primitives
{
    [RenderSubgraph("spline")]
    public class SplineRenderer : CompositeNode
    {
        private CalculateCurveThroughPoints calculateCurve = new CalculateCurveThroughPoints();
        private OrientCurveNormals orientNormals = new OrientCurveNormals();

        private CalculatePerSelectionValue<UnityEngine.Color> calculateCurveColors =
            new CalculatePerSelectionValue<UnityEngine.Color>();

        private CalculatePerSelectionValue<float> calculateCurveScales =
            new CalculatePerSelectionValue<float>();

        private CalculateSplineFromCurve calculateSpline = new CalculateSplineFromCurve();
        private ParticleRenderer sphereRenderer = new ParticleRenderer();
        private SplineSegmentRenderer splineRenderer = new SplineSegmentRenderer();

        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        private ArrayProperty<IReadOnlyList<int>> sequences =
            new ArrayProperty<IReadOnlyList<int>>();

        private ArrayProperty<UnityEngine.Color> colors = new ArrayProperty<UnityEngine.Color>();

        private ArrayProperty<float> scales = new ArrayProperty<float>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IReadOnlyProperty<Vector3[]> ParticlePositions => particlePositions;

        [InputProperty(StandardProperties.SequenceParticles)]
        public IReadOnlyProperty<IReadOnlyList<int>[]> Sequences => sequences;

        [InputProperty(StandardProperties.ParticleColors)]
        public IReadOnlyProperty<UnityEngine.Color[]> ParticleColors => colors;

        [InputProperty(StandardProperties.ParticleScales)]
        public IReadOnlyProperty<float[]> ParticleScales => scales;

        private SerializableProperty<float> scale = new SerializableProperty<float>
        {
            Value = 0.1f
        };

        [InputProperty(StandardProperties.Scale)]
        public IProperty<float> Scale => scale;

        public SplineRenderer()
        {
            splineRenderer.Scale.LinkedProperty = scale;
            sphereRenderer.RendererScale.LinkedProperty = scale;

            calculateCurve.Positions.LinkedProperty = particlePositions;
            calculateCurve.Sequences.LinkedProperty = sequences;

            orientNormals.InputCurves.LinkedProperty = calculateCurve.Curves;

            calculateCurveColors.Input.LinkedProperty = colors;
            calculateCurveColors.Selections.LinkedProperty = sequences;

            calculateCurveScales.Input.LinkedProperty = scales;
            calculateCurveScales.Selections.LinkedProperty = sequences;

            calculateSpline.Curves.LinkedProperty = orientNormals.OutputCurves;
            calculateSpline.CurveColors.LinkedProperty = calculateCurveColors.Output;
            calculateSpline.CurveScales.LinkedProperty = calculateCurveScales.Output;

            splineRenderer.SplineSegments.LinkedProperty = calculateSpline.SplineSegments;
        }

        public override VisualisationNode[] Nodes => new VisualisationNode[]
        {
            calculateCurve, orientNormals, calculateCurveColors, calculateCurveScales,
            calculateSpline, sphereRenderer, splineRenderer
        };

        public override Transform Transform
        {
            set
            {
                sphereRenderer.Transform = value;
                splineRenderer.Transform = value;
            }
        }
    }
}