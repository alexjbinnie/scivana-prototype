/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using Scivana.Visualisation.Utility;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node.Renderer
{
    /// <summary>
    /// Visualisation node for rendering particles using spheres.
    /// </summary>
    /// <remarks>
    /// A call to <see cref="IndirectMeshRenderer.Refresh" /> should be made
    /// every frame. Depending
    /// on use case, either call <see cref="IndirectMeshRenderer.Render" /> to draw for
    /// a single frame, or
    /// add to a <see cref="CommandBuffer" /> using
    /// <see cref="IndirectMeshRenderer.AppendToCommandBuffer" />.
    /// </remarks>
    [Serializable]
    public class ParticleRenderer : IndirectMeshRenderer
    {
        /// <summary>
        /// Positions of particles.
        /// </summary>
        [InputProperty(StandardProperties.ParticlePositions)]
        public IProperty<Vector3[]> ParticlePositions => particlePositions;

        /// <summary>
        /// Color of particles.
        /// </summary>
        [InputProperty(StandardProperties.ParticleColors)]
        public IProperty<UnityEngine.Color[]> ParticleColors => particleColors;

        /// <summary>
        /// Scale of particles.
        /// </summary>
        [InputProperty(StandardProperties.ParticleScales)]
        public IProperty<float[]> ParticleScales => particleScales;

        /// <summary>
        /// Overall color of the renderer. Each particle color will be multiplied by this
        /// value.
        /// </summary>
        [InputProperty(StandardProperties.Color, Local = true)]
        public IProperty<UnityEngine.Color> RendererColor => rendererColor;

        /// <summary>
        /// Overall scaling of the renderer. Each particle scale will be multiplied by this
        /// value.
        /// </summary>
        [InputProperty(StandardProperties.Scale, Local = true)]
        public IProperty<float> RendererScale => rendererScale;

        [InputProperty(StandardProperties.Mesh, Local = true)]
        [DefaultPropertyValue("cube")]
        public IProperty<Mesh> Mesh => mesh;

        [InputProperty(StandardProperties.Material, Local = true)]
        [DefaultPropertyValue("raycast sphere")]
        public IProperty<Material> Material => material;

#pragma warning disable 0649
        [SerializeField]
        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        [SerializeField]
        private ArrayProperty<UnityEngine.Color> particleColors
            = new ArrayProperty<UnityEngine.Color>();

        [SerializeField]
        private ArrayProperty<float> particleScales = new ArrayProperty<float>();

        [SerializeField]
        private SerializableProperty<UnityEngine.Color> rendererColor
            = new SerializableProperty<UnityEngine.Color>
            {
                Value = UnityEngine.Color.white
            };

        [SerializeField]
        private SerializableProperty<float> rendererScale = new SerializableProperty<float>
        {
            Value = 0.1f
        };

        [SerializeField]
        private SerializableProperty<Mesh> mesh = new SerializableProperty<Mesh>();

        [SerializeField]
        private SerializableProperty<Material> material = new SerializableProperty<Material>();
#pragma warning restore 0649

        public override bool ShouldRender => mesh.HasNonNullValue()
                                          && material.HasNonNullValue()
                                          && particlePositions.HasNonEmptyValue()
                                          && rendererColor.HasValue
                                          && rendererScale.HasValue
                                          && Transform != null
                                          && InstanceCount > 0;

        private int InstanceCount => particlePositions.Value.Length;

        public override bool IsInputDirty => mesh.IsDirty
                                          || material.IsDirty
                                          || rendererColor.IsDirty
                                          || rendererScale.IsDirty
                                          || particlePositions.IsDirty
                                          || particleColors.IsDirty
                                          || particleScales.IsDirty;

        public override bool DoesCommandBufferNeedRefresh => mesh.IsDirty
                                                          || material.IsDirty;


        protected override void UpdateInput()
        {
            UpdateMeshAndMaterials();

            DrawCommand.SetInstanceCount(InstanceCount);
            DrawCommand.SetFloat("_Scale", rendererScale.Value);
            DrawCommand.SetColor("_Color", rendererColor.Value);
            rendererScale.IsDirty = false;
            rendererColor.IsDirty = false;

            UpdateBuffers();
        }

        protected override void UpdateBuffers()
        {
            base.UpdateBuffers();
            UpdatePositionsIfDirty();
            UpdateColorsIfDirty();
            UpdateScalesIfDirty();
        }

        private void UpdatePositionsIfDirty()
        {
            if ((particlePositions.IsDirty) && particlePositions.HasNonEmptyValue())
            {
                InstancingUtility.SetPositions(DrawCommand, particlePositions.Value);

                particlePositions.IsDirty = false;
            }
        }

        private void UpdateColorsIfDirty()
        {
            if ((particleColors.IsDirty) && particleColors.HasNonEmptyValue())
            {
                InstancingUtility.SetColors(DrawCommand, particleColors.Value);
                particleColors.IsDirty = false;
            }
        }

        private void UpdateScalesIfDirty()
        {
            if ((particleScales.IsDirty) && particleScales.HasNonEmptyValue())
            {
                InstancingUtility.SetScales(DrawCommand, particleScales.Value);

                particleScales.IsDirty = false;
            }
        }

        private void UpdateMeshAndMaterials()
        {
            DrawCommand.SetMesh(mesh);
            DrawCommand.SetMaterial(material);
            mesh.IsDirty = false;
            material.IsDirty = false;
        }
    }
}