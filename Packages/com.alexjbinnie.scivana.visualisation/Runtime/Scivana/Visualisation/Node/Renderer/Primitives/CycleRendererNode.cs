/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node.Renderer
{
    [Serializable]
    public class CycleRendererNode : RendererNode
    {
        private Mesh mesh;

        [SerializeField]
        private ArrayProperty<IReadOnlyList<int>> cycles = new ArrayProperty<IReadOnlyList<int>>();

        [InputProperty(StandardProperties.Cycles)]
        public IProperty<IReadOnlyList<int>[]> Cycles => cycles;

        [SerializeField]
        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IProperty<Vector3[]> ParticlePositions => particlePositions;

        [SerializeField]
        private ArrayProperty<UnityEngine.Color> particleColors = new ArrayProperty<UnityEngine.Color>();

        [InputProperty(StandardProperties.ParticleColors)]
        public IProperty<UnityEngine.Color[]> ParticleColors => particleColors;

        [SerializeField]
        private SerializableProperty<float> offset = new SerializableProperty<float>();

        [InputProperty(StandardProperties.Scale, Local = true)]
        public IProperty<float> Scale => offset;

        [SerializeField]
        private SerializableProperty<UnityEngine.Color> color
            = new SerializableProperty<UnityEngine.Color>();

        [InputProperty(StandardProperties.Color, Local = true)]
        public IProperty<UnityEngine.Color> Color => color;

        [SerializeField]
        private SerializableProperty<Material> material = new SerializableProperty<Material>();

        [InputProperty(StandardProperties.Material, Local = true)]
        [DefaultPropertyValue("cycles")]
        public IProperty<Material> Material => material;

        protected override void AddToCommandBuffer(CommandBuffer buffer)
        {
            buffer.DrawMesh(mesh, Transform.localToWorldMatrix, material, 0, 0, block);
            buffer.DrawMesh(mesh, Transform.localToWorldMatrix, material, 0, 1, block);
        }

        public override bool ShouldRender => cycles.HasNonEmptyValue()
                                          && particlePositions.HasNonEmptyValue();

        public override bool IsInputDirty => cycles.IsDirty
                                          || particlePositions.IsDirty
                                          || color.IsDirty
                                          || offset.IsDirty
                                          || material.IsDirty;

        public override bool DoesCommandBufferNeedRefresh => true;

        private Vector3[] vertices = new Vector3[0];
        private Vector3[] normals = new Vector3[0];
        private UnityEngine.Color[] colors = new UnityEngine.Color[0];
        private int[] triangles = new int[0];
        private int triCount = 0;

        private void CalculateTriangles()
        {
            triCount = 0;
            foreach (var cycle in cycles.Value)
            {
                for (var i = 0; i < cycle.Count - 2; i++)
                {
                    for (var j = i + 1; j < cycle.Count - 1; j++)
                    {
                        for (var k = j + 1; k < cycle.Count; k++)
                        {
                            triCount += 1;
                        }
                    }
                }
            }

            Array.Resize(ref vertices, triCount * 3);
            Array.Resize(ref normals, triCount * 3);
            Array.Resize(ref colors, triCount * 3);

            triangles = Enumerable.Range(0, triCount * 3).ToArray();
        }

        private void GenerateCycleMeshes()
        {
            if (mesh == null)
                mesh = new Mesh();

            if (cycles.IsDirty)
            {
                CalculateTriangles();
            }

            var positionArray = particlePositions.Value;

            var particleColorArray =
                particleColors.HasNonEmptyValue() ? particleColors.Value : null;

            var vertexIndex = 0;
            var normalIndex = 0;
            var colorIndex = 0;

            var ci = 0;
            foreach (var cycle in cycles.Value)
            {
                var cycleLength = cycle.Count;
                for (var i = 0; i < cycleLength - 2; i++)
                {
                    var color1 = particleColorArray?[cycle[i]] ?? UnityEngine.Color.white;

                    var pos1 = positionArray[cycle[i]];

                    for (var j = i + 1; j < cycleLength - 1; j++)
                    {
                        var color2 = particleColorArray?[cycle[j]] ?? UnityEngine.Color.white;

                        var pos2 = positionArray[cycle[j]];
                        var d1 = pos2 - pos1;
                        for (var k = j + 1; k < cycleLength; k++)
                        {
                            var color3 = particleColorArray?[cycle[k]] ?? UnityEngine.Color.white;

                            var pos3 = positionArray[cycle[k]];
                            var normal = Vector3.Cross(d1, pos3 - pos1).normalized;
                            vertices[vertexIndex++] = pos1;
                            vertices[vertexIndex++] = pos2;
                            vertices[vertexIndex++] = pos3;
                            normals[normalIndex++] = normal;
                            normals[normalIndex++] = normal;
                            normals[normalIndex++] = normal;
                            colors[colorIndex++] = color1;
                            colors[colorIndex++] = color2;
                            colors[colorIndex++] = color3;
                        }
                    }
                }

                ci++;
            }

            mesh.Clear();

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.colors = colors;
            mesh.triangles = triangles;
        }

        private MaterialPropertyBlock block;

        protected override void UpdateInput()
        {
            base.UpdateInput();

            if (cycles.HasNonEmptyValue() && particlePositions.HasNonEmptyValue())
            {
                GenerateCycleMeshes();
            }

            cycles.IsDirty = false;
            particlePositions.IsDirty = false;

            block = new MaterialPropertyBlock();
            if (offset.HasValue)
                block.SetFloat("_Offset", offset.Value / 2f);

            if (color.HasValue)
                block.SetColor("_Color", color.Value);
        }

        protected override void BeforeBufferUpdate()
        {
            base.BeforeBufferUpdate();
            if (mesh == null)
                mesh = new Mesh();
        }
    }
}