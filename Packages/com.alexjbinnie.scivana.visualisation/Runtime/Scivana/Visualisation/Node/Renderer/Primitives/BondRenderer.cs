/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using Scivana.Visualisation.Utility;
using UnityEngine;

namespace Scivana.Visualisation.Node.Renderer.Primitives
{
    /// <summary>
    /// Visualisation node for rendering bonds between particles.
    /// </summary>
    [Serializable]
    public class BondRenderer : IndirectMeshRenderer
    {
        #region Input Properties

        /// <summary>
        /// Set of bond pairs to render.
        /// </summary>
        [InputProperty(StandardProperties.BondPairs)]
        public IProperty<BondPair[]> BondPairs => bondPairs;

        /// <summary>
        /// Positions of particles which will be connected by bonds.
        /// </summary>
        [InputProperty(StandardProperties.ParticlePositions)]
        public IProperty<Vector3[]> ParticlePositions => particlePositions;

        /// <summary>
        /// Color of particles which will be connected by bonds.
        /// </summary>
        [InputProperty(StandardProperties.ParticleColors)]
        public IProperty<UnityEngine.Color[]> ParticleColors => particleColors;

        /// <summary>
        /// Scale of particles which will be connected by bonds.
        /// </summary>
        [InputProperty(StandardProperties.ParticleScales)]
        public IProperty<float[]> ParticleScales => particleScales;

        /// <summary>
        /// Overall color of the renderer. Each particle color will be multiplied by this
        /// value.
        /// </summary>
        [InputProperty(StandardProperties.Color, Local = true)]
        public IProperty<UnityEngine.Color> RendererColor => rendererColor;

        /// <summary>
        /// Scaling of the particles. Each particle scale will be multiplied by this
        /// value.
        /// </summary>
        [InputProperty(StandardProperties.Scale, Local = true)]
        public IProperty<float> ParticleScale => particleScale;

        [InputProperty(StandardProperties.Mesh, Local = true)]
        [DefaultPropertyValue("cube")]
        public IProperty<Mesh> Mesh => mesh;

        [InputProperty(StandardProperties.Material, Local = true)]
        [DefaultPropertyValue("raycast cylinder")]
        public virtual IProperty<Material> Material => material;

        /// <summary>
        /// Scale of the bonds.
        /// </summary>
        [InputProperty(StandardProperties.BondScale)]
        public IProperty<float> BondScale => bondScale;

        public IProperty<int[]> BondOrders => bondOrders;

        [SerializeField]
        private SerializableProperty<Material> material = new SerializableProperty<Material>();

        [SerializeField]
        private SerializableProperty<Mesh> mesh = new SerializableProperty<Mesh>();

        [SerializeField]
        private SerializableProperty<float> bondScale = new SerializableProperty<float>()
        {
            Value = 0.05f
        };

        [SerializeField]
        private ArrayProperty<BondPair> bondPairs = new ArrayProperty<BondPair>();

        [SerializeField]
        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        [SerializeField]
        private ArrayProperty<UnityEngine.Color> particleColors
            = new ArrayProperty<UnityEngine.Color>();

        [SerializeField]
        private ArrayProperty<float> particleScales = new ArrayProperty<float>();

        [SerializeField]
        private SerializableProperty<UnityEngine.Color> rendererColor
            = new SerializableProperty<UnityEngine.Color>()
            {
                Value = UnityEngine.Color.white
            };

        [SerializeField]
        private SerializableProperty<float> particleScale = new SerializableProperty<float>()
        {
            Value = 0.1f
        };

        [SerializeField]
        private SerializableProperty<float> edgeSharpness = new SerializableProperty<float>()
        {
            Value = 1f
        };

        public IProperty<float> EdgeSharpness => edgeSharpness;

        [SerializeField]
        private ArrayProperty<int> bondOrders = new ArrayProperty<int>();

        #endregion

        public override bool ShouldRender => mesh.HasNonNullValue()
                                          && material.HasNonNullValue()
                                          && bondPairs.HasNonEmptyValue()
                                          && particlePositions.HasNonEmptyValue()
                                          && rendererColor.HasValue
                                          && particleScale.HasValue
                                          && Transform != null;

        private int InstanceCount => bondPairs.Value.Length;

        public override bool IsInputDirty => mesh.IsDirty
                                          || material.IsDirty
                                          || rendererColor.IsDirty
                                          || bondScale.IsDirty
                                          || particlePositions.IsDirty
                                          || particleColors.IsDirty
                                          || particleScales.IsDirty
                                          || edgeSharpness.IsDirty;

        public override bool DoesCommandBufferNeedRefresh => mesh.IsDirty
                                                          || material.IsDirty;

        protected override void UpdateInput()
        {
            UpdateMeshAndMaterials();

            SetMaterialParameters();

            UpdateBuffers();

            DrawCommand.SetInstanceCount(InstanceCount);
        }

        protected virtual void SetMaterialParameters()
        {
            DrawCommand.SetFloat("_EdgeScale", bondScale.HasValue ? bondScale.Value : 1f);
            DrawCommand.SetFloat("_ParticleScale",
                                 particleScale.HasValue ? particleScale.Value : 1f);
            DrawCommand.SetFloat("_Scale", particleScale.HasValue ? particleScale.Value : 1f);
            DrawCommand.SetColor("_Color", rendererColor.Value);
            DrawCommand.SetFloat("_EdgeSharpness",
                                 edgeSharpness.HasValue ? edgeSharpness.Value : 0f);

            bondScale.IsDirty = false;
            particleScale.IsDirty = false;
            rendererColor.IsDirty = false;
        }

        protected void UpdateBuffers(bool forceDirty = false)
        {
            UpdatePositionsIfDirty(forceDirty);
            UpdateColorsIfDirty(forceDirty);
            UpdateScalesIfDirty(forceDirty);
            UpdateBondsIfDirty(forceDirty);
        }

        private void UpdatePositionsIfDirty(bool forceDirty = false)
        {
            if ((forceDirty || particlePositions.IsDirty) && particlePositions.HasNonEmptyValue())
            {
                InstancingUtility.SetPositions(DrawCommand, particlePositions.Value);

                particlePositions.IsDirty = false;
            }
        }

        private void UpdateColorsIfDirty(bool forceDirty = false)
        {
            if ((forceDirty || particleColors.IsDirty) && particleColors.HasNonEmptyValue())
            {
                InstancingUtility.SetColors(DrawCommand, particleColors.Value);

                particleColors.IsDirty = false;
            }
        }

        private void UpdateScalesIfDirty(bool forceDirty = false)
        {
            if ((forceDirty || particleScales.IsDirty) && particleScales.HasNonEmptyValue())
            {
                InstancingUtility.SetScales(DrawCommand, particleScales.Value);

                particleScales.IsDirty = false;
            }
        }

        private void UpdateBondsIfDirty(bool forceDirty = false)
        {
            if ((forceDirty || bondPairs.IsDirty) && bondPairs.HasNonEmptyValue())
            {
                InstancingUtility.SetEdges(DrawCommand, bondPairs.Value);

                bondPairs.IsDirty = false;
            }

            if ((forceDirty || bondOrders.IsDirty) && bondOrders.HasNonEmptyValue())
            {
                InstancingUtility.SetEdgeCounts(DrawCommand, bondOrders.Value);

                bondOrders.IsDirty = false;
            }
        }

        private void UpdateMeshAndMaterials()
        {
            DrawCommand.SetMesh(mesh);
            DrawCommand.SetMaterial(material);
        }
    }
}