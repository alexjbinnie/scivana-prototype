﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Renderer.Primitives;
using Scivana.Visualisation.Property;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node.Renderer
{
    [Serializable]
    [RenderSubgraph("ball and stick")]
    public class BallAndStickRenderer : CompositeNode
    {
        private ParticleRenderer balls = new ParticleRenderer();
        private BondRenderer bonds = new BondRenderer();

        public ParticleRenderer BallRenderer => balls;
        public BondRenderer BondRenderer => bonds;

        private SerializableProperty<float> particleScale = new SerializableProperty<float>()
        {
            Value = 0.1f
        };

        [InputProperty(StandardProperties.ParticleScale)]
        public IProperty<float> ParticleScale => particleScale;

        private SerializableProperty<float> bondScale = new SerializableProperty<float>()
        {
            Value = 0.05f
        };

        [InputProperty(StandardProperties.BondScale)]
        public IProperty<float> BondScale => bondScale;

        private SerializableProperty<Material> particleMaterial =
            new SerializableProperty<Material>();

        [InputProperty("particle." + StandardProperties.Material)]
        [DefaultPropertyValue("raycast sphere")]
        public IProperty<Material> ParticleMaterial => particleMaterial;


        private SerializableProperty<Material> bondMaterial = new SerializableProperty<Material>();

        [InputProperty("bond." + StandardProperties.Material)]
        [DefaultPropertyValue("raycast cylinder")]
        public IProperty<Material> BondMaterial => bondMaterial;


        private SerializableProperty<Mesh> particleMesh =
            new SerializableProperty<Mesh>();

        [InputProperty("particle." + StandardProperties.Mesh)]
        [DefaultPropertyValue("cube")]
        public IProperty<Mesh> ParticleMesh => particleMesh;


        private SerializableProperty<Mesh> bondMesh = new SerializableProperty<Mesh>();

        [InputProperty("bond." + StandardProperties.Mesh)]
        [DefaultPropertyValue("cube")]
        public IProperty<Mesh> BondMesh => bondMesh;

        public BallAndStickRenderer()
        {
            balls.RendererScale.LinkedProperty = particleScale;
            bonds.ParticleScale.LinkedProperty = particleScale;
            bonds.BondScale.LinkedProperty = bondScale;
            balls.Material.LinkedProperty = particleMaterial;
            bonds.Material.LinkedProperty = bondMaterial;
            balls.Mesh.LinkedProperty = particleMesh;
            bonds.Mesh.LinkedProperty = bondMesh;
        }

        public override VisualisationNode[] Nodes => new VisualisationNode[]
        {
            balls, bonds
        };

        public Transform Transform
        {
            set
            {
                balls.Transform = value;
                bonds.Transform = value;
            }
        }

        public IEnumerable<CommandBuffer> CommandBuffers
        {
            get
            {
                yield return balls.CommandBuffer;
                yield return bonds.CommandBuffer;
            }
        }
    }
}