/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Utility;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node.Renderer
{
    public abstract class IndirectMeshRenderer : RendererNode, IDisposable
    {
        public void AppendToCommandBuffer(CommandBuffer buffer)
        {
            Refresh();
            DrawCommand.AppendToCommandBuffer(buffer);
        }

        protected IndirectMeshDrawCommand DrawCommand { get; } = new IndirectMeshDrawCommand();

        /// <inheritdoc cref="IDisposable.Dispose" />
        public void Dispose()
        {
            DrawCommand.Dispose();
        }

        protected override void BeforeBufferUpdate()
        {
            base.BeforeBufferUpdate();

            InstancingUtility.SetTransform(DrawCommand, Transform);
        }

        public override void Refresh()
        {
            if (!DrawCommand.HasDrawArguments)
            {
                foreach (var (_, prop, _) in GetInputProperties())
                    if (prop is IDirtyProperty dirtable)
                        dirtable.IsDirty = true;
            }

            base.Refresh();
        }


        protected override void AddToCommandBuffer(CommandBuffer buffer)
        {
            DrawCommand.AppendToCommandBuffer(buffer);
        }

        public void AddCustomBuffer<TType>(string key,
                                           string keyword,
                                           string bufferName,
                                           IReadOnlyProperty<TType[]> linked)
        {
            var property = new ArrayProperty<TType>
            {
                LinkedProperty = linked
            };
            customInputs.Add((key, keyword, bufferName, property));
        }

        public void AddCustomBuffer<TType>(string key, string keyword, string bufferName, TType[] value = null)
        {
            var property = new ArrayProperty<TType>();
            if (value != null)
            {
                property.Value = value;
            }

            customInputs.Add((key, keyword, bufferName, property));
        }

        protected virtual void UpdateBuffers()
        {
            foreach (var (_, keyword, bufferName, property) in customInputs)
            {
                if (property.IsDirty && property.HasValue && property.Value is Array array)
                {
                    InstancingUtility.SetArray(DrawCommand, keyword, bufferName, array);
                    property.IsDirty = false;
                }
            }
        }

        private List<(string Key, string Keyword, string BufferName, IDirtyProperty Property)> customInputs =
            new List<(string Key, string Keyword, string BufferName, IDirtyProperty Property)>();

        public override IEnumerable<(string Name, IProperty Property, PropertyAttributes Attributes)>
            GetInputProperties()
        {
            foreach (var prop in base.GetInputProperties())
                yield return prop;

            foreach (var (key, _, _, property) in customInputs)
                yield return (key, property, new PropertyAttributes()
                                 {
                                     SearchInPreviousSubgraphs = true
                                 });
        }
    }
}