﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node.Renderer
{
    /// <summary>
    /// A base node for a a rendering node, which generates a <see cref="CommandBuffer" /> which executes
    /// the given render commands. Only one <see cref="CommandBuffer" /> exists for the lifetime of the
    /// renderer.
    /// </summary>
    public abstract class RendererNode : VisualisationNode
    {
        private CommandBuffer internalBuffer;

        /// <summary>
        /// As a <see cref="CommandBuffer" /> cannot be created during MonoBehaviour creation, it
        /// is created at the the first point it is accessed, which called this command.
        /// </summary>
        private void EnsureCommandBufferExists()
        {
            if (internalBuffer == null)
            {
                internalBuffer = new CommandBuffer
                {
                    // Naming the command buffer helps identify it in the render chain
                    name = GetType().Name
                };
            }
        }

        /// <summary>
        /// Update the <see cref="CommandBuffer" />.
        /// </summary>
        /// <param name="needsUpdate">
        /// Does the buffer need updating. If it is empty, it is updated regardless
        /// of this parameter. This is useful as some command buffers don't need updating every frame.
        /// </param>
        private void UpdateBuffer(bool needsUpdate = false)
        {
            EnsureCommandBufferExists();

            if (needsUpdate)
            {
                internalBuffer.Clear();
            }
            else if (internalBuffer.sizeInBytes == 0)
            {
                needsUpdate = true;
            }

            if (needsUpdate)
            {
                // Wrap the buffer in a sampling group
                internalBuffer.BeginSample(GetType().Name);
                AddToCommandBuffer(internalBuffer);
                internalBuffer.EndSample(GetType().Name);
            }
        }

        /// <summary>
        /// Clear the internal <see cref="CommandBuffer" />, preventing rendering.
        /// </summary>
        private void ClearBuffer()
        {
            internalBuffer.Clear();
        }

        /// <summary>
        /// Add commands the the <see cref="CommandBuffer" /> to actually perform the rendering.
        /// </summary>
        protected abstract void AddToCommandBuffer(CommandBuffer buffer);

        /// <summary>
        /// Should this node actually render. Rendering will be disabled if this is false. This should
        /// check that all required inputs are set & valid.
        /// </summary>
        public abstract bool ShouldRender { get; }

        /// <summary>
        /// Are any of the input properties dirty?
        /// </summary>
        public abstract bool IsInputDirty { get; }

        /// <summary>
        /// Does the command buffer need a refresh? Depending on implementation, it may not need
        /// refreshing every frame (if, for example, it doesn't rely on a transform matrix).
        /// </summary>
        public abstract bool DoesCommandBufferNeedRefresh { get; }

        public override void Refresh()
        {
            if (ShouldRender)
            {
                var bufferNeedRefresh = DoesCommandBufferNeedRefresh;

                if (IsInputDirty)
                {
                    UpdateInput();
                }

                BeforeBufferUpdate();

                UpdateBuffer(bufferNeedRefresh);
            }
            else
            {
                ClearBuffer();
            }
        }

        protected virtual void OnPreRender()
        {
        }

        /// <summary>
        /// Update the renderer based upon the current input values, of which at least one
        /// will be dirty.
        /// </summary>
        protected virtual void UpdateInput()
        {
        }

        /// <summary>
        /// Called before the command buffer is updated.
        /// </summary>
        protected virtual void BeforeBufferUpdate()
        {
        }

        public CommandBuffer CommandBuffer
        {
            get
            {
                EnsureCommandBufferExists();
                return internalBuffer;
            }
        }

        public void RenderImmediate()
        {
            Graphics.ExecuteCommandBuffer(CommandBuffer);
        }

        public override IEnumerable<(CameraEvent, CommandBuffer)> GetCommandBuffers()
        {
            yield return (CameraEvent.BeforeForwardOpaque, CommandBuffer);
        }
    }
}