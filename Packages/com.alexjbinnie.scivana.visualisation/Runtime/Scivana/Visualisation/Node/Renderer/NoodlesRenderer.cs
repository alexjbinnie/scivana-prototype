﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Renderer.Primitives;
using Scivana.Visualisation.Node.Spline;
using Scivana.Visualisation.Property;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node.Renderer
{
    [Serializable]
    [RenderSubgraph("noodles")]
    public class NoodlesRenderer : CompositeNode
    {
        private CalculateCurvedBonds curved = new CalculateCurvedBonds();
        private ParticleRenderer balls = new ParticleRenderer();
        private SplineSegmentRenderer spline = new SplineSegmentRenderer();

        private SerializableProperty<float> scale = new SerializableProperty<float>()
        {
            Value = 0.1f
        };

        [InputProperty(StandardProperties.Scale)]
        public IProperty<float> Scale => scale;

        public NoodlesRenderer()
        {
            spline.SplineSegments.LinkedProperty = curved.SplineSegments;
            balls.RendererScale.LinkedProperty = scale;
            spline.Scale.LinkedProperty = scale;
        }

        public override VisualisationNode[] Nodes => new VisualisationNode[]
        {
            curved, balls, spline
        };

        public Transform Transform
        {
            set
            {
                balls.Transform = value;
                spline.Transform = value;
            }
        }

        public IEnumerable<CommandBuffer> CommandBuffers
        {
            get
            {
                yield return balls.CommandBuffer;
                yield return spline.CommandBuffer;
            }
        }
    }
}