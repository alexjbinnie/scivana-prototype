﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core;
using UnityEngine;
using UnityEngine.Rendering;
using Object = UnityEngine.Object;

namespace Scivana.Visualisation.Node.Renderer
{
    /// <summary>
    /// Base node for a renderer based upon using <see cref="CommandBuffer" />,
    /// which is a list of commands (rendering to textures, blitting, etc.)
    /// that can be executed at some point in the rendering pipeline.
    /// </summary>
    public abstract class CommandBufferRendererNode : IDisposable
    {
        /// <summary>
        /// Cached store of per camera command buffers.
        /// </summary>
        private Dictionary<Camera, List<(CameraEvent, CommandBuffer)>> buffers =
            new Dictionary<Camera, List<(CameraEvent, CommandBuffer)>>();

        /// <summary>
        /// Cleanup all buffers, removing them from the cameras.
        /// </summary>
        public virtual void Dispose()
        {
            foreach (var (camera, buffers) in buffers)
            {
                if (camera)
                {
                    foreach (var (evnt, buffer) in buffers)
                    {
                        camera.RemoveCommandBuffer(evnt, buffer);
                        buffer.Dispose();
                    }
                }
            }

            buffers.Clear();
            foreach (var material in materials)
                Object.DestroyImmediate(material);
        }

        /// <summary>
        /// Materials created for this renderer. Stored so they
        /// can be destroyed by <see cref="Cleanup()" />
        /// </summary>
        private List<Material> materials = new List<Material>();

        /// <summary>
        /// Create a new material for use with this renderer.
        /// </summary>
        protected Material CreateMaterial(Shader shader)
        {
            var material = new Material(shader);
            materials.Add(material);
            return material;
        }

        /// <summary>
        /// Render to the given camera using a command buffer, using a cached buffer
        /// if it has already been created.
        /// </summary>
        /// <param name="cam"></param>
        public virtual void Render(Camera cam)
        {
            if (buffers.ContainsKey(cam))
                return;
            buffers[cam] = new List<(CameraEvent, CommandBuffer)>();
            foreach (var (cameraEvent, buffer) in GetBuffers(cam))
            {
                cam.AddCommandBuffer(cameraEvent, buffer);
                buffers[cam].Add((cameraEvent, buffer));
            }
        }

        /// <summary>
        /// Get the command buffers and their triggering events for a given camera.
        /// </summary>
        protected abstract IEnumerable<(CameraEvent Event, CommandBuffer Buffer)> GetBuffers(
            Camera camera);
    }
}