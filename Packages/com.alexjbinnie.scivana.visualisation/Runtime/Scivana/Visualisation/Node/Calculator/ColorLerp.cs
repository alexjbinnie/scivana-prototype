/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Calculator
{
    [Serializable]
    public class ColorLerp : ValueLerp<UnityEngine.Color>
    {
        [SerializeField]
        private SerializableProperty<float> speed = new SerializableProperty<float>()
        {
            Value = 1f
        };

        [InputProperty(StandardProperties.Speed)]
        public IProperty<float> Speed => speed;

        private float Delta => Application.isPlaying ? speed * Time.deltaTime : 999f;

        protected override UnityEngine.Color MoveTowards(UnityEngine.Color current,
                                                         UnityEngine.Color target)
        {
            return new UnityEngine.Color(
                Mathf.MoveTowards(current.r, target.r, Delta),
                Mathf.MoveTowards(current.g, target.g, Delta),
                Mathf.MoveTowards(current.b, target.b, Delta),
                Mathf.MoveTowards(current.a, target.a, Delta)
            );
        }
    }
}