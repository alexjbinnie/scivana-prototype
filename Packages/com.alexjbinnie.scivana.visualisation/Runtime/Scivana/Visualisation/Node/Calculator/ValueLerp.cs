/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Calculator
{
    public abstract class ValueLerp<TValue> : GenericOutputNode
    {
        [SerializeField]
        private ArrayProperty<TValue> input = new ArrayProperty<TValue>();

        [InputProperty(StandardProperties.Input)]
        public IProperty<TValue[]> Input => input;

        private ArrayProperty<TValue> output = new ArrayProperty<TValue>();

        [OutputProperty(StandardProperties.Output)]
        public IProperty<TValue[]> Output => output;

        private TValue[] cached = new TValue[0];

        protected override bool IsInputValid => input.HasNonNullValue();
        protected override bool IsInputDirty => input.IsDirty;

        protected override void ClearDirty()
        {
            input.IsDirty = false;
        }

        protected override void UpdateOutput()
        {
            var input = this.input.Value;

            if (cached.Length != input.Length)
            {
                output.Resize(input.Length);
                Array.Resize(ref cached, input.Length);
                Array.Copy(input, cached, input.Length);
            }

            for (var i = 0; i < input.Length; i++)
            {
                cached[i] = MoveTowards(cached[i], input[i]);
            }

            output.Value = cached;
        }

        protected abstract TValue MoveTowards(TValue current, TValue target);

        protected override void ClearOutput()
        {
            output.UndefineValue();
        }
    }
}