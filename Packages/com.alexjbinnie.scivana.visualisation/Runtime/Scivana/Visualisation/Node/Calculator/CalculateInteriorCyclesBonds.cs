/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Calculator
{
    /// <summary>
    /// Generates all internal bonds requires for a cycle.
    /// </summary>
    [Serializable]
    public class CalculateInteriorCyclesBonds : VisualisationNode
    {
        [SerializeField]
        private ArrayProperty<IReadOnlyList<int>> cycles = new ArrayProperty<IReadOnlyList<int>>();

        [InputProperty(StandardProperties.Cycles)]
        public IProperty<IReadOnlyList<int>[]> Cycles => cycles;

        private readonly ArrayProperty<BondPair> interiorBonds = new ArrayProperty<BondPair>();

        [OutputProperty(StandardProperties.BondPairs)]
        public IProperty<BondPair[]> Bonds => interiorBonds;

        public override void Refresh()
        {
            if (cycles.IsDirty && cycles.HasValue)
            {
                var bonds = new List<BondPair>();

                foreach (var cycle in cycles.Value)
                {
                    for (var i = 0; i < cycle.Count - 2; i++)
                    {
                        for (var j = i + 2; j < cycle.Count; j++)
                        {
                            bonds.Add(new BondPair(cycle[i], cycle[j]));
                        }
                    }
                }

                interiorBonds.Value = bonds.ToArray();
                cycles.IsDirty = false;
            }
        }
    }
}