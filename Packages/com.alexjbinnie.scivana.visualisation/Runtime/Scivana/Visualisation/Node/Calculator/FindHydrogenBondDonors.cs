﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Trajectory;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Calculator
{
    public class FindHydrogenBondDonors : GenericOutputNode
    {
        private ArrayProperty<Element> particleElements = new ArrayProperty<Element>();

        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        private ArrayProperty<BondPair> bonds = new ArrayProperty<BondPair>();

        private ArrayProperty<Vector3> donorPositions = new ArrayProperty<Vector3>();

        private ArrayProperty<Quaternion> donorRotations = new ArrayProperty<Quaternion>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IProperty<Vector3[]> ParticlePositions => particlePositions;

        [InputProperty(StandardProperties.ParticleElements)]
        public IProperty<Element[]> ParticleElements => particleElements;

        [InputProperty(StandardProperties.BondPairs)]
        public IProperty<BondPair[]> BondPairs => bonds;

        public IReadOnlyProperty<Vector3[]> DonorPositions => donorPositions;

        public IReadOnlyProperty<Quaternion[]> DonorRotations => donorRotations;

        protected override bool IsInputValid => particleElements.HasNonNullValue() &&
                                                bonds.HasNonNullValue() &&
                                                particlePositions.HasNonNullValue();

        protected override bool IsInputDirty => particleElements.IsDirty ||
                                                bonds.IsDirty ||
                                                particlePositions.IsDirty;

        protected override void ClearDirty()
        {
            particleElements.IsDirty = false;
            bonds.IsDirty = false;
            particlePositions.IsDirty = false;
        }

        private List<(int Donor, int Hydrogen)> donors = new List<(int Donor, int Hydrogen)>();

        protected override void UpdateOutput()
        {
            var count = particleElements.Value.Length;

            if (particleElements.IsDirty || bonds.IsDirty)
            {
                donors.Clear();

                for (var i = 0; i < count; i++)
                {
                    var element = particleElements.Value[i];
                    if (element == Element.Oxygen ||
                        element == Element.Nitrogen ||
                        element == Element.Sulfur)
                    {
                        foreach (var bond in bonds.Value)
                        {
                            if (bond.A == i && particleElements.Value[bond.B] == Element.Hydrogen)
                                donors.Add((i, bond.B));
                            else if (bond.B == i && particleElements.Value[bond.A] == Element.Hydrogen)
                                donors.Add((i, bond.A));
                        }
                    }
                }

                donorPositions.Resize(donors.Count);
                donorRotations.Resize(donors.Count);
            }

            if (IsInputDirty)
            {
                var i = 0;
                foreach (var donor in donors)
                {
                    var position = particlePositions.Value[donor.Donor];
                    var offset = Vector3.Normalize(
                        particlePositions.Value[donor.Hydrogen] -
                        particlePositions.Value[donor.Donor]);
                    offset = offset.normalized;
                    var rotation = Quaternion.LookRotation(offset);

                    donorPositions.Value[i] = position;
                    donorRotations.Value[i] = rotation;

                    i++;
                }

                donorPositions.MarkValueAsChanged();
                donorRotations.MarkValueAsChanged();
            }
        }

        protected override void ClearOutput()
        {
            donorPositions.UndefineValue();
            donorRotations.UndefineValue();
        }
    }
}