/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Components.Calculator;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Calculator
{
    /// <summary>
    /// Locate cycles by looking at the bonding in a molecule, optionally limiting the
    /// search to only include cycles which lie completely within a single residue.
    /// </summary>
    [Serializable]
    public class CalculateCycles : GenericOutputNode
    {
        /// <summary>
        /// Bonds involved in the molecule.
        /// </summary>
        [InputProperty(StandardProperties.BondPairs)]
        public IProperty<BondPair[]> Bonds => bonds;

        /// <inheritdoc cref="Bonds" />
        [SerializeField]
        private ArrayProperty<BondPair> bonds = new ArrayProperty<BondPair>();

        /// <summary>
        /// Total number of particles involved in the system.
        /// </summary>
        [InputProperty(StandardProperties.ParticleCount)]
        public IProperty<int> ParticleCount => particleCount;

        /// <inheritdoc cref="ParticleCount" />
        [SerializeField]
        private SerializableProperty<int> particleCount = new SerializableProperty<int>();

        /// <summary>
        /// List of set of indices that make up each cycle.
        /// </summary>
        [OutputProperty(StandardProperties.Cycles)]
        public IReadOnlyProperty<Cycle[]> Cycles => cycles;

        /// <inheritdoc cref="Cycles" />
        private readonly ArrayProperty<Cycle> cycles = new ArrayProperty<Cycle>();

        /// <summary>
        /// Set of particle residue indices.
        /// </summary>
        [InputProperty(StandardProperties.ParticleResidues)]
        public IReadOnlyProperty<int[]> ParticleResidues => particleResidues;

        /// <inheritdoc cref="ParticleResidues" />
        private readonly ArrayProperty<int> particleResidues = new ArrayProperty<int>();

        /// <summary>
        /// Number of cycles.
        /// </summary>
        [OutputProperty(StandardProperties.CyclesCount)]
        public IReadOnlyProperty<int> CyclesCount => cyclesCount;

        /// <inheritdoc cref="CyclesCount" />
        private readonly SerializableProperty<int> cyclesCount = new SerializableProperty<int>();

        /// <inheritdoc cref="GenericOutputNode.IsInputDirty" />
        protected override bool IsInputDirty => bonds.IsDirty || particleCount.IsDirty;

        /// <inheritdoc cref="GenericOutputNode.IsInputValid" />
        protected override bool IsInputValid => bonds.HasNonEmptyValue();

        /// <inheritdoc cref="GenericOutputNode.ClearDirty" />
        protected override void ClearDirty()
        {
            bonds.IsDirty = false;
            particleCount.IsDirty = false;
        }

        /// <inheritdoc cref="GenericOutputNode.UpdateOutput" />
        protected override void UpdateOutput()
        {
            var particleCount = this.particleCount.Value;
            var neighbourList = new List<int>[particleCount];
            for (var i = 0; i < particleCount; i++)
                neighbourList[i] = new List<int>();

            foreach (var bond in bonds)
            {
                // Only include bonds within residues.
                if (particleResidues.HasValue)
                {
                    var res1 = particleResidues.Value[bond.A];
                    var res2 = particleResidues.Value[bond.B];
                    if (res1 != res2)
                        continue;
                }

                neighbourList[bond.A].Add(bond.B);
                neighbourList[bond.B].Add(bond.A);
            }

            cycles.Value = ComputeChordlessCycles(particleCount, neighbourList).ToArray();
            cyclesCount.Value = cycles.Value.Length;
        }

        /// <inheritdoc cref="GenericOutputNode.ClearOutput" />
        protected override void ClearOutput()
        {
            cycles.UndefineValue();
            cyclesCount.UndefineValue();
        }

        /// <summary>
        /// Chordless cycle computation using https://arxiv.org/pdf/1309.1051.pdf
        /// </summary>
        private IEnumerable<Cycle> ComputeChordlessCycles(int count, List<int>[] adjacency)
        {
            var T = new Queue<List<int>>();
            for (var u = 0; u < count; u++)
                foreach (var x in adjacency[u])
                foreach (var y in adjacency[u])
                {
                    // Ensure u < x
                    if (u >= x)
                        continue;

                    // Ensure x < y and hence u < x < y
                    if (x >= y)
                        continue;

                    if (adjacency[x].Contains(y))
                    {
                        yield return new Cycle(x, u, y);
                    }
                    else
                    {
                        T.Enqueue(new List<int>
                        {
                            x,
                            u,
                            y
                        });
                    }
                }

            while (T.Any())
            {
                var p = T.Dequeue();

                if (p.Count == 6)
                    continue;

                var u2 = p[1];
                var ut = p.Last();
                foreach (var v in adjacency[ut])
                {
                    if (v <= u2)
                        continue;
                    if (Enumerable.Range(1, p.Count - 2)
                                  .Any(i => adjacency[p[i]].Contains(v)))
                        continue;

                    var p2 = new List<int>();
                    p2.AddRange(p);
                    p2.Add(v);

                    if (adjacency[p[0]].Contains(v))
                        yield return new Cycle(p2.ToArray());
                    else
                        T.Enqueue(p2);
                }
            }
        }
    }
}