﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Trajectory;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Calculator
{
    public class FindHydrogenBondAcceptors : GenericOutputNode
    {
        private ArrayProperty<Element> particleElements = new ArrayProperty<Element>();

        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        private ArrayProperty<BondPair> bonds = new ArrayProperty<BondPair>();

        private ArrayProperty<Vector3> acceptorPositions = new ArrayProperty<Vector3>();

        private ArrayProperty<Quaternion> acceptorRotations = new ArrayProperty<Quaternion>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IProperty<Vector3[]> ParticlePositions => particlePositions;

        [InputProperty(StandardProperties.ParticleElements)]
        public IProperty<Element[]> ParticleElements => particleElements;

        [InputProperty(StandardProperties.BondPairs)]
        public IProperty<BondPair[]> BondPairs => bonds;

        public IReadOnlyProperty<Vector3[]> AcceptorPositions => acceptorPositions;

        public IReadOnlyProperty<Quaternion[]> AcceptorRotations => acceptorRotations;

        protected override bool IsInputValid => particleElements.HasNonNullValue() &&
                                                bonds.HasNonNullValue() &&
                                                particlePositions.HasNonNullValue();

        protected override bool IsInputDirty => particleElements.IsDirty ||
                                                bonds.IsDirty ||
                                                particlePositions.IsDirty;

        protected override void ClearDirty()
        {
            particleElements.IsDirty = false;
            bonds.IsDirty = false;
            particlePositions.IsDirty = false;
        }

        private List<(int Acceptor, List<int> Neighbours)> acceptors = new List<(int Acceptor, List<int> Neighbours)>();

        protected override void UpdateOutput()
        {
            if (particleElements.IsDirty || bonds.IsDirty)
            {
                var count = particleElements.Value.Length;

                acceptors.Clear();

                for (var i = 0; i < count; i++)
                {
                    var element = particleElements.Value[i];
                    if (element == Element.Oxygen ||
                        element == Element.Nitrogen ||
                        element == Element.Sulfur)
                    {
                        var neighbours = new List<int>();
                        foreach (var bond in bonds.Value)
                        {
                            if (bond.A == i && particleElements.Value[bond.B] != Element.Hydrogen)
                                neighbours.Add(bond.B);
                            else if (bond.B == i && particleElements.Value[bond.A] != Element.Hydrogen)
                                neighbours.Add(bond.A);
                        }

                        if (neighbours.Count > 0)
                            acceptors.Add((i, neighbours));
                    }
                }

                acceptorPositions.Resize(acceptors.Count);
                acceptorRotations.Resize(acceptors.Count);
            }

            if (IsInputDirty)
            {
                var i = 0;
                foreach (var acceptor in acceptors)
                {
                    var position = particlePositions.Value[acceptor.Acceptor];

                    var offset = Vector3.zero;
                    foreach (var j in acceptor.Neighbours)
                        offset += Vector3.Normalize(position - particlePositions.Value[j]);
                    offset = offset.normalized;
                    var rotation = Quaternion.LookRotation(offset);

                    acceptorPositions.Value[i] = position;
                    acceptorRotations.Value[i] = rotation;

                    i++;
                }

                acceptorPositions.MarkValueAsChanged();
                acceptorRotations.MarkValueAsChanged();
            }
        }

        protected override void ClearOutput()
        {
            acceptorPositions.UndefineValue();
            acceptorRotations.UndefineValue();
        }
    }
}