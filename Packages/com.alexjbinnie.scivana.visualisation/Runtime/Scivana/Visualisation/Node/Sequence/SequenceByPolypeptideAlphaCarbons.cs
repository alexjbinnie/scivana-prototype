﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Adaptor;
using Scivana.Visualisation.Property;

namespace Scivana.Visualisation.Node.Sequence
{
    [SequenceSubgraph("polypeptide", "protein")]
    public class SequenceByPolypeptideAlphaCarbons : CompositeNode
    {
        private CalculatePolypeptideSequences polypeptide = new CalculatePolypeptideSequences();
        private ParticleFilteredAdaptorNode filter = new ParticleFilteredAdaptorNode();

        [InputProperty(StandardProperties.Parent)]
        public IProperty<IDynamicPropertyProvider> Parent => parent;

        private SerializableProperty<IDynamicPropertyProvider> parent
            = new SerializableProperty<IDynamicPropertyProvider>();

        public SequenceByPolypeptideAlphaCarbons()
        {
            filter.ParticleFilter.LinkedProperty = polypeptide.AlphaCarbonSequences.Flatten();
            filter.AddOverrideProperty<IReadOnlyList<int>[]>(StandardProperties.SequenceParticles)
                  .LinkedProperty = polypeptide.AlphaCarbonSequences;
            filter.ParentAdaptor.LinkedProperty = parent;
        }

        public override VisualisationNode[] Nodes => new VisualisationNode[]
        {
            polypeptide, filter
        };
    }
}