/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Sequence
{
    /// <summary>
    /// Calculates sequences of subsequent residues in entities which are standard
    /// amino acids, hence forming polypeptide chains.
    /// </summary>
    [Serializable]
    public class CalculatePolypeptideSequences : GenericOutputNode
    {
        [InputProperty(StandardProperties.ParticleNames)]
        public IProperty<string[]> AtomNames => atomNames;

        /// <summary>
        /// The name of each atom.
        /// </summary>
        [SerializeField]
        private ArrayProperty<string> atomNames = new ArrayProperty<string>();

        [InputProperty(StandardProperties.ParticleResidues)]
        public IProperty<int[]> AtomResidues => atomResidues;

        /// <summary>
        /// The residue index for each atom.
        /// </summary>
        [SerializeField]
        private ArrayProperty<int> atomResidues = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ResidueNames)]
        public IProperty<string[]> ResidueNames => residueNames;

        /// <summary>
        /// The name of each residue.
        /// </summary>
        [SerializeField]
        private ArrayProperty<string> residueNames = new ArrayProperty<string>();

        [InputProperty(StandardProperties.ResidueEntities)]
        public IProperty<int[]> ResidueEntities => residueEntities;

        /// <summary>
        /// The entity (chain) index for each residue.
        /// </summary>
        [SerializeField]
        private ArrayProperty<int> residueEntities = new ArrayProperty<int>();

        public IProperty<IReadOnlyList<int>[]> ResidueSequences => residueSequences;

        /// <summary>
        /// A set of sequences of residue indices that form polypeptide chains.
        /// </summary>
        private readonly ArrayProperty<IReadOnlyList<int>> residueSequences
            = new ArrayProperty<IReadOnlyList<int>>();

        /// <summary>
        /// A set of alpha carbon atom indices that form polypeptide chains.
        /// </summary>
        private readonly ArrayProperty<IReadOnlyList<int>> alphaCarbonSequences
            = new ArrayProperty<IReadOnlyList<int>>();

        [OutputProperty(StandardProperties.SequenceParticles)]
        public IReadOnlyProperty<IReadOnlyList<int>[]> AlphaCarbonSequences => alphaCarbonSequences;

        /// <inheritdoc cref="IsInputValid" />
        protected override bool IsInputValid => atomNames.HasNonNullValue()
                                             && atomResidues.HasNonNullValue()
                                             && residueNames.HasNonNullValue()
                                             && residueEntities.HasNonNullValue();

        /// <inheritdoc cref="IsInputDirty" />
        protected override bool IsInputDirty => atomNames.IsDirty
                                             || atomResidues.IsDirty
                                             || residueNames.IsDirty
                                             || residueEntities.IsDirty;

        /// <inheritdoc cref="ClearDirty" />
        protected override void ClearDirty()
        {
            atomNames.IsDirty = false;
            atomResidues.IsDirty = false;
            residueNames.IsDirty = false;
            residueEntities.IsDirty = false;
        }

        /// <inheritdoc cref="UpdateOutput" />
        protected override void UpdateOutput()
        {
            var (resSequences, alphaSequences) = CalculateSequences(residueNames.Value,
                                                                    atomNames.Value,
                                                                    atomResidues.Value,
                                                                    residueEntities.Value);
            residueSequences.Value = resSequences;
            alphaCarbonSequences.Value = alphaSequences;
        }

        /// <summary>
        /// Calculate sequences of polypeptides
        /// </summary>
        private static (IReadOnlyList<int>[] residueSequences, IReadOnlyList<int>[]
            alphaCarbonSequences)
            CalculateSequences(string[] residueNames,
                               string[] atomNames,
                               int[] atomResidues,
                               int[] residueEntities)
        {
            var residueSequences = new List<IReadOnlyList<int>>();
            var alphaCarbonSequences = new List<IReadOnlyList<int>>();
            var currentResidues = new List<int>();
            var currentAlphaCarbons = new List<int>();
            var currentEntity = -1;

            for (var atom = 0; atom < atomNames.Length; atom++)
            {
                if (!string.Equals(atomNames[atom], "ca",
                                   StringComparison.InvariantCultureIgnoreCase))
                    continue;
                var residue = atomResidues[atom];
                if (!AminoAcid.IsStandardAminoAcid(residueNames[residue]))
                    continue;
                var entity = residueEntities[residue];
                if (currentEntity != entity && currentResidues.Any())
                {
                    residueSequences.Add(currentResidues);
                    alphaCarbonSequences.Add(currentAlphaCarbons);
                    currentResidues = new List<int>();
                    currentAlphaCarbons = new List<int>();
                }

                currentEntity = entity;
                currentResidues.Add(residue);
                currentAlphaCarbons.Add(atom);
            }

            if (currentResidues.Any())
            {
                residueSequences.Add(currentResidues);
                alphaCarbonSequences.Add(currentAlphaCarbons);
            }

            return (residueSequences.ToArray(), alphaCarbonSequences.ToArray());
        }

        /// <inheritdoc cref="ClearOutput" />
        protected override void ClearOutput()
        {
            residueSequences.UndefineValue();
            alphaCarbonSequences.UndefineValue();
        }
    }
}