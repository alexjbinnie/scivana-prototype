/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Sequence
{
    /// <summary>
    /// Works out groupings of continuous entity IDs.
    /// </summary>
    [Serializable]
    public class SequenceByEntity : GenericOutputNode
    {
        [SerializeField]
        private ArrayProperty<int> residueEntities = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ResidueEntities)]
        public IProperty<int[]> ResidueEntities => residueEntities;

        [SerializeField]
        private ArrayProperty<int> particleResidues = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ParticleResidues)]
        public IProperty<int[]> ParticleResidues => particleResidues;

        private ArrayProperty<IReadOnlyList<int>> sequences
            = new ArrayProperty<IReadOnlyList<int>>();

        [OutputProperty(StandardProperties.SequenceParticles)]
        public IReadOnlyProperty<IReadOnlyList<int>[]> Sequences => sequences;

        protected override bool IsInputValid => residueEntities.HasValue
                                             && particleResidues.HasValue;

        protected override bool IsInputDirty => residueEntities.IsDirty
                                             || particleResidues.IsDirty;

        protected override void ClearDirty()
        {
            residueEntities.IsDirty = false;
            particleResidues.IsDirty = false;
        }

        protected override void UpdateOutput()
        {
            var particleCount = this.particleResidues.Value.Length;
            var particleResidues = this.particleResidues.Value;
            var residueEntities = this.residueEntities.Value;

            var sequences = new List<IReadOnlyList<int>>();

            var currentSequence = new List<int>();
            var currentEntityId = -1;

            for (var i = 0; i < particleCount; i++)
            {
                var residue = particleResidues[i];
                var entity = residueEntities[residue];
                if (currentEntityId != entity)
                {
                    if (currentSequence.Count > 0)
                    {
                        sequences.Add(currentSequence);
                        currentSequence = new List<int>();
                    }

                    currentSequence.Add(i);
                    currentEntityId = entity;
                }
                else
                {
                    currentSequence.Add(i);
                }
            }

            if (currentSequence.Count > 0)
                sequences.Add(currentSequence);

            this.sequences.Value = sequences.ToArray();
        }

        protected override void ClearOutput()
        {
            sequences.UndefineValue();
        }
    }
}