/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Adaptor
{
    /// <summary>
    /// A variation of a <see cref="FrameSourceAdaptorNode" /> which applies a filter to any
    /// key of the form 'particle.*'.
    /// </summary>
    [Serializable]
    public class ParticleFilteredAdaptorNode : ParentedAdaptorNode
    {
        /// <summary>
        /// A filter which will affect all fields of the form 'particle.*'.
        /// </summary>
        public IProperty<int[]> ParticleFilter => particleFilter;

        [SerializeField]
        private ArrayProperty<int> particleFilter = new ArrayProperty<int>();

        private readonly Dictionary<string, IReadOnlyProperty> filteredProperties =
            new Dictionary<string, IReadOnlyProperty>();

        /// <inheritdoc cref="BaseAdaptorNode.GetProperties" />
        public override IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        {
            foreach (var (key, value) in filteredProperties)
                yield return (key, value);
        }

        /// <inheritdoc cref="BaseAdaptorNode.GetProperty" />
        public override IReadOnlyProperty GetProperty(string key)
        {
            return filteredProperties.TryGetValue(key, out var value)
                       ? value
                       : null;
        }

        /// <inheritdoc cref="BaseAdaptorNode.GetOrCreateProperty{T}" />
        public override IReadOnlyProperty<T> GetOrCreateProperty<T>(string name)
        {
            if (GetProperty(name) is IReadOnlyProperty<T> existing)
                return existing;

            var property = base.GetOrCreateProperty<T>(name);

            if (property is IReadOnlyProperty<BondPair[]> bondPairProperty
             && name.Equals(FrameFields.Bonds.Key))
            {
                var bondFiltered = new FilterBondProperty(bondPairProperty, particleFilter);
                filteredProperties[name] = bondFiltered;
                return bondFiltered as IReadOnlyProperty<T>;
            }

            if (!typeof(T).IsArray)
            {
                filteredProperties[name] = property;
                return property;
            }

            if (name.Contains(".particles") && property is IReadOnlyProperty<int[]> indexProp)
            {
                var filtered = new IndexFilteredProperty(indexProp, particleFilter);

                filteredProperties[name] = filtered;
                return filtered as IReadOnlyProperty<T>;
            }

            if (name.Contains(".particles") && property is IReadOnlyProperty<IReadOnlyList<int>[]> selections)
            {
                var filtered = new RemappedSelectionProperty(selections, particleFilter);

                filteredProperties[name] = filtered;
                return filtered as IReadOnlyProperty<T>;
            }

            if (name.Contains("particle."))
            {
                var elementType = typeof(T).GetElementType();
                var filteredType = typeof(FilteredProperty<>).MakeGenericType(elementType);

                var filtered =
                    Activator.CreateInstance(filteredType, property, particleFilter) as
                        IReadOnlyProperty<T>;

                filteredProperties[name] = filtered;
                return filtered;
            }

            filteredProperties[name] = property;
            return property;
        }
    }
}