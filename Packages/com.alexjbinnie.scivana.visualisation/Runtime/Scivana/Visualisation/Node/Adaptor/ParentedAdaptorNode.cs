/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Adaptor
{
    /// <summary>
    /// An <see cref="BaseAdaptorNode" /> which is linked to another adaptor. This
    /// adaptor contains its own properties, but links them to the parent. This means
    /// that the parent can be changed without listeners to this adaptor needing to
    /// change their links.
    /// </summary>
    [Serializable]
    public class ParentedAdaptorNode : BaseAdaptorNode
    {
        /// <inheritdoc cref="ParentAdaptor" />
        [SerializeField]
        private InterfaceProperty<IDynamicPropertyProvider> adaptor
            = new InterfaceProperty<IDynamicPropertyProvider>();

        /// <summary>
        /// The adaptor that this adaptor inherits from.
        /// </summary>
        [InputProperty(StandardProperties.Parent, Local = true)]
        public IProperty<IDynamicPropertyProvider> ParentAdaptor => adaptor;

        /// <inheritdoc cref="BaseAdaptorNode.OnCreateProperty{T}" />
        protected override IReadOnlyProperty<T> OnCreateProperty<T>(
            string key,
            IProperty<T> property)
        {
            base.OnCreateProperty(key, property);
            if (adaptor.HasNonNullValue())
                property.LinkedProperty = adaptor.Value.GetOrCreateProperty<T>(key);
            return property;
        }

        public void GetValueFromParent(string key, IProperty property)
        {
            if (IsPropertyOverriden(key))
                return;

            if (adaptor.HasNonNullValue())
            {
                property.TrySetLinkedProperty(
                    adaptor.Value.GetOrCreateProperty(key, property.PropertyType));
            }
            else
            {
                property.TrySetLinkedProperty(null);
            }
        }

        public override void RemoveOverrideProperty(string name)
        {
            base.RemoveOverrideProperty(name);
            GetValueFromParent(name, GetProperty(name) as IProperty);
        }

        /// <inheritdoc cref="BaseAdaptorNode.Refresh" />
        public override void Refresh()
        {
            if (adaptor.IsDirty)
            {
                foreach (var (key, property) in Properties)
                    GetValueFromParent(key, property);

                adaptor.IsDirty = false;
            }
        }
    }
}