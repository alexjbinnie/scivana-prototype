/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;

namespace Scivana.Visualisation.Node.Adaptor
{
    /// <summary>
    /// Visualisation node which reads frames using <see cref="IFrameConsumer" /> and
    /// dynamically provides the frame's data as properties for the visualisation
    /// system.
    /// </summary>
    /// <remarks>
    /// This visualisation node acts as the bridge between the underlying trajectory
    /// and the visualisation system.
    /// </remarks>
    [Serializable]
    public class FrameSourceAdaptorNode : BaseAdaptorNode
    {
        /// <inheritdoc cref="BaseDynamicPBaseAdaptorNodeperty{T}" />
        protected override IReadOnlyProperty<T> OnCreateProperty<T>(
            string key,
            IProperty<T> property)
        {
            GetPropertyValueFromFrame(key, property);
            return property;
        }

        public override void RemoveOverrideProperty(string name)
        {
            base.RemoveOverrideProperty(name);
            GetPropertyValueFromFrame(name, GetProperty(name) as IProperty);
        }

        /// <summary>
        /// Update a property by getting the value with the given key from the current
        /// frame, ignoring if this property is marked as an override.
        /// </summary>
        private void GetPropertyValueFromFrame(string key, IProperty property)
        {
            if (IsPropertyOverriden(key))
                return;
            if (FrameSource?.CurrentFrame != null
             && FrameSource.CurrentFrame.Data.TryGetValue(key, out var value))
            {
                property.TrySetValue(value);
            }
        }

        /// <summary>
        /// Callback for when the frame is changed. Updates the output properties
        /// selectively depending on if the field is marked as having changed.
        /// </summary>
        private void OnFrameUpdated(Frame frame, FrameChanges changes)
        {
            if (frame == null)
                return;

            foreach (var (key, property) in Properties)
            {
                if (changes.HasChanged(key))
                    GetPropertyValueFromFrame(key, property);
            }
        }

        private ITrajectorySnapshot source;

        /// <inheritdoc cref="IFrameConsumer.FrameSource" />
        public ITrajectorySnapshot FrameSource
        {
            get => source;
            set
            {
                if (source != null)
                    source.FrameChanged -= OnFrameUpdated;
                source = value;
                if (source != null)
                {
                    source.FrameChanged += OnFrameUpdated;
                    OnFrameUpdated(source.CurrentFrame, FrameChanges.All);
                }
            }
        }
    }
}