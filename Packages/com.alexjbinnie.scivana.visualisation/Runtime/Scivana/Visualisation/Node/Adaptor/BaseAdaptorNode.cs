/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Narupa.Protocol.Trajectory;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Trajectory;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Adaptor
{
    /// <summary>
    /// An Adaptor node is a node which can provide any property. This allows it to act
    /// as a gateway, for both connecting the visualisation system to external sources
    /// (such as a frame source) and for filtering out values.
    /// </summary>
    public abstract class BaseAdaptorNode : VisualisationNode, IDynamicPropertyProvider
    {
        /// <inheritdoc cref="Properties" />
        private readonly Dictionary<string, Core.Properties.Property> properties =
            new Dictionary<string, Core.Properties.Property>();

        private HashSet<string> propertyOverrides = new HashSet<string>();

        /// <summary>
        /// Add a property with the given type and name to this adaptor that is not
        /// affected by the frame.
        /// </summary>
        public IProperty<TValue> AddOverrideProperty<TValue>(string name)
        {
            GetOrCreateProperty<TValue>(name);
            propertyOverrides.Add(name);
            return properties[name] as IProperty<TValue>;
        }

        /// <summary>
        /// Remove a property with the given type and name from this adaptor that is not
        /// affected by the frame.
        /// </summary>
        public virtual void RemoveOverrideProperty(string name)
        {
            propertyOverrides.Remove(name);
        }

        /// <summary>
        /// Get a property if it is overriden
        /// </summary>
        public IProperty<TValue> GetOverrideProperty<TValue>(string name)
        {
            if (propertyOverrides.Contains(name))
                return properties[name] as IProperty<TValue>;
            return null;
        }

        public bool IsPropertyOverriden(string key)
        {
            return propertyOverrides.Contains(key);
        }

        /// <summary>
        /// Dynamic properties created by the system, with the keys corresponding to the
        /// keys in the frame's data.
        /// </summary>
        protected IReadOnlyDictionary<string, Core.Properties.Property> Properties => properties;

        /// <inheritdoc cref="IDynamicPropertyProvider.GetOrCreateProperty{T}" />
        public virtual IReadOnlyProperty<T> GetOrCreateProperty<T>(string name)
        {
            if (GetProperty(name) is IReadOnlyProperty<T> existing)
                return existing;

            var property = new SerializableProperty<T>();
            properties[name] = property;

            return OnCreateProperty(name, property);
            ;
        }

        /// <inheritdoc cref="IDynamicPropertyProvider.GetPotentialProperties" />
        public IEnumerable<(string name, Type type)> GetPotentialProperties()
        {
            return FrameFields.All.Select(prop => (prop.Key, prop.Type));
        }

        /// <inheritdoc cref="IDynamicPropertyProvider.CanDynamicallyProvideProperty{T}" />
        public bool CanDynamicallyProvideProperty<T>(string name)
        {
            return true;
        }

        /// <inheritdoc cref="IDynamicPropertyProvider.GetProperty" />
        public virtual IReadOnlyProperty GetProperty(string key)
        {
            return properties.TryGetValue(key, out var value)
                       ? value
                       : null;
        }

        /// <inheritdoc cref="IDynamicPropertyProvider.GetProperties" />
        public virtual IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        {
            foreach (var (key, value) in properties)
                yield return (key, value);
        }

        /// <summary>
        /// Callback when a property has been requested, created and added to the
        /// <see cref="Properties" /> dictionary, but has not yet been returned to the
        /// requestor.
        /// </summary>
        protected virtual IReadOnlyProperty<T> OnCreateProperty<T>(
            string key,
            IProperty<T> property)
        {
            return property;
        }

        /// <summary>
        /// Array of elements of the provided frame.
        /// </summary>
        public IReadOnlyProperty<Element[]> ParticleElements =>
            GetOrCreateProperty<Element[]>(FrameData.ParticleElementArrayKey);

        /// <summary>
        /// Array of particle positions of the provided frame.
        /// </summary>
        public IReadOnlyProperty<Vector3[]> ParticlePositions =>
            GetOrCreateProperty<Vector3[]>(FrameData.ParticlePositionArrayKey);

        /// <summary>
        /// Array of bonds of the provided frame.
        /// </summary>
        public IReadOnlyProperty<BondPair[]> BondPairs =>
            GetOrCreateProperty<BondPair[]>(FrameData.BondArrayKey);

        /// <summary>
        /// Array of bond orders of the provided frame.
        /// </summary>
        public IReadOnlyProperty<int[]> BondOrders =>
            GetOrCreateProperty<int[]>(FrameData.BondOrderArrayKey);

        /// <summary>
        /// Array of particle residues of the provided frame.
        /// </summary>
        public IReadOnlyProperty<int[]> ParticleResidues =>
            GetOrCreateProperty<int[]>(FrameData.ParticleResidueArrayKey);

        /// <summary>
        /// Array of particle names of the provided frame.
        /// </summary>
        public IReadOnlyProperty<string[]> ParticleNames =>
            GetOrCreateProperty<string[]>(FrameData.ParticleNameArrayKey);

        /// <summary>
        /// Array of residue names of the provided frame.
        /// </summary>
        public IReadOnlyProperty<string[]> ResidueNames =>
            GetOrCreateProperty<string[]>(FrameData.ResidueNameArrayKey);

        /// <summary>
        /// Array of residue entities of the provided frame.
        /// </summary>
        public IReadOnlyProperty<int[]> ResidueEntities =>
            GetOrCreateProperty<int[]>(FrameData.ResidueChainArrayKey);

        /// <summary>
        /// Array of residue entities of the provided frame.
        /// </summary>
        public IReadOnlyProperty<int> ResidueCount =>
            GetOrCreateProperty<int>(FrameData.ResidueCountValueKey);

        /// <summary>
        /// Refresh the adaptor. Should be called every frame.
        /// </summary>
        public override void Refresh()
        {
        }
    }
}