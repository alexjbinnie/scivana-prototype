/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Protein
{
    /// <summary>
    /// Calculates secondary structure using the DSSP Algorithm.
    /// </summary>
    [Serializable]
    public class DsspCalculatorNode
    {
        #region Input Properties

        /// <summary>
        /// Array of atomic positions. This should contains the atoms which are relevant to
        /// the protein backbone.
        /// </summary>
        public IProperty<Vector3[]> AtomPositions => atomPositions;

        /// <inheritdoc cref="AtomPositions" />
        [SerializeField]
        private ArrayProperty<Vector3> atomPositions = new ArrayProperty<Vector3>();

        /// <summary>
        /// Array of residue indices which may appear in
        /// <see cref="PeptideResidueSequences" /> for each atom.
        /// </summary>
        public IProperty<int[]> AtomResidues => atomResidues;

        /// <inheritdoc cref="AtomResidues" />
        [SerializeField]
        private ArrayProperty<int> atomResidues = new ArrayProperty<int>();

        /// <summary>
        /// Array of atom names. Each amino acid should have atoms named 'CA', 'C', 'N' and
        /// 'O'.
        /// </summary>
        public IProperty<string[]> AtomNames => atomNames;

        /// <inheritdoc cref="AtomNames" />
        [SerializeField]
        private ArrayProperty<string> atomNames = new ArrayProperty<string>();

        /// <summary>
        /// Number of residues involved. The maximum index referenced in both
        /// <see cref="AtomResidues" /> and <see cref="PeptideResidueSequences" /> should
        /// be less than this value.
        /// </summary>
        public IProperty<int> ResidueCount => residueCount;

        /// <inheritdoc cref="ResidueCount" />
        [SerializeField]
        private SerializableProperty<int> residueCount = new SerializableProperty<int>();

        /// <summary>
        /// Array of residue indices that indicate which residues are involved in a protein
        /// chain.
        /// </summary>
        public IProperty<IReadOnlyList<int>[]> PeptideResidueSequences => peptideResidueSequences;

        /// <inheritdoc cref="PeptideResidueSequences" />
        [SerializeField]
        private ArrayProperty<IReadOnlyList<int>> peptideResidueSequences
            = new ArrayProperty<IReadOnlyList<int>>();

        /// <summary>
        /// Options to configure the DSSP algorithm.
        /// </summary>
        public DsspOptions DsspOptions
        {
            get => dsspOptions;
            set => dsspOptions = value;
        }

        /// <inheritdoc cref="DsspOptions" />
        [SerializeField]
        private DsspOptions dsspOptions = new DsspOptions();

        #endregion

        #region Output Properties

        /// <summary>
        /// Secondary structure assignments for each residue. The size of this array will
        /// be equal to <see cref="ResidueCount" />, with residues that are not in one of
        /// the peptide chains provided in <see cref="PeptideResidueSequences" /> being
        /// given the assignment <see cref="SecondaryStructureAssignment.None" />
        /// </summary>
        public IReadOnlyProperty<SecondaryStructureAssignment[]> ResidueSecondaryStructure =>
            residueSecondaryStructure;

        /// <inheritdoc cref="ResidueSecondaryStructure" />
        private readonly ArrayProperty<SecondaryStructureAssignment> residueSecondaryStructure =
            new ArrayProperty<SecondaryStructureAssignment>();

        /// <summary>
        /// Array of calculated hydrogen bonds, based on indices of atoms in the
        /// <see cref="AtomPositions" />.
        /// </summary>
        public IReadOnlyProperty<BondPair[]> HydrogenBonds => hydrogenBonds;

        /// <inheritdoc cref="HydrogenBonds" />
        private ArrayProperty<BondPair> hydrogenBonds = new ArrayProperty<BondPair>();

        #endregion

        #region State Management

        /// <summary>
        /// Does the secondary structure require recalculating.
        /// </summary>
        private bool needRecalculate = true;

        /// <summary>
        /// Set of residue data (positions of hydrogen-bonding involved atoms) for each
        /// residue in each sequence specified in <see cref="PeptideResidueSequences" />.
        /// </summary>
        private List<SecondaryStructureResidueData[]> sequenceResidueData =
            new List<SecondaryStructureResidueData[]>();

        #endregion

        public bool IsInputValid => peptideResidueSequences.HasNonNullValue()
                                 && residueCount.HasNonNullValue();

        public bool AreResiduesDirty => atomResidues.IsDirty || peptideResidueSequences.IsDirty ||
                                        atomNames.IsDirty || residueCount.IsDirty;

        public bool AreResiduesValid => atomResidues.HasNonEmptyValue() &&
                                        peptideResidueSequences.HasNonEmptyValue() &&
                                        atomNames.HasNonEmptyValue();

        public void Refresh()
        {
            if (IsInputValid)
            {
                if (AreResiduesDirty)
                {
                    if (AreResiduesValid)
                        UpdateResidues();
                }

                if (atomPositions.IsDirty)
                    UpdatePositions();

                if (needRecalculate || Time.frameCount % 30 == 0)
                {
                    CalculateSecondaryStructure();
                    CalculateHydrogenBonds();
                    needRecalculate = false;
                }
            }
        }

        private void UpdateResidues()
        {
            sequenceResidueData.Clear();
            foreach (var sequence in peptideResidueSequences.Value)
                sequenceResidueData.Add(
                    DsspAlgorithm.GetResidueData(sequence, atomResidues, atomNames));

            needRecalculate = true;
        }

        private void CalculateSecondaryStructure()
        {
            foreach (var peptideSequence in sequenceResidueData)
                DsspAlgorithm.CalculateSecondaryStructure(peptideSequence, dsspOptions);

            residueSecondaryStructure.Resize(residueCount.Value);

            foreach (var sequence in sequenceResidueData)
            foreach (var data in sequence)
                residueSecondaryStructure.Value[data.ResidueIndex] = data.SecondaryStructure;

            residueSecondaryStructure.MarkValueAsChanged();
        }

        private void CalculateHydrogenBonds()
        {
            var bonds = new List<BondPair>();
            foreach (var sequence in sequenceResidueData)
            {
                foreach (var data in sequence)
                    if (data.DonorHydrogenBondResidue != null)
                        bonds.Add(new BondPair(data.OxygenIndex,
                                               data.DonorHydrogenBondResidue.NitrogenIndex));
            }

            hydrogenBonds.Value = bonds.ToArray();
        }

        private void UpdatePositions()
        {
            foreach (var t in sequenceResidueData)
                DsspAlgorithm.UpdateResidueAtomPositions(atomPositions.Value, t);

            needRecalculate = true;
        }
    }
}