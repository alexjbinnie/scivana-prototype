﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Components.Calculator;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Protein
{
    [Serializable]
    public class CalculateSolidPeptideBlocks : GenericOutputNode
    {
        private ArrayProperty<ProteinIndices> residueProteinIndices =
            new ArrayProperty<ProteinIndices>();

        public IProperty<ProteinIndices[]> ResidueProteinIndices => residueProteinIndices;

        [SerializeField]
        private ArrayProperty<Vector3> particlePositions = new ArrayProperty<Vector3>();

        [InputProperty(StandardProperties.ParticlePositions)]
        public IProperty<Vector3[]> ParticlePositions => particlePositions;

        [SerializeField]
        private ArrayProperty<UnityEngine.Color> particleColors =
            new ArrayProperty<UnityEngine.Color>();

        [InputProperty(StandardProperties.ParticleColors)]
        public IProperty<UnityEngine.Color[]> ParticleColors => particleColors;

        [SerializeField]
        private ArrayProperty<int> residueEntities = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ResidueEntities)]
        public IProperty<int[]> ResidueEntities => residueEntities;

        private ArrayProperty<Vector3> outputPositions = new ArrayProperty<Vector3>();

        public IReadOnlyProperty<Vector3[]> OutputPositions => outputPositions;

        private ArrayProperty<UnityEngine.Color> outputColors =
            new ArrayProperty<UnityEngine.Color>();

        public IReadOnlyProperty<UnityEngine.Color[]> OutputColors => outputColors;

        private ArrayProperty<BondPair> outputBonds = new ArrayProperty<BondPair>();

        public IReadOnlyProperty<BondPair[]> OutputBonds => outputBonds;

        private ArrayProperty<Cycle> outputCycles = new ArrayProperty<Cycle>();

        public IReadOnlyProperty<Cycle[]> OutputFaces => outputCycles;

        protected override bool IsInputValid => residueProteinIndices.HasValue
                                             && residueEntities.HasValue;

        protected override bool IsInputDirty => residueProteinIndices.IsDirty
                                             || residueEntities.IsDirty
                                             || particlePositions.IsDirty
                                             || particleColors.IsDirty;

        protected override void ClearDirty()
        {
            residueProteinIndices.IsDirty = false;
            residueEntities.IsDirty = false;
            particleColors.IsDirty = false;
            particlePositions.IsDirty = false;
        }

        private List<int[]> validQuads = new List<int[]>();

        protected override void UpdateOutput()
        {
            var regenerated = false;

            if (residueProteinIndices.IsDirty || residueEntities.IsDirty)
            {
                validQuads.Clear();

                var prev = -2;
                for (var i = 0; i < residueProteinIndices.Value.Length; i++)
                {
                    var info = residueProteinIndices.Value[i];
                    if (info.AlphaCarbonIndex >= 0 && info.HydrogenIndex >= 0)
                        if (prev == i - 1 &&
                            residueEntities.Value[prev] == residueEntities.Value[i])
                            validQuads.Add(new[]
                            {
                                residueProteinIndices.Value[prev].AlphaCarbonIndex,
                                residueProteinIndices.Value[prev].OxygenIndex,
                                residueProteinIndices.Value[i].HydrogenIndex,
                                residueProteinIndices.Value[i].AlphaCarbonIndex
                            });
                    if (info.AlphaCarbonIndex >= 0 && info.OxygenIndex >= 0)
                    {
                        prev = i;
                    }
                }

                outputPositions.Resize(validQuads.Count * 4);
                outputColors.Resize(validQuads.Count * 4);
                outputBonds.Resize(validQuads.Count * 5);
                outputCycles.Resize(validQuads.Count * 2);

                var b = 0;
                var p = 0;
                foreach (var quad in validQuads)
                {
                    outputBonds.Value[b++] = new BondPair(p, p + 1);
                    outputBonds.Value[b++] = new BondPair(p, p + 2);
                    outputBonds.Value[b++] = new BondPair(p, p + 3);
                    outputBonds.Value[b++] = new BondPair(p + 1, p + 3);
                    outputBonds.Value[b++] = new BondPair(p + 2, p + 3);
                    p += 4;
                }

                outputBonds.MarkValueAsChanged();

                var c = 0;
                p = 0;
                foreach (var quad in validQuads)
                {
                    outputCycles.Value[c++] = new Cycle(p, p + 1, p + 3);
                    outputCycles.Value[c++] = new Cycle(p, p + 2, p + 3);
                    p += 4;
                }

                outputCycles.MarkValueAsChanged();

                regenerated = true;
            }


            if (regenerated || particlePositions.IsDirty)
            {
                if (particlePositions.HasValue)
                {
                    var k = 0;
                    for (var i = 0; i < validQuads.Count; i++)
                    {
                        var quad = validQuads[i];
                        foreach (var j in quad)
                        {
                            outputPositions.Value[k] = particlePositions.Value[j];
                            k++;
                        }
                    }

                    outputPositions.MarkValueAsChanged();
                }
                else
                {
                    outputPositions.UndefineValue();
                }
            }

            if (regenerated || particleColors.IsDirty)
            {
                if (particleColors.HasValue)
                {
                    var k = 0;
                    for (var i = 0; i < validQuads.Count; i++)
                    {
                        var quad = validQuads[i];
                        foreach (var j in quad)
                        {
                            outputColors.Value[k] = particleColors.Value[j];
                            k++;
                        }
                    }

                    outputColors.MarkValueAsChanged();
                }
                else
                {
                    outputColors.UndefineValue();
                }
            }
        }

        protected override void ClearOutput()
        {
            outputPositions.UndefineValue();
            outputColors.UndefineValue();
            outputBonds.UndefineValue();
            outputCycles.UndefineValue();
        }
    }
}