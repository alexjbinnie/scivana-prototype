/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace Scivana.Visualisation.Node.Protein
{
    /// <summary>
    /// Utility class for storing information about protein residues for use with DSSP.
    /// </summary>
    public class SecondaryStructureResidueData
    {
        /// <summary>
        /// Index of the alpha carbon.
        /// </summary>
        public int AlphaCarbonIndex { get; set; }

        /// <summary>
        /// Position of the alpha carbon.
        /// </summary>
        public Vector3 AlphaCarbonPosition { get; set; }

        /// <summary>
        /// Index of the carbonyl carbon.
        /// </summary>
        public int CarbonIndex { get; set; }

        /// <summary>
        /// Position of the carbonyl carbon.
        /// </summary>
        public Vector3 CarbonPosition { get; set; }

        /// <summary>
        /// Index of the amine hydrogen.
        /// </summary>
        public int HydrogenIndex { get; set; }

        /// <summary>
        /// Position of the amine hydrogen.
        /// </summary>
        public Vector3 HydrogenPosition { get; set; }

        /// <summary>
        /// Index of the amine nitrogen.
        /// </summary>
        public int NitrogenIndex { get; set; }

        /// <summary>
        /// Position of the amine nitrogen.
        /// </summary>
        public Vector3 NitrogenPosition { get; set; }

        /// <summary>
        /// Index of the carbonyl oxygen.
        /// </summary>
        public int OxygenIndex { get; set; }

        /// <summary>
        /// Position of the carbonyl oxygen.
        /// </summary>
        public Vector3 OxygenPosition { get; set; }

        /// <summary>
        /// Secondary structure patterns that this residue exhibits.
        /// </summary>
        public SecondaryStructurePattern Pattern = SecondaryStructurePattern.None;

        /// <summary>
        /// Assignment for this residue's secondary structure.
        /// </summary>
        public SecondaryStructureAssignment SecondaryStructure { get; set; }

        /// <summary>
        /// The lowest energy hydrogen bond formed by this residue's carbonyl.
        /// </summary>
        public double AcceptorHydrogenBondEnergy { get; set; } = 1e10;

        /// <summary>
        /// The lowest energy hydrogen bond formed by this residue's amine.
        /// </summary>
        public double DonorHydrogenBondEnergy { get; set; } = 1e10;

        /// <summary>
        /// The residue to which this carbonyl is bonded to.
        /// </summary>
        public SecondaryStructureResidueData AcceptorHydrogenBondResidue { get; set; }

        /// <summary>
        /// The residue to which this amine is bonded to.
        /// </summary>
        public SecondaryStructureResidueData DonorHydrogenBondResidue { get; set; }

        /// <summary>
        /// The ordinal of this residue in the sequence.
        /// </summary>
        public int ordinal { get; set; }

        /// <summary>
        /// The index of this residue in the system.
        /// </summary>
        public int ResidueIndex { get; set; }
    }
}