﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Protein
{
    [Serializable]
    public class CalculatePeptidePlane : GenericOutputNode
    {
        [SerializeField]
        private ArrayProperty<int> particleResidues = new ArrayProperty<int>();

        [SerializeField]
        private ArrayProperty<string> particleNames = new ArrayProperty<string>();

        [SerializeField]
        private SerializableProperty<int> residueCount = new SerializableProperty<int>();

        private ArrayProperty<ProteinIndices> residueProteinIndices = new ArrayProperty<ProteinIndices>();

        [InputProperty(StandardProperties.ParticleResidues)]
        public IProperty<int[]> ParticleResidues => particleResidues;

        [InputProperty(StandardProperties.ParticleNames)]
        public IProperty<string[]> ParticleNames => particleNames;

        [InputProperty(StandardProperties.ResidueCount)]
        public IProperty<int> ResidueCount => residueCount;

        [OutputProperty(StandardProperties.ResidueProteinIndices)]
        public IReadOnlyProperty<ProteinIndices[]> ResidueProteinIndices => residueProteinIndices;

        private ProteinIndices[] residueInfo = new ProteinIndices[0];

        protected override bool IsInputValid => particleResidues.HasNonEmptyValue()
                                             && particleNames.HasNonEmptyValue()
                                             && residueCount.HasNonNullValue();

        protected override bool IsInputDirty => particleResidues.IsDirty
                                             || particleNames.IsDirty
                                             || residueCount.IsDirty;

        protected override void ClearDirty()
        {
            particleNames.IsDirty = false;
            particleResidues.IsDirty = false;
            residueCount.IsDirty = false;
        }

        protected override void UpdateOutput()
        {
            Array.Resize(ref residueInfo, residueCount.Value);
            for (var i = 0; i < residueCount.Value; i++)
            {
                residueInfo[i].AlphaCarbonIndex = -1;
                residueInfo[i].CarbonIndex = -1;
                residueInfo[i].OxygenIndex = -1;
                residueInfo[i].NitrogenIndex = -1;
                residueInfo[i].HydrogenIndex = -1;
            }

            for (var i = 0; i < particleResidues.Value.Length; i++)
            {
                var resId = particleResidues.Value[i];
                var particleName = particleNames.Value[i];
                switch (particleName)
                {
                    case "CA":
                        residueInfo[resId].AlphaCarbonIndex = i;
                        break;
                    case "C":
                        residueInfo[resId].CarbonIndex = i;
                        break;
                    case "O":
                        residueInfo[resId].OxygenIndex = i;
                        break;
                    case "N":
                        residueInfo[resId].NitrogenIndex = i;
                        break;
                    case "H":
                        residueInfo[resId].HydrogenIndex = i;
                        break;
                }
            }

            residueProteinIndices.Value = residueInfo;
        }

        protected override void ClearOutput()
        {
            residueProteinIndices.UndefineValue();
        }
    }
}