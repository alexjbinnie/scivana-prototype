/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using JetBrains.Annotations;
using Scivana.Core.Properties;
using Scivana.Visualisation.Node.Adaptor;
using Scivana.Visualisation.Node.Sequence;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace Scivana.Visualisation.Node.Protein
{
    /// <summary>
    /// A calculator which finds polypeptide sequences and performs secondary structure
    /// calculations on them.
    /// </summary>
    [Serializable]
    public class CalculateSecondaryStructure : VisualisationNode
    {
        internal CalculatePolypeptideSequences polypeptideSequence = new CalculatePolypeptideSequences();
        internal DsspCalculatorNode secondaryStructure = new DsspCalculatorNode();

        public IReadOnlyProperty<SecondaryStructureAssignment[]> Assignments => assignments;

        [NotNull]
        private ArrayProperty<SecondaryStructureAssignment> assignments
            = new ArrayProperty<SecondaryStructureAssignment>();

        [SerializeField]
        private DsspOptions options = new DsspOptions();

        public void LinkToAdaptor(ParentedAdaptorNode adaptor)
        {
            polypeptideSequence.AtomNames.LinkedProperty = adaptor.ParticleNames;
            secondaryStructure.AtomPositions.LinkedProperty = adaptor.ParticlePositions;
            polypeptideSequence.AtomResidues.LinkedProperty = adaptor.ParticleResidues;
            polypeptideSequence.ResidueNames.LinkedProperty = adaptor.ResidueNames;
            polypeptideSequence.ResidueEntities.LinkedProperty = adaptor.ResidueEntities;
            secondaryStructure.ResidueCount.LinkedProperty = adaptor.ResidueCount;

            secondaryStructure.AtomNames.LinkedProperty = polypeptideSequence.AtomNames;
            secondaryStructure.AtomResidues.LinkedProperty = polypeptideSequence.AtomResidues;
            secondaryStructure.PeptideResidueSequences.LinkedProperty =
                polypeptideSequence.ResidueSequences;
            assignments.LinkedProperty = secondaryStructure.ResidueSecondaryStructure;

            secondaryStructure.DsspOptions = options;

            var overrideProperty = adaptor.AddOverrideProperty<SecondaryStructureAssignment[]>(
                VisualiserFactory.ResidueSecondaryStructure.Key);
            overrideProperty.LinkedProperty = assignments;
        }

        /// <inheritdoc cref="BaseAdaptorNode.Refresh" />
        public override void Refresh()
        {
            polypeptideSequence.Refresh();
            secondaryStructure.Refresh();
        }
    }
}