/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace Scivana.Visualisation.Node.Protein
{
    /// <summary>
    /// Patterns that occur in protein secondary structure.
    /// </summary>
    [Flags]
    public enum SecondaryStructurePattern
    {
        None = 0x0,
        ThreeTurn = 0x1,
        FourTurn = 0x2,
        FiveTurn = 0x4,
        ParallelBridge = 0x8,
        AntiparallelBridge = 0x10,
        Bridge = ParallelBridge | AntiparallelBridge,
        Turn = ThreeTurn | FourTurn | FiveTurn
    }
}