﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Narupa.Protocol.Trajectory;

namespace Scivana.Visualisation.Node
{
    public static class StandardProperties
    {
        public const string HighlightedParticles = "highlighted.particles";
        public const string HighlightColor = "highlighted.color";

        public const string BondPairs = FrameData.BondArrayKey;

        public const string BondOrders = FrameData.BondOrderArrayKey;

        public const string EntityCount = FrameData.ChainCountValueKey;

        public const string EntityNames = FrameData.ChainNameArrayKey;

        public const string KineticEnergy = FrameData.KineticEnergyValueKey;

        public const string ParticleCount = FrameData.ParticleCountValueKey;

        public const string ParticleElements = FrameData.ParticleElementArrayKey;

        public const string ParticleNames = FrameData.ParticleNameArrayKey;

        public const string ParticleColors = "particle.colors";

        public const string ParticleWidths = "particle.widths";

        public const string ParticlePositions = FrameData.ParticlePositionArrayKey;

        public const string ParticleResidues = FrameData.ParticleResidueArrayKey;

        public const string ParticleTypes = FrameData.ParticleTypeArrayKey;

        public const string PotentialEnergy = FrameData.PotentialEnergyValueKey;

        public const string ResidueEntities = FrameData.ResidueChainArrayKey;

        public const string ResidueCount = FrameData.ResidueCountValueKey;

        public const string ResidueNames = FrameData.ResidueNameArrayKey;

        public const string ResidueProteinIndices = "residue.protein_indices";

        public const string ParticleScales = "particle.scales";

        public const string ResidueSecondaryStructures = "residue.secondarystructures";

        public const string BoxTransformation = "system.box.vectors";

        public const string Scheme = "scheme";

        public const string Gradient = "gradient";

        public const string Input = "input";

        public const string Maximum = "maximum";

        public const string Minimum = "minimum";

        public const string Scale = "scale";

        public const string Width = "width";

        public const string Speed = "speed";

        public const string Output = "output";

        public const string Cycles = "cycles";

        public const string CyclesCount = "cycles.count";

        public const string SequenceParticles = "sequences.particles";

        public const string Color = "color";

        public const string Mesh = "mesh";

        public const string Material = "material";

        public const string Parent = "parent";

        public const string BondScale = "bond.scale";

        public const string ParticleScale = "particle.scale";

        public const string SplineSegments = "spline_segments";
    }
}