/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Node.Protein;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Color
{
    [Serializable]
    [ColorSubgraph("secondary structure")]
    public class ColorBySecondaryStructure : ParticleColorNode
    {
        [SerializeField]
        private ArrayProperty<SecondaryStructureAssignment> residueSecondaryStructure =
            new ArrayProperty<SecondaryStructureAssignment>();

        [InputProperty(StandardProperties.ResidueSecondaryStructures)]
        public IProperty<SecondaryStructureAssignment[]> ResidueSecondaryStructure =>
            residueSecondaryStructure;

        [SerializeField]
        private ArrayProperty<int> particleResidues = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ParticleResidues)]
        public IProperty<int[]> ParticleResidues => particleResidues;

        [SerializeField]
        private InterfaceProperty<IMapping<SecondaryStructureAssignment, UnityEngine.Color>> scheme
            = new InterfaceProperty<IMapping<SecondaryStructureAssignment, UnityEngine.Color>>();

        [InputProperty(StandardProperties.Scheme, Local = true)]
        [DefaultPropertyValue("rcsb secondary structure")]
        public IProperty<IMapping<SecondaryStructureAssignment, UnityEngine.Color>> Scheme =>
            scheme;

        protected override bool IsInputValid => residueSecondaryStructure.HasNonEmptyValue()
                                             && particleResidues.HasNonNullValue();

        protected override bool IsInputDirty => residueSecondaryStructure.IsDirty
                                             || particleResidues.IsDirty;

        protected override void ClearDirty()
        {
            residueSecondaryStructure.IsDirty = false;
            particleResidues.IsDirty = false;
        }

        protected override void ClearOutput()
        {
            colors.UndefineValue();
        }

        protected override void UpdateOutput()
        {
            var secondaryStructure = residueSecondaryStructure.Value;
            var residues = particleResidues.Value;
            var colorArray = colors.HasValue ? colors.Value : new UnityEngine.Color[0];
            Array.Resize(ref colorArray, residues.Length);
            for (var i = 0; i < residues.Length; i++)
                colorArray[i] = scheme.Value.Map(secondaryStructure[residues[i]]);
            colors.Value = colorArray;
        }
    }
}