/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Color
{
    /// <summary>
    /// Base code for a Visualiser node which generates colors based upon atomic
    /// elements.
    /// </summary>
    [Serializable]
    [ColorSubgraph("goodsell")]
    public class GoodsellColorByEntity : ParticleColorNode
    {
        [SerializeField]
        private ArrayProperty<Element> elements = new ArrayProperty<Element>();

        [InputProperty(StandardProperties.ParticleElements)]
        public IProperty<Element[]> Elements => elements;

        [SerializeField]
        private ArrayProperty<int> particleResidues = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ParticleResidues)]
        public IProperty<int[]> ParticleResidues => particleResidues;

        [SerializeField]
        private ArrayProperty<int> residueEntities = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ResidueEntities)]
        public IProperty<int[]> ResidueEntities => residueEntities;

        [SerializeField]
        private ArrayProperty<string> residueNames = new ArrayProperty<string>();

        [InputProperty(StandardProperties.ResidueNames)]
        public IProperty<string[]> ResidueNames => residueNames;

        [SerializeField]
        private UnityEngine.Color waterColor = new UnityEngine.Color(1f, 0.5f, 0.5f);

        [SerializeField]
        private InterfaceProperty<IReadOnlyList<UnityEngine.Color>> scheme
            = new InterfaceProperty<IReadOnlyList<UnityEngine.Color>>();

        [InputProperty(StandardProperties.Scheme)]
        [DefaultPropertyValue("goodsell")]
        public IProperty<IReadOnlyList<UnityEngine.Color>> Scheme => scheme;

        protected override bool IsInputDirty => elements.IsDirty
                                             || particleResidues.IsDirty
                                             || residueEntities.IsDirty
                                             || residueNames.IsDirty
                                             || scheme.IsDirty;

        protected override bool IsInputValid => elements.HasNonEmptyValue()
                                             && particleResidues.HasNonEmptyValue()
                                             && residueEntities.HasNonEmptyValue()
                                             && residueNames.HasNonEmptyValue()
                                             && scheme.HasNonNullValue();

        protected override void UpdateOutput()
        {
            var colorArray = colors.HasNonNullValue() ? colors.Value : new UnityEngine.Color[0];
            Array.Resize(ref colorArray, elements.Value.Length);

            var elementArray = elements.Value;
            var residueArray = particleResidues.Value;
            var entityArray = residueEntities.Value;
            var nameArray = residueNames.Value;

            for (var i = 0; i < elements.Value.Length; i++)
            {
                var resid = residueArray[i];
                colorArray[i] = GetColor(elementArray[i], entityArray[resid], nameArray[resid]);
            }

            colors.Value = colorArray;
        }

        protected override void ClearOutput()
        {
            colors.UndefineValue();
        }

        protected override void ClearDirty()
        {
            elements.IsDirty = false;
            particleResidues.IsDirty = false;
            residueEntities.IsDirty = false;
            residueNames.IsDirty = false;
        }

        private UnityEngine.Color GetColor(Element element, int entityId, string resname)
        {
            if (resname == "HOH")
                return waterColor;
            var i = entityId % scheme.Value.Count;
            var color = scheme.Value[i];
            if (element == Element.Carbon)
            {
                var max = Mathf.Max(color.r, color.g, color.b);
                if (color.r < max)
                    color.r += 0.1f;
                if (color.g < max)
                    color.g += 0.1f;
                if (color.b < max)
                    color.b += 0.1f;
            }

            return color;
        }
    }
}