﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using JetBrains.Annotations;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Color
{
    /// <summary>
    /// Applies an oscillating color change to a subset of particles.
    /// </summary>
    [Serializable]
    [ColorSubgraph("color pulser")]
    public class ColorPulserNode : VisualisationNode
    {
        [SerializeField]
        private ArrayProperty<UnityEngine.Color> inputColors
            = new ArrayProperty<UnityEngine.Color>();

        [UsedImplicitly]
        [InputProperty(StandardProperties.ParticleColors)]
        public IProperty<UnityEngine.Color[]> InputColors => inputColors;

        private ArrayProperty<UnityEngine.Color> outputColors
            = new ArrayProperty<UnityEngine.Color>();

        [UsedImplicitly]
        [OutputProperty(StandardProperties.ParticleColors)]
        public IReadOnlyProperty<UnityEngine.Color[]> OutputColors => outputColors;

        [SerializeField]
        private ArrayProperty<int> highlightFilter = new ArrayProperty<int>();

        [UsedImplicitly]
        [InputProperty(StandardProperties.HighlightedParticles)]
        public IProperty<int[]> HighlightedParticles => highlightFilter;

        [SerializeField]
        private SerializableProperty<UnityEngine.Color> highlightColor = new SerializableProperty<UnityEngine.Color>();

        [UsedImplicitly]
        [InputProperty(StandardProperties.HighlightColor)]
        public IProperty<UnityEngine.Color> HighlightColor => highlightColor;

        private UnityEngine.Color[] cachedArray = new UnityEngine.Color[0];

        private float speed = 6f;

        private float maximum = 0.6f;

        private float minimum = 0f;

        public override void Refresh()
        {
            if (inputColors.IsDirty)
            {
                if (inputColors.HasValue)
                {
                    Array.Resize(ref cachedArray, inputColors.Value.Length);
                    Array.Copy(inputColors.Value, cachedArray, inputColors.Value.Length);
                    outputColors.Resize(cachedArray.Length);
                }
                else
                {
                    outputColors.UndefineValue();
                }
            }

            if (inputColors.HasValue)
            {
                if (inputColors.IsDirty || highlightFilter.IsDirty ||
                    highlightFilter.HasNullValue() ||
                    highlightFilter.HasNonEmptyValue())
                {
                    var intensity = Mathf.Lerp(minimum, maximum,
                                               0.5f + 0.5f * Mathf.Sin(speed * Time.time));

                    Array.Copy(cachedArray, outputColors.Value, cachedArray.Length);

                    var highlightColor = this.highlightColor.HasValue
                                             ? this.highlightColor.Value
                                             : UnityEngine.Color.white;

                    if (highlightFilter.HasNonEmptyValue())
                    {
                        var filter = highlightFilter.Value;
                        foreach (var i in filter)
                        {
                            outputColors.Value[i] += intensity * highlightColor;
                        }
                    }
                    else if (highlightFilter.HasNullValue())
                    {
                        for (var i = 0; i < outputColors.Value.Length; i++)
                        {
                            outputColors.Value[i] += intensity * highlightColor;
                        }
                    }

                    outputColors.MarkValueAsChanged();
                }
            }

            highlightFilter.IsDirty = false;
            inputColors.IsDirty = false;
        }
    }
}