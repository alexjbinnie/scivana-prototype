/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Color
{
    /// <summary>
    /// Visualiser node which generates colors based upon residue names.
    /// </summary>
    [Serializable]
    [ColorSubgraph("residue name")]
    public class ColorByResidueName : ParticleColorNode
    {
        [SerializeField]
        private ArrayProperty<string> residueNames = new ArrayProperty<string>();

        [InputProperty(StandardProperties.ResidueNames)]
        public IProperty<string[]> ResidueNames => residueNames;

        [SerializeField]
        private ArrayProperty<int> particleResidues = new ArrayProperty<int>();

        [InputProperty(StandardProperties.ParticleResidues)]
        public IProperty<int[]> ParticleResidues => particleResidues;

        [SerializeField]
        private InterfaceProperty<IMapping<string, UnityEngine.Color>> mapping
            = new InterfaceProperty<IMapping<string, UnityEngine.Color>>();

        [InputProperty(StandardProperties.Scheme)]
        [DefaultPropertyValue("rcsb residue names")]
        public IProperty<IMapping<string, UnityEngine.Color>> Scheme => mapping;

        protected override bool IsInputValid => residueNames.HasNonEmptyValue()
                                             && particleResidues.HasNonEmptyValue()
                                             && mapping.HasNonNullValue();

        protected override bool IsInputDirty => residueNames.IsDirty
                                             || particleResidues.IsDirty
                                             || mapping.IsDirty;

        protected override void ClearDirty()
        {
            residueNames.IsDirty = false;
            particleResidues.IsDirty = false;
            mapping.IsDirty = false;
        }

        protected override void UpdateOutput()
        {
            var particleResidues = this.particleResidues.Value;
            var residueNames = this.residueNames.Value;
            var colorArray = colors.HasValue ? colors.Value : new UnityEngine.Color[0];
            Array.Resize(ref colorArray, particleResidues.Length);
            for (var i = 0; i < particleResidues.Length; i++)
                colorArray[i] = mapping.Value.Map(residueNames[particleResidues[i]]);
            colors.Value = colorArray;
        }

        protected override void ClearOutput()
        {
            colors.UndefineValue();
        }
    }
}