/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Color
{
    [Serializable]
    public abstract class ColorGradient : ParticleColorNode
    {
        [SerializeField]
        private SerializableProperty<Gradient> gradient = new SerializableProperty<Gradient>
        {
            Value = new Gradient
            {
                colorKeys = new[]
                {
                    new GradientColorKey(UnityEngine.Color.red, 0),
                    new GradientColorKey(UnityEngine.Color.green, 0.5f),
                    new GradientColorKey(UnityEngine.Color.blue, 1f)
                }
            }
        };

        [InputProperty(StandardProperties.Gradient)]
        public IProperty<Gradient> Gradient => gradient;

        protected override bool IsInputValid => gradient.HasNonNullValue();

        protected override bool IsInputDirty => gradient.IsDirty;

        protected override void ClearDirty()
        {
            gradient.IsDirty = false;
        }

        private float[] cachedValues = null;

        protected override void UpdateOutput()
        {
            var array = colors.HasValue ? colors.Value : new UnityEngine.Color[0];

            cachedValues = cachedValues ?? new float[0];
            GetValues(ref cachedValues);
            var gradient = this.gradient.Value;
            var count = cachedValues.Length;
            Array.Resize(ref array, count);
            for (var i = 0; i < count; i++)
                array[i] = gradient.Evaluate(cachedValues[i]);
            colors.Value = array;
        }

        public abstract void GetValues(ref float[] values);

        protected override void ClearOutput()
        {
            colors.UndefineValue();
        }
    }
}