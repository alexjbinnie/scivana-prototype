/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Data;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Color
{
    /// <summary>
    /// Visualiser node that generates atomic colors from atomic positions, based
    /// upon an <see cref="ElementColorMapping" />
    /// </summary>
    [Serializable]
    [ColorSubgraph("particle element", "cpk", "element")]
    public class ColorByElementMapping : ColorByElement
    {
        [SerializeField]
        private InterfaceProperty<IMapping<Element, UnityEngine.Color>> mapping
            = new InterfaceProperty<IMapping<Element, UnityEngine.Color>>();

        /// <summary>
        /// The color mapping between elements and colors.
        /// </summary>
        [InputProperty(StandardProperties.Scheme)]
        [DefaultPropertyValue("narupa element")]
        public IProperty<IMapping<Element, UnityEngine.Color>> Mapping => mapping;

        /// <inheritdoc cref="GenericOutputNode.IsInputDirty" />
        protected override bool IsInputDirty => base.IsInputDirty || mapping.IsDirty;

        /// <inheritdoc cref="GenericOutputNode.IsInputValid" />
        protected override bool IsInputValid => base.IsInputValid && mapping.HasNonNullValue();

        /// <inheritdoc cref="GenericOutputNode.ClearDirty" />
        protected override void ClearDirty()
        {
            base.ClearDirty();
            mapping.IsDirty = false;
        }

        /// <inheritdoc cref="ColorByElement.GetColor" />
        protected override UnityEngine.Color GetColor(Element element)
        {
            return mapping.HasNonNullValue()
                       ? mapping.Value.Map(element)
                       : UnityEngine.Color.white;
        }
    }
}