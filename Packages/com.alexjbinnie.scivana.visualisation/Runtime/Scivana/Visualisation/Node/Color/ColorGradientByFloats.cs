/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Node.Color
{
    [Serializable]
    [ColorSubgraph("float data gradient")]
    public class ColorGradientByFloats : ColorGradient
    {
        [SerializeField]
        private ArrayProperty<float> values = new ArrayProperty<float>();

        [InputProperty(StandardProperties.Input)]
        public IProperty<float[]> Values => values;

        [SerializeField]
        private SerializableProperty<float> minimum = new SerializableProperty<float>
        {
            Value = 0
        };

        [InputProperty(StandardProperties.Minimum)]
        public IProperty<float> Minimum => minimum;

        [SerializeField]
        private SerializableProperty<float> maximum = new SerializableProperty<float>
        {
            Value = 1
        };

        [InputProperty(StandardProperties.Maximum)]
        public IProperty<float> Maximum => maximum;

        protected override bool IsInputValid => base.IsInputValid &&
                                                values.HasNonEmptyValue() &&
                                                minimum.HasNonNullValue() &&
                                                maximum.HasNonNullValue();

        protected override bool IsInputDirty => base.IsInputDirty
                                             || values.IsDirty
                                             || minimum.IsDirty
                                             || maximum.IsDirty;

        protected override void ClearDirty()
        {
            base.ClearDirty();
            values.IsDirty = false;
            minimum.IsDirty = false;
            maximum.IsDirty = false;
        }

        public override void GetValues(ref float[] values)
        {
            values = this.values.Value;
        }
    }
}