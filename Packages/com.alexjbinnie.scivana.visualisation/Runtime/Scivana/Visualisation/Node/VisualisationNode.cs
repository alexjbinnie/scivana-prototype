﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Scivana.Core.Properties;
using Scivana.Visualisation.Attributes;
using Scivana.Visualisation.Property;
using UnityEngine;
using UnityEngine.Rendering;

namespace Scivana.Visualisation.Node
{
    // TODO: Rename to get rid of interface prefix.

    public abstract class VisualisationNode
    {
        public abstract void Refresh();

        protected VisualisationNode()
        {
            foreach (var (prop, value) in GetDefaultValues())
            {
                if (!prop.HasValue)
                    VisualiserFactory.SetPropertyValue(prop, value);
            }
        }

        public bool HasInputProperty(string name)
        {
            return GetInputProperty(name) != null;
        }

        public IReadOnlyProperty GetOutputProperty(string name)
        {
            return GetOutputProperties().FirstOrDefault(i => i.Name == name).Property;
        }

        public IProperty GetInputProperty(string name)
        {
            return GetInputProperties().FirstOrDefault(i => i.Name == name).Property;
        }

        public virtual IEnumerable<(string Name, IProperty Property, PropertyAttributes Attributes)>
            GetInputProperties()
        {
            foreach (var property in GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (property.GetCustomAttribute<InputPropertyAttribute>() is InputPropertyAttribute
                        input && property.GetValue(this) is IProperty prop)
                {
                    yield return (input.Key, prop, new PropertyAttributes()
                                     {
                                         SearchInPreviousSubgraphs = !input.Local
                                     });
                }
            }
        }

        public virtual IEnumerable<(string Name, IReadOnlyProperty Property)> GetOutputProperties(
            bool includeDynamic = false)
        {
            foreach (var property in GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (property.GetCustomAttribute<OutputPropertyAttribute>() is OutputPropertyAttribute
                        input && property.GetValue(this) is IProperty prop)
                    yield return (input.Key, prop);
            }

            if (includeDynamic)
            {
                if (this is IDynamicPropertyProvider dynamic)
                {
                    foreach (var (name, prop) in dynamic.GetProperties())
                        yield return (name, prop);
                }
            }
        }

        /// <summary>
        /// Get the set of default values defined on these properties.
        /// </summary>
        private IEnumerable<(IProperty Property, object Value)> GetDefaultValues()
        {
            foreach (var property in GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (property.GetCustomAttribute<DefaultPropertyValueAttribute>() is { } input &&
                    property.GetValue(this) is IProperty prop)
                    yield return (prop, input.Value);
            }
        }

        public IProperty<IDynamicPropertyProvider> GetDynamicInputProperty()
        {
            foreach (var property in GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (property.GetCustomAttribute<InputPropertyAttribute>() is InputPropertyAttribute
                        input &&
                    property.GetValue(this) is IProperty<IDynamicPropertyProvider> prop)
                    return prop;
            }

            return null;
        }

        public virtual void AddToCommandBuffer(CommandBuffer buffer)
        {
        }

        public virtual Transform Transform { get; set; }

        public virtual IDynamicPropertyProvider GetDynamicPropertyProvider()
        {
            return this as IDynamicPropertyProvider;
        }

        public virtual IEnumerable<(CameraEvent, CommandBuffer)> GetCommandBuffers()
        {
            yield break;
        }
    }
}