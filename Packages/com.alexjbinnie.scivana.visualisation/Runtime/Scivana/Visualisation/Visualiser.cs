﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Node;
using Scivana.Visualisation.Node.Adaptor;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation
{
    public class Visualiser : MonoBehaviour
    {
        [SerializeField]
        private CommandBufferRenderer commandBufferRenderer;

        internal VisualisationNode[] subgraphs = new VisualisationNode[0];

        protected VisualisationNode[] Subgraphs => subgraphs;

        public void Setup(params VisualisationNode[] subgraphs)
        {
            this.subgraphs = subgraphs;

            if (commandBufferRenderer == null)
                commandBufferRenderer = gameObject.GetComponent<CommandBufferRenderer>();
            if (commandBufferRenderer == null)
                commandBufferRenderer = gameObject.AddComponent<CommandBufferRenderer>();

            ParentAdaptor = GetAdaptor(subgraphs).ParentAdaptor;
            ParticleFilter = GetFilter(subgraphs).ParticleFilter;

            OnEnable();
        }

        protected static ParticleFilteredAdaptorNode GetFilter(IEnumerable<VisualisationNode> nodes)
        {
            return (nodes.FirstOrDefault(subgraph => subgraph is ParticleFilteredAdaptorNode) as
                        ParticleFilteredAdaptorNode);
        }

        protected static ParentedAdaptorNode GetAdaptor(IEnumerable<VisualisationNode> nodes)
        {
            return (nodes.FirstOrDefault(subgraph => subgraph is ParentedAdaptorNode) as
                        ParentedAdaptorNode);
        }

        protected virtual void OnEnable()
        {
            foreach (var subgraph in subgraphs)
            {
                subgraph.Transform = transform;
                foreach (var (evnt, buffer) in subgraph.GetCommandBuffers())
                {
                    commandBufferRenderer.AddCommandBuffer(evnt, buffer);
                }
            }

            foreach (var subgraph in subgraphs)
                subgraph.Refresh();
        }

        public IProperty<int[]> ParticleFilter { get; private set; }

        public IProperty<IDynamicPropertyProvider> ParentAdaptor { get; private set; }

        private void OnDisable()
        {
            foreach (var subgraph in subgraphs)
            {
                foreach (var (evnt, buffer) in subgraph.GetCommandBuffers())
                {
                    subgraph.Transform = transform;
                    commandBufferRenderer.RemoveCommandBuffer(evnt, buffer);
                }

                if (subgraph is IDisposable disposable)
                    disposable.Dispose();
            }
        }

        public void Update()
        {
            foreach (var subgraph in subgraphs)
                subgraph.Refresh();
        }

        private void OnDestroy()
        {
            ParticleFilter.LinkedProperty = null;
            ParentAdaptor.LinkedProperty = null;

            foreach (var subgraph in subgraphs)
            {
                if (subgraph is IDisposable disposable)
                    disposable.Dispose();
            }
        }
    }
}