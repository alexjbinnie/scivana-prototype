/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Narupa.Protocol.Trajectory;
using NSubstitute;
using NUnit.Framework;
using Scivana.Core.Science;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using Scivana.Visualisation.Node.Adaptor;
using UnityEngine;

namespace Scivana.Visualisation.Tests.Node.Adaptor
{
    public class FrameAdaptorCallbackTests
    {
        private FrameSnapshot source;

        private FrameSourceAdaptorNode adaptor;

        private Action positionsChangedHandler;
        private Action elementsChangedHandler;
        private Action bondsChangedHandler;

        [SetUp]
        public void Setup()
        {
            source = new FrameSnapshot();
            adaptor = new FrameSourceAdaptorNode
            {
                FrameSource = source
            };
            positionsChangedHandler = Substitute.For<Action>();
            adaptor.ParticlePositions.ValueChanged += positionsChangedHandler;
            elementsChangedHandler = Substitute.For<Action>();
            adaptor.ParticleElements.ValueChanged += elementsChangedHandler;
            bondsChangedHandler = Substitute.For<Action>();
            adaptor.BondPairs.ValueChanged += bondsChangedHandler;
        }

        [Test]
        public void PositionUpdated()
        {
            var positionArray = new[]
            {
                Vector3.up, Vector3.down
            };
            source.Update(new Frame
                          {
                              ParticlePositions = positionArray
                          },
                          FrameChanges.WithChanges(FrameData.ParticlePositionArrayKey)
            );

            positionsChangedHandler.ReceivedWithAnyArgs(1).Invoke();
            elementsChangedHandler.ReceivedWithAnyArgs(0).Invoke();
            bondsChangedHandler.ReceivedWithAnyArgs(0).Invoke();

            CollectionAssert.AreEqual(positionArray, adaptor.ParticlePositions.Value);
        }

        [Test]
        public void ElementsUpdated()
        {
            var elementArray = new[]
            {
                Element.Hydrogen, Element.Carbon
            };
            source.Update(new Frame
                          {
                              ParticleElements = elementArray
                          },
                          FrameChanges.WithChanges(FrameData.ParticleElementArrayKey)
            );

            positionsChangedHandler.ReceivedWithAnyArgs(0).Invoke();
            elementsChangedHandler.ReceivedWithAnyArgs(1).Invoke();
            bondsChangedHandler.ReceivedWithAnyArgs(0).Invoke();

            CollectionAssert.AreEqual(elementArray, adaptor.ParticleElements.Value);
        }

        [Test]
        public void BondsUpdated()
        {
            var bondArray = new[]
            {
                new BondPair(0, 1), new BondPair(1, 2)
            };
            source.Update(new Frame
                          {
                              BondPairs = bondArray
                          },
                          FrameChanges.WithChanges(FrameData.BondArrayKey)
            );

            positionsChangedHandler.ReceivedWithAnyArgs(0).Invoke();
            elementsChangedHandler.ReceivedWithAnyArgs(0).Invoke();
            bondsChangedHandler.ReceivedWithAnyArgs(1).Invoke();

            CollectionAssert.AreEqual(bondArray, adaptor.BondPairs.Value);
        }
    }
}