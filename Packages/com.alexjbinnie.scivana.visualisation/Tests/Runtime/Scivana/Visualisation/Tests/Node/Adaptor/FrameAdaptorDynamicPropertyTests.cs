/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Properties;
using Scivana.Visualisation.Node.Adaptor;

namespace Scivana.Visualisation.Tests.Node.Adaptor
{
    public class FrameAdaptorDynamicPropertyTests
    {
        [Test]
        public void InitialNoProperties()
        {
            var adaptor = new FrameSourceAdaptorNode();

            CollectionAssert.IsEmpty(adaptor.GetProperties());
        }

        [Test]
        public void GetExistingNullProperty()
        {
            var adaptor = new FrameSourceAdaptorNode();

            Assert.IsNull(adaptor.GetProperty("missing.property"));
        }

        [Test]
        public void GetExistingProperty_WrongName()
        {
            var adaptor = new FrameSourceAdaptorNode();

            var property = adaptor.GetOrCreateProperty<int>("property");

            Assert.IsNull(adaptor.GetProperty("missing.property"));
        }

        [Test]
        public void GetExistingProperty()
        {
            var adaptor = new FrameSourceAdaptorNode();

            var property = adaptor.GetOrCreateProperty<int>("property");

            Assert.IsNotNull(property);
            Assert.AreEqual(property, adaptor.GetProperty("property"));
        }

        [Test]
        public void GetExistingProperties()
        {
            var adaptor = new FrameSourceAdaptorNode();

            var property1 = adaptor.GetOrCreateProperty<int>("property1");
            var property2 = adaptor.GetOrCreateProperty<bool>("property2");

            var expected = new (string, IReadOnlyProperty)[]
            {
                ("property1", property1), ("property2", property2)
            };

            CollectionAssert.AreEquivalent(expected, adaptor.GetProperties());
        }
    }
}