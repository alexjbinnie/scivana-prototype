/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Properties;
using Scivana.Core.Science;
using Scivana.Visualisation.Node.Scale;
using Scivana.Visualisation.Properties;

namespace Scivana.Visualisation.Tests.Node.Scale
{
    public class VdwScaleTests
    {
        private Element[] ValidElements =
        {
            Element.Carbon, Element.Hydrogen
        };

        [Test]
        public void NullScale()
        {
            var node = new ScaleByVdw();

            node.Elements.Value = ValidElements;
            node.Scale.UndefineValue();

            node.Refresh();

            Assert.IsFalse(node.Scales.HasNonNullValue());
        }

        [Test]
        public void NullElements()
        {
            var node = new ScaleByVdw();

            node.Scale.Value = 1f;
            node.Elements.UndefineValue();

            node.Refresh();

            Assert.IsFalse(node.Scales.HasNonNullValue());
        }

        [Test]
        public void EmptyElements()
        {
            var node = new ScaleByVdw();

            node.Elements.Value = new Element[0];
            node.Scale.Value = 1f;

            node.Refresh();

            Assert.IsFalse(node.Scales.HasNonNullValue());
        }

        [Test]
        public void ValidInput()
        {
            var node = new ScaleByVdw();

            node.Elements.Value = ValidElements;
            node.Scale.Value = 1f;

            node.Refresh();

            Assert.IsTrue(node.Scales.HasNonNullValue());
        }

        [Test]
        public void OnlyRefreshOnce()
        {
            var node = new ScaleByVdw();
            var linked = new ArrayProperty<float>
            {
                LinkedProperty = node.Scales,
                IsDirty = false
            };
            Assert.IsFalse(linked.IsDirty);

            node.Elements.Value = ValidElements;
            node.Scale.Value = 1f;

            node.Refresh();

            Assert.IsTrue(linked.IsDirty);
            linked.IsDirty = false;

            node.Refresh();

            Assert.IsFalse(linked.IsDirty);
        }
    }
}