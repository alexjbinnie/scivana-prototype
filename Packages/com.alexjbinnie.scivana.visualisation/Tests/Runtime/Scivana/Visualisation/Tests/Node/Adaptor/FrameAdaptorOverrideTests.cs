/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Scivana.Core.Collections;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using Scivana.Visualisation.Node.Adaptor;
using UnityEngine;

namespace Scivana.Visualisation.Tests.Node.Adaptor
{
    public class FrameAdaptorOverrideTests
    {
        private FrameSnapshot source;
        private FrameSourceAdaptorNode adaptor;
        private Frame frame;
        private IReadOnlyProperty<Vector3[]> property;

        private static readonly Vector3[] FramePositions =
        {
            Vector3.up, Vector3.left, Vector3.down
        };

        private static readonly Vector3[] OverridePositions =
        {
            Vector3.right, Vector3.zero, Vector3.back
        };

        [SetUp]
        public void Setup()
        {
            source = new FrameSnapshot();
            adaptor = new FrameSourceAdaptorNode
            {
                FrameSource = source
            };
            frame = new Frame
            {
                ParticlePositions = FramePositions
            };
        }

        [Test]
        public void NoOverrideGetPropertyFirst()
        {
            var prop = adaptor.ParticlePositions;
            source.Update(frame, FrameChanges.All);
            CollectionAssert.AreEqual(FramePositions, prop.Value);
        }

        [Test]
        public void NoOverrideGetPropertyAfter()
        {
            source.Update(frame, FrameChanges.All);
            var prop = adaptor.ParticlePositions;
            CollectionAssert.AreEqual(FramePositions, prop.Value);
        }

        private static IEnumerable<IEnumerable<Action<FrameAdaptorOverrideTests>>>
            GetOverrideActions()
        {
            var set = new Action<FrameAdaptorOverrideTests>[]
            {
                GetProperty, UpdateFrame, AddOverride
            };
            return set.GetPermutations().Select(s => s.AsPretty(t => t.Method.Name));
        }

        private static IEnumerable<IEnumerable<Action<FrameAdaptorOverrideTests>>>
            GetOverrideThenRemoveActions()
        {
            var set = new Action<FrameAdaptorOverrideTests>[]
            {
                GetProperty, UpdateFrame, AddOverride, RemoveOverride
            };
            return set.GetPermutations()
                      .Where(s => s.IndexOf(AddOverride) < s.IndexOf(RemoveOverride))
                      .Select(s => s.AsPretty(t => t.Method.Name));
        }


        private static void GetProperty(FrameAdaptorOverrideTests test)
        {
            test.property = test.adaptor.ParticlePositions;
        }

        private static void UpdateFrame(FrameAdaptorOverrideTests test)
        {
            test.source.Update(test.frame, FrameChanges.All);
        }

        private static void AddOverride(FrameAdaptorOverrideTests test)
        {
            var @override =
                test.adaptor.AddOverrideProperty<Vector3[]>(
                    FrameFields.ParticlePositions.Key);
            @override.Value = OverridePositions;
        }

        private static void RemoveOverride(FrameAdaptorOverrideTests test)
        {
            test.adaptor.RemoveOverrideProperty(FrameFields.ParticlePositions.Key);
        }

        [Test]
        public void Override(
            [ValueSource(nameof(GetOverrideActions))] IEnumerable<Action<FrameAdaptorOverrideTests>> actions)
        {
            foreach (var action in actions)
                action(this);

            CollectionAssert.AreEqual(OverridePositions, property.Value);
        }

        [Test]
        public void OverrideThenRemove([ValueSource(nameof(GetOverrideThenRemoveActions))]
                                       IEnumerable<Action<FrameAdaptorOverrideTests>> actions)
        {
            foreach (var action in actions)
                action(this);

            CollectionAssert.AreEqual(FramePositions, property.Value);
        }
    }
}