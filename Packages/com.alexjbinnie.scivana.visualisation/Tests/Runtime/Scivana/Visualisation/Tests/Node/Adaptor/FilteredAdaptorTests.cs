/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Scivana.Core.Collections;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using Scivana.Visualisation.Node.Adaptor;
using UnityEngine;

namespace Scivana.Visualisation.Tests.Node.Adaptor
{
    public class FilteredAdaptorDynamicPropertyTests
    {
        [Test]
        public void InitialNoProperties()
        {
            SetAdaptorsFrame(this);
            GetProperty(this);
            CollectionAssert.AreEqual(positions, property.Value);
        }

        private Frame emptyFrame;
        private Frame frameWithPositions;
        private FrameSourceAdaptorNode frameAdaptor;
        private ParticleFilteredAdaptorNode filterAdaptor;
        private int[] filter;
        private IReadOnlyProperty<Vector3[]> property;
        private Vector3[] filteredPositions;
        private Vector3[] positions;

        [SetUp]
        public void Setup()
        {
            positions = new[]
            {
                Vector3.zero, Vector3.right, Vector3.up, Vector3.forward
            };

            filteredPositions = new[]
            {
                Vector3.right, Vector3.forward
            };

            emptyFrame = new Frame();

            frameWithPositions = new Frame
            {
                ParticlePositions = positions
            };

            frameAdaptor = new FrameSourceAdaptorNode();
            filterAdaptor = new ParticleFilteredAdaptorNode();
            filterAdaptor.ParentAdaptor.Value = frameAdaptor;

            filter = new[]
            {
                1, 3
            };
        }

        private static void SetAdaptorsFilter(FilteredAdaptorDynamicPropertyTests test)
        {
            test.filterAdaptor.ParticleFilter.Value = test.filter;
        }

        private static void SetAdaptorsFrame(FilteredAdaptorDynamicPropertyTests test)
        {
            var frameSource = new FrameSnapshot();
            frameSource.Update(test.frameWithPositions, FrameChanges.All);
            test.frameAdaptor.FrameSource = frameSource;
        }

        private static void GetProperty(FilteredAdaptorDynamicPropertyTests test)
        {
            test.property = test.filterAdaptor.ParticlePositions;
        }

        public delegate void SetupFunction(FilteredAdaptorDynamicPropertyTests test);

        private static IEnumerable<IEnumerable<SetupFunction>> GetActionsForFilteredProperty()
        {
            var actions = new SetupFunction[]
            {
                SetAdaptorsFilter, SetAdaptorsFrame, GetProperty
            };
            return actions.GetPermutations()
                          .Select(e => e.AsPretty(t => t.Method.Name));
        }

        [Test]
        public void Filter([ValueSource(nameof(GetActionsForFilteredProperty))]
                           IEnumerable<SetupFunction> actions)
        {
            foreach (var setup in actions)
                setup(this);

            CollectionAssert.AreEqual(filteredPositions, property.Value);
        }
    }
}