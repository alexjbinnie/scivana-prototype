﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;
using Scivana.Visualisation.Parsing;
using UnityEngine;

namespace Scivana.Visualisation.Tests.Parsing
{
    internal class ParseColorTests
    {
        [Test]
        public void DoesParserExist()
        {
            Assert.IsNotNull(VisualisationParser.GetParser(typeof(Color)));
        }

        private static IEnumerable<(object value, Color color)> List3Parameters()
        {
            yield return (new object[]
                             {
                                 1.0, 0.0, 0.0
                             }, Color.red);
            yield return (new object[]
                             {
                                 0.0, 1.0, 0.0
                             }, Color.green);
            yield return (new object[]
                             {
                                 0.0, 0.0, 1.0
                             }, Color.blue);
            yield return (new object[]
                             {
                                 1.0, 1.0, 1.0
                             }, Color.white);
        }

        [Test]
        public void TestList3(
            [ValueSource(nameof(List3Parameters))] (object value, Color color) parameter)
        {
            Assert.AreEqual(parameter.color, VisualisationParser.ParseColor(parameter.value));
        }

        internal static IEnumerable<(string value, Color color)> HexParameters()
        {
            yield return ("0xff0000", Color.red);
            yield return ("0x00ff00", Color.green);
            yield return ("0x0000ff", Color.blue);
            yield return ("0xffffff", Color.white);
        }

        [Test]
        public void TestHex(
            [ValueSource(nameof(HexParameters))] (string value, Color color) parameter)
        {
            Assert.AreEqual(parameter.color, VisualisationParser.ParseColor(parameter.value));
        }

        internal static IEnumerable<(string value, Color color)> NameParameters()
        {
            yield return ("red", Color.red);
            yield return ("lime", Color.green);
            yield return ("blue", Color.blue);
            yield return ("white", Color.white);

            yield return ("Red", Color.red);
            yield return ("Lime", Color.green);
            yield return ("Blue", Color.blue);
            yield return ("White", Color.white);

            yield return ("RED", Color.red);
            yield return ("LIME", Color.green);
            yield return ("BLUE", Color.blue);
            yield return ("WHITE", Color.white);
        }

        [Test]
        public void TestName(
            [ValueSource(nameof(NameParameters))] (string value, Color color) parameter)
        {
            Assert.AreEqual(parameter.color, VisualisationParser.ParseColor(parameter.value));
        }
    }
}