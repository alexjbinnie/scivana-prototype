﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Scivana.Visualisation.Parsing;
using UnityEngine;

namespace Scivana.Visualisation.Tests.Parsing
{
    internal class ParseGradientTests
    {
        [Test]
        public void DoesParserExist()
        {
            Assert.IsNotNull(VisualisationParser.GetParser(typeof(Gradient)));
        }

        public static IEnumerable<object> InvalidGradients()
        {
            yield return null;
            yield return Color.blue;
            yield return new Color[0];
            yield return new object[0];
            yield return new[]
            {
                Color.white
            };
            yield return new object[]
            {
                "red"
            };
        }

        public static IEnumerable<(object value, Gradient gradient)> ValidGradients()
        {
            var colors = ParseColorTests.HexParameters()
                                        .Concat(ParseColorTests.NameParameters())
                                        .ToArray();

            for (var i = 0; i < 10; i++)
            {
                var length = Random.Range(2, 8);
                var arr = new object[length];
                var gradientColors = new GradientColorKey[length];
                for (var j = 0; j < length; j++)
                {
                    var (value, color) = colors[Random.Range(0, colors.Length)];
                    arr[j] = value;
                    gradientColors[j] = new GradientColorKey(color, j / (length - 1f));
                }

                var gradient = new Gradient
                {
                    colorKeys = gradientColors
                };
                yield return (arr, gradient);
            }
        }

        [Test]
        public void TestGradient(
            [ValueSource(nameof(ValidGradients))] (object value, Gradient gradient) parameter)
        {
            Assert.AreEqual(parameter.gradient, VisualisationParser.ParseGradient(parameter.value));
        }

        [Test]
        public void TestInvalidGradient(
            [ValueSource(nameof(InvalidGradients))] object value)
        {
            Assert.IsNull(VisualisationParser.ParseGradient(value));
        }
    }
}