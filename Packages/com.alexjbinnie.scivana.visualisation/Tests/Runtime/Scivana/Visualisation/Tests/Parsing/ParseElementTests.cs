﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;
using Scivana.Core.Science;
using Scivana.Visualisation.Parsing;

namespace Scivana.Visualisation.Tests.Parsing
{
    internal class ParseElementTests
    {
        [Test]
        public void DoesParserExist()
        {
            Assert.IsNotNull(VisualisationParser.GetParser(typeof(Element)));
        }

        private static IEnumerable<(int value, Element element)> AtomicNumberParameters()
        {
            yield return (1, Element.Hydrogen);
            yield return (8, Element.Oxygen);
            yield return (26, Element.Iron);
        }

        private static IEnumerable<int> InvalidAtomicNumberParameters()
        {
            yield return -1;
            yield return 242;
            yield return int.MaxValue;
            yield return int.MinValue;
        }

        [Test]
        public void TestAtomicNumber(
            [ValueSource(nameof(AtomicNumberParameters))] (int value, Element element) parameter)
        {
            Assert.AreEqual(parameter.element, VisualisationParser.ParseElement(parameter.value));
        }

        [Test]
        public void TestAtomicNumberInvalid(
            [ValueSource(nameof(InvalidAtomicNumberParameters))]
            int value)
        {
            Assert.IsNull(VisualisationParser.ParseElement(value));
        }

        private static IEnumerable<(string value, Element element)> AtomicSymbolParameters()
        {
            yield return ("h", Element.Hydrogen);
            yield return ("o", Element.Oxygen);
            yield return ("fe", Element.Iron);

            yield return ("H", Element.Hydrogen);
            yield return ("O", Element.Oxygen);
            yield return ("Fe", Element.Iron);
            yield return ("FE", Element.Iron);

            yield return (" H", Element.Hydrogen);
            yield return ("O ", Element.Oxygen);
            yield return (" Fe ", Element.Iron);
        }

        private static IEnumerable<string> InvalidAtomicSymbolParameters()
        {
            yield return null;
            yield return string.Empty;
            yield return " ";
            yield return "X";
            yield return "\u4592\u1829\u0241";
        }

        [Test]
        public void TestAtomicSymbol(
            [ValueSource(nameof(AtomicSymbolParameters))] (string value, Element element) parameter)
        {
            Assert.AreEqual(parameter.element, VisualisationParser.ParseElement(parameter.value));
        }

        [Test]
        public void TestAtomicSymbolInvalid(
            [ValueSource(nameof(InvalidAtomicSymbolParameters))]
            string value)
        {
            Assert.IsNull(VisualisationParser.ParseElement(value));
        }
    }
}