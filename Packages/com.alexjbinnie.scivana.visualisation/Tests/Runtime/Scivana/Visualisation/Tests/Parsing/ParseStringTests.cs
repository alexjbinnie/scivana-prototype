﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;
using Scivana.Visualisation.Parsing;

namespace Scivana.Visualisation.Tests.Parsing
{
    internal class ParseStringTests
    {
        [Test]
        public void DoesParserExist()
        {
            Assert.IsNotNull(VisualisationParser.GetParser(typeof(string)));
        }

        public static IEnumerable<object> InvalidStrings()
        {
            yield return null;
        }

        public static IEnumerable<(object value, string text)> ValidStrings()
        {
            yield return ("", "");
            yield return (" ", " ");
            yield return ("abc", "abc");
            yield return ("\x4214", "\x4214");
        }

        [Test]
        public void TestStrings(
            [ValueSource(nameof(ValidStrings))] (object value, string text) parameter)
        {
            Assert.AreEqual(parameter.text, VisualisationParser.ParseString(parameter.value));
        }

        [Test]
        public void TestInvalidStrings([ValueSource(nameof(InvalidStrings))] object value)
        {
            Assert.IsNull(VisualisationParser.ParseString(value));
        }
    }
}