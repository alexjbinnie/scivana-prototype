﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;
using Scivana.Visualisation.Parsing;

namespace Scivana.Visualisation.Tests.Parsing
{
    internal class ParseFloatTests
    {
        [Test]
        public void DoesParserExist()
        {
            Assert.IsNotNull(VisualisationParser.GetParser(typeof(float)));
        }

        public static IEnumerable<object> InvalidFloats()
        {
            yield return null;
            yield return string.Empty;
        }

        public static IEnumerable<(object value, float number)> ValidFloats()
        {
            yield return (0, 0);
            yield return (0f, 0);
            yield return (0d, 0);
            yield return (23, 23);
            yield return (23.53f, 23.53f);
            yield return (23.5314198d, 23.5314198f);
            yield return (-2136.32f, -2136.32f);
            yield return (2.4215e4, 2.4215e4f);
        }

        [Test]
        public void TestFloats(
            [ValueSource(nameof(ValidFloats))] (object value, float number) parameter)
        {
            Assert.AreEqual(parameter.number, VisualisationParser.ParseFloat(parameter.value));
        }

        [Test]
        public void TestInvalidFloats([ValueSource(nameof(InvalidFloats))] object value)
        {
            Assert.IsNull(VisualisationParser.ParseFloat(value));
        }
    }
}