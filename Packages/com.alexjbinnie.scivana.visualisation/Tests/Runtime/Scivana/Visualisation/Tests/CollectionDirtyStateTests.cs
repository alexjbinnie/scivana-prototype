/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.ObjectModel;
using NUnit.Framework;
using Scivana.Visualisation.Utility;

namespace Scivana.Visualisation.Tests
{
    internal class CollectionDirtyStateTests
    {
        private ObservableCollection<int> collection;
        private CollectionDirtyState<int> dirtyState;

        [SetUp]
        public void Setup()
        {
            collection = new ObservableCollection<int>
            {
                0,
                1,
                2
            };
            dirtyState = new CollectionDirtyState<int>(collection);
        }

        [Test]
        public void InitialDirtyItems()
        {
            CollectionAssert.AreEquivalent(collection, dirtyState.DirtyItems);
        }

        [Test]
        public void ClearDirty()
        {
            dirtyState.ClearAllDirty();

            CollectionAssert.AreEquivalent(new int[0], dirtyState.DirtyItems);
        }

        [Test]
        public void Add_DirtyItems()
        {
            collection.Add(3);

            CollectionAssert.AreEquivalent(new[]
            {
                0, 1, 2, 3
            }, dirtyState.DirtyItems);
        }

        [Test]
        public void Add_DirtyItems_New()
        {
            dirtyState.ClearAllDirty();

            collection.Add(3);

            CollectionAssert.AreEquivalent(new[]
            {
                3
            }, dirtyState.DirtyItems);
        }

        [Test]
        public void Remove_DirtyItems()
        {
            collection.Remove(1);

            CollectionAssert.AreEquivalent(new[]
            {
                0, 2
            }, dirtyState.DirtyItems);
        }

        [Test]
        public void Replace_DirtyItems()
        {
            collection[0] = 3;

            CollectionAssert.AreEquivalent(new[]
            {
                1, 2, 3
            }, dirtyState.DirtyItems);
        }

        [Test]
        public void IsDirty()
        {
            Assert.IsTrue(dirtyState.IsDirty(0));
            Assert.IsFalse(dirtyState.IsDirty(3));
        }

        [Test]
        public void ClearDirtyState()
        {
            dirtyState.ClearDirty(1);

            Assert.IsTrue(dirtyState.IsDirty(0));
            Assert.IsFalse(dirtyState.IsDirty(1));
        }

        [Test]
        public void MarkDirty()
        {
            dirtyState.ClearAllDirty();

            dirtyState.MarkDirty(1);

            Assert.IsFalse(dirtyState.IsDirty(0));
            Assert.IsTrue(dirtyState.IsDirty(1));
        }

        [Test]
        public void SetDirty()
        {
            Assert.IsTrue(dirtyState.IsDirty(0));

            dirtyState.SetDirty(0, false);

            Assert.IsFalse(dirtyState.IsDirty(0));

            dirtyState.SetDirty(0, true);

            Assert.IsTrue(dirtyState.IsDirty(0));
        }

        [Test]
        public void IsDirtyKey()
        {
            dirtyState.SetDirty(0, false);

            Assert.IsFalse(dirtyState[0]);
            Assert.IsTrue(dirtyState[1]);
        }
    }
}