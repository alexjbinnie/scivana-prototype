/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;
using Scivana.Core.Collections;
using Scivana.Visualisation.Utility;

namespace Scivana.Visualisation.Tests
{
    internal class DictionaryDirtyStateTests
    {
        private ObservableDictionary<string, int> dictionary;
        private DictionaryDirtyState<string, int> dirtyState;

        [SetUp]
        public void Setup()
        {
            dictionary = new ObservableDictionary<string, int>
            {
                ["a"] = 0,
                ["b"] = 1
            };
            dirtyState = new DictionaryDirtyState<string, int>(dictionary);
        }

        [Test]
        public void InitialDirtyItems()
        {
            CollectionAssert.AreEquivalent(dictionary.Keys, dirtyState.DirtyKeys);
        }

        [Test]
        public void InitialDirtyValues()
        {
            CollectionAssert.AreEquivalent(dictionary.Values, dirtyState.DirtyValues);
        }

        [Test]
        public void DirtyKeysChanged()
        {
            dirtyState.ClearAllDirty();
            dictionary["a"] = 2;

            CollectionAssert.AreEquivalent(new[]
            {
                "a"
            }, dirtyState.DirtyKeys);
        }

        [Test]
        public void DirtyValuesChanged()
        {
            dirtyState.ClearAllDirty();
            dictionary["a"] = 2;

            CollectionAssert.AreEquivalent(new[]
            {
                2
            }, dirtyState.DirtyValues);
        }

        [Test]
        public void DirtyKeyValuesChanged()
        {
            dirtyState.ClearAllDirty();
            dictionary["a"] = 2;

            CollectionAssert.AreEquivalent(new[]
                                           {
                                               new KeyValuePair<string, int>("a", 2)
                                           },
                                           dirtyState.DirtyKeyValuePairs);
        }
    }
}