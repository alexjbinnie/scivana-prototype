/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Properties;
using Scivana.Visualisation.Property;

namespace Scivana.Visualisation.Tests.Property
{
    public abstract class
        ArrayPropertyTests<TProperty, TElement> : PropertyTests<TProperty, TElement[]>
        where TProperty : SerializableProperty<TElement[]>, new()
    {
        protected TElement[] EmptyArray => new TElement[0];

        protected override bool IsReferenceType => true;

        [Test]
        public void InitialProperty_HasNonEmptyValue()
        {
            var property = new TProperty();

            Assert.IsFalse(property.HasNonEmptyValue());
        }

        [Test]
        public void HasNonEmptyValue_Empty()
        {
            var property = new TProperty()
            {
                Value = EmptyArray
            };

            Assert.IsFalse(property.HasNonEmptyValue());
        }

        [Test]
        public void HasNonEmptyValue_NonEmpty()
        {
            var property = new TProperty()
            {
                Value = ExampleNonNullValue
            };

            Assert.IsTrue(property.HasNonEmptyValue());
        }
    }
}