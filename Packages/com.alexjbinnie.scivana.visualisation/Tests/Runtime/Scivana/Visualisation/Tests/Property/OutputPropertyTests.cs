/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Visualisation.Properties;
using Scivana.Visualisation.Property;
using UnityEngine;

namespace Scivana.Visualisation.Tests.Property
{
    internal class IntPropertyTests : PropertyTests<SerializableProperty<int>, int>
    {
        protected override int ExampleNonNullValue => 1;
        protected override int DifferentNonNullValue => -4;
    }

    internal class IntArrayPropertyTests : ArrayPropertyTests<ArrayProperty<int>, int>
    {
        protected override int[] ExampleNonNullValue => new[]
        {
            1, 3
        };

        protected override int[] DifferentNonNullValue => new[]
        {
            -2, 0
        };
    }

    internal class FloatPropertyTests : PropertyTests<SerializableProperty<float>, float>
    {
        protected override float ExampleNonNullValue => 1.1f;
        protected override float DifferentNonNullValue => -22013.3223f;
    }

    internal class FloatArrayPropertyTests : ArrayPropertyTests<ArrayProperty<float>, float>
    {
        protected override float[] ExampleNonNullValue => new[]
        {
            1f, 2.5f
        };

        protected override float[] DifferentNonNullValue => new[]
        {
            -0.5f, 42425.332f
        };
    }

    internal class Vector3PropertyTests : PropertyTests<SerializableProperty<Vector3>, Vector3>
    {
        protected override Vector3 ExampleNonNullValue => Vector3.zero;
        protected override Vector3 DifferentNonNullValue => Vector3.left * 2f;
    }

    internal class Vector3ArrayPropertyTests : ArrayPropertyTests<ArrayProperty<Vector3>, Vector3>
    {
        protected override Vector3[] ExampleNonNullValue => new[]
        {
            Vector3.zero, Vector3.right
        };

        protected override Vector3[] DifferentNonNullValue => new[]
        {
            Vector3.left * 2f, new Vector3(0.5f, -24.2f, 53.64f)
        };
    }

    internal class ColorPropertyTests : PropertyTests<SerializableProperty<Color>, Color>
    {
        protected override Color ExampleNonNullValue => Color.red;
        protected override Color DifferentNonNullValue => Color.black;
    }

    internal class ColorArrayPropertyTests : ArrayPropertyTests<ArrayProperty<Color>, Color>
    {
        protected override Color[] ExampleNonNullValue => new[]
        {
            Color.red, Color.green
        };

        protected override Color[] DifferentNonNullValue => new[]
        {
            Color.yellow, Color.black
        };
    }
}