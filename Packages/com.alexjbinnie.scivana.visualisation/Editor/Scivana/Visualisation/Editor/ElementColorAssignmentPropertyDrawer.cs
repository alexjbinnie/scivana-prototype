/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Science;
using Scivana.Visualisation.Data;
using UnityEditor;
using UnityEngine;

namespace Scivana.Visualisation.Editor
{
    /// <summary>
    /// Property drawer to render element palette assignments in the editor.
    /// </summary>
    [CustomPropertyDrawer(typeof(ElementColorMapping.ElementColorAssignment))]
    public sealed class ElementColorAssignmentPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var atomicNumberProperty =
                property.FindPropertyRelative(
                    nameof(ElementColorMapping.ElementColorAssignment.atomicNumber));

            var colorProperty =
                property.FindPropertyRelative(
                    nameof(ElementColorMapping.ElementColorAssignment.color));

            // Get the element name to display
            var elementId = atomicNumberProperty.intValue;
            Element? element = null;
            if (Enum.IsDefined(typeof(Element), elementId))
                element = (Element) elementId;

            // Prefix each line with the element name label
            position = EditorGUI.PrefixLabel(position,
                                             new GUIContent(element.ToString() ?? ""));

            var numberBoxWidth = Mathf.Min(72f, position.width / 2f);

            var numberRect = new Rect(position.x,
                                      position.y,
                                      numberBoxWidth,
                                      position.height);
            var colorRect = new Rect(position.x + numberBoxWidth + 8,
                                     position.y,
                                     position.width - 8 - numberBoxWidth,
                                     position.height);

            EditorGUI.PropertyField(numberRect,
                                    atomicNumberProperty,
                                    GUIContent.none);

            EditorGUI.PropertyField(colorRect,
                                    colorProperty,
                                    GUIContent.none);
        }
    }
}