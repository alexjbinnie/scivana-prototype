/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Node;
using Scivana.Visualisation.Property;
using UnityEditor;
using UnityEngine;

namespace Scivana.Visualisation.Editor
{
    /// <summary>
    /// Generates a graph of the currently selected visualiser as graph in the DOT
    /// language, suitable for visualisation using GraphViz.
    /// </summary>
    public static class GraphVizUtility
    {
        [MenuItem("Test/GraphViz")]
        public static void Test()
        {
            var renderer = (Selection.activeObject as GameObject).GetComponent<Visualiser>();
            GenerateGraphVizToKeyboard(renderer.subgraphs);
        }

        private class State
        {
            internal List<(long, long)> Connections = new List<(long, long)>();
            internal List<long> IdentifiedObjects = new List<long>();
            internal List<IReadOnlyProperty> Properties = new List<IReadOnlyProperty>();
        }

        private static void GenerateGraphVizToKeyboard(IEnumerable<VisualisationNode> nodes)
        {
            var file = new StringWriter();

            file.WriteLine("digraph G {");

            file.WriteLine("rankdir=RL");

            var state = new State();

            foreach (var node in nodes)
                DrawObject(node, file, state);

            foreach (var connection in state.Connections)
                file.WriteLine($"{connection.Item1} -> {connection.Item2};");

            var unidentified = state.Properties
                                    .Where(obj => !state.IdentifiedObjects.Contains(GetId(obj)));

            foreach (var prop in unidentified)
            {
                var color = prop.HasValue ? "green" : "red";
                file.WriteLine($"{GetId(prop)} [label=\"{prop.GetType().Name}\" color={color}];");
            }

            file.WriteLine("}");

            var str = file.ToString();

            EditorGUIUtility.systemCopyBuffer = str;
        }

        private static void DrawObject(VisualisationNode node,
                                       TextWriter file,
                                       State state
        )
        {
            file.WriteLine($"subgraph cluster_{GetId(node)} {{");
            file.WriteLine($"label = \"{node.GetType().Name}\"");

            foreach (var (name, prop, attributes) in node.GetInputProperties())
            {
                var label = name;
                var color = prop.HasValue ? "green" : "red";
                file.WriteLine($"{GetId(prop)} [label=\"{label}\\n{prop.GetType().Name}\" color={color} shape=box];");
                state.IdentifiedObjects.Add(GetId(prop));
                FindConnections(prop, state);
            }

            foreach (var (name, prop) in node.GetOutputProperties(true))
            {
                var label = name;
                var color = prop.HasValue ? "green" : "red";
                file.WriteLine($"{GetId(prop)} [label=\"{label}\\n{prop.GetType().Name}\" color={color} shape=box];");
                state.IdentifiedObjects.Add(GetId(prop));
                FindConnections(prop, state);
            }

            file.WriteLine("}");
        }

        private static void FindConnections(IReadOnlyProperty prop, State state)
        {
            state.Properties.Add(prop);

            if (prop is IProperty property && property.HasLinkedProperty)
            {
                var linked = property.LinkedProperty;
                var conn = (GetId(prop), GetId(linked));
                if (!state.Connections.Contains(conn))
                    state.Connections.Add(conn);
                FindConnections(linked, state);
            }

            if (prop is IFilteredProperty filtered)
            {
                var conn1 = (GetId(prop), GetId(filtered.SourceProperty));
                if (!state.Connections.Contains(conn1))
                    state.Connections.Add(conn1);
                var conn2 = (GetId(prop), GetId(filtered.FilterProperty));
                if (!state.Connections.Contains(conn2))
                    state.Connections.Add(conn2);

                FindConnections(filtered.SourceProperty, state);
                FindConnections(filtered.FilterProperty, state);
            }

            if (prop is IPropertyMutation transformation)
            {
                var conn1 = (GetId(prop), GetId(transformation.OriginalProperty));
                if (!state.Connections.Contains(conn1))
                    state.Connections.Add(conn1);

                FindConnections(transformation.OriginalProperty, state);
            }
        }

        internal static long GetId(object obj)
        {
            return (long) int.MaxValue + (long) obj.GetHashCode();
        }
    }
}