﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Reflection;
using Scivana.Core;
using UnityEditor;

namespace Scivana.Visualisation.Editor
{
    public static class EditorHelpers
    {
        public static object GetValue(SerializedProperty property)
        {
            var pathSplit = property.propertyPath.Split('.');
            object currentObject = property.serializedObject.targetObject;

            FieldInfo field = null;
            foreach (var name in pathSplit)
            {
                if (field != null)
                {
                    currentObject = field.GetValue(currentObject);
                }

                field = currentObject.GetType()
                                     .GetFieldInSelfOrParents(name, BindingFlags.Instance
                                                                  | BindingFlags.Public
                                                                  | BindingFlags
                                                                        .NonPublic);
            }

            return field.GetValue(currentObject);
        }
    }
}