﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace Scivana.State
{
    /// <summary>
    /// Represents a small part of the application, that can depend on other pieces and handles a small
    /// task.
    /// </summary>
    public interface IState
    {
        object Instance { get; }
        void EnterState();

        void ExitState();

        ContextStateState CurrentState { get; }

        void SuspendState();

        void ResumeState();

        List<IState> Dependencies { get; }

        List<(Context Context, ContextUsage Usage)> Contexts { get; }

        event Action StateExited;

        event Action StateSuspended;

        event Action StateResumed;

        event Action StateEntered;
    }

    public sealed class State<TType> : IState
        where TType : class
    {
        object IState.Instance => Instance;

        public TType Instance { get; }

        public string Name { get; }

        internal State(TType instance, string name)
        {
            Instance = instance;
            Name = name;
        }

        public static implicit operator TType(State<TType> state) => state?.Instance;

        /// <inheritdoc />
        public void EnterState()
        {
            if (CurrentState != ContextStateState.Uninitialized)
            {
                throw new InvalidOperationException(
                    $"Cannot enter state {this}. Can only enter state which is uninitialized.");
            }

            CurrentState = ContextStateState.Active;
            StateEntered?.Invoke();
        }

        /// <inheritdoc />
        public ContextStateState CurrentState { get; private set; } =
            ContextStateState.Uninitialized;

        /// <inheritdoc />
        public void ExitState()
        {
            if (CurrentState != ContextStateState.Active &&
                CurrentState != ContextStateState.Suspended)
            {
                throw new InvalidOperationException(
                    $"Cannot exit state {this}. Can only exit state which is active or suspended.");
            }

            CurrentState = ContextStateState.ScheduledForDeletion;
            StateExited?.Invoke();
        }

        /// <inheritdoc />
        public void SuspendState()
        {
            if (CurrentState != ContextStateState.Active && CurrentState != ContextStateState.Uninitialized)
            {
                throw new InvalidOperationException(
                    $"Cannot suspend state {this}. Can only suspend state which is active or unitialized.");
            }

            CurrentState = ContextStateState.Suspended;
            StateSuspended?.Invoke();
        }

        /// <inheritdoc />
        public void ResumeState()
        {
            if (CurrentState != ContextStateState.Suspended)
            {
                throw new InvalidOperationException(
                    $"Cannot resume state {this}. Can only resume state which is suspended.");
            }

            CurrentState = ContextStateState.Active;
            StateResumed?.Invoke();
        }

        /// <inheritdoc />
        public List<(Context Context, ContextUsage Usage)> Contexts { get; } =
            new List<(Context, ContextUsage)>();

        /// <inheritdoc />
        public List<IState> Dependencies { get; } = new List<IState>();

        /// <inheritdoc />
        public event Action StateExited;

        /// <inheritdoc />
        public event Action StateSuspended;

        /// <inheritdoc />
        public event Action StateResumed;

        /// <inheritdoc />
        public event Action StateEntered;

        public override string ToString()
        {
            return Name;
        }
    }
}