﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace Scivana.State
{
    public static class ContextExtensionMethods
    {
        public static TState AddContext<TState>(this TState state,
                                                Context context,
                                                ContextUsage usage) where TState : IState
        {
            if (state.CurrentState != ContextStateState.Uninitialized)
            {
                throw new InvalidOperationException(
                    $"Cannot add context to {state}. Can only edit states which are uninitialized.");
            }

            state.Contexts.Add((context, usage));
            return state;
        }

        public static TState AddDependency<TState>(this TState state, IState dependency)
            where TState : IState
        {
            if (state.CurrentState != ContextStateState.Uninitialized)
            {
                throw new InvalidOperationException(
                    $"Cannot add dependency to {state}. Can only edit states which are uninitialized.");
            }

            state.Dependencies.Add(dependency);
            return state;
        }
    }
}