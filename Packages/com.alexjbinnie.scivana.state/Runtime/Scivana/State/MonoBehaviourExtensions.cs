﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.State;
using UnityEngine;

namespace ScivanaIMD.State
{
    public static class MonoBehaviourExtensions
    {
        public static State<TType> AsState<TType>(this TType obj, string name, bool deleteAtEnd = true)
            where TType : class
        {
            var state = new State<TType>(obj, name);
            if (obj is IStateEnteredHandler entered)
                state.StateEntered += entered.OnStateEntered;
            if (obj is IStateExitedHandler exited)
                state.StateExited += exited.OnStateExited;

            if (obj is MonoBehaviour instance)
            {
                if (instance.gameObject.activeSelf)
                {
                    Debug.LogWarning(
                        $"Turning MonoBehaviour into state, but it's already active! ({instance})");
                }

#if UNITY_EDTIOR
                if (PrefabUtility.IsPartOfPrefabAsset(instance))
                {
                    throw new InvalidOperationException(
                        $"Tried using prefab {instance} as a state. Try instantiating first!");
                }
#endif

                state.StateEntered += () => instance.gameObject.SetActive(true);
                state.StateExited += () =>
                {
                    if (instance == null)
                        return;
                    if (deleteAtEnd)
                        Object.Destroy(instance.gameObject);
                    else
                        instance.gameObject.SetActive(false);
                };
                state.StateSuspended += () => instance.gameObject.SetActive(false);
                state.StateResumed += () => instance.gameObject.SetActive(true);
            }

            return state;
        }
    }
}