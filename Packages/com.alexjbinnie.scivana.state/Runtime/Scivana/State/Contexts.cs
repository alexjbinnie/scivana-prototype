﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Scivana.State
{
    public static class Contexts
    {
        public static Context UIInteraction { get; } = new Context("UI Interaction");

        public static Context Display { get; } = new Context("Display");

        public static Context ControllerCursors { get; } = new Context("Controller");

        public static Context FullScreenUI { get; } = new Context("Full Screen UI");

        public static Context QuickMenuUI { get; } = new Context("Quick Menu UI");

        public static Context DisplayedUI { get; } = new Context("Displayed UI");

        public static Context SelectionOutline { get; } = new Context("Selection Outline");

        public static Context Mode { get; } = new Context("Mode");
        public static Context LeftArmUI { get; } = new Context("Left Arm UI");

        /// <summary>
        /// States which render the contents of a simulation - i.e., the atoms and particles that
        /// make it up.
        /// </summary>
        public static Context SimulationVisualisation { get; } = new Context("Sim Vis");

        /// <summary>
        /// States which modify the highlighted particles of the system visualisation. Should only really
        /// be one state doing this.
        /// </summary>
        public static Context SimulationVisualisationHighlight { get; } = new Context("Sim Vis Highlight");
    }
}