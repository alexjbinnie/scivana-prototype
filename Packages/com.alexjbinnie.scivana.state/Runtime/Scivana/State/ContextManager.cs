﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scivana.State
{
    public class ContextManager
    {
        public IReadOnlyList<IState> States => states;

        public IEnumerable<Context> Contexts => states
                                                .SelectMany(state => state.Contexts)
                                                .Select(state => state.Item1).Distinct();

        private readonly List<IState> states = new List<IState>();

        private void RefreshStates()
        {
            if (markedForDestruction)
                return;

            var contexts = new Dictionary<Context, List<(IState, ContextUsage)>>();

            (IState, ContextUsage) GetLatestUsage(Context context, IEnumerable<IState> statesToIgnore)
            {
                if (contexts.ContainsKey(context))
                {
                    return contexts[context]
                        .LastOrDefault(a => !statesToIgnore.Contains(a.Item1));
                }

                return (null, ContextUsage.None);
            }

            var newStates = new Dictionary<IState, ContextStateState>();

            foreach (var state in states)
            {
                var scheduleDeletion = false;
                var scheduleSuspension = false;

                var newContexts = state.Contexts.ToList();
                var dependencies = state.Dependencies.ToList();

                foreach (var (context, _) in newContexts)
                {
                    var (previousState, previousUsage) = GetLatestUsage(context, dependencies);
                    // A higher state as absolute control over a context
                    if (previousUsage == ContextUsage.Absolute)
                    {
                        scheduleDeletion = true;
                    }
                    else if (previousUsage == ContextUsage.Exclusive)
                    {
                        scheduleSuspension = true;
                    }
                }

                foreach (var dependency in dependencies)
                {
                    if (newStates[dependency] == ContextStateState.ScheduledForDeletion)
                    {
                        scheduleDeletion = true;
                    }

                    if (newStates[dependency] == ContextStateState.Suspended)
                        scheduleSuspension = true;
                }


                if (scheduledDeletions.Contains(state))
                {
                    scheduleDeletion = true;
                }

                if (scheduleDeletion)
                {
                    newStates[state] = ContextStateState.ScheduledForDeletion;
                }
                else if (scheduleSuspension)
                {
                    newStates[state] = ContextStateState.Suspended;
                }
                else // active
                {
                    newStates[state] = ContextStateState.Active;

                    foreach (var (context, usage) in newContexts)
                    {
                        if (!contexts.ContainsKey(context))
                            contexts[context] = new List<(IState, ContextUsage)>();
                        contexts[context].Add((state, usage));
                    }
                }
            }

            scheduledDeletions.Clear();

            var stateSnapshot = states.ToArray();

            foreach (var state in stateSnapshot)
            {
                if (state.CurrentState == newStates[state])
                    continue;
                switch (oldState: state.CurrentState, newState: newStates[state])
                {
                    case (ContextStateState.Uninitialized, ContextStateState.Active):
                        state.EnterState();
                        break;
                    case (ContextStateState.Uninitialized, ContextStateState.Suspended):
                        state.SuspendState();
                        break;
                    case (ContextStateState.Suspended, ContextStateState.Active):
                        state.ResumeState();
                        break;
                    case (ContextStateState.Active, ContextStateState.Suspended):
                        state.SuspendState();
                        break;
                    case (ContextStateState.Active, ContextStateState.ScheduledForDeletion):
                    case (ContextStateState.Suspended, ContextStateState.ScheduledForDeletion):
                        state.ExitState();
                        states.Remove(state);
                        break;
                    default:
                        throw new InvalidOperationException(
                            $"Cannot transition {state} from {state.CurrentState} to {newStates[state]}");
                }
            }
        }

        private bool isModifying;

        private readonly List<IState> scheduledDeletions = new List<IState>();

        public void DeleteState(IState state)
        {
            using (ModifyStates())
            {
                scheduledDeletions.Add(state);
            }
        }

        public TState AddState<TState>(TState state)
            where TState : IState
        {
            if (markedForDestruction)
                return state;

            using (ModifyStates())
            {
                var index = 0;
                foreach (var requirement in state.Dependencies)
                {
                    var dependencyIndex = states.IndexOf(requirement);
                    if (dependencyIndex == -1)
                    {
                        throw new InvalidOperationException(
                            $"Added state {state} without dependency {requirement}");
                    }

                    index = Mathf.Max(index, dependencyIndex + 1);
                }

                states.Insert(index, state);
            }

            return state;
        }

        public void ClearAllStates()
        {
            foreach (var state in states)
            {
                state.ExitState();
            }

            states.Clear();
        }

        public void DeleteStatesWithContext(Context context)
        {
            var toDelete = states.Where(state => state.Contexts.Any(a => a.Item1 == context));
            DeleteStates(toDelete);
        }

        public void DeleteStates(IEnumerable<IState> states)
        {
            using (ModifyStates())
            {
                scheduledDeletions.AddRange(states);
            }
        }

        public void DeleteStates<TType>()
            where TType : class
        {
            var toDelete = states.Where(state => state is State<TType>).ToList();
            if (toDelete.Count == 0)
                return;
            DeleteStates(toDelete);
        }

        public IDisposable ModifyStates()
        {
            return new StateModificationContext(this);
        }

        private class StateModificationContext : IDisposable
        {
            private bool wasModifying;
            private ContextManager manager;

            public StateModificationContext(ContextManager manager)
            {
                wasModifying = manager.isModifying;
                this.manager = manager;
                this.manager.isModifying = true;
            }

            public void Dispose()
            {
                manager.isModifying = wasModifying;
                if (!wasModifying)
                {
                    manager.RefreshStates();
                }
            }
        }

        /// <summary>
        /// Get the first active state of type T.
        /// </summary>
        public State<TType> GetState<TType>(bool includeInactive = false)
            where TType : class
        {
            foreach (var state in states)
            {
                if (!includeInactive && state.CurrentState != ContextStateState.Active)
                    continue;
                if (state is State<TType> validState)
                    return validState;
            }

            return null;
        }

        public IState GetActiveStateOfInstance(object instance)
        {
            foreach (var state in states)
            {
                if (state.CurrentState != ContextStateState.Active)
                    continue;
                if (state.Instance == instance)
                    return state;
                if (!(instance is Component behaviour1) || !(state.Instance is Component behaviour2))
                    continue;
                if (behaviour1.gameObject == behaviour2.gameObject)
                    return state;
            }

            return null;
        }

        public IState GetFirstActiveState(Context context)
        {
            foreach (var state in states)
            {
                if (state.Contexts.Any(c => c.Context == context) &&
                    state.CurrentState == ContextStateState.Active)
                    return state;
            }

            return null;
        }

        public void DeleteStateOfInstance(object instance)
        {
            DeleteState(states.FirstOrDefault(state => state.Instance == instance));
        }

        private bool markedForDestruction = false;

        public void MarkForDestruction()
        {
            markedForDestruction = true;
        }
    }
}