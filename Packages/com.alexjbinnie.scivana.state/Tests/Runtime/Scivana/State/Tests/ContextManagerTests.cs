﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using ScivanaIMD.State;

namespace Scivana.State.Tests
{
    public class ContextManagerTests
    {
        private ContextManager manager;

        private Context context1 = new Context("Context 1");
        private Context context2 = new Context("Context 2");

        [SetUp]
        public void Setup()
        {
            manager = new ContextManager();
        }

        public IState TestState => "test".AsState("Test");

        [Test]
        public void AddSingleState()
        {
            var state = TestState
                .AddContext(context1, ContextUsage.Inclusive);

            manager.AddState(state);

            Assert.AreEqual(ContextStateState.Active, state.CurrentState);
        }

        [Test]
        public void TwoStatesInclusive()
        {
            var state = TestState
                .AddContext(context1, ContextUsage.Inclusive);

            var state2 = TestState
                .AddContext(context1, ContextUsage.Inclusive);

            manager.AddState(state);
            manager.AddState(state2);

            Assert.AreEqual(ContextStateState.Active, state.CurrentState);
            Assert.AreEqual(ContextStateState.Active, state2.CurrentState);
        }

        [Test]
        public void TwoStatesExclusive()
        {
            var state = TestState
                .AddContext(context1, ContextUsage.Exclusive);

            var state2 = TestState
                .AddContext(context1, ContextUsage.Exclusive);

            manager.AddState(state);
            manager.AddState(state2);

            Assert.AreEqual(ContextStateState.Suspended, state.CurrentState);
            Assert.AreEqual(ContextStateState.Active, state2.CurrentState);
        }

        [Test]
        public void TwoStatesAbsolute()
        {
            var state = TestState
                .AddContext(context1, ContextUsage.Absolute);

            var state2 = TestState
                .AddContext(context1, ContextUsage.Absolute);

            manager.AddState(state);
            manager.AddState(state2);

            Assert.AreEqual(ContextStateState.ScheduledForDeletion, state.CurrentState);
            Assert.AreEqual(ContextStateState.Active, state2.CurrentState);
        }

        [Test]
        public void TwoStatesAbsolute_DifferentContexts()
        {
            var state = TestState
                .AddContext(context1, ContextUsage.Absolute);

            var state2 = TestState
                .AddContext(context2, ContextUsage.Absolute);

            manager.AddState(state);
            manager.AddState(state2);

            Assert.AreEqual(ContextStateState.Active, state.CurrentState);
            Assert.AreEqual(ContextStateState.Active, state2.CurrentState);
        }

        [Test]
        public void TwoStatesAbsolute_Dependency()
        {
            var state = TestState
                .AddContext(context1, ContextUsage.Absolute);

            var state2 = TestState
                         .AddContext(context1, ContextUsage.Absolute)
                         .AddDependency(state);

            manager.AddState(state);
            manager.AddState(state2);

            Assert.AreEqual(ContextStateState.Active, state.CurrentState);
            Assert.AreEqual(ContextStateState.Active, state2.CurrentState);
        }
    }
}