﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using NSubstitute;
using NUnit.Framework;
using ScivanaIMD.State;

namespace Scivana.State.Tests
{
    public class StateTests
    {
        public IState TestState => "test".AsState("Test");

        [Test]
        public void StateEntered_Invoked()
        {
            var state = TestState;

            var handler = Substitute.For<Action>();
            state.StateEntered += handler;

            state.EnterState();

            handler.Received(1).Invoke();
        }

        [Test]
        public void StateConstructed_StateIsUninitialized()
        {
            var state = TestState;

            Assert.AreEqual(ContextStateState.Uninitialized, state.CurrentState);
        }

        [Test]
        public void StateEntered_StateIsActive()
        {
            var state = TestState;

            state.EnterState();

            Assert.AreEqual(ContextStateState.Active, state.CurrentState);
        }

        [Test]
        public void StateSuspended_Invoked()
        {
            var state = TestState;

            var handler = Substitute.For<Action>();
            state.StateSuspended += handler;

            state.EnterState();

            state.SuspendState();

            handler.Received(1).Invoke();
        }

        [Test]
        public void StateSuspended_StateIsSuspended()
        {
            var state = TestState;

            state.EnterState();

            state.SuspendState();

            Assert.AreEqual(ContextStateState.Suspended, state.CurrentState);
        }

        [Test]
        public void StateResumed_Invoked()
        {
            var state = TestState;

            var handler = Substitute.For<Action>();
            state.StateResumed += handler;

            state.EnterState();

            state.SuspendState();

            state.ResumeState();

            handler.Received(1).Invoke();
        }

        [Test]
        public void StateResumed_StateIsActive()
        {
            var state = TestState;

            state.EnterState();

            state.SuspendState();

            state.ResumeState();

            Assert.AreEqual(ContextStateState.Active, state.CurrentState);
        }

        [Test]
        public void StateExited_Invoked()
        {
            var state = TestState;

            var handler = Substitute.For<Action>();
            state.StateExited += handler;

            state.EnterState();

            state.ExitState();

            handler.Received(1).Invoke();
        }

        [Test]
        public void StateExited_StateIsScheduledForDeletion()
        {
            var state = TestState;

            state.EnterState();

            state.ExitState();

            Assert.AreEqual(ContextStateState.ScheduledForDeletion, state.CurrentState);
        }

        [Test]
        public void ResumeState_ExceptionIfNotEntered()
        {
            var state = TestState;

            Assert.Throws<InvalidOperationException>(() => state.ResumeState());
        }

        [Test]
        public void ExitState_ExceptionIfNotEntered()
        {
            var state = TestState;

            Assert.Throws<InvalidOperationException>(() => state.ExitState());
        }

        [Test]
        public void EnterState_ExceptionIfAlreadyEntered()
        {
            var state = TestState;

            state.EnterState();

            Assert.Throws<InvalidOperationException>(() => state.EnterState());
        }

        [Test]
        public void ResumeState_ExceptionIfActive()
        {
            var state = TestState;

            state.EnterState();

            Assert.Throws<InvalidOperationException>(() => state.ResumeState());
        }
    }
}