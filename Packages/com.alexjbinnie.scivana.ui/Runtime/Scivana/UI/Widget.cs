﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;
using UnityEngine.EventSystems;

namespace Scivana.UI
{
    /// <summary>
    /// A widget represents a user interface element (such as a label or button) without the
    /// implementation. It generates an implementation which actually handles displaying the user
    /// interface, and allows the user to focus on the features of their UI rather than the implementation.
    /// A widget is customised by adding one or more traits, such as <see cref="WidgetButton" /> or
    /// <see cref="WidgetLabel" />. The implementation can then use these to customise its behaviour.
    /// </summary>
    [ExecuteAlways]
    [RequireComponent(typeof(RectTransform))]
    public sealed class Widget : UIBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private bool useImplementationSize;

        [SerializeField]
        private GameObject implementation;

        private GameObject currentImplementation;

        private DrivenRectTransformTracker drivenTransformTracker;

        /// <inheritdoc />
        protected override void OnEnable()
        {
            base.OnEnable();

            if (implementation != null && currentImplementation == null)
                SpawnImplementation();

            RefreshDrivenTransform();
        }

        private void RefreshDrivenTransform()
        {
            drivenTransformTracker.Clear();

            if (useImplementationSize)
            {
                drivenTransformTracker.Add(this,
                                           GetComponent<RectTransform>(),
                                           DrivenTransformProperties.SizeDelta);

                if (currentImplementation != null)
                {
                    GetComponent<RectTransform>().sizeDelta =
                        currentImplementation.GetComponent<RectTransform>().sizeDelta;
                }
            }
            else if (currentImplementation != null)
            {
                currentImplementation.GetComponent<RectTransform>().anchorMin = Vector2.zero;
                currentImplementation.GetComponent<RectTransform>().anchorMax = Vector2.one;
                currentImplementation.GetComponent<RectTransform>().offsetMin = Vector2.zero;
                currentImplementation.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            }
        }

        private void SpawnImplementation()
        {
            currentImplementation = Instantiate(implementation, transform);
            currentImplementation.transform.SetSiblingIndex(0);
            currentImplementation.hideFlags = HideFlags.HideAndDontSave;
        }

        /// <inheritdoc />
        protected override void OnDisable()
        {
            base.OnDisable();

            if (!Application.isPlaying && currentImplementation != null)
                DestroyImplementation();

            drivenTransformTracker.Clear();
        }

        private void DestroyImplementation()
        {
            if (currentImplementation == null)
                return;

            if (Application.isPlaying)
                Destroy(currentImplementation);
            else
                DestroyImmediate(currentImplementation);
        }

#if UNITY_EDITOR
        /// <inheritdoc />
        protected override void OnValidate()
        {
            base.OnValidate();
            RefreshDrivenTransform();
        }
#endif

        /// <inheritdoc />
        public void OnPointerEnter(PointerEventData eventData)
        {
            SendMessageUpwards("OnWidgetHoverEnter", this, SendMessageOptions.DontRequireReceiver);
        }

        /// <inheritdoc />
        public void OnPointerExit(PointerEventData eventData)
        {
            SendMessageUpwards("OnWidgetHoverExit", this, SendMessageOptions.DontRequireReceiver);
        }
    }
}