﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI.Traits;
using UnityEngine;
using UnityEngine.Events;

namespace Scivana.UI.Implementation
{
    [ExecuteAlways]
    public class ColorImplementation : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<Color> setBackgroundColor;

        [SerializeField]
        private UnityEvent<Color> setForegroundColor;

        [SerializeField]
        private UnityEvent<Color> setShadowColor;

        [SerializeField]
        private Color backgroundColor;

        [SerializeField]
        private Color foregroundColor;

        [SerializeField]
        private Color disabledBackgroundColor;

        [SerializeField]
        private Color disabledForegroundColor;

        [SerializeField]
        private Color shadowColor;

        [SerializeField]
        private Color disabledShadowColor;

        private InteractableImplementation interactable;
        private WidgetBackgroundColor bgColor;
        private WidgetForegroundColor fgColor;

        private void OnEnable()
        {
            interactable = GetComponent<InteractableImplementation>();
            bgColor = GetComponentInParent<WidgetBackgroundColor>();
            fgColor = GetComponentInParent<WidgetForegroundColor>();

            if (interactable != null)
                interactable.InteractableChanged += RefreshColor;

            if (bgColor != null)
                bgColor.ValueChanged += RefreshColor;
            if (fgColor != null)
                fgColor.ValueChanged += RefreshColor;
            RefreshColor();
        }

        private void OnDisable()
        {
            if (bgColor != null)
                bgColor.ValueChanged -= RefreshColor;
            if (fgColor != null)
                fgColor.ValueChanged -= RefreshColor;
        }

        private void RefreshColor()
        {
            var isInteractable = interactable?.IsInteractable ?? true;

            var backgroundColor = bgColor != null ? bgColor.Value : this.backgroundColor;
            var foregroundColor = fgColor != null ? fgColor.Value : this.foregroundColor;
            backgroundColor = isInteractable
                                  ? backgroundColor
                                  : disabledBackgroundColor;
            foregroundColor = isInteractable
                                  ? foregroundColor
                                  : disabledForegroundColor;
            var shadowColor = isInteractable ? this.shadowColor : disabledShadowColor;

            setBackgroundColor?.Invoke(backgroundColor);
            setForegroundColor?.Invoke(foregroundColor);
            setShadowColor?.Invoke(shadowColor);
        }
    }
}