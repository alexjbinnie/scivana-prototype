﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI.Traits;
using UnityEngine;
using UnityEngine.UI;

namespace Scivana.UI.Implementation
{
    [ExecuteAlways]
    public class ToggleImplementation : MonoBehaviour, IWidgetImplementation<Toggle>
    {
        [SerializeField]
        private Toggle toggle;

        [SerializeField]
        private Image positiveBackground;

        [SerializeField]
        private Image negativeBackground;

        [SerializeField]
        private Image neutralBackground;

        [SerializeField]
        private Image strikeThrough;

        private void Start()
        {
            var toggleButton = GetComponentInParent<WidgetToggle>();
            UpdateState(toggleButton.Toggle);
            toggleButton.ToggleSet += UpdateState;
            toggle.onValueChanged.AddListener((a) => RefreshGraphic());
            toggle.onValueChanged.AddListener((a) => toggleButton.SetToggle(a));
        }

        private void UpdateState(bool t)
        {
            toggle.SetIsOnWithoutNotify(t);
            RefreshGraphic();
        }

        private void RefreshGraphic()
        {
            var toggled = toggle.isOn;
            positiveBackground.enabled = toggled;
            negativeBackground.enabled = !toggled;
            if (neutralBackground != null)
                neutralBackground.enabled = false;
            strikeThrough.enabled = !toggled;
        }
    }
}