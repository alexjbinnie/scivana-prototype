﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI.Traits;
using UnityEngine;
using UnityEngine.UI;

namespace Scivana.UI.Implementation
{
    [ExecuteAlways]
    public class IconImplementation : MonoBehaviour, IWidgetImplementation<WidgetIcon>
    {
        [SerializeField]
        private Image image;

        private WidgetIcon widget;

        private void OnEnable()
        {
            widget = GetComponentInParent<WidgetIcon>();
            if (widget != null)
            {
                SetIcon(widget.Value);
                widget.ValueSet += SetIcon;
            }
            else
            {
                SetIcon(null);
            }
        }

        private void OnDisable()
        {
            if (widget != null)
                widget.ValueSet -= SetIcon;
        }

        private void SetIcon(Sprite value)
        {
            if (value == null)
                image.enabled = false;
            else
            {
                image.enabled = true;
                image.sprite = value;
            }
        }
    }
}