﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.UI.Traits;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scivana.UI.Implementation
{
    [ExecuteAlways]
    public class InteractableImplementation : UIBehaviour
    {
        [SerializeField]
        private bool interactable = true;

        private bool isInteractable;

        private readonly List<CanvasGroup> canvasGroupList = new List<CanvasGroup>();

        public bool IsInteractable => isInteractable;

        public event Action InteractableChanged;

        protected override void OnEnable()
        {
            base.OnEnable();
            OnCanvasGroupChanged();
        }

        protected override void OnCanvasGroupChanged()
        {
            var groupAllowInteraction = true;
            var t = transform;
            while (t != null)
            {
                t.GetComponents(canvasGroupList);
                var shouldBreak = false;
                foreach (var canvasGroup in canvasGroupList)
                {
                    if (!canvasGroup.interactable)
                    {
                        groupAllowInteraction = false;
                        shouldBreak = true;
                    }

                    if (canvasGroup.ignoreParentGroups)
                        shouldBreak = true;
                }

                if (shouldBreak)
                    break;

                t = t.parent;
            }

            var isDisabled = GetComponentInParent<WidgetDisabled>() is { } disablable
                          && disablable.Disabled;

            var oldInteractable = isInteractable;
            isInteractable = groupAllowInteraction && interactable && !isDisabled;

            if (isInteractable != oldInteractable)
                InteractableChanged?.Invoke();
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            OnCanvasGroupChanged();
        }
#endif
    }
}