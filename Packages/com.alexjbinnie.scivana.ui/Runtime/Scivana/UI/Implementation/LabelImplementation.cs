﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.UI.Traits;
using TMPro;
using UnityEngine;

namespace Scivana.UI.Implementation
{
    [ExecuteAlways]
    public class LabelImplementation : MonoBehaviour, IWidgetImplementation<WidgetLabel>
    {
        [SerializeField]
        private TMP_Text text;

        [SerializeField]
        private TMP_Text background;

        private WidgetLabel label;

        private void OnEnable()
        {
            label = GetComponentInParent<WidgetLabel>();
            if (label != null)
            {
                SetLabel(label.Value);
                label.ValueSet += SetLabel;

                SetAlignment(label.Alignment);
                label.AlignmentSet += SetAlignment;

                SetFontSize(label.FontSize);
                label.FontSizeSet += SetFontSize;
            }
            else
            {
                SetLabel(null);
            }
        }

        private void SetLabel(string value)
        {
            text.text = value;
            background.text = value;
        }

        private void SetAlignment(WidgetLabel.LabelAlignment alignment)
        {
            if (alignment != WidgetLabel.LabelAlignment.Default)
            {
                text.alignment = ConvertAlignment(alignment);
                background.alignment = ConvertAlignment(alignment);
            }
        }

        private void SetFontSize(float? size)
        {
            if (size.HasValue)
            {
                text.fontSize = size.Value;
                background.fontSize = size.Value;
            }
        }

        private TextAlignmentOptions ConvertAlignment(WidgetLabel.LabelAlignment alignment)
        {
            switch (alignment)
            {
                case WidgetLabel.LabelAlignment.Center:
                    return TextAlignmentOptions.Center;
                case WidgetLabel.LabelAlignment.Left:
                    return TextAlignmentOptions.Left;
                case WidgetLabel.LabelAlignment.Right:
                    return TextAlignmentOptions.Right;
                default:
                    throw new InvalidOperationException($"Cannot convert {alignment}");
            }
        }
    }
}