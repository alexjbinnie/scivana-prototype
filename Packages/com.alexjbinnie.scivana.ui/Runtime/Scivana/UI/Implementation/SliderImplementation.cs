﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI.Traits;
using UnityEngine;
using UnityEngine.UI;

namespace Scivana.UI.Implementation
{
    [ExecuteAlways]
    public class SliderImplementation : MonoBehaviour
    {
        [SerializeField]
        private Slider slider;

        private WidgetSlider widget;

        private void OnEnable()
        {
            widget = GetComponentInParent<WidgetSlider>();
            if (widget != null)
            {
                RefreshValues();
                widget.ValueChanged += RefreshValues;
            }

            slider.onValueChanged.AddListener(OnSliderChanged);
        }

        private void OnDisable()
        {
            if (widget != null)
                widget.ValueChanged -= RefreshValues;
        }

        private void RefreshValues()
        {
            slider.minValue = widget.Minimum;
            slider.maxValue = widget.Maximum;
            slider.SetValueWithoutNotify(widget.Value);
        }

        private void OnSliderChanged(float val)
        {
            if (widget != null)
                widget.Value = val;
        }
    }
}