﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine.EventSystems;

namespace Scivana.UI
{
    /// <summary>
    /// An implementation of <see cref="EventTrigger" /> that also propagates the events upwards in the
    /// hierarchy, rather than capturing them and preventing further callbacks.
    /// </summary>
    public class PropagatingEventTrigger : EventTrigger
    {
        /// <inheritdoc />
        public override void OnCancel(BaseEventData eventData)
        {
            base.OnCancel(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.cancelHandler);
        }

        /// <inheritdoc />
        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.deselectHandler);
        }

        /// <inheritdoc />
        public override void OnDrag(PointerEventData eventData)
        {
            base.OnDrag(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.dragHandler);
        }

        /// <inheritdoc />
        public override void OnDrop(PointerEventData eventData)
        {
            base.OnDrop(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.dropHandler);
        }

        /// <inheritdoc />
        public override void OnMove(AxisEventData eventData)
        {
            base.OnMove(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.moveHandler);
        }

        /// <inheritdoc />
        public override void OnScroll(PointerEventData eventData)
        {
            base.OnScroll(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.scrollHandler);
        }

        /// <inheritdoc />
        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.selectHandler);
        }

        /// <inheritdoc />
        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.submitHandler);
        }

        /// <inheritdoc />
        public override void OnBeginDrag(PointerEventData eventData)
        {
            base.OnBeginDrag(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.beginDragHandler);
        }

        /// <inheritdoc />
        public override void OnEndDrag(PointerEventData eventData)
        {
            base.OnEndDrag(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.endDragHandler);
        }

        /// <inheritdoc />
        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.pointerClickHandler);
        }

        /// <inheritdoc />
        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.pointerDownHandler);
        }

        /// <inheritdoc />
        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.pointerEnterHandler);
        }

        /// <inheritdoc />
        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.pointerExitHandler);
        }

        /// <inheritdoc />
        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.pointerUpHandler);
        }

        /// <inheritdoc />
        public override void OnUpdateSelected(BaseEventData eventData)
        {
            base.OnUpdateSelected(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.updateSelectedHandler);
        }

        /// <inheritdoc />
        public override void OnInitializePotentialDrag(PointerEventData eventData)
        {
            base.OnInitializePotentialDrag(eventData);
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject,
                                           eventData,
                                           ExecuteEvents.initializePotentialDrag);
        }
    }
}