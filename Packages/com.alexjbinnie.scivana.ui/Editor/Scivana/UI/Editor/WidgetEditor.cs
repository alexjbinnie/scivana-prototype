/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.UI.Implementation;
using Scivana.UI.Traits;
using UnityEditor;
using UnityEngine;

namespace Scivana.UI.Editor
{
    [CustomEditor(typeof(Widget))]
    public class WidgetEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var obj = (target as Widget).gameObject;

            var implementation = serializedObject.FindProperty("implementation")
                                                 .objectReferenceValue as GameObject;

            if (implementation == null)
            {
                EditorGUILayout.HelpBox($"Missing implementation!",
                                        MessageType.Error);
            }

            foreach (var (name, type) in GetWidgets())
            {
                if (obj.GetComponent(type) == null)
                {
                    if (GUILayout.Button($"Add {name}"))
                        Undo.AddComponent(obj, type);
                }
                else
                {
                    if (GUILayout.Button($"Remove {name}"))
                        Undo.DestroyObjectImmediate(obj.GetComponent(type));

                    var implementationType = typeof(IWidgetImplementation<>).MakeGenericType(new[]
                    {
                        type
                    });

                    if (implementation != null &&
                        implementation.GetComponent(implementationType) == null)
                    {
                        EditorGUILayout.HelpBox($"Implementation does not implement {name}",
                                                MessageType.Error);
                    }
                }
            }
        }

        private IEnumerable<(string name, Type type)> GetWidgets()
        {
            yield return ("Button", typeof(WidgetButton));
            yield return ("Icon", typeof(WidgetIcon));
            yield return ("Label", typeof(WidgetLabel));
            yield return ("Disabled", typeof(WidgetDisabled));
        }
    }
}