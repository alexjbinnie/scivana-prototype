/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Google.Protobuf.WellKnownTypes;
using JetBrains.Annotations;
using Narupa.Protocol;
using Narupa.Protocol.Trajectory;
using Scivana.Core;
using Scivana.Core.Science;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using UnityEngine;

namespace Scivana.Network.Frame
{
    /// <summary>
    /// Conversion methods for converting <see cref="FrameData" />, the standard protocol for transmitting
    /// frame information, into a <see cref="Frame" /> representation used by the frontend.
    /// </summary>
    public static class FrameConverter
    {
        /// <summary>
        /// Convert data into a <see cref="Frame" />.
        /// </summary>
        /// <param name="data"><see cref="FrameData" /> to be converted to a frame.</param>
        /// <param name="previousFrame">A previous frame, from which to copy existing arrays if they exist.</param>
        public static (Scivana.Trajectory.Frame Frame, FrameChanges Update) ConvertFrame(
            [NotNull] FrameData data,
            [CanBeNull] Scivana.Trajectory.Frame previousFrame = null)
        {
            var frame = previousFrame != null
                            ? Scivana.Trajectory.Frame.ShallowCopy(previousFrame)
                            : new Scivana.Trajectory.Frame();
            var changes = FrameChanges.None;

            foreach (var (id, array) in data.Arrays)
            {
                frame.Data[id] = DeserializeArray(id, array);
                changes.MarkAsChanged(id);
            }

            foreach (var (id, value) in data.Values)
            {
                frame.Data[id] = DeserializeValue(id, value);
                changes.MarkAsChanged(id);
            }

            return (frame, changes);
        }

        /// <summary>
        /// Deserialize a protobuf <see cref="Value" /> to a C# object, using a converter nif defined.
        /// </summary>
        private static object DeserializeValue(string id, Value value)
        {
            return valueConverters.TryGetValue(id, out var converter)
                       ? converter(value)
                       : value.ToObject();
        }

        /// <summary>
        /// Deserialize a protobuf <see cref="ValueArray" /> to a C# object, using a converter if defined.
        /// </summary>
        private static object DeserializeArray(string id, ValueArray array)
        {
            return arrayConverters.TryGetValue(id, out var converter)
                       ? converter(array)
                       : array.ToArray();
        }

        /// <summary>
        /// Builtin array converters for <see cref="FrameData" />.
        /// </summary>
        private static readonly Dictionary<string, Converter<ValueArray, object>> arrayConverters =
            new Dictionary<string, Converter<ValueArray, object>>
            {
                [FrameData.BondArrayKey] = FrameConversions.ToBondPairArray,
                [FrameData.ParticleElementArrayKey] = FrameConversions.ToElementArray,
                [FrameData.ParticlePositionArrayKey] = Conversions.ToVector3Array,
                [FrameFields.BoxTransformation.Key] = (obj) => (object) obj.ToAffineTransformation(),
            };

        /// <summary>
        /// Builtin value converters for <see cref="FrameData" />.
        /// </summary>
        private static readonly Dictionary<string, Converter<Value, object>> valueConverters =
            new Dictionary<string, Converter<Value, object>>
            {
                [FrameData.ParticleCountValueKey] = s => s.ToInt(),
                [FrameData.ResidueCountValueKey] = s => s.ToInt(),
                [FrameData.ChainCountValueKey] = s => s.ToInt(),
            };

        /// <summary>
        /// Convert a frame into a format which is transmissible over gRPC.
        /// </summary>
        public static FrameData ToFrameData(Scivana.Trajectory.Frame frame)
        {
            var frameData = new FrameData();
            ValueArray valueArray = default;
            foreach (var (key, value) in frame.Data)
            {
                switch (value)
                {
                    case int[] intArray:
                        valueArray = new ValueArray();
                        valueArray.IndexValues = new IndexArray();
                        valueArray.IndexValues.Values.AddRange(intArray.Select(i => (uint) i));
                        frameData.Arrays[key] = valueArray;
                        break;
                    case string[] stringArray:
                        valueArray = new ValueArray();
                        valueArray.StringValues = new StringArray();
                        valueArray.StringValues.Values.AddRange(stringArray.ToArray());
                        frameData.Arrays[key] = valueArray;
                        break;
                    case float[] floatArray:
                        valueArray = new ValueArray();
                        valueArray.FloatValues = new FloatArray();
                        valueArray.FloatValues.Values.AddRange(floatArray);
                        frameData.Arrays[key] = valueArray;
                        break;
                    case BondPair[] bondArray:

                        IEnumerable<uint> BondPairAsUints(BondPair pair)
                        {
                            yield return (uint) pair.A;
                            yield return (uint) pair.B;
                        }

                        valueArray = new ValueArray();
                        valueArray.IndexValues = new IndexArray();
                        valueArray.IndexValues.Values.AddRange(bondArray.SelectMany(BondPairAsUints));
                        frameData.Arrays[key] = valueArray;
                        break;
                    case Element[] elementArray:
                        valueArray = new ValueArray();
                        valueArray.IndexValues = new IndexArray();
                        valueArray.IndexValues.Values.AddRange(elementArray.Select(e => (uint) (int) e));
                        frameData.Arrays[key] = valueArray;
                        break;
                    case Vector3[] vector3Array:

                        IEnumerable<float> VectorAsFloats(Vector3 vec)
                        {
                            yield return vec.x;
                            yield return vec.y;
                            yield return vec.z;
                        }

                        valueArray = new ValueArray();
                        valueArray.FloatValues = new FloatArray();
                        valueArray.FloatValues.Values.AddRange(vector3Array.SelectMany(VectorAsFloats));
                        frameData.Arrays[key] = valueArray;
                        break;
                    default:
                        frameData.Values[key] = value.ToProtobufValue();
                        break;
                }
            }

            return frameData;
        }
    }
}