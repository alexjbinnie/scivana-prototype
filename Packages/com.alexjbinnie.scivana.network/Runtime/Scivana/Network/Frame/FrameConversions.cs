/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Narupa.Protocol;
using Scivana.Core.Math;
using Scivana.Core.Science;
using Scivana.Trajectory;
using UnityEngine;

namespace Scivana.Network.Frame
{
    /// <summary>
    /// Conversion methods for changing between frame-specific gRPC values and C# objects.
    /// </summary>
    public static class FrameConversions
    {
        /// <summary>
        /// Convert a protobuf <see cref="ValueArray" /> to an array of <see cref="BondPair" />, by
        /// interpreting it as a flat list of integer values.
        /// </summary>
        public static BondPair[] ToBondPairArray(this ValueArray valueArray)
        {
            if (valueArray.ValuesCase != ValueArray.ValuesOneofCase.IndexValues)
                throw new ArgumentException("ValueArray is of wrong type");

            var bondValues = valueArray.IndexValues.Values;

            if (bondValues.Count % 2 > 0)
                throw new ArgumentException("Odd number of indices for bond array");

            var bondCount = bondValues.Count / 2;

            var bondArray = new BondPair[bondCount];
            for (var i = 0; i < bondCount; i++)
            {
                bondArray[i] = new BondPair(
                    (int) bondValues[2 * i],
                    (int) bondValues[2 * i + 1]);
            }

            return bondArray;
        }

        /// <summary>
        /// Convert a protobuf <see cref="ValueArray" /> to an array of <see cref="Element" />, interpreting
        /// each integer value as an atomic number.
        /// </summary>
        public static Element[] ToElementArray(this ValueArray valueArray)
        {
            if (valueArray.ValuesCase != ValueArray.ValuesOneofCase.IndexValues)
                throw new ArgumentException("ValueArray is of wrong type");

            var indexArray = valueArray.IndexValues.Values;

            var elementCount = indexArray.Count;
            var elementArray = new Element[elementCount];

            for (var i = 0; i < elementCount; i++)
                elementArray[i] = (Element) indexArray[i];

            return elementArray;
        }

        /// <summary>
        /// Convert a protobuf <see cref="ValueArray" /> to a <see cref="AffineTransformation" />.
        /// </summary>
        public static AffineTransformation ToAffineTransformation(this ValueArray valueArray)
        {
            if (valueArray == null)
                throw new ArgumentNullException(nameof(valueArray));
            if (valueArray.ValuesCase != ValueArray.ValuesOneofCase.FloatValues)
                throw new ArgumentException("ValueArray is of wrong type");

            var floatArray = valueArray.FloatValues.Values;

            return floatArray.Count switch
            {
                12 => new AffineTransformation(new Vector3(floatArray[0], floatArray[1], floatArray[2]),
                                               new Vector3(floatArray[3], floatArray[4], floatArray[5]),
                                               new Vector3(floatArray[6], floatArray[7], floatArray[8]),
                                               new Vector3(floatArray[9], floatArray[10], floatArray[11])),
                9 => new LinearTransformation(new Vector3(floatArray[0], floatArray[1], floatArray[2]),
                                              new Vector3(floatArray[3], floatArray[4], floatArray[5]),
                                              new Vector3(floatArray[6], floatArray[7], floatArray[8])),
                _ => throw new ArgumentException("Incorrect number of floats to specify an affine transformation"),
            };
        }
    }
}