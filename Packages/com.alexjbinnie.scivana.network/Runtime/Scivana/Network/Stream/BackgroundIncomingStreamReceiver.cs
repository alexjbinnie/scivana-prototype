/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading.Tasks;
using Scivana.Core.Async;

namespace Scivana.Network.Stream
{
    /// <summary>
    /// A utility to start two separate tasks for an incoming stream - a background thread which is
    /// constantly polling the gRPC stream (not limited to Unity's update loop) and merging concurrent data
    /// together, and a task which runs on the main thread and invokes a callback on this data.
    /// </summary>
    public class BackgroundIncomingStreamReceiver<TResponse>
        where TResponse : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackgroundIncomingStreamReceiver{TResponse}" /> class.
        /// </summary>
        /// <param name="stream">The stream that should be wrapped.</param>
        /// <param name="callback">Callback for when a response should be polled.</param>
        /// <param name="merge">Function to merge two responses together.</param>
        /// <param name="onException">Callback for an exception occuring in the stream.</param>
        internal BackgroundIncomingStreamReceiver(IncomingStream<TResponse> stream,
                                                  Action<TResponse> callback,
                                                  Action<TResponse, TResponse> merge,
                                                  Action<Exception> onException = null)
        {
            this.stream = stream;
            stream.MessageReceived += ReceiveOnBackgroundThread;
            BackgroundThreadTask().AwaitInBackgroundIgnoreCancellation();
            MainThreadTask().AwaitInBackgroundIgnoreCancellation();
            this.callback = callback;
            this.merge = merge;
            exceptionCallback = onException;
        }

        /// <summary>
        /// Background task which does not run on the Unity thread, and hence is not limited by the speed of
        /// the Unity update loop. This reads the stream and calls <see cref="ReceiveOnBackgroundThread" /> on
        /// each item in the stream.
        /// </summary>
        private async Task BackgroundThreadTask()
        {
            try
            {
                await Task.Run(stream.StartReceiving, stream.GetCancellationToken());
            }
            catch (StreamDisconnectedException exception)
            {
                if (exceptionCallback != null)
                    exceptionCallback.Invoke(exception);
                else
                    throw;
            }
        }

        /// <summary>
        /// Handle a response received on the background thread. As this response should not be handled until
        /// the main thread asks for it, the response is either stored (if there isn't one already stored) or
        /// merged with the existing response.
        /// </summary>
        private void ReceiveOnBackgroundThread(TResponse response)
        {
            if (receivedData == null)
                receivedData = response;
            else
                merge(receivedData, response);
        }

        private readonly IncomingStream<TResponse> stream;

        /// <summary>
        /// Task which will be run on the main thread. Once per Unity tick, it will get the latest received
        /// data (which will have involved either no new updates, a single new update or several new updates
        /// which have been merged into one) and invokes the callback.
        /// </summary>
        private async Task MainThreadTask()
        {
            while (true)
            {
                if (stream.IsCancelled)
                    return;

                if (receivedData != null)
                {
                    var newReceivedData = receivedData;
                    receivedData = null;
                    callback?.Invoke(newReceivedData);
                }

                await Task.Delay(1, stream.GetCancellationToken());
            }
        }

        private readonly Action<TResponse> callback;
        private readonly Action<TResponse, TResponse> merge;
        private readonly Action<Exception> exceptionCallback;

        private TResponse receivedData;
    }
}