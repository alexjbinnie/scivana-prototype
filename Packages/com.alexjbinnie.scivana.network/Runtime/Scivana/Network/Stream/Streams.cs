﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using Grpc.Core;

namespace Scivana.Network.Stream
{
    /// <summary>
    /// Factory methods for starting both incoming and outgoing streams.
    /// </summary>
    public static class Streams
    {
        /// <inheritdoc cref="BackgroundIncomingStreamReceiver{TResponse}" />
        public static BackgroundIncomingStreamReceiver<TResponse>
            StartInBackground<TResponse>(IncomingStream<TResponse> stream,
                                         Action<TResponse> callback,
                                         Action<TResponse, TResponse> merge,
                                         Action<Exception> onError = null)
            where TResponse : class
        {
            return new BackgroundIncomingStreamReceiver<TResponse>(stream, callback, merge, onError);
        }

        /// <summary>
        /// Call a gRPC method with the provided <paramref name="request" />,
        /// and return a stream which has not been started yet.
        /// </summary>
        public static IncomingStream<TResponse> Create<TRequest, TResponse>(
            ServerStreamingCall<TRequest, TResponse> grpcCall,
            TRequest request,
            params CancellationToken[] externalTokens)
        {
            var stream = new IncomingStream<TResponse>(externalTokens);

            stream.streamingCall = grpcCall(request,
                                            Metadata.Empty,
                                            null,
                                            stream.GetCancellationToken());

            return stream;
        }

        /// <summary>
        /// Create a stream from a gRPC call.
        /// </summary>
        public static OutgoingStream<TOutgoing, TIncoming> Create<TOutgoing, TIncoming>(
            ClientStreamingCall<TOutgoing, TIncoming> grpcCall,
            params CancellationToken[] externalTokens)
        {
            var stream = new OutgoingStream<TOutgoing, TIncoming>(externalTokens);

            stream.streamingCall = grpcCall(Metadata.Empty,
                                            null,
                                            stream.GetCancellationToken());

            return stream;
        }
    }
}