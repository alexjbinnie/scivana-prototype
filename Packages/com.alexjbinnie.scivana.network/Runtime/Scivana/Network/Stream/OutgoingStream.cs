/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Grpc.Core;
using Scivana.Core.Async;
using Channel = System.Threading.Channels.Channel;

namespace Scivana.Network.Stream
{
    /// <summary>
    /// Delegate for a gRPC server streaming call which takes a number of
    /// TOutgoing messages then replies with a single TIncoming message.
    /// </summary>
    public delegate AsyncClientStreamingCall<TOutgoing, TIncoming> ClientStreamingCall<TOutgoing,
        TIncoming>(
        Metadata headers = null,
        DateTime? deadline = null,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Wraps the outgoing request stream of a gRPC call.
    /// </summary>
    /// <remarks>
    /// Currently provides no mechanism to deal with the final response message
    /// from the server.
    /// </remarks>
    public sealed class OutgoingStream<TOutgoing, TIncoming> : Cancellable, IAsyncClosable
    {
        /// <summary>
        /// Is the stream currently active (A call to <see cref="StartSending" /> has occured).
        /// </summary>
        public bool IsSendingStarted => sendingTask != null;

        private readonly Channel<TOutgoing> messageQueue;

        internal AsyncClientStreamingCall<TOutgoing, TIncoming> streamingCall;
        private Task sendingTask;

        /// <summary>
        /// Initializes a new instance of the <see cref="OutgoingStream{TOutgoing, TIncoming}" /> class.
        /// </summary>
        internal OutgoingStream(params CancellationToken[] externalTokens)
            : base(externalTokens)
        {
            messageQueue = Channel.CreateUnbounded<TOutgoing>(new UnboundedChannelOptions
            {
                SingleWriter = true,
                SingleReader = true,
            });
        }

        /// <summary>
        /// Send a message asynchronously over the stream.
        /// </summary>
        public async Task QueueMessageAsync(TOutgoing message)
        {
            if (!IsSendingStarted)
                throw new InvalidOperationException("Stream has not started yet.");
            if (message == null)
                throw new ArgumentNullException(nameof(message), "Message cannot be null.");

            await messageQueue.Writer.WriteAsync(message);
        }

        /// <inheritdoc cref="IAsyncClosable.CloseAsync" />
        public async Task CloseAsync()
        {
            try
            {
                messageQueue.Writer.TryComplete();

                if (IsSendingStarted)
                    await sendingTask;

                Cancel();
            }
            catch (TaskCanceledException)
            {
            }

            streamingCall.Dispose();
        }

        /// <summary>
        /// Start the stream, allowing it to accept input.
        /// </summary>
        public Task StartSending()
        {
            if (IsSendingStarted)
                throw new InvalidOperationException("Streaming has already started.");

            if (IsCancelled)
                throw new InvalidOperationException("Stream has already been closed.");

            sendingTask = StartSendingLoop();

            return sendingTask;
        }

        private async Task StartSendingLoop()
        {
            var token = GetCancellationToken();

            try
            {
                while (await messageQueue.Reader.WaitToReadAsync(token))
                {
                    var message = await messageQueue.Reader.ReadAsync(token);
                    await streamingCall.RequestStream.WriteAsync(message);
                }

                await streamingCall.RequestStream.CompleteAsync();
            }
            catch (OperationCanceledException)
            {
            }
        }
    }
}