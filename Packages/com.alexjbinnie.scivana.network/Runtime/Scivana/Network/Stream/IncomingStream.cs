/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Scivana.Core.Async;

namespace Scivana.Network.Stream
{
    /// <summary>
    /// Delegate for a gRPC server streaming call
    /// </summary>
    public delegate AsyncServerStreamingCall<TReply> ServerStreamingCall<in TRequest, TReply>(
        TRequest request,
        Metadata headers = null,
        DateTime? deadline = null,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Wraps the incoming response stream of a gRPC call and raises an event
    /// when new content is received.
    /// </summary>
    public sealed class IncomingStream<TIncoming> : Cancellable, IAsyncClosable
    {
        /// <summary>
        /// Callback for when a new item is received from the stream.
        /// </summary>
        public event Action<TIncoming> MessageReceived;

        internal AsyncServerStreamingCall<TIncoming> streamingCall;
        private Task iterationTask;

        /// <summary>
        /// Initializes a new instance of the <see cref="IncomingStream{TIncoming}" /> class.
        /// </summary>
        internal IncomingStream(params CancellationToken[] externalTokens)
            : base(externalTokens)
        {
        }

        /// <summary>
        /// Start consuming the stream and raising events. Returns the
        /// iteration task.
        /// </summary>
        public Task StartReceiving()
        {
            if (iterationTask != null)
                throw new InvalidOperationException("Streaming has already started.");

            if (IsCancelled)
                throw new InvalidOperationException("Stream has already been closed.");

            var enumerator =
                new GrpcAsyncEnumeratorWrapper<TIncoming>(streamingCall.ResponseStream);

            iterationTask = enumerator.ForEachAsync(OnMessageReceived,
                                                    GetCancellationToken());

            return iterationTask;
        }

        private void OnMessageReceived(TIncoming message)
        {
            MessageReceived?.Invoke(message);
        }

        /// <summary>
        /// Wrapper around a gRPC <see cref="IAsyncStreamReader{T}" /> because
        /// <see cref="MoveNext" /> throws an <see cref="RpcException" /> when the provided
        /// token is already cancelled, instead of the expected
        /// <see cref="OperationCanceledException" />.
        /// </summary>
        private class GrpcAsyncEnumeratorWrapper<T> : IAsyncStreamReader<T>
        {
            private readonly IAsyncStreamReader<T> enumerator;

            /// <inheritdoc cref="IAsyncStreamReader{T}.Current" />
            public T Current => enumerator.Current;

            public GrpcAsyncEnumeratorWrapper(IAsyncStreamReader<T> wrapped)
            {
                enumerator = wrapped;
            }

            /// <inheritdoc cref="IAsyncStreamReader{T}.MoveNext" />
            public async Task<bool> MoveNext(CancellationToken cancellationToken)
            {
                try
                {
                    return await enumerator.MoveNext(cancellationToken);
                }
                catch (RpcException e)
                {
                    // Throw if the cancellation token is cancelled
                    cancellationToken.ThrowIfCancellationRequested();

                    // Rethrow any exception not related to cancellation
                    throw new StreamDisconnectedException("Stream disconnected.", e);
                }
            }
        }

        /// <inheritdoc cref="IAsyncClosable.CloseAsync" />
        public Task CloseAsync()
        {
            Cancel();
            return Task.CompletedTask;
        }
    }
}