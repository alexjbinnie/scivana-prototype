/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;

namespace Scivana.Network.Stream
{
    /// <summary>
    /// Extension methods for iterating over asynchronous enumerators.
    /// </summary>
    public static class AsyncEnumeratorExtensions
    {
        /// <summary>
        /// Asynchronously iterate over an enumerator and execute the callback with each
        /// element.
        /// </summary>
        /// <remarks>
        /// If an exception occurs at any point in the iteration, the whole task will end.
        /// </remarks>
        public static async Task ForEachAsync<T>(this IAsyncStreamReader<T> enumerator,
                                                 Action<T> callback,
                                                 CancellationToken cancellationToken)
        {
            while (await enumerator.MoveNext(cancellationToken))
            {
                cancellationToken.ThrowIfCancellationRequested();
                callback.Invoke(enumerator.Current);
            }
        }
    }
}