/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Google.Protobuf.WellKnownTypes;
using Narupa.Protocol;
using Scivana.Core;
using UnityEngine;

namespace Scivana.Network
{
    /// <summary>
    /// Utility methods for converting from protobuf data structures to C# objects.
    /// </summary>
    public static class Conversions
    {
        /// <summary>
        /// Convert a protobuf <see cref="ValueArray" /> to the corresponding C# array.
        /// </summary>
        public static object ToArray(this ValueArray valueArray)
        {
            switch (valueArray.ValuesCase)
            {
                case ValueArray.ValuesOneofCase.FloatValues:
                    return valueArray.FloatValues.Values.ToArray();
                case ValueArray.ValuesOneofCase.IndexValues:
                    return valueArray.IndexValues.Values.ToArray();
                case ValueArray.ValuesOneofCase.StringValues:
                    return valueArray.StringValues.Values.ToArray();
                default:
                    throw new ArgumentOutOfRangeException(
                        $"Cannot convert ValueArray of type {valueArray.ValuesCase} to an array.");
            }
        }

        /// <summary>
        /// Convert a protobuf <see cref="ListValue" /> to a list of C# objects.
        /// </summary>
        public static List<object> ToList(this ListValue value)
        {
            return value.Values
                        .Select(item => item.ToObject())
                        .ToList();
        }

        /// <summary>
        /// Convert a protobuf <see cref="Struct" /> to a C# dictionary.
        /// </summary>
        public static Dictionary<string, object> ToDictionary(this Struct value)
        {
            var list = new Dictionary<string, object>();
            if (value == null)
                return list;
            foreach (var item in value.Fields)
            {
                list.Add(item.Key, item.Value.ToObject());
            }

            return list;
        }

        /// <summary>
        /// Convert a protobuf <see cref="ValueArray" /> to an array of
        /// <see cref="Vector3" />.
        /// </summary>
        public static Vector3[] ToVector3Array(this ValueArray valueArray)
        {
            if (valueArray.ValuesCase != ValueArray.ValuesOneofCase.FloatValues)
                throw new ArgumentException("ValueArray is of wrong type");

            var positionCoordinateArray = valueArray.FloatValues.Values;

            if (positionCoordinateArray.Count % 3 > 0)
                throw new ArgumentException("Array size is not multiple of 3");

            var positionCount = positionCoordinateArray.Count / 3;
            var positionArray = new Vector3[positionCount];

            for (var i = 0; i < positionCount; i++)
            {
                positionArray[i].x = positionCoordinateArray[3 * i];
                positionArray[i].y = positionCoordinateArray[3 * i + 1];
                positionArray[i].z = positionCoordinateArray[3 * i + 2];
            }

            return positionArray;
        }

        /// <summary>
        /// Convert a protobuf <see cref="Value" /> to the corresponding C# type.
        /// </summary>
        public static object ToObject(this Value value)
        {
            return value.KindCase switch
            {
                Value.KindOneofCase.NullValue => (object) null,
                Value.KindOneofCase.NumberValue => value.NumberValue,
                Value.KindOneofCase.StringValue => value.StringValue,
                Value.KindOneofCase.BoolValue => value.BoolValue,
                Value.KindOneofCase.ListValue => value.ListValue.ToList(),
                Value.KindOneofCase.StructValue => value.StructValue.ToDictionary(),
                _ => throw new ArgumentOutOfRangeException(nameof(value)),
            };
        }

        /// <summary>
        /// Convert a C# dictionary to a protobuf Value (as a Struct).
        /// </summary>
        public static Value ToProtobufValue(this IDictionary<string, object> dictionary)
        {
            return Value.ForStruct(dictionary.ToProtobufStruct());
        }

        /// <summary>
        /// Convert a C# dictionary to a protobuf Struct.
        /// </summary>
        public static Struct ToProtobufStruct(this IDictionary<string, object> dictionary)
        {
            var @struct = new Struct();

            foreach (var (key, value) in dictionary)
            {
                @struct.Fields.Add(key, value.ToProtobufValue());
            }

            return @struct;
        }

        /// <summary>
        /// Convert a C# IEnumerable to a protobuf Value (as a Value list).
        /// </summary>
        public static Value ToProtobufValue(this IEnumerable<object> enumerable)
        {
            var values = enumerable.Select(@object => @object.ToProtobufValue());
            return Value.ForList(values.ToArray());
        }

        /// <summary>
        /// Convert a C# object to a protobuf Value.
        /// </summary>
        public static Value ToProtobufValue(this object genericObject)
        {
            return genericObject switch
            {
                Value value => value,
                null => Value.ForNull(),
                bool boolean => Value.ForBool(boolean),
                int number => Value.ForNumber(number),
                float number => Value.ForNumber(number),
                double number => Value.ForNumber(number),
                string @string => Value.ForString(@string),
                IDictionary<string, object> dictionary => dictionary.ToProtobufValue(),
                IEnumerable<object> enumerable => enumerable.ToProtobufValue(),
                _ => throw new ArgumentOutOfRangeException(nameof(genericObject),
                                                           $"Failed to convert {genericObject} to a Protobuf Value"),
            };
        }

        /// <summary>
        /// Convert a protobuf <see cref="Value" /> to an integer.
        /// </summary>
        public static int ToInt(this Value value)
        {
            if (value.KindCase != Value.KindOneofCase.NumberValue)
            {
                throw new ArgumentException(
                    "Value is not numeric, and cannot be converted to an integer");
            }

            return (int) value.NumberValue;
        }

        /// <summary>
        /// Convert a C# dictionary to a protobuf Struct.
        /// </summary>
        public static Struct ToProtobufStruct<T>(this IDictionary<string, T> dictionary)
        {
            var @struct = new Struct();

            foreach (var pair in dictionary)
            {
                @struct.Fields.Add(pair.Key, pair.Value.ToProtobufValue());
            }

            return @struct;
        }
    }
}