/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using JetBrains.Annotations;
using Narupa.Protocol.Command;
using Scivana.Core.Async;
using Scivana.Network.Stream;

namespace Scivana.Network
{
    /// <summary>
    /// Base implementation of a C# wrapper around a gRPC client to a specific service.
    /// </summary>
    public abstract class GrpcClient<TClient> : Cancellable, IAsyncClosable
        where TClient : ClientBase<TClient>
    {
        /// <summary>
        /// gRPC Client that provides access to RPC calls.
        /// </summary>
        protected TClient Client { get; }

        /// <summary>
        /// The client used to access the Command service on the same port as this client.
        /// </summary>
        private Command.CommandClient CommandClient { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GrpcClient{TClient}" /> class, which connects to a
        /// server described by the provided <see cref="GrpcConnection" />.
        /// </summary>
        protected GrpcClient([NotNull] GrpcConnection connection)
            : base(connection.GetCancellationToken())
        {
            if (connection.IsCancelled)
                throw new ArgumentException("Connection has already been shutdown.");

            Client = (TClient) Activator.CreateInstance(typeof(TClient), connection.Channel);

            CommandClient = new Command.CommandClient(connection.Channel);
        }

        /// <summary>
        /// Run a command on a gRPC service that uses the command service.
        /// </summary>
        /// <param name="command">
        /// The name of the command to run, which must be registered
        /// on the server.
        /// </param>
        /// <param name="arguments">Name/value arguments to provide to the command.</param>
        /// <returns>Dictionary of results produced by the command.</returns>
        public async Task<Dictionary<string, object>> RunCommandAsync(string command,
                                                                      Dictionary<string, object> arguments = null)
        {
            var message = new CommandMessage
            {
                Name = command,
                Arguments = arguments?.ToProtobufStruct(),
            };
            return (await CommandClient.RunCommandAsync(message)).Result.ToDictionary();
        }

        /// <summary>
        /// Get a list of all commands provided by the server.
        /// </summary>
        public async Task<string[]> GetCommandsAsync()
        {
            var request = new GetCommandsRequest();
            return (await CommandClient.GetCommandsAsync(request))
                   .Commands.Select(a => a.Name)
                   .ToArray();
        }

        /// <summary>
        /// Create an incoming stream from the definition of a gRPC call.
        /// </summary>
        protected IncomingStream<TResponse> GetIncomingStream<TRequest, TResponse>(
            ServerStreamingCall<TRequest, TResponse> call,
            TRequest request,
            CancellationToken externalToken = default)
        {
            if (IsCancelled)
                throw new InvalidOperationException("The client is closed.");

            return Streams.Create(call,
                                  request,
                                  GetCancellationToken(),
                                  externalToken);
        }

        /// <summary>
        /// Create an outgoing stream from the definition of a gRPC call.
        /// </summary>
        protected OutgoingStream<TOutgoing, TResponse> GetOutgoingStream<TOutgoing, TResponse>(
            ClientStreamingCall<TOutgoing, TResponse> call,
            CancellationToken externalToken = default)
        {
            if (IsCancelled)
                throw new InvalidOperationException("The client is closed.");

            return Streams.Create(call,
                                  GetCancellationToken(),
                                  externalToken);
        }

        /// <inheritdoc cref="IAsyncClosable.CloseAsync" />
        public Task CloseAsync()
        {
            Cancel();

            return Task.CompletedTask;
        }

        /// <summary>
        /// Close the client and cancel any streams that are active.
        /// </summary>
        public void CloseAndCancelAllSubscriptions()
        {
            CloseAsync();
        }
    }
}