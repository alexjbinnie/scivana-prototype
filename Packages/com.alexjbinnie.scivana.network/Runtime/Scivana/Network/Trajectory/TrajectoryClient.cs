/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Threading;
using JetBrains.Annotations;
using Narupa.Protocol.Trajectory;
using Scivana.Network.Stream;

namespace Scivana.Network.Trajectory
{
    /// <summary>
    /// Wraps a <see cref="TrajectoryService.TrajectoryServiceClient" /> and
    /// provides access to a stream of frames from a trajectory provided by a
    /// server over a <see cref="GrpcConnection" />.
    /// </summary>
    public sealed class TrajectoryClient : GrpcClient<TrajectoryService.TrajectoryServiceClient>
    {
        /// <summary>
        /// Command the server to play the simulation if it is paused.
        /// </summary>
        public const string CommandPlay = "playback/play";

        /// <summary>
        /// Command the server to pause the simulation if it is playing.
        /// </summary>
        public const string CommandPause = "playback/pause";

        /// <summary>
        /// Command the server to advance by one simulation step.
        /// </summary>
        public const string CommandStep = "playback/step";

        /// <summary>
        /// Command the server to reset the simulation to its initial state.
        /// </summary>
        public const string CommandReset = "playback/reset";

        /// <summary>
        /// The default rate at which trajectories should be requested.
        /// </summary>
        private const float DefaultUpdateInterval = 1f / 30f;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrajectoryClient" /> class.
        /// </summary>
        public TrajectoryClient([NotNull] GrpcConnection connection)
            : base(connection)
        {
        }

        /// <summary>
        /// Begin a new subscription to trajectory frames and asynchronously enumerate over them, calling the
        /// provided callback for each frame received.
        /// </summary>
        /// <param name="updateInterval">
        /// How many seconds the service should wait and aggregate updates between
        /// sending them to us.
        /// </param>
        /// <param name="externalToken">
        /// An external cancellation token, which allows the stream to be
        /// cancelled.
        /// </param>
        /// <remarks>
        /// Corresponds to the SubscribeLatestFrames gRPC call.
        /// </remarks>
        public IncomingStream<GetFrameResponse> SubscribeLatestFrames(
            float updateInterval = DefaultUpdateInterval,
            CancellationToken externalToken = default)
        {
            var request = new GetFrameRequest();
            request.FrameInterval = updateInterval;
            return GetIncomingStream(Client.SubscribeLatestFrames,
                                     request,
                                     externalToken);
        }
    }
}