/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Narupa.Protocol.Trajectory;
using Scivana.Core;
using Scivana.Core.Async;
using Scivana.Network.Frame;
using Scivana.Network.Stream;
using Scivana.Trajectory;

namespace Scivana.Network.Trajectory
{
    // TODO: Reconsider how the TrajectorySession exists independently of the contained client

    /// <summary>
    /// Adapts <see cref="TrajectoryClient" /> into an
    /// <see cref="ITrajectorySnapshot" /> where
    /// <see cref="ITrajectorySnapshot.CurrentFrame" /> is the latest received frame.
    /// </summary>
    public class TrajectorySession : ITrajectorySnapshot, IDisposable
    {
        /// <inheritdoc cref="ITrajectorySnapshot.CurrentFrame" />
        public Scivana.Trajectory.Frame CurrentFrame => trajectorySnapshot.CurrentFrame;

        /// <summary>
        /// The index of the latest frame received.
        /// </summary>
        public int CurrentFrameIndex { get; private set; }

        /// <inheritdoc cref="ITrajectorySnapshot.FrameChanged" />
        public event FrameChanged FrameChanged;

        /// <summary>
        /// Triggered when there is an error in the connection to the server.
        /// </summary>
        public event Action<Exception> Disconnected;

        /// <summary>
        /// Underlying <see cref="TrajectorySnapshot" /> for tracking
        /// <see cref="CurrentFrame" />.
        /// </summary>
        private readonly TrajectorySnapshot trajectorySnapshot = new TrajectorySnapshot();

        /// <summary>
        /// Underlying TrajectoryClient for receiving new frames.
        /// </summary>
        private TrajectoryClient trajectoryClient;

        private IncomingStream<GetFrameResponse> frameStream;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrajectorySession" /> class.
        /// </summary>
        public TrajectorySession()
        {
            trajectorySnapshot.FrameChanged += (sender, args) => FrameChanged?.Invoke(sender, args);
        }

        /// <summary>
        /// The rate at which updates should be streamed from the server.
        /// </summary>
        public const float TrajectoryUpdateInterval = 1f / 30f;

        /// <summary>
        /// Connect to a trajectory service over the given connection and
        /// listen in the background for frame changes. Closes any existing
        /// client.
        /// </summary>
        public void OpenClient(GrpcConnection connection)
        {
            CloseClient();
            trajectorySnapshot.Clear();

            trajectoryClient = new TrajectoryClient(connection);
            frameStream = trajectoryClient.SubscribeLatestFrames(TrajectoryUpdateInterval);
            Streams.StartInBackground(frameStream, ReceiveFrame, MergeFrames, CatchException);

            void ReceiveFrame(GetFrameResponse response)
            {
                CurrentFrameIndex = (int) response.FrameIndex;
                var (frame, changes) = FrameConverter.ConvertFrame(response.Frame, CurrentFrame);
                trajectorySnapshot.SetCurrentFrame(frame, changes);
            }

            static void MergeFrames(GetFrameResponse dest, GetFrameResponse toMerge)
            {
                dest.FrameIndex = toMerge.FrameIndex;
                foreach (var (key, array) in toMerge.Frame.Arrays)
                    dest.Frame.Arrays[key] = array;
                foreach (var (key, value) in toMerge.Frame.Values)
                    dest.Frame.Values[key] = value;
            }

            void CatchException(Exception exception)
            {
                switch (exception)
                {
                    case StreamDisconnectedException _:
                        CloseClient();
                        Disconnected?.Invoke(exception);
                        break;
                    default:
                        throw exception;
                }
            }
        }

        /// <summary>
        /// Close the current trajectory client.
        /// </summary>
        public void CloseClient()
        {
            trajectoryClient?.CloseAndCancelAllSubscriptions();
            trajectoryClient?.Dispose();
            trajectoryClient = null;

            frameStream?.CloseAsync();
            frameStream?.Dispose();
            frameStream = null;
        }

        /// <inheritdoc cref="IDisposable.Dispose" />
        public void Dispose()
        {
            CloseClient();
        }

        /// <inheritdoc cref="TrajectoryClient.CommandPlay" />
        public void Play()
        {
            trajectoryClient?.RunCommandAsync(TrajectoryClient.CommandPlay);
        }

        /// <summary>
        /// Asynchronously obtain the list of commands provided by the server.
        /// </summary>
        public async Task<string[]> GetCommandsAsync()
        {
            return await trajectoryClient?.GetCommandsAsync();
        }

        /// <inheritdoc cref="TrajectoryClient.CommandPause" />
        public void Pause()
        {
            trajectoryClient?.RunCommandAsync(TrajectoryClient.CommandPause);
        }

        /// <inheritdoc cref="TrajectoryClient.CommandReset" />
        public void Reset()
        {
            trajectoryClient?.RunCommandAsync(TrajectoryClient.CommandReset);
        }

        /// <inheritdoc cref="TrajectoryClient.CommandStep" />
        public void Step()
        {
            trajectoryClient?.RunCommandAsync(TrajectoryClient.CommandStep);
        }

        /// <summary>
        /// Execute a command of the given name with the given parameters in the background.
        /// </summary>
        public void RunCommand(string name, Dictionary<string, object> commands)
        {
            trajectoryClient?.RunCommandAsync(name, commands).AwaitInBackgroundIgnoreCancellation();
        }

        /// <summary>
        /// Execute a command of the given name with the given parameters, awaiting to see the response.
        /// </summary>
        public Task<Dictionary<string, object>> RunCommandAsync(string name, Dictionary<string, object> commands)
        {
            return trajectoryClient?.RunCommandAsync(name, commands);
        }
    }
}