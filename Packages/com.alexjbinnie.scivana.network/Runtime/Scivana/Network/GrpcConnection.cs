/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using JetBrains.Annotations;
using Scivana.Core.Async;

namespace Scivana.Network
{
    /// <summary>
    /// Represents a connection to a server using the gRPC protocol. A gRPC server provides several
    /// services, which can be connected to using <see cref="Channel" />. An implementation of a client for
    /// a gRPC service should hold this connection and reference the CancellationToken to shut itself down
    /// when the connection is terminated.
    /// </summary>
    public sealed class GrpcConnection : ICancellationTokenSource, IAsyncClosable
    {
        /// <summary>
        /// gRPC channel which represents a connection to a gRPC server.
        /// </summary>
        [CanBeNull]
        public Channel Channel { get; private set; }

        /// <summary>
        /// Is this connection cancelled?
        /// </summary>
        public bool IsCancelled => Channel == null
                                || Channel.ShutdownToken.IsCancellationRequested;

        /// <summary>
        /// The address of the server this connection points to.
        /// </summary>
        public string Address { get; }

        /// <summary>
        /// The port of the server this connection points to.
        /// </summary>
        public int Port { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GrpcConnection" /> class, representing a connection to
        /// a gRPC server and begin connecting.
        /// </summary>
        /// <param name="address">The ip address of the server.</param>
        /// <param name="port">The port of the server.</param>
        public GrpcConnection(string address, int port)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));
            if (port < 0)
                throw new ArgumentOutOfRangeException(nameof(port));

            Address = address;
            Port = port;

            Channel = new Channel($"{address}:{port}", ChannelCredentials.Insecure);
        }

        /// <exception cref="InvalidOperationException">When the connection has already been shut down.</exception>
        /// <inheritdoc cref="ICancellationTokenSource.GetCancellationToken" />
        public CancellationToken GetCancellationToken()
        {
            if (IsCancelled || Channel == null)
            {
                throw new InvalidOperationException(
                    "Trying to get a cancellation token for an already shutdown connection.");
            }

            return Channel.ShutdownToken;
        }

        /// <summary>
        /// Closes the GRPC channel asynchronously. This can be awaited or
        /// executed in the background.
        /// </summary>
        public async Task CloseAsync()
        {
            if (IsCancelled || Channel == null)
                return;

            await Channel.ShutdownAsync();
            Channel = null;
        }
    }
}