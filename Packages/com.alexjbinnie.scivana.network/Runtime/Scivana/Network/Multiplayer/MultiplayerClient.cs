﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using JetBrains.Annotations;
using Narupa.Protocol.State;
using Scivana.Network.Stream;

namespace Scivana.Network.Multiplayer
{
    /// <summary>
    /// Wraps a <see cref="Multiplayer.MultiplayerClient" /> and provides access to avatars and the shared
    /// key/value store on the server over a <see cref="GrpcConnection" />.
    /// </summary>
    public class MultiplayerClient : GrpcClient<State.StateClient>
    {
        /// <summary>
        /// The default update interval at which updates will be sent to the server.
        /// </summary>
        private const float DefaultUpdateInterval = 1f / 30f;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiplayerClient" /> class, connecting automatically
        /// using the given <see cref="GrpcConnection" />.
        /// </summary>
        public MultiplayerClient([NotNull] GrpcConnection connection)
            : base(connection)
        {
        }

        /// <summary>
        /// Starts an <see cref="IncomingStream{StateUpdate}" /> on which the server provides updates to the
        /// shared key/value store at the requested time interval (in seconds).
        /// </summary>
        /// <remarks>
        /// Corresponds to the SubscribeStateUpdates gRPC call.
        /// </remarks>
        public IncomingStream<StateUpdate> SubscribeStateUpdates(float updateInterval = DefaultUpdateInterval,
                                                                 CancellationToken externalToken = default)
        {
            var request = new SubscribeStateUpdatesRequest
            {
                UpdateInterval = updateInterval,
            };

            return GetIncomingStream(Client.SubscribeStateUpdates, request, externalToken);
        }

        /// <summary>
        /// Send an update to the server updating a set of keys, as well as removing another set.
        /// </summary>
        /// <remarks>
        /// Corresponds to the UpdateState gRPC call.
        /// </remarks>
        public async Task<bool> UpdateState(string token,
                                            Dictionary<string, object> updates,
                                            IEnumerable<string> removals)
        {
            var request = new UpdateStateRequest
            {
                AccessToken = token,
                Update = CreateStateUpdate(updates, removals),
            };

            var response = await Client.UpdateStateAsync(request);

            return response.Success;
        }

        /// <summary>
        /// Send an update on the set of locks and releases to the server.
        /// </summary>
        /// <remarks>
        /// Corresponds to the UpdateLocks gRPC call.
        /// </remarks>
        public async Task<bool> UpdateLocks(string token,
                                            IDictionary<string, float> toAcquire,
                                            IEnumerable<string> toRemove)
        {
            var request = new UpdateLocksRequest
            {
                AccessToken = token,
                LockKeys = CreateLockUpdate(toAcquire, toRemove),
            };

            var response = await Client.UpdateLocksAsync(request);

            return response.Success;
        }

        private static Struct CreateLockUpdate(IDictionary<string, float> toAcquire,
                                               IEnumerable<string> toRelease)
        {
            var str = toAcquire.ToProtobufStruct();
            foreach (var releasedKey in toRelease)
                str.Fields[releasedKey] = Value.ForNull();
            return str;
        }

        private static StateUpdate CreateStateUpdate(IDictionary<string, object> updates,
                                                     IEnumerable<string> removals)
        {
            var update = new StateUpdate();
            var updatesAsStruct = updates.ToProtobufStruct();
            update.ChangedKeys = updatesAsStruct;
            foreach (var removal in removals)
                update.ChangedKeys.Fields[removal] = Value.ForNull();
            return update;
        }
    }
}