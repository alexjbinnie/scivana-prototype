﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Narupa.Protocol.State;
using Scivana.Core;
using Scivana.Core.Async;
using Scivana.Network.Stream;

namespace Scivana.Network.Multiplayer
{
    /// <summary>
    /// Manages the state of a single user engaging in multiplayer. Tracks local and remote avatars and
    /// maintains a copy of the latest values in the shared key/value store.
    /// </summary>
    public sealed class MultiplayerSession : IRemoteSharedState, IDisposable
    {
        /// <summary>
        /// The access token to be used to communicate with the multiplayer service. This is used as a de facto
        /// identification ID for the current user.
        /// </summary>
        public string AccessToken { get; private set; }

        /// <summary>
        /// The set of multiplayer avatars.
        /// </summary>
        public MultiplayerAvatarCollection Avatars { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiplayerSession" /> class.
        /// </summary>
        public MultiplayerSession()
        {
            Avatars = new MultiplayerAvatarCollection(this);
        }

        /// <summary>
        /// Dictionary of the shared state as received directly from the server. Local modifications are not
        /// made to this dictionary.
        /// </summary>
        public Dictionary<string, object> RemoteSharedStateDictionary { get; } =
            new Dictionary<string, object>();

        /// <summary>
        /// Is there an open client on this session?
        /// </summary>
        public bool IsOpen => client != null;

        /// <summary>
        /// How many milliseconds to put between sending our requested value changes.
        /// </summary>
        public int ValuePublishInterval { get; set; } = 1000 / 30;

        private MultiplayerClient client;

        private IncomingStream<StateUpdate> IncomingValueUpdates { get; set; }

        private readonly Dictionary<string, object> pendingValues
            = new Dictionary<string, object>();

        private readonly List<string> pendingRemovals
            = new List<string>();

        private Task valueFlushingTask;

        /// <inheritdoc />
        public IEnumerable<string> RemoteKeys => RemoteSharedStateDictionary.Keys;

        /// <summary>
        /// An update to a value has been received from the server. Note that a local change may still be in
        /// effect, so use a <see cref="MultiplayerResource" /> to keep track.
        /// </summary>
        public event Action<string, object> RemoteSharedStateKeyUpdated;

        /// <summary>
        /// The request to remove a value has been received from the server. Note that a local change may still
        /// be in effect, so use a <see cref="MultiplayerResource" /> to keep track.
        /// </summary>
        public event Action<string> RemoveSharedStateKeyRemoved;

        /// <summary>
        /// An update has been received from the server.
        /// </summary>
        public event Action ReceiveStateUpdate;

        /// <summary>
        /// The multiplayer has been joined, and an access token issued which can be accessed using
        /// <see cref="AccessToken" />.
        /// </summary>
        public event Action MultiplayerJoined;

        /// <inheritdoc cref="IRemoteSharedState.NextUpdateIndex" />
        public int NextUpdateIndex { get; private set; }

        /// <inheritdoc cref="IRemoteSharedState.LastReceivedIndex" />
        public int LastReceivedIndex { get; private set; } = -1;

        /// <summary>
        /// There has been a gRPC error and the connction to the server has been severed.
        /// </summary>
        public event Action<Exception> Disconnected;

        private string UpdateIndexKey => $"update.index.{AccessToken}";

        /// <summary>
        /// Connect to a Multiplayer service over the given connection.
        /// Closes any existing client.
        /// </summary>
        public void OpenClient(GrpcConnection connection)
        {
            CloseClient();

            client = new MultiplayerClient(connection);
            AccessToken = Guid.NewGuid().ToString();

            if (valueFlushingTask == null)
            {
                valueFlushingTask = CallbackInterval(() => FlushValues(), ValuePublishInterval);
                valueFlushingTask.AwaitInBackground();
            }

            IncomingValueUpdates = client.SubscribeStateUpdates();
            Streams.StartInBackground(IncomingValueUpdates,
                                      OnResourceValuesUpdateReceived,
                                      MergeResourceUpdates,
                                      CatchException);

            void MergeResourceUpdates(StateUpdate dest, StateUpdate src)
            {
                foreach (var (key, value) in src.ChangedKeys.Fields)
                    dest.ChangedKeys.Fields[key] = value;
            }

            void CatchException(Exception exception)
            {
                switch (exception)
                {
                    case StreamDisconnectedException _:
                        CloseClient();
                        Disconnected?.Invoke(exception);
                        break;
                    default:
                        throw exception;
                }
            }

            MultiplayerJoined?.Invoke();
        }

        /// <summary>
        /// Close the current Multiplayer client and dispose all streams.
        /// </summary>
        public void CloseClient()
        {
            Avatars.DeleteLocalAvatar();
            FlushValues(true);

            client?.CloseAndCancelAllSubscriptions();
            client?.Dispose();
            client = null;

            AccessToken = null;

            ClearSharedState(false);
            pendingValues.Clear();
            pendingRemovals.Clear();
        }

        /// <inheritdoc cref="ScheduleSharedStateUpdate" />
        public void ScheduleSharedStateUpdate(string key, object value)
        {
            pendingRemovals.Remove(key);
            pendingValues[key] = value.ToProtobufValue();
        }

        /// <inheritdoc cref="ScheduleSharedStateRemoval" />
        public void ScheduleSharedStateRemoval(string key)
        {
            pendingValues.Remove(key);
            pendingRemovals.Add(key);
        }

        /// <summary>
        /// Does the shared state contain an item with the given key?
        /// </summary>
        public bool HasRemoteSharedStateValue(string key)
        {
            return RemoteSharedStateDictionary.ContainsKey(key);
        }

        /// <summary>
        /// Get a key in the shared state dictionary.
        /// </summary>
        public object GetRemoteSharedStateValue(string key)
        {
            return RemoteSharedStateDictionary.TryGetValue(key, out var value) ? value : null;
        }

        /// <summary>
        /// Attempt to gain exclusive write access to the shared value of the given key.
        /// </summary>
        public async Task<bool> LockResourceAsync(string id)
        {
            return await client.UpdateLocks(AccessToken,
                                            new Dictionary<string, float> { [id] = 1f },
                                            new string[0]);
        }

        /// <summary>
        /// Release the lock on the given object of a given key.
        /// </summary>
        public async Task<bool> ReleaseResourceAsync(string id)
        {
            return await client.UpdateLocks(AccessToken,
                                            new Dictionary<string, float>(),
                                            new[] { id });
        }

        /// <inheritdoc cref="IDisposable.Dispose" />
        public void Dispose()
        {
            FlushValues();

            CloseClient();
        }

        private void ClearSharedState(bool sendCallback = true)
        {
            var keys = RemoteSharedStateDictionary.Keys.ToList();
            RemoteSharedStateDictionary.Clear();

            if (sendCallback)
            {
                foreach (var key in keys)
                {
                    RemoveSharedStateKeyRemoved?.Invoke(key);
                }
            }
        }

        private void OnResourceValuesUpdateReceived(StateUpdate update)
        {
            ReceiveStateUpdate?.Invoke();

            if (update.ChangedKeys.Fields.ContainsKey(UpdateIndexKey))
            {
                LastReceivedIndex = (int) update.ChangedKeys
                                                .Fields[UpdateIndexKey]
                                                .NumberValue;
            }

            foreach (var (key, value1) in update.ChangedKeys.Fields)
            {
                var value = value1.ToObject();
                if (value == null)
                {
                    RemoveRemoteValue(key);
                }
                else
                {
                    UpdateRemoteValue(key, value);
                }
            }
        }

        private void FlushValues(bool force = false)
        {
            if (!IsOpen)
                return;

            if (pendingValues.Any() || pendingRemovals.Any())
            {
                pendingValues[UpdateIndexKey] = NextUpdateIndex;

                var task = client.UpdateState(AccessToken, pendingValues, pendingRemovals);

                try
                {
                    if (force)
                        task.AwaitInForegroundWithTimeout(1000);
                    else
                        task.AwaitInBackgroundHandleException((e) => Disconnected?.Invoke(e));
                }
                catch (RpcException exception)
                {
                    Disconnected?.Invoke(exception);
                }

                pendingValues.Clear();
                pendingRemovals.Clear();

                NextUpdateIndex++;
            }
        }

        private static async Task CallbackInterval(Action callback, int interval)
        {
            while (true)
            {
                callback();
                await Task.Delay(interval);
            }
        }

        private void RemoveRemoteValue(string key)
        {
            RemoteSharedStateDictionary.Remove(key);
            RemoveSharedStateKeyRemoved?.Invoke(key);
        }

        private void UpdateRemoteValue(string key, object value)
        {
            RemoteSharedStateDictionary[key] = value;
            RemoteSharedStateKeyUpdated?.Invoke(key, value);
        }

        private readonly Dictionary<string, WeakReference<MultiplayerResource>> multiplayerResources =
            new Dictionary<string, WeakReference<MultiplayerResource>>();

        /// <summary>
        /// Get a <see cref="MultiplayerResource" />, which is a typed wrapper around a
        /// value in
        /// the shared state dictionary that is automatically kept up to date with the
        /// remote
        /// shared state. It also allows its value to be set, and returns this local value
        /// until
        /// the server replies with a more up to date one. The session keeps a weak
        /// reference to
        /// each resource, so a resource with a given key is reused by repeated calls to
        /// this
        /// function. However, when everyone stops listening to the resource it will be
        /// deleted.
        /// </summary>
        public MultiplayerResource<TType> GetSharedResource<TType>(string key)
        {
            if (multiplayerResources.TryGetValue(key, out var existingWeakRef))
            {
                if (existingWeakRef.TryGetTarget(out var existing))
                {
                    if (existing is MultiplayerResource<TType> correctExisting)
                        return correctExisting;
                    throw new KeyNotFoundException("Tried getting multiplayer resource with "
                                                 + $"key {key} and type {typeof(MultiplayerResource<TType>)}, but found"
                                                 + $"existing incompatible type {existing.GetType()}");
                }
            }

            var added = new MultiplayerResource<TType>(this, key);
            multiplayerResources[key] = new WeakReference<MultiplayerResource>(added);
            ResourceCreated?.Invoke(key, added);
            return added;
        }

        /// <inheritdoc />
        public IEnumerable<(string Key, MultiplayerResource Resource)> Resources
        {
            get
            {
                foreach (var (key, weakRef) in multiplayerResources)
                {
                    if (weakRef.TryGetTarget(out var resource))
                    {
                        yield return (key, resource);
                    }
                }
            }
        }

        /// <summary>
        /// Mapping of prefixes to multiplayer collections, such that two requests for the same collection
        /// yield the
        /// same collection.
        /// </summary>
        private Dictionary<string, object> collections = new Dictionary<string, object>();

        /// <inheritdoc />
        public MultiplayerCollection<TType> GetSharedCollection<TType>(string prefix)
        {
            if (collections.TryGetValue(prefix, out var existing))
                return existing as MultiplayerCollection<TType>;
            foreach (var existingKey in collections.Keys)
            {
                if (existingKey.StartsWithIgnoreCase(prefix) || prefix.StartsWithIgnoreCase(existingKey))
                {
                    throw new ArgumentException(
                        $"Conflicting collections with keys {prefix} and {existingKey}");
                }
            }

            var added = new MultiplayerCollection<TType>(this, prefix);
            collections[prefix] = added;
            return added;
        }

        /// <summary>
        /// A new multiplayer resource has been created. This does not been it has been given a value, only
        /// that something has request a resource with the given key.
        /// </summary>
        public event Action<string, MultiplayerResource> ResourceCreated;
    }
}