/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core;

namespace Scivana.Network.Multiplayer
{
    /// <summary>
    /// Represents a collection of related keys in the multiplayer shared state, all of
    /// which will be deserialized into <typeparamref name="TType" />.
    /// </summary>
    public class MultiplayerCollection<TType> : IDictionary<string, TType>
    {
        private readonly string prefix;
        private readonly IRemoteSharedState sharedState;

        /// <summary>
        /// A resource in this collection has just been removed, either locally or remotely.
        /// </summary>
        public event Action<string> ItemRemoved;

        /// <summary>
        /// A resource in this collection has just been updated, either locally or remotely.
        /// </summary>
        public event Action<string> ItemUpdated;

        /// <summary>
        /// A resource in this collection has just been created, either locally or remotely.
        /// </summary>
        public event Action<string> ItemCreated;

        /// <summary>
        /// A set of all the <see cref="MultiplayerResource{TValue}" /> which belong in this collection and
        /// currently have a value. They are automatically added and removed due to events triggered by the
        /// resource itself.
        /// </summary>
        private readonly Dictionary<string, MultiplayerResource<TType>> resourcesWithValues =
            new Dictionary<string, MultiplayerResource<TType>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiplayerCollection{TType}" /> class, which will
        /// collate
        /// all multiplayer resources in the given shared state that begin with a certain prefix.
        /// </summary>
        internal MultiplayerCollection(IRemoteSharedState sharedState, string prefix)
        {
            this.sharedState = sharedState;
            this.prefix = prefix;
            sharedState.ResourceCreated += SharedStateOnResourceCreated;
            sharedState.RemoteSharedStateKeyUpdated += SessionOnSharedStateDictionaryKeyUpdated;

            // Add any resources that are already in the shared state.
            foreach (var (key, resource) in sharedState.Resources)
                SharedStateOnResourceCreated(key, resource);

            // Add any keys that are already in the shared state.
            foreach (var key in sharedState.RemoteKeys)
                EnsureKeyIsPresentInCollectionIfValid(key);
        }

        private void SharedStateOnResourceCreated(string key, MultiplayerResource resource)
        {
            if (!key.StartsWithIgnoreCase(prefix))
                return;

            if (!(resource is MultiplayerResource<TType> res))
            {
                throw new InvalidOperationException(
                    $"A multiplayer resource with key {key} has been assumed to be of type {typeof(TType)}, but has already been determined to be a different type.");
            }

            res.ValueRemoved += () =>
            {
                if (resourcesWithValues.ContainsKey(key))
                {
                    resourcesWithValues.Remove(key);
                    ItemRemoved?.Invoke(key);
                }
            };
            res.ValueUpdated += () =>
            {
                if (!resourcesWithValues.ContainsKey(key))
                {
                    resourcesWithValues[key] = res;
                    ItemCreated?.Invoke(key);
                }
                else
                {
                    ItemUpdated?.Invoke(key);
                }
            };
            if (res.HasValue)
            {
                resourcesWithValues[key] = res;
                ItemCreated?.Invoke(key);
            }
        }

        private void SessionOnSharedStateDictionaryKeyUpdated(string key, object value)
        {
            EnsureKeyIsPresentInCollectionIfValid(key);
        }

        private void EnsureKeyIsPresentInCollectionIfValid(string key)
        {
            if (key.StartsWithIgnoreCase(prefix) && !resourcesWithValues.ContainsKey(key))
            {
                sharedState.GetSharedResource<TType>(key);
            }
        }

        /// <inheritdoc />
        public IEnumerator<KeyValuePair<string, TType>> GetEnumerator()
        {
            foreach (var (key, resource) in resourcesWithValues)
                yield return new KeyValuePair<string, TType>(key, resource.Value);
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <inheritdoc />
        public void Add(KeyValuePair<string, TType> item)
        {
            Add(item.Key, item.Value);
        }

        /// <inheritdoc />
        public void Clear()
        {
            var resources = resourcesWithValues.Values.ToArray();
            foreach (var resource in resources)
                resource.Remove();
        }

        /// <inheritdoc />
        public bool Contains(KeyValuePair<string, TType> item)
        {
            return ContainsKey(item.Key) && Equals(this[item.Key], item.Value);
        }

        /// <inheritdoc />
        public void CopyTo(KeyValuePair<string, TType>[] array, int arrayIndex)
        {
            var i = arrayIndex;
            foreach (var pair in this)
                array[i++] = pair;
        }

        /// <inheritdoc />
        public bool Remove(KeyValuePair<string, TType> item)
        {
            return Remove(item.Key);
        }

        /// <inheritdoc />
        public int Count => resourcesWithValues.Count;

        /// <inheritdoc />
        public bool IsReadOnly => false;

        /// <inheritdoc />
        public void Add(string key, TType value)
        {
            this[key] = value;
        }

        /// <inheritdoc />
        public bool ContainsKey(string key)
        {
            return resourcesWithValues.ContainsKey(key);
        }

        /// <inheritdoc />
        public bool Remove(string key)
        {
            if (!ContainsKey(key))
                return false;
            GetResource(key).Remove();
            return true;
        }

        /// <inheritdoc />
        public bool TryGetValue(string key, out TType value)
        {
            value = default;
            if (!resourcesWithValues.ContainsKey(key))
                return false;
            value = this[key];
            return true;
        }

        /// <inheritdoc />
        public TType this[string key]
        {
            get => resourcesWithValues[key].Value;
            set
            {
                if (!resourcesWithValues.ContainsKey(key))
                    sharedState.GetSharedResource<TType>(key).SetLocalValue(value);
                else
                    resourcesWithValues[key].SetLocalValue(value);
            }
        }

        /// <summary>
        /// Get the set of keys of multiplayer resources that belong in this collection.
        /// </summary>
        public IEnumerable<string> Keys => resourcesWithValues.Keys;

        /// <inheritdoc />
        ICollection<TType> IDictionary<string, TType>.Values =>
            resourcesWithValues.Values.Select(res => res.Value).ToArray();

        /// <inheritdoc />
        ICollection<string> IDictionary<string, TType>.Keys => resourcesWithValues.Keys;

        /// <summary>
        /// Get the set of values of multiplayer resources that belong in this collection.
        /// </summary>
        public IEnumerable<TType> Values => Keys.Select(key => this[key]);

        /// <summary>
        /// Get a resource for the given key.
        /// </summary>
        public MultiplayerResource<TType> GetResource(string key)
        {
            if (resourcesWithValues.ContainsKey(key))
                return resourcesWithValues[key];
            else
                return sharedState.GetSharedResource<TType>(key);
        }

        /// <summary>
        /// The dictionary of multiplayer resources that currently have values.
        /// </summary>
        public IReadOnlyDictionary<string, MultiplayerResource<TType>> Resources => resourcesWithValues;
    }
}