/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading.Tasks;
using Scivana.Core.Async;
using Scivana.Core.Serialization;

namespace Scivana.Network.Multiplayer
{
    /// <summary>
    /// Represents a reference to an item in the multiplayer shared state. They can only be created by the
    /// <see cref="MultiplayerSession" />, which ensures for a given shared state there is exactly one
    /// <see cref="MultiplayerResource" /> for each key.
    /// </summary>
    public abstract class MultiplayerResource
    {
    }

    // TODO: Improve this documentation with an overview of multiplayer resources.

    /// <summary>
    /// Represents a reference to an item in the multiplayer shared state, which is serialized and
    /// deserialized into type <typeparamref name="TValue" />. They can only be created by the
    /// <see cref="MultiplayerSession" />, which ensures for a given shared state there is exactly one
    /// <see cref="MultiplayerResource" /> for each key.
    /// A multiplayer resource represents a 'pointer' to a value with the given key. It stores local
    /// modifications to
    /// the value, so if it is set locally this new value will be used until the remote source gets back to
    /// us.
    /// </summary>
    /// <typeparam name="TValue">The type to interpret the value of this key as.</typeparam>
    public class MultiplayerResource<TValue> : MultiplayerResource
    {
        /// <summary>
        /// The multiplayer session that hosts this resource.
        /// </summary>
        private IRemoteSharedState SharedState { get; }

        /// <summary>
        /// The full key that this resource will be found in the shared state.
        /// </summary>
        public string ResourceKey { get; }

        /// <summary>
        /// Callback for when a lock request is accepted.
        /// </summary>
        public event Action LockAccepted;

        /// <summary>
        /// Callback for when a lock request is rejected.
        /// </summary>
        public event Action LockRejected;

        /// <summary>
        /// Callback for when a lock is released.
        /// </summary>
        public event Action LockReleased;

        /// <summary>
        /// Is there a corresponding value in the remote source for this resource.
        /// </summary>
        private bool hasRemoteValue;

        /// <summary>
        /// The current value as last received from the remote source. This value only makes sense if
        /// <see cref="hasRemoteValue" /> is true.
        /// </summary>
        private TValue currentRemoteValue;

        /// <summary>
        /// The current local value of this resource, which has not been acknowledged and reflected in the
        /// remote source yet. This is only valid if <see cref="hasPendingLocalValue" /> is true.
        /// </summary>
        private TValue currentLocalValue;

        /// <summary>
        /// Is there a local change to the value which has yet to be acknowledged by the remote source.
        /// </summary>
        private bool hasPendingLocalValue;

        /// <summary>
        /// Has this value been removed locally, but not yet reflected by the remote source.
        /// </summary>
        private bool hasPendingLocalRemoval;

        /// <summary>
        /// Get the current value of the resource. This reflects any local modifications such as a change or
        /// removal which may not yet be reflected in the remote source.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Thrown if this resource does not currently have a
        /// value.
        /// </exception>
        public TValue Value
        {
            get
            {
                if (!HasValue)
                {
                    throw new InvalidOperationException(
                        "Multiplayer resource does not have Value. Call HasValue first!");
                }

                return hasPendingLocalValue ? currentLocalValue : currentRemoteValue;
            }
        }

        /// <summary>
        /// Does this multiplayer resource have a value, either locally set or from a remote source.?
        /// </summary>
        public bool HasValue
        {
            get
            {
                if (hasPendingLocalRemoval)
                    return false;
                if (hasPendingLocalValue)
                    return true;
                return hasRemoteValue;
            }
        }

        /// <summary>
        /// The current state of the lock on this resource.
        /// </summary>
        public MultiplayerResourceLockState LockState { get; private set; }

        private int sentUpdateIndex = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiplayerResource{TValue}" /> class.
        /// </summary>
        /// <param name="sharedState">The multiplayer session that will provide this value.</param>
        /// <param name="key">The key that identifies this resource in the dictionary.</param>
        internal MultiplayerResource(IRemoteSharedState sharedState, string key)
        {
            ResourceKey = key;
            SharedState = sharedState;
            hasRemoteValue = sharedState.HasRemoteSharedStateValue(key);
            currentRemoteValue = hasRemoteValue ? Deserialize(sharedState.GetRemoteSharedStateValue(key)) : default;
            LockState = MultiplayerResourceLockState.Unlocked;

            sharedState.RemoteSharedStateKeyUpdated += OnRemoteValueUpdated;
            sharedState.RemoveSharedStateKeyRemoved += OnRemoteValueRemoved;
        }

        /// <summary>
        /// Invoked when the value of this resource is updated or removed, as determined by the latest
        /// information received from the remote source.
        /// </summary>
        public event Action RemoteValueChanged;

        /// <summary>
        /// Invoked when the value of this resource is updated or removed. This may be due to receiving new
        /// information from the remote source, or from a local modification to the resource.
        /// </summary>
        public event Action ValueChanged;

        /// <summary>
        /// Invoked when the value of this resource is removed, either remotely or locally.
        /// </summary>
        public event Action ValueRemoved;

        /// <summary>
        /// Invoked when the value of this resource is updated, either remotely or locally.
        /// </summary>
        public event Action ValueUpdated;

        private void OnRemoteValueUpdated(string key, object value)
        {
            if (key != ResourceKey)
                return;

            currentRemoteValue = Deserialize(value);
            hasRemoteValue = true;

            // If we are up to date with the server, remove our pending value
            if (sentUpdateIndex <= SharedState.LastReceivedIndex)
            {
                currentLocalValue = default;
                hasPendingLocalValue = false;
                hasPendingLocalRemoval = false;
            }

            // If no local changes are pending, then the remote value is the one being used
            if (!hasPendingLocalValue && !hasPendingLocalRemoval)
            {
                ValueUpdated?.Invoke();
                ValueChanged?.Invoke();
            }

            RemoteValueChanged?.Invoke();
        }

        private void OnRemoteValueRemoved(string key)
        {
            if (key != ResourceKey)
                return;

            currentRemoteValue = default;
            hasRemoteValue = false;
            hasPendingLocalRemoval = false;

            // If no local changes are pending, then the remote value is the one being used
            if (!hasPendingLocalValue && !hasPendingLocalRemoval)
            {
                ValueRemoved?.Invoke();
                ValueChanged?.Invoke();
            }

            RemoteValueChanged?.Invoke();
        }

        /// <summary>
        /// Copy the locally set version of this value to the multiplayer service.
        /// </summary>
        private void SendLocalValueToRemote()
        {
            sentUpdateIndex = SharedState.NextUpdateIndex;
            SharedState.ScheduleSharedStateUpdate(ResourceKey, Serialize(Value));
        }

        /// <summary>
        /// Inform the multiplayer service that an item has been removed.
        /// </summary>
        private void SendLocalRemovalToRemote()
        {
            sentUpdateIndex = SharedState.NextUpdateIndex;
            SharedState.ScheduleSharedStateRemoval(ResourceKey);
        }

        /// <summary>
        /// Begin the action of obtaining a lock in the background.
        /// </summary>
        public void ObtainLock()
        {
            ObtainLockAsync().AwaitInBackground();
        }

        /// <summary>
        /// Release any current lock on this resource asynchronously in the background.
        /// </summary>
        public void ReleaseLock()
        {
            ReleaseLockAsync().AwaitInBackground();
        }

        private async Task ObtainLockAsync()
        {
            LockState = MultiplayerResourceLockState.Pending;
            var success = await SharedState.LockResourceAsync(ResourceKey);
            LockState = success
                            ? MultiplayerResourceLockState.Locked
                            : MultiplayerResourceLockState.Unlocked;
            if (success)
                OnLockAccepted();
            else
                OnLockRejected();
        }

        private void OnLockAccepted()
        {
            if (hasPendingLocalValue)
                SendLocalValueToRemote();
            LockAccepted?.Invoke();
        }

        private void OnLockRejected()
        {
            currentLocalValue = default;
            hasPendingLocalValue = false;
            hasPendingLocalRemoval = false;
            sentUpdateIndex = -1;
            LockRejected?.Invoke();
        }

        private async Task ReleaseLockAsync()
        {
            if (LockState != MultiplayerResourceLockState.Unlocked)
            {
                LockState = MultiplayerResourceLockState.Unlocked;
                LockReleased?.Invoke();
                await SharedState.ReleaseResourceAsync(ResourceKey);
            }
        }

        /// <summary>
        /// Set the local value of this key, and try to lock this resource to send it to everyone. If it is
        /// rejected, the value will revert to the default. While the lock is pending, updates are not sent to
        /// the remote source. Hence, this method should be called once per frame whilst the value should be
        /// controlled.
        /// </summary>
        public void UpdateValueAndAttemptLock(TValue value)
        {
            currentLocalValue = value;
            hasPendingLocalValue = true;
            hasPendingLocalRemoval = false;

            ValueUpdated?.Invoke();
            ValueChanged?.Invoke();

            switch (LockState)
            {
                case MultiplayerResourceLockState.Unlocked:
                    ObtainLock();
                    return;
                case MultiplayerResourceLockState.Pending:
                    return;
                case MultiplayerResourceLockState.Locked:
                    SendLocalValueToRemote();
                    return;
            }
        }

        private static object Serialize(TValue value)
        {
            return Serialization.ToDataStructure(value);
        }

        private static TValue Deserialize(object value)
        {
            return value == null ? default : Serialization.FromDataStructure<TValue>(value);
        }

        /// <summary>
        /// Set the local value of this resource and synchronise it to the remote source. This does so with no
        /// regards for the locking key mechanism.
        /// </summary>
        public void SetLocalValue(TValue value)
        {
            currentLocalValue = value;
            hasPendingLocalValue = true;
            hasPendingLocalRemoval = false;

            SendLocalValueToRemote();

            ValueUpdated?.Invoke();
            ValueChanged?.Invoke();
        }

        /// <summary>
        /// Remove the current value for this resource, and synchronise this change to the remote source. This
        /// does so with no regards for the locking key mechanism. If the remote source rejects this, the
        /// resource will just be reinstated with its remote value.
        /// </summary>
        public void Remove()
        {
            currentLocalValue = default;
            hasPendingLocalValue = false;
            hasPendingLocalRemoval = true;

            SendLocalRemovalToRemote();

            ValueRemoved?.Invoke();
            ValueChanged?.Invoke();
        }
    }
}