/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Runtime.Serialization;
using Scivana.Core.Math;
using UnityEngine;

namespace Scivana.Network.Multiplayer
{
    /// <summary>
    /// A representation of a multiplayer avatar, which is shared with other users
    /// using the shared state dictionary under the 'avatar.{playerid}' key.
    /// </summary>
    [DataContract]
    public class MultiplayerAvatar
    {
        /// <summary>
        /// Standard component name for a VR headset.
        /// </summary>
        public const string HeadsetName = "headset";

        /// <summary>
        /// Standard component name for the left controller.
        /// </summary>
        public const string LeftHandName = "hand.left";

        /// <summary>
        /// Standard component name for the right controller.
        /// </summary>
        public const string RightHandName = "hand.right";

        /// <summary>
        /// Player ID associated with this avatar.
        /// </summary>
        [DataMember(Name = "playerid")]
        public string ID { get; set; }

        /// <summary>
        /// Player name associated with this avatar.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Player color associated with this avatar.
        /// </summary>
        [DataMember(Name = "color")]
        public Color Color { get; set; }

        /// <summary>
        /// List of <see cref="Component" /> such as headsets and controllers
        /// </summary>
        [DataMember(Name = "components")]
        public List<Component> Components { get; set; } = new List<Component>();

        /// <summary>
        /// Set the three transformations used by this avatar.
        /// </summary>
        public void SetTransformations(Transformation? headsetTransformation,
                                       Transformation? leftHandTransformation,
                                       Transformation? rightHandTransformation)
        {
            Components.Clear();
            if (headsetTransformation.HasValue)
            {
                Components.Add(new Component(HeadsetName,
                                             headsetTransformation.Value.position,
                                             headsetTransformation.Value.rotation));
            }

            if (leftHandTransformation.HasValue)
            {
                Components.Add(new Component(LeftHandName,
                                             leftHandTransformation.Value.position,
                                             leftHandTransformation.Value.rotation));
            }

            if (rightHandTransformation.HasValue)
            {
                Components.Add(new Component(RightHandName,
                                             rightHandTransformation.Value.position,
                                             rightHandTransformation.Value.rotation));
            }
        }

        /// <summary>
        /// A part of an avatar, such as a headset or controller.
        /// </summary>
        [DataContract]
        public struct Component
        {
            /// <summary>
            /// The name of the component, which defines its type.
            /// </summary>
            [DataMember(Name = "name")]
            public string Name { get; set; }

            /// <summary>
            /// The position of the component.
            /// </summary>
            [DataMember(Name = "position")]
            public Vector3 Position { get; set; }

            /// <summary>
            /// The rotation of the component.
            /// </summary>
            [DataMember(Name = "rotation")]
            public Quaternion Rotation { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="Component" /> struct, with the given name, position
            /// and rotation.
            /// </summary>
            public Component(string name, Vector3 position, Quaternion rotation)
            {
                Name = name;
                Position = position;
                Rotation = rotation;
            }

            /// <summary>
            /// The component as a <see cref="UnitScaleTransformation" />
            /// </summary>
            [IgnoreDataMember]
            public UnitScaleTransformation Transformation => new UnitScaleTransformation(Position, Rotation);
        }
    }
}