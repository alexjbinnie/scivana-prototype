/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scivana.Network.Multiplayer
{
    /// <summary>
    /// An API for communication with a remote shared state, where a dictionary of arbitrary data is stored
    /// on a remote server and synced to the client. The client can override keys locally using
    /// <see cref="MultiplayerResource" /> or a <see cref="MultiplayerCollection" />, which allows a local
    /// value to be used up until a more recent update from the server is received.
    /// </summary>
    public interface IRemoteSharedState
    {
        /// <summary>
        /// Does the current copy of the remote shared state have the given key. This does not account for
        /// local changes or removals.
        /// </summary>
        bool HasRemoteSharedStateValue(string key);

        /// <summary>
        /// Get the value associated with a given key in the current copy of the remote shared state. This does
        /// not account for local changes or removals.
        /// </summary>
        object GetRemoteSharedStateValue(string key);

        /// <summary>
        /// The index of the last update that the client has sent to the server that was incorporated into the
        /// last received update from the server.
        /// </summary>
        int LastReceivedIndex { get; }

        /// <summary>
        /// The index of the next update that will sent to the server. A key `update.index.{player_id}` will be
        /// inserted with this value. By obtaining the current value of this index before sending an item, you
        /// can compare with <see cref="LastReceivedIndex" /> to determine if an update from the server has
        /// included your sent value.
        /// </summary>
        int NextUpdateIndex { get; }

        /// <summary>
        /// The set of multiplayer resources and their keys that are contained in the shared state. Resources
        /// are removed when they do not have a value.
        /// </summary>
        IEnumerable<(string Key, MultiplayerResource Resource)> Resources { get; }

        /// <summary>
        /// The set of keys as received directly from the remote source.
        /// This does not account for local changes or removals.
        /// </summary>
        IEnumerable<string> RemoteKeys { get; }

        /// <summary>
        /// Invoked when an update is received from the remote source, indicating a given key has been updated.
        /// This does not account for local changes or removals.
        /// </summary>
        event Action<string, object> RemoteSharedStateKeyUpdated;

        /// <summary>
        /// Invoked when an update is received from the remote source, indicating a given key has been removed.
        /// This does not account for local changes or removals.
        /// </summary>
        event Action<string> RemoveSharedStateKeyRemoved;

        /// <summary>
        /// Add a pending update to the shared state dictionary, which will be sent to the server at the next
        /// publish interval.
        /// </summary>
        void ScheduleSharedStateUpdate(string key, object value);

        /// <summary>
        /// Add a pending removal to the shared state dictionary, which will be sent to the server at the next
        /// publish interval.
        /// </summary>
        void ScheduleSharedStateRemoval(string key);

        /// <summary>
        /// Asynchronously lock a resource, which informs the server that no other client should be allowed to
        /// modify the value with the given key.
        /// </summary>
        Task<bool> LockResourceAsync(string key);

        /// <summary>
        /// Asynchronously release a resource, which informs the server that others clients may now modify the
        /// value with the given key.
        /// </summary>
        Task<bool> ReleaseResourceAsync(string key);

        /// <summary>
        /// Get a view of a single key, which is automatically converted to the given type. This view allows
        /// local changes and removals to be made. It is guaranteed that all
        /// <see cref="MultiplayerResource{TValue}" />'s for a given key are the same object.
        /// </summary>
        MultiplayerResource<TType> GetSharedResource<TType>(string key);

        /// <summary>
        /// Get a view of a set of keys with the same prefix, with their values automatically  converted to the
        /// given type.
        /// </summary>
        MultiplayerCollection<TType> GetSharedCollection<TType>(string prefix);

        /// <summary>
        /// Invoked when a <see cref="MultiplayerResource" /> is created for the first time. This allows
        /// collections to be aware when resources are created which should belong to them.
        /// </summary>
        event Action<string, MultiplayerResource> ResourceCreated;
    }
}