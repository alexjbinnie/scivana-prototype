/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Scivana.Network.Multiplayer
{
    /// <summary>
    /// A specific <see cref="MultiplayerCollection{TType}" /> for the set of multiplayer avatars.
    /// </summary>
    public class MultiplayerAvatarCollection : MultiplayerCollection<MultiplayerAvatar>
    {
        private readonly MultiplayerSession multiplayer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiplayerAvatarCollection" /> class for a given
        /// <see cref="MultiplayerSession" />.
        /// </summary>
        internal MultiplayerAvatarCollection(MultiplayerSession session)
            : base(session, "avatar.")
        {
            multiplayer = session;
            session.MultiplayerJoined += OnMultiplayerJoined;
        }

        private void OnMultiplayerJoined()
        {
            localAvatar = new MultiplayerAvatar
            {
                ID = multiplayer.AccessToken,
            };
        }

        /// <summary>
        /// A list of all <see cref="MultiplayerAvatar" />'s, including the current user.
        /// </summary>
        public IEnumerable<MultiplayerAvatar> AllAvatars => Values;

        /// <summary>
        /// A list of <see cref="MultiplayerAvatar" /> which are not the current player.
        /// </summary>
        public IEnumerable<MultiplayerAvatar> OtherPlayerAvatars =>
            Values.Where(avatar => avatar.ID != multiplayer.AccessToken);

        [CanBeNull]
        private MultiplayerAvatar localAvatar;

        /// <summary>
        /// The <see cref="MultiplayerAvatar" /> which is the local player, and hence not controlled by the
        /// shared state dictionary.
        /// </summary>
        public MultiplayerAvatar LocalAvatar
        {
            get
            {
                if (localAvatar == null)
                    throw new InvalidOperationException(
                        "Cannot access LocalAvatar before multiplayer has been joined.");
                return localAvatar;
            }
        }

        private string LocalAvatarId => $"avatar.{multiplayer.AccessToken}";

        /// <summary>
        /// Add your local avatar to the shared state dictionary.
        /// </summary>
        public void FlushLocalAvatar()
        {
            this[LocalAvatarId] = LocalAvatar;
        }

        /// <summary>
        /// Delete the local avatar. Efforts should be made to call this on termination of the application and
        /// the changes synced to the server, to prevent phantom avatars remaining from disconnected players.
        /// </summary>
        internal void DeleteLocalAvatar()
        {
            Remove(LocalAvatarId);
        }
    }
}