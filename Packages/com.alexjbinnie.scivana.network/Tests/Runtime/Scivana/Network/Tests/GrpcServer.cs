/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Scivana.Core.Async;

namespace Scivana.Network.Tests
{
    /// <summary>
    /// Generic gRPC server for testing purposes.
    /// </summary>
    public class GrpcServer : IAsyncClosable
    {
        private Server server;

        private LatencySimulator latency;

        public int StreamLatency
        {
            get => latency.ServerStreamLatency;
            set => latency.ServerStreamLatency = value;
        }

        public int ReplyLatency
        {
            get => latency.ServerReplyLatency;
            set => latency.ServerReplyLatency = value;
        }

        public GrpcServer(params IBindableService[] services) : this(
            services.Select(s => s.BindService()).ToArray())
        {
        }

        public GrpcServer(params ServerServiceDefinition[] services)
        {
            server = new Server
            {
                Ports =
                {
                    new ServerPort("localhost", 0, ServerCredentials.Insecure)
                }
            };
            latency = new LatencySimulator();
            foreach (var service in services)
                server.Services.Add(service.Intercept(latency));
            server.Start();
        }

        public int Port => server.Ports.First().BoundPort;

        public async Task CloseAsync()
        {
            if (server == null)
                return;
            await server.KillAsync();
            server = null;
        }

        public static (GrpcServer server, GrpcConnection connection) CreateServerAndConnection(
            params IBindableService[] services)
        {
            var server = new GrpcServer(services);
            return (server, new GrpcConnection("localhost", server.Port));
        }
    }
}