/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Narupa.Protocol.Command;

namespace Scivana.Network.Tests.Commands
{
    public class CommandService : Command.CommandBase, IBindableService
    {
        public event Action<string, Struct> ReceivedCommand;

        private List<string> possibleCommands = new List<string>();

        public void RegisterCommand(string name)
        {
            possibleCommands.Add(name);
        }

        public override Task<CommandReply> RunCommand(CommandMessage request,
                                                      ServerCallContext context)
        {
            ReceivedCommand?.Invoke(request.Name, request.Arguments);
            return Task.FromResult(new CommandReply());
        }

        public override Task<GetCommandsReply> GetCommands(
            GetCommandsRequest request,
            ServerCallContext context)
        {
            var reply = new GetCommandsReply();
            reply.Commands.AddRange(possibleCommands.Select(name => new CommandMessage
            {
                Name = name
            }));
            return Task.FromResult(reply);
        }

        public ServerServiceDefinition BindService()
        {
            return Command.BindService(this);
        }
    }
}