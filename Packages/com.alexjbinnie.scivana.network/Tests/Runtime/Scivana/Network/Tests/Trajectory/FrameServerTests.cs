/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Narupa.Protocol.Trajectory;
using NSubstitute;
using NUnit.Framework;
using Scivana.Core.Testing.Async;
using Scivana.Network.Tests.Async;
using Scivana.Network.Trajectory;

namespace Scivana.Network.Tests.Trajectory
{
    internal class FrameServerTests : BaseClientTests<QueueTrajectoryService, TrajectoryClient>
    {
        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<FrameServerTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        [SetUp]
        public void AsyncSetUp()
        {
            AsyncUnitTests.RunAsyncSetUp(this);
        }

        [TearDown]
        public void AsyncTearDown()
        {
            AsyncUnitTests.RunAsyncTearDown(this);
        }

        private FrameData data;

        protected override QueueTrajectoryService GetService()
        {
            data = new FrameData();
            data.SetBondPairs(new[]
            {
                0u, 1u, 1u, 2u
            });
            data.SetParticleElements(new[]
            {
                1u, 6u, 1u
            });
            data.SetParticlePositions(new[]
            {
                -1f, 1f, 0f, 0f, 0f, 0f, 1f, -1f, 0f
            });

            return new QueueTrajectoryService(data);
        }

        protected override TrajectoryClient GetClient(GrpcConnection connection)
        {
            return new TrajectoryClient(connection);
        }

        [AsyncSetUp]
        public override async Task SetUp()
        {
            await base.SetUp();
        }

        [AsyncTearDown]
        public override async Task TearDown()
        {
            await base.TearDown();
        }


        [AsyncTest]
        public async Task FrameDataTransmission()
        {
            var callback = Substitute.For<Action<GetFrameResponse>>();

            var stream = client.SubscribeLatestFrames();
            stream.MessageReceived += callback;
            var getFrameTask = stream.StartReceiving();

            await Task.WhenAny(getFrameTask, Task.Delay(500));

            callback.Received(1)
                    .Invoke(Arg.Is<GetFrameResponse>(rep => rep.Frame.Equals(data)));
        }
    }
}