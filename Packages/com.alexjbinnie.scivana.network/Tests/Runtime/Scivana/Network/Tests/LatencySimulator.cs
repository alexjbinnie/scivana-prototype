/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;

namespace Scivana.Network.Tests
{
    /// <summary>
    /// gRPC interceptor which adds a configurable latency to messages streamed from
    /// the server.
    /// </summary>
    public class LatencySimulator : Interceptor
    {
        private int serverStreamLatency;

        private event Action ServerStreamLatencyChanged;

        public int ServerStreamLatency
        {
            get => serverStreamLatency;
            set
            {
                serverStreamLatency = value;
                ServerStreamLatencyChanged?.Invoke();
            }
        }

        public int ServerReplyLatency { get; set; }

        public override Task ServerStreamingServerHandler<TRequest, TResponse>(TRequest request,
                                                                               IServerStreamWriter<
                                                                                       TResponse>
                                                                                   responseStream,
                                                                               ServerCallContext
                                                                                   context,
                                                                               ServerStreamingServerMethod
                                                                                   <TRequest,
                                                                                       TResponse>
                                                                                   continuation)
        {
            var delayed = new DelayedStreamWriter<TResponse>(responseStream);
            ServerStreamLatencyChanged += () => delayed.Latency = serverStreamLatency;
            return base.ServerStreamingServerHandler(request, delayed, context, continuation);
        }

        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
            TRequest request,
            ServerCallContext context,
            UnaryServerMethod<TRequest, TResponse> continuation)
        {
            var result = await base.UnaryServerHandler(request, context, continuation);
            await Task.Delay(ServerReplyLatency);
            return result;
        }
    }
}