/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Threading.Tasks;
using Narupa.Protocol.Trajectory;
using NUnit.Framework;
using Scivana.Core.Testing.Async;
using Scivana.Network.Tests.Trajectory;
using Scivana.Network.Trajectory;

namespace Scivana.Network.Tests.Session
{
    internal class TrajectorySessionSetupTests
    {
        private QueueTrajectoryService service;
        private GrpcServer server;
        private GrpcConnection connection;

        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<TrajectorySessionSetupTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        [SetUp]
        public void Setup()
        {
            service = new QueueTrajectoryService(new FrameData());
            (server, connection) = GrpcServer.CreateServerAndConnection(service);
        }

        [TearDown]
        public void AsyncTearDown()
        {
            AsyncUnitTests.RunAsyncTearDown(this);
        }

        [AsyncTearDown]
        public async Task TearDown()
        {
            await connection.CloseAsync();
            await server.CloseAsync();
        }

        [AsyncTest]
        public async Task ReceivesFrameFromServer()
        {
            var session = new TrajectorySession();
            Assert.IsNull(session.CurrentFrame);

            session.OpenClient(connection);

            void HasReceivedFrame() => Assert.IsNotNull(session.CurrentFrame);

            await AsyncAssert.PassesWithinTimeout(HasReceivedFrame);
        }
    }
}