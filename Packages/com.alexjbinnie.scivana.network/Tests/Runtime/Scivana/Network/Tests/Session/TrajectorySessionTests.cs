/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Narupa.Protocol.Trajectory;
using NSubstitute;
using NUnit.Framework;
using Scivana.Core.Testing.Async;
using Scivana.Network.Tests.Commands;
using Scivana.Network.Tests.Trajectory;
using Scivana.Network.Trajectory;

namespace Scivana.Network.Tests.Session
{
    internal class TrajectorySessionTests
    {
        private QueueTrajectoryService service;
        private CommandService commandService;
        private GrpcServer server;
        private GrpcConnection connection;
        private TrajectorySession session;
        private Action<string, Struct> callback;

        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<TrajectorySessionTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        [SetUp]
        public void Setup()
        {
            service = new QueueTrajectoryService(new FrameData());
            commandService = new CommandService();
            (server, connection) = GrpcServer.CreateServerAndConnection(service, commandService);
            session = new TrajectorySession();
            session.OpenClient(connection);
            callback = Substitute.For<Action<string, Struct>>();
            commandService.ReceivedCommand += callback;
        }

        [TearDown]
        public void AsyncTearDown()
        {
            AsyncUnitTests.RunAsyncTearDown(this);
        }

        [AsyncTearDown]
        public async Task TearDown()
        {
            session.CloseClient();
            await connection.CloseAsync();
            await server.CloseAsync();
        }

        [AsyncTest]
        public async Task CommandPlay()
        {
            session.Play();

            void ReceivedCommand() => callback.Received(1)
                                              .Invoke(TrajectoryClient.CommandPlay,
                                                      Arg.Any<Struct>());

            await AsyncAssert.PassesWithinTimeout(ReceivedCommand);
        }

        [AsyncTest]
        public async Task CommandPause()
        {
            session.Pause();

            void ReceivedCommand() => callback.Received(1)
                                              .Invoke(TrajectoryClient.CommandPause,
                                                      Arg.Any<Struct>());

            await AsyncAssert.PassesWithinTimeout(ReceivedCommand);
        }

        [AsyncTest]
        public async Task CommandReset()
        {
            session.Reset();

            void ReceivedCommand() => callback.Received(1)
                                              .Invoke(TrajectoryClient.CommandReset,
                                                      Arg.Any<Struct>());

            await AsyncAssert.PassesWithinTimeout(ReceivedCommand);
        }

        [AsyncTest]
        public async Task CommandStep()
        {
            session.Step();

            void ReceivedCommand() => callback.Received(1)
                                              .Invoke(TrajectoryClient.CommandStep,
                                                      Arg.Any<Struct>());

            await AsyncAssert.PassesWithinTimeout(ReceivedCommand);
        }

        [AsyncTest]
        public async Task ArbitraryCommand()
        {
            await session.RunCommandAsync("some_command",
                                          new Dictionary<string, object>
                                          {
                                              ["abc"] = 1.23,
                                              ["xyz"] = "string"
                                          });

            void ReceivedCommand() => callback.Received(1)
                                              .Invoke("some_command",
                                                      Arg.Is<Struct>(
                                                          s => s.Fields["abc"].NumberValue == 1.23
                                                            && s.Fields["xyz"].StringValue ==
                                                               "string"));

            await AsyncAssert.PassesWithinTimeout(ReceivedCommand);
        }
    }
}