/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Scivana.Core.Async;
using Scivana.Core.Testing.Async;

namespace Scivana.Network.Tests.Async
{
    internal abstract class ClientTests<TService, TClient> : BaseClientTests<TService, TClient>
        where TService : IBindableService
        where TClient : IAsyncClosable, ICancellable
    {
        [AsyncTest]
        public async Task CloseConnection_ConnectionToken_IsCancelled()
        {
            var connectionToken = connection.GetCancellationToken();

            await AsyncAssert.CompletesWithinTimeout(connection.CloseAsync());

            Assert.IsTrue(connectionToken.IsCancellationRequested);
        }

        [AsyncTest]
        public async Task CloseConnection_ConnectionTokenAfter_Exception()
        {
            await connection.CloseAsync();

            CancellationToken token;

            Assert.Throws<InvalidOperationException>(
                () => token = connection.GetCancellationToken());
        }

        [AsyncTest]
        public async Task CloseConnection_ClientToken_IsCancelled()
        {
            var clientToken = client.GetCancellationToken();

            await AsyncAssert.CompletesWithinTimeout(connection.CloseAsync());

            Assert.IsTrue(clientToken.IsCancellationRequested);
        }

        [AsyncTest]
        public async Task CloseConnection_ClientTokenAfter_Exception()
        {
            CancellationToken clientToken;

            await AsyncAssert.CompletesWithinTimeout(connection.CloseAsync());

            Assert.Throws<InvalidOperationException>(
                () => clientToken = client.GetCancellationToken());
        }


        [AsyncTest]
        public async Task CloseClient_ClientToken_IsCancelled()
        {
            var clientToken = client.GetCancellationToken();

            await AsyncAssert.CompletesWithinTimeout(client.CloseAsync());

            Assert.IsTrue(clientToken.IsCancellationRequested);
        }

        [AsyncTest]
        public Task CancelClient_ClientToken_IsCancelled()
        {
            var clientToken = client.GetCancellationToken();

            client.Cancel();

            Assert.IsTrue(clientToken.IsCancellationRequested);

            return Task.CompletedTask;
        }


        [AsyncTest]
        public async Task CloseClient_ClientTokenAfter_Exception()
        {
            CancellationToken clientToken;

            await AsyncAssert.CompletesWithinTimeout(client.CloseAsync());

            Assert.Throws<InvalidOperationException>(
                () => clientToken = client.GetCancellationToken());
        }

        [AsyncTest]
        public async Task CancelClient_ClientTokenAfter_Exception()
        {
            CancellationToken clientToken;

            await AsyncAssert.CompletesWithinTimeout(client.CloseAsync());

            Assert.Throws<InvalidOperationException>(
                () => clientToken = client.GetCancellationToken());
        }

        [AsyncTest]
        public async Task CloseClient_Idempotent()
        {
            await AsyncAssert.CompletesWithinTimeout(client.CloseAsync());

            await AsyncAssert.CompletesWithinTimeout(client.CloseAsync());
        }

        [AsyncTest]
        public Task CancelClient_Idempotent()
        {
            client.Cancel();

            client.Cancel();

            return Task.CompletedTask;
        }

        [AsyncTest]
        public async Task CloseThenCancelClient()
        {
            await AsyncAssert.CompletesWithinTimeout(client.CloseAsync());

            client.Cancel();
        }

        [AsyncTest]
        public async Task CancelThenCloseClient()
        {
            client.Cancel();

            await AsyncAssert.CompletesWithinTimeout(client.CloseAsync());
        }

        [AsyncTest]
        public async Task CloseClient_Simultaneous()
        {
            await AsyncAssert.CompletesWithinTimeout(
                Task.WhenAll(client.CloseAsync(), client.CloseAsync()));
        }
    }
}