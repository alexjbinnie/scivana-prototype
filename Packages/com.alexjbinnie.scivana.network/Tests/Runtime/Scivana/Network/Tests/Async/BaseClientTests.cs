/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Threading.Tasks;
using Scivana.Core.Async;
using Scivana.Core.Testing.Async;

namespace Scivana.Network.Tests.Async
{
    /// <summary>
    /// Setup a client and a server for testing gRPC.
    /// </summary>
    internal abstract class BaseClientTests<TService, TClient>
        where TService : IBindableService
        where TClient : IAsyncClosable, ICancellable
    {
        protected abstract TService GetService();
        protected abstract TClient GetClient(GrpcConnection connection);

        protected GrpcServer server;
        protected TService service;
        protected TClient client;
        protected GrpcConnection connection;

        [AsyncSetUp]
        public virtual Task SetUp()
        {
            service = GetService();
            (server, connection) = GrpcServer.CreateServerAndConnection(service);
            client = GetClient(connection);

            return Task.CompletedTask;
        }

        [AsyncTearDown]
        public virtual async Task TearDown()
        {
            await connection.CloseAsync();
            await server.CloseAsync();
        }
    }
}