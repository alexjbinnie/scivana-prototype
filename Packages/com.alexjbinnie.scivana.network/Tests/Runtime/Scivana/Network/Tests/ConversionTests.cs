﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Google.Protobuf.WellKnownTypes;
using NUnit.Framework;

namespace Scivana.Network.Tests
{
    public class ConversionTests
    {
        private static string[] testStrings = new[]
        {
            "", "a", "abc", "abc def", "abc\ndef"
        };

        private static bool[] testBools = new[]
        {
            true, false
        };

        private static double[] testNumbers = new[]
        {
            0.0, -1.0, 1.0, 4.2e3, -5.2e-2, double.NaN
        };

        [Test]
        public void StringValue([ValueSource(nameof(testStrings))] string str)
        {
            var value = Value.ForString(str);
            Assert.AreEqual(str, value.ToObject());
        }

        [Test]
        public void BoolValue([ValueSource(nameof(testBools))] bool bl)
        {
            var value = Value.ForBool(bl);
            Assert.AreEqual(bl, value.ToObject());
        }

        [Test]
        public void NumberValue([ValueSource(nameof(testNumbers))] double num)
        {
            var value = Value.ForNumber(num);
            Assert.AreEqual(num, value.ToObject());
        }

        [Test]
        public void NullValue()
        {
            var value = Value.ForNull();
            Assert.AreEqual(null, value.ToObject());
        }

        [Test]
        public void EmptyList()
        {
            var value = Value.ForList();
            CollectionAssert.AreEqual(Array.Empty<object>(), value.ToObject() as IEnumerable);
        }

        [Test]
        public void MixedList()
        {
            var value = Value.ForList(Value.ForString("abc"), Value.ForNumber(1.0),
                                      Value.ForBool(false));
            CollectionAssert.AreEqual(new object[]
            {
                "abc", 1.0, false
            }, value.ToObject() as IEnumerable);
        }

        [Test]
        public void EmptyStruct()
        {
            var value = new Struct();
            CollectionAssert.AreEqual(new Dictionary<string, object>(), value.ToDictionary());
        }
    }
}