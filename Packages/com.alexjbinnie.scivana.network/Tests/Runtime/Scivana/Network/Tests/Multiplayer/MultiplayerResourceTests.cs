/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using Scivana.Core.Testing;
using Scivana.Core.Testing.Async;
using Scivana.Network.Multiplayer;

namespace Scivana.Network.Tests.Multiplayer
{
    public class MultiplayerResourceTests
    {
        private MultiplayerService service;
        private GrpcServer server;
        private MultiplayerSession session;
        private GrpcConnection connection;

        [SetUp]
        public void AsyncSetup()
        {
            AsyncUnitTests.RunAsyncSetUp(this);
        }

        [AsyncSetUp]
        public async Task Setup()
        {
            service = new MultiplayerService();
            server = new GrpcServer(service);

            session = new MultiplayerSession();

            connection = new GrpcConnection("localhost", server.Port);
            session.OpenClient(connection);

            await Task.Delay(500);
        }

        [TearDown]
        public void AsyncTearDown()
        {
            AsyncUnitTests.RunAsyncTearDown(this);
        }

        [AsyncTearDown]
        public async Task TearDown()
        {
            session?.CloseClient();
            if (connection != null)
                await connection.CloseAsync();
            if (server != null)
                await server.CloseAsync();
        }

        private const string Key = "abc";
        private const string OtherPlayerId = "other_player";

        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<MultiplayerResourceTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        public MultiplayerResource<string> GetResource()
        {
            return session.GetSharedResource<string>(Key);
        }

        [AsyncTest]
        public async Task GetResource_InitialValue()
        {
            service.SetValueDirect(Key, "my_value");
            await Task.Delay(500);
            var resource = GetResource();
            Assert.AreEqual("my_value", resource.Value);
        }

        [AsyncTest]
        public async Task RemoteValueChanged_Occurs()
        {
            var resource = GetResource();
            var callback = CallbackAssert.Callback();
            resource.RemoteValueChanged += callback;
            service.SetValueDirect(Key, "my_value");

            await Task.Delay(500);

            CallbackAssert.WasCalled(callback);
        }

        [AsyncTest]
        public async Task SimulateSlowConnection()
        {
            var resource = GetResource();
            service.SetValueDirect(Key, "old_value");
            await Task.Delay(500);

            // Initially, resource and local share state are "old_value"
            Assert.AreEqual("old_value", resource.Value);
            Assert.AreEqual("old_value", session.GetRemoteSharedStateValue(Key));

            // Introduce 500 ms latency to resource stream
            server.StreamLatency = 500;

            // Set the resource value to "new_value". Latency means local shared state is
            // out of date
            resource.UpdateValueAndAttemptLock("new_value");
            await Task.Delay(50);
            Assert.AreEqual("new_value", resource.Value);
            Assert.AreEqual("old_value", session.GetRemoteSharedStateValue(Key));

            // Release the lock. Latency means local shared state is out of data
            resource.ReleaseLock();
            Assert.AreEqual("new_value", resource.Value);
            Assert.AreEqual("old_value", session.GetRemoteSharedStateValue(Key));

            // Even a short time later, local shared state is out of date. However, 
            // value is still correct as an up-to-date update has not been received.
            await Task.Delay(50);
            Assert.AreEqual("new_value", resource.Value);
            Assert.AreEqual("old_value", session.GetRemoteSharedStateValue(Key));

            // After latency has been overcome, values now agree.
            await Task.Delay(1000);
            Assert.AreEqual("new_value", resource.Value);
            Assert.AreEqual("new_value", session.GetRemoteSharedStateValue(Key));
        }
    }
}