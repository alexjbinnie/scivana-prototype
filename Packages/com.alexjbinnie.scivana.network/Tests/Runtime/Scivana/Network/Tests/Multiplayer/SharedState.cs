/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Scivana.Core;
using Scivana.Network.Multiplayer;

namespace Scivana.Network.Tests.Multiplayer
{
    /// <summary>
    /// A testing example of a shared state interface which does not require a server
    /// or gRPC
    /// connection, hence elinimating concerns about synchronizity when testing nuances
    /// of
    /// multiplayer resources & collections.
    /// </summary>
    public class SharedState : IRemoteSharedState
    {
        private Dictionary<string, object> remoteSharedState = new Dictionary<string, object>();

        private List<string> pendingRemovals = new List<string>();
        private Dictionary<string, object> pendingUpdates = new Dictionary<string, object>();

        public bool HasRemoteSharedStateValue(string key)
        {
            return remoteSharedState.ContainsKey(key);
        }

        public object GetRemoteSharedStateValue(string key)
        {
            return remoteSharedState[key];
        }

        public int LastReceivedIndex { get; private set; } = -1;
        public int NextUpdateIndex { get; private set; } = 0;

        public IEnumerable<(string, MultiplayerResource)> Resources
        {
            get
            {
                foreach (var (key, resource) in resources)
                    yield return (key, resource);
            }
        }

        public IEnumerable<string> RemoteKeys => resources.Keys;

        public void ReplyToChanges()
        {
            LastReceivedIndex = NextUpdateIndex;
            NextUpdateIndex++;
            foreach (var key in pendingRemovals)
            {
                remoteSharedState.Remove(key);
                RemoveSharedStateKeyRemoved?.Invoke(key);
            }

            foreach (var (key, value) in pendingUpdates)
            {
                remoteSharedState[key] = value;
                RemoteSharedStateKeyUpdated?.Invoke(key, value);
            }

            pendingRemovals.Clear();
            pendingUpdates.Clear();
        }

        public event Action<string, object> RemoteSharedStateKeyUpdated;

        public event Action<string> RemoveSharedStateKeyRemoved;

        public void ScheduleSharedStateUpdate(string key, object value)
        {
            pendingUpdates[key] = value;
            pendingRemovals.Remove(key);
        }

        public void ScheduleSharedStateRemoval(string key)
        {
            pendingRemovals.Add(key);
            pendingUpdates.Remove(key);
        }

        public Task<bool> LockResourceAsync(string key)
        {
            return Task.FromResult(true);
        }

        public Task<bool> ReleaseResourceAsync(string key)
        {
            return Task.FromResult(true);
        }

        private readonly Dictionary<string, MultiplayerResource> resources =
            new Dictionary<string, MultiplayerResource>();

        public MultiplayerResource<TType> GetSharedResource<TType>(string key)
        {
            if (resources.TryGetValue(key, out var existing))
                return existing as MultiplayerResource<TType>;
            var added = new MultiplayerResource<TType>(this, key);
            resources[key] = added;
            ResourceCreated?.Invoke(key, added);
            return added;
        }

        public MultiplayerCollection<TType> GetSharedCollection<TType>(string prefix)
        {
            return new MultiplayerCollection<TType>(this, prefix);
        }

        public event Action<string, MultiplayerResource> ResourceCreated;

        public void SetRemoteValueAndSendChanges(string key, object value)
        {
            ScheduleSharedStateUpdate(key, value);
            ReplyToChanges();
        }

        public void RemoveRemoteValueAndSendChanges(string key)
        {
            ScheduleSharedStateRemoval(key);
            ReplyToChanges();
        }
    }
}