/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Runtime.Serialization;
using NSubstitute;
using NUnit.Framework;
using Scivana.Core.Serialization;
using Scivana.Network.Multiplayer;

namespace Scivana.Network.Tests.Multiplayer
{
    [DataContract]
    [Serializable]
    internal class Item
    {
        protected bool Equals(Item other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Item) obj);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }

        [DataMember(Name = "value")]
        public string Value;

        public Item(string value)
        {
            Value = value;
        }
    }

    internal class MultiplayerCollectionTests
    {
        private SharedState sharedState;
        private MultiplayerCollection<Item> collection;

        [SetUp]
        public void Setup()
        {
            sharedState = new SharedState();

            collection = sharedState.GetSharedCollection<Item>("item.");
        }

        [Test]
        public void IsInitialCollectionEmpty()
        {
            Assert.AreEqual(0, collection.Count);
        }

        private void AddItemToRemoteAndSendChanges(string key, Item item)
        {
            sharedState.SetRemoteValueAndSendChanges(key,
                                                     Serialization.ToDataStructure(item));
        }

        private void RemoveItemFromRemoteAndSendChanges(string key)
        {
            sharedState.RemoveRemoteValueAndSendChanges(key);
        }


        [Test]
        public void AddItemRemote_IsItemInCollection()
        {
            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            Assert.AreEqual(1, collection.Count);
            Assert.AreEqual(item, collection["item.abc"]);
        }

        [Test]
        public void AddItemRemote_IsItemCreatedInvoked()
        {
            var itemCreated = Substitute.For<Action<string>>();
            collection.ItemCreated += itemCreated;

            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            itemCreated.Received(1).Invoke("item.abc");
        }

        [Test]
        public void AddItemLocal_RemoteDisagrees()
        {
            var item = new Item("value");
            collection.Add("item.abc", item);

            Assert.IsTrue(collection.ContainsKey("item.abc"));
            Assert.IsFalse(sharedState.HasRemoteSharedStateValue("item.abc"));
        }

        [Test]
        public void AddItemLocal_SendChanges_RemoteAgrees()
        {
            var item = new Item("value");
            collection.Add("item.abc", item);

            sharedState.ReplyToChanges();

            Assert.IsTrue(collection.ContainsKey("item.abc"));
            Assert.IsTrue(sharedState.HasRemoteSharedStateValue("item.abc"));
        }

        [Test]
        public void AddItemLocal_IsItemInCollection()
        {
            var item = new Item("value");
            collection.Add("item.abc", item);

            Assert.AreEqual(1, collection.Count);
            Assert.AreEqual(item, collection["item.abc"]);
        }

        [Test]
        public void AddItemLocal_IsItemCreatedInvoked()
        {
            var itemCreated = Substitute.For<Action<string>>();
            collection.ItemCreated += itemCreated;

            var item = new Item("value");
            collection.Add("item.abc", item);

            itemCreated.Received(1).Invoke("item.abc");
        }

        [Test]
        public void AddItemLocal_PushChanges_IsItemCreatedInvokedOnce()
        {
            var itemCreated = Substitute.For<Action<string>>();
            collection.ItemCreated += itemCreated;

            var itemRemoved = Substitute.For<Action<string>>();
            collection.ItemUpdated += itemRemoved;

            var item = new Item("value");
            collection.Add("item.abc", item);

            sharedState.ReplyToChanges();

            itemCreated.Received(1).Invoke("item.abc");
            itemRemoved.Received(1).Invoke("item.abc");
        }

        [Test]
        public void RemoveItemRemote_IsItemNotInCollection()
        {
            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            RemoveItemFromRemoteAndSendChanges("item.abc");

            Assert.AreEqual(0, collection.Count);
        }

        [Test]
        public void RemoveItemRemote_IsItemRemovedInvoked()
        {
            var itemRemoved = Substitute.For<Action<string>>();
            collection.ItemRemoved += itemRemoved;

            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            RemoveItemFromRemoteAndSendChanges("item.abc");

            itemRemoved.Received(1).Invoke("item.abc");
        }

        [Test]
        public void RemoveItemLocal_RemoteDisagrees()
        {
            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            collection.Remove("item.abc");

            Assert.IsFalse(collection.ContainsKey("item.abc"));
            Assert.IsTrue(sharedState.HasRemoteSharedStateValue("item.abc"));
        }

        [Test]
        public void RemoveItemLocal_SendChanges_RemoteAgrees()
        {
            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            collection.Remove("item.abc");

            sharedState.ReplyToChanges();

            Assert.IsFalse(collection.ContainsKey("item.abc"));
            Assert.IsFalse(sharedState.HasRemoteSharedStateValue("item.abc"));
        }

        [Test]
        public void RemoveItemLocal_IsItemInCollection()
        {
            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            collection.Remove("item.abc");

            Assert.AreEqual(0, collection.Count);
        }

        [Test]
        public void RemoveItemLocal_IsItemRemovedInvoked()
        {
            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            var itemRemoved = Substitute.For<Action<string>>();
            collection.ItemRemoved += itemRemoved;

            collection.Remove("item.abc");

            itemRemoved.Received(1).Invoke("item.abc");
        }

        [Test]
        public void RemoveItemLocal_PushChanges_IsItemRemovedInvokedOnce()
        {
            var item = new Item("value");
            AddItemToRemoteAndSendChanges("item.abc", item);

            var itemRemoved = Substitute.For<Action<string>>();
            collection.ItemRemoved += itemRemoved;

            collection.Remove("item.abc");

            sharedState.ReplyToChanges();

            itemRemoved.Received(1).Invoke("item.abc");
        }
    }
}