/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using Scivana.Core.Testing.Async;
using Scivana.Network.Multiplayer;

namespace Scivana.Network.Tests.Multiplayer
{
    internal class TwoPersonMultiplayerSessionTests
    {
        private MultiplayerService service;
        private GrpcServer server;
        private MultiplayerSession session;
        private GrpcConnection connection;

        private MultiplayerSession session2;
        private GrpcConnection connection2;

        [SetUp]
        public void AsyncSetup()
        {
            AsyncUnitTests.RunAsyncSetUp(this);
        }

        [AsyncSetUp]
        public async Task Setup()
        {
            service = new MultiplayerService();
            server = new GrpcServer(service);

            session = new MultiplayerSession();
            session2 = new MultiplayerSession();

            connection = new GrpcConnection("localhost", server.Port);
            session.OpenClient(connection);

            connection2 = new GrpcConnection("localhost", server.Port);
            session2.OpenClient(connection2);

            await Task.Delay(500);
        }

        [TearDown]
        public void AsyncTearDown()
        {
            AsyncUnitTests.RunAsyncTearDown(this);
        }

        [AsyncTearDown]
        public async Task TearDown()
        {
            session?.CloseClient();
            session2?.CloseClient();
            if (connection != null)
                await connection.CloseAsync();
            if (connection2 != null)
                await connection2.CloseAsync();
            if (server != null)
                await server.CloseAsync();
        }

        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<TwoPersonMultiplayerSessionTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        [AsyncTest]
        public async Task TryLock_SomeoneElseHasLock()
        {
            var value2 = session2.GetSharedResource<double>("abc");
            value2.ObtainLock();

            void DoesPlayer2HaveLock() => Assert.IsTrue(service.Locks.TryGetValue("abc", out var v1)
                                                     && v1.Equals(session2.AccessToken));

            await AsyncAssert.PassesWithinTimeout(DoesPlayer2HaveLock);

            var value1 = session.GetSharedResource<double>("abc");
            value1.ObtainLock();

            Assert.AreEqual(MultiplayerResourceLockState.Pending, value1.LockState);

            void IsPlayer1LockRejected() =>
                Assert.AreEqual(MultiplayerResourceLockState.Unlocked, value1.LockState);


            await AsyncAssert.PassesWithinTimeout(IsPlayer1LockRejected);
            await AsyncAssert.PassesWithinTimeout(DoesPlayer2HaveLock);
        }

        [AsyncTest]
        public async Task TryLock_SomeoneElseHadLockThenReleased()
        {
            var value2 = session2.GetSharedResource<double>("abc");
            value2.ObtainLock();

            void DoesPlayer2HaveLock() => Assert.IsTrue(service.Locks.TryGetValue("abc", out var v1)
                                                     && v1.Equals(session2.AccessToken));

            await AsyncAssert.PassesWithinTimeout(DoesPlayer2HaveLock);

            var value1 = session.GetSharedResource<double>("abc");
            value1.ObtainLock();

            Assert.AreEqual(MultiplayerResourceLockState.Pending, value1.LockState);

            void IsPlayer1LockRejected() =>
                Assert.AreEqual(MultiplayerResourceLockState.Unlocked, value1.LockState);

            void IsPlayer1LockAccepted() =>
                Assert.AreEqual(MultiplayerResourceLockState.Locked, value1.LockState);

            void DoesPlayer1HaveLock() => Assert.IsTrue(service.Locks.TryGetValue("abc", out var v1)
                                                     && v1.Equals(session.AccessToken));

            await AsyncAssert.PassesWithinTimeout(IsPlayer1LockRejected);
            await AsyncAssert.PassesWithinTimeout(DoesPlayer2HaveLock);

            value2.ReleaseLock();

            void NoLocks() => CollectionAssert.IsEmpty(service.Locks);

            await AsyncAssert.PassesWithinTimeout(NoLocks);

            value1.ObtainLock();
            Assert.AreEqual(MultiplayerResourceLockState.Pending, value1.LockState);

            await AsyncAssert.PassesWithinTimeout(IsPlayer1LockAccepted);
            await AsyncAssert.PassesWithinTimeout(DoesPlayer1HaveLock);
        }
    }
}