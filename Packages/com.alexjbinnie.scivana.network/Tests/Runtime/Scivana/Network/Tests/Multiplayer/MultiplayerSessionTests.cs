/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Scivana.Core.Testing.Async;
using Scivana.Network.Multiplayer;

namespace Scivana.Network.Tests.Multiplayer
{
    internal class MultiplayerSessionTests
    {
        private MultiplayerService service;
        private GrpcServer server;
        private MultiplayerSession session;
        private GrpcConnection connection;

        [SetUp]
        public void AsyncSetup()
        {
            AsyncUnitTests.RunAsyncSetUp(this);
        }

        [AsyncSetUp]
        public async Task Setup()
        {
            service = new MultiplayerService();
            server = new GrpcServer(service);

            session = new MultiplayerSession();

            connection = new GrpcConnection("localhost", server.Port);
            session.OpenClient(connection);

            await Task.Delay(500);
        }

        [TearDown]
        public void AsyncTearDown()
        {
            AsyncUnitTests.RunAsyncTearDown(this);
        }

        [AsyncTearDown]
        public async Task TearDown()
        {
            session?.CloseClient();
            if (connection != null)
                await connection.CloseAsync();
            if (server != null)
                await server.CloseAsync();
        }

        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<MultiplayerSessionTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        [AsyncTest]
        public async Task ValueChanged_ClientGetsUpdate()
        {
            service.SetValueDirect("abc", 1.2);

            void HasReceivedKey() => CollectionAssert.Contains(
                session.RemoteSharedStateDictionary.Keys,
                "abc");

            await AsyncAssert.PassesWithinTimeout(HasReceivedKey);
        }

        [AsyncTest]
        public async Task ValueChanged_ClientCallback()
        {
            var callback = Substitute.For<Action<string, object>>();
            session.RemoteSharedStateKeyUpdated += callback;

            void HasReceivedCallback() =>
                callback.Received(1).Invoke(Arg.Is("abc"), Arg.Any<object>());

            service.SetValueDirect("abc", 1.2);

            await AsyncAssert.PassesWithinTimeout(HasReceivedCallback);
        }

        [AsyncTest]
        public async Task ValueChanged_MultiplayerResource()
        {
            var value = session.GetSharedResource<double>("abc");
            service.SetValueDirect("abc", 1.2);

            void HasReceivedValue() => Assert.AreEqual(1.2, value.Value);

            await AsyncAssert.PassesWithinTimeout(HasReceivedValue);
        }

        [AsyncTest]
        public async Task ValueChanged_MultiplayerResourceCallback()
        {
            var callback = Substitute.For<Action>();
            var value = session.GetSharedResource<double>("abc");
            value.ValueChanged += callback;

            service.SetValueDirect("abc", 1.2);

            void HasReceivedCallback() => callback.Received(1).Invoke();

            await AsyncAssert.PassesWithinTimeout(HasReceivedCallback);
        }

        [AsyncTest]
        public async Task TryLock_Success()
        {
            server.ReplyLatency = 400;
            var value = session.GetSharedResource<double>("abc");
            value.ObtainLock();
            Assert.AreEqual(MultiplayerResourceLockState.Pending, value.LockState);

            void LockSuccessful()
            {
                Assert.AreEqual(MultiplayerResourceLockState.Locked, value.LockState);
                Assert.IsTrue(service.Locks.TryGetValue("abc", out var v)
                           && v.Equals(session.AccessToken));
            }

            await AsyncAssert.PassesWithinTimeout(LockSuccessful, timeout: 2000);
        }

        [Test]
        public void GetSharedResource_CalledTwice_ReturnSameObject()
        {
            var resource1 = session.GetSharedResource<string>("abc");
            var resource2 = session.GetSharedResource<string>("abc");
            Assert.AreSame(resource1, resource2);
        }
    }
}