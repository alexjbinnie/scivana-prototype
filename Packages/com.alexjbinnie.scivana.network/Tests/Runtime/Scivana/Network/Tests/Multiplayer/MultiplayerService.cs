/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Narupa.Protocol.State;
using Scivana.Core;
using Scivana.Core.Collections;

namespace Scivana.Network.Tests.Multiplayer
{
    public class MultiplayerService : State.StateBase, IBindableService
    {
        private ObservableDictionary<string, Value> resources
            = new ObservableDictionary<string, Value>();

        private Dictionary<string, string> locks = new Dictionary<string, string>();

        public IDictionary<string, Value> Resources => resources;

        public IReadOnlyDictionary<string, string> Locks => locks;

        public override Task<UpdateLocksResponse> UpdateLocks(
            UpdateLocksRequest request,
            ServerCallContext context)
        {
            var token = request.AccessToken;

            foreach (var requestKey in request.LockKeys.Fields.Keys)
                if (locks.ContainsKey(requestKey) && locks[requestKey] != token)
                    return Task.FromResult(new UpdateLocksResponse
                    {
                        Success = false
                    });
            foreach (var (key, lockTime) in request.LockKeys.Fields)
            {
                if (lockTime.KindCase == Value.KindOneofCase.NullValue)
                {
                    locks.Remove(key);
                }
                else
                {
                    locks[key] = token;
                }
            }

            return Task.FromResult(new UpdateLocksResponse
            {
                Success = true
            });
        }

        public override Task<UpdateStateResponse> UpdateState(
            UpdateStateRequest request,
            ServerCallContext context)
        {
            var token = request.AccessToken;
            foreach (var requestKey in request.Update.ChangedKeys.Fields.Keys)
                if (locks.ContainsKey(requestKey) && locks[requestKey] != token)
                    return Task.FromResult(new UpdateStateResponse
                    {
                        Success = false
                    });
            foreach (var (key, value) in request.Update.ChangedKeys.Fields)
            {
                if (value.KindCase == Value.KindOneofCase.NullValue)
                {
                    resources.Remove(key);
                }
                else
                {
                    resources[key] = value;
                }
            }

            return Task.FromResult(new UpdateStateResponse
            {
                Success = true
            });
        }

        public override async Task SubscribeStateUpdates(SubscribeStateUpdatesRequest request,
                                                         IServerStreamWriter<StateUpdate>
                                                             responseStream,
                                                         ServerCallContext context)
        {
            var millisecondTiming = (int) (request.UpdateInterval * 1000);
            var update = new StateUpdate
            {
                ChangedKeys = new Struct()
            };

            void ResourcesOnCollectionChanged(object sender,
                                              NotifyCollectionChangedEventArgs e)
            {
                var (changes, removals) = e.AsChangesAndRemovals<string>();

                foreach (var change in changes)
                    update.ChangedKeys.Fields[change] = resources[change];
                foreach (var removal in removals)
                    update.ChangedKeys.Fields[removal] = Value.ForNull();
            }

            resources.CollectionChanged += ResourcesOnCollectionChanged;

            var currentState = new StateUpdate()
            {
                ChangedKeys = new Struct()
            };

            foreach (var (key, value) in resources)
                currentState.ChangedKeys.Fields[key] = value;

            await responseStream.WriteAsync(currentState);

            while (true)
            {
                await Task.Delay(millisecondTiming);
                if (update.ChangedKeys.Fields.Any())
                {
                    var toSend = update;
                    update = new StateUpdate
                    {
                        ChangedKeys = new Struct()
                    };
                    await responseStream.WriteAsync(toSend);
                }
            }
        }

        public ServerServiceDefinition BindService()
        {
            return State.BindService(this);
        }

        public void SetValueDirect(string key, object value)
        {
            resources[key] = value.ToProtobufValue();
        }

        public void RemoveValueDirect(string key)
        {
            resources.Remove(key);
        }
    }
}