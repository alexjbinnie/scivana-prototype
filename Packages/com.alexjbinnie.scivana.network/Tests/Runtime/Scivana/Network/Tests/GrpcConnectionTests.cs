﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using NUnit.Framework;
using Scivana.Core.Testing.Async;
using Scivana.Network.Tests.Trajectory;
using Scivana.Network.Trajectory;

namespace Scivana.Network.Tests
{
    public class GrpcConnectionTests
    {
        private static IEnumerable<AsyncUnitTests.AsyncTestInfo> GetTests()
        {
            return AsyncUnitTests.FindAsyncTestsInClass<GrpcConnectionTests>();
        }

        [Test]
        public void TestAsync([ValueSource(nameof(GetTests))] AsyncUnitTests.AsyncTestInfo test)
        {
            AsyncUnitTests.RunAsyncTest(this, test);
        }

        [Test]
        public void Constructor_NullAddress_Exception()
        {
            GrpcConnection connection;

            Assert.Throws<ArgumentNullException>(
                () => connection = new GrpcConnection(null, 54321));
        }

        [Test]
        public void Constructor_NegativePort_Exception()
        {
            GrpcConnection connection;

            Assert.Throws<ArgumentOutOfRangeException>(
                () => connection = new GrpcConnection("localhost", -1));
        }

        [AsyncTest]
        public async Task NoServer_IsChannelIdle()
        {
            var connection = new GrpcConnection("localhost", 54321);

            try
            {
                await Task.Delay(100);
                Assert.AreEqual(ChannelState.Idle, connection.Channel.State);
            }
            finally
            {
                await connection.CloseAsync();
            }
        }

        [AsyncTest]
        public async Task Server_IsChannelReady()
        {
            var service = new QueueTrajectoryService();
            var (server, connection) = GrpcServer.CreateServerAndConnection(service);

            try
            {
                var client = new TrajectoryClient(connection);

                using (var token = new CancellationTokenSource())
                {
                    var stream = client.SubscribeLatestFrames(externalToken: token.Token);
                    var task = stream.StartReceiving();

                    await Task.WhenAny(task, Task.Delay(100));

                    token.Cancel();

                    Assert.AreEqual(ChannelState.Ready, connection.Channel.State);
                }
            }
            finally
            {
                await server.CloseAsync();
                await connection.CloseAsync();
            }
        }

        [AsyncTest]
        public async Task DisconnectedServer_IsChannelIdle()
        {
            var service = new QueueTrajectoryService();
            var (server, connection) = GrpcServer.CreateServerAndConnection(service);

            try
            {
                var client = new TrajectoryClient(connection);

                using (var token = new CancellationTokenSource())
                {
                    var stream = client.SubscribeLatestFrames(externalToken: token.Token);
                    var task = stream.StartReceiving();

                    await Task.WhenAny(task, Task.Delay(100));

                    await server.CloseAsync();

                    await Task.Delay(100);

                    Assert.AreEqual(ChannelState.Idle, connection.Channel.State);

                    token.Cancel();
                }
            }
            finally
            {
                await server.CloseAsync();
                await connection.CloseAsync();
            }
        }

        [AsyncTest]
        public async Task Connection_CloseAsync_IsAtomic()
        {
            var service = new QueueTrajectoryService();
            var (server, connection) = GrpcServer.CreateServerAndConnection(service);

            await connection.CloseAsync();
            await connection.CloseAsync();

            await server.CloseAsync();
        }
    }
}