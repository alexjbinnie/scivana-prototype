﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Math;
using UnityEngine.XR;

namespace Scivana.App.XR
{
    /// <summary>
    /// Extensions for Unity's XR system, in which you make queries about
    /// XRNode types (e.g LeftHand, TrackingReference, etc) and receive
    /// XRNodeState objects containing identifier and tracking information
    /// for that XR node.
    /// </summary>
    public static class UnityXRExtensions
    {
        /// <summary>
        /// Get the XRNodeState for a given XRNode type, if available.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Thrown when there
        /// are multiple nodes of this type.
        /// </exception>
        public static XRNodeState? GetSingleNodeState(this XRNode nodeType)
        {
            var nodes = NodeStates.Where(state => state.nodeType == nodeType)
                                  .ToList();

            if (nodes.Count == 0)
            {
                return null;
            }
            else if (nodes.Count > 1)
            {
                throw new InvalidOperationException(
                    $"Cannot decide between multiple XRNodes of type {nodeType}.");
            }

            return nodes[0];
        }

        /// <summary>
        /// Return the node state's pose matrix, if available.
        /// </summary>
        public static UnitScaleTransformation? GetPose(this XRNodeState node)
        {
            if (node.TryGetPosition(out var position)
             && node.TryGetRotation(out var rotation))
            {
                return new UnitScaleTransformation(position, rotation);
            }

            return null;
        }

        /// <summary>
        /// Return the pose matrix for a given XRNode type, if available.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Thrown when there are
        /// multiple nodes of this type
        /// </exception>
        public static UnitScaleTransformation? GetTransformation(this XRNode nodeType)
        {
            return nodeType.GetSingleNodeState()?.GetPose();
        }

        private static readonly List<XRNodeState> nodeStates = new List<XRNodeState>();

        /// <summary>
        /// Get all the states for tracked XR objects from Unity's XR system.
        /// </summary>
        public static IReadOnlyList<XRNodeState> NodeStates
        {
            get
            {
                InputTracking.GetNodeStates(nodeStates);

                return nodeStates;
            }
        }
    }
}