﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.Utilities;

namespace Scivana.App.XR
{
#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif
// Determine how GetBindingDisplayString() formats the composite by applying
// the  DisplayStringFormat attribute.
    [DisplayStringFormat("{axis}")]
    public class ButtonFrom2DAxis : InputBindingComposite<float>
    {
        // Each part binding is represented as a field of type int and annotated with
        // InputControlAttribute. Setting "layout" restricts the controls that
        // are made available for picking in the UI.
        //
        // On creation, the int value is set to an integer identifier for the binding
        // part. This identifier can read values from InputBindingCompositeContext.
        // See ReadValue() below.
        [InputControl(layout = "Button")]
        public int axis;

        // This method computes the resulting input value of the composite based
        // on the input from its part bindings.
        public override float ReadValue(ref InputBindingCompositeContext context)
        {
            var value = context.ReadValue<Vector2, Vector2MagnitudeComparer>(axis);
            return Mathf.Clamp01(-value.y);
        }

        // This method computes the current actuation of the binding as a whole.
        public override float EvaluateMagnitude(ref InputBindingCompositeContext context)
        {
            return ReadValue(ref context);
        }

        static ButtonFrom2DAxis()
        {
            // Can give custom name or use default (type name with "Composite" clipped off).
            // Same composite can be registered multiple times with different names to introduce
            // aliases.
            //
            // NOTE: Registering from the static constructor using InitializeOnLoad and
            //       RuntimeInitializeOnLoadMethod is only one way. You can register the
            //       composite from wherever it works best for you. Note, however, that
            //       the registration has to take place before the composite is first used
            //       in a binding. Also, for the composite to show in the editor, it has
            //       to be registered from code that runs in edit mode.
            InputSystem.RegisterBindingComposite<ButtonFrom2DAxis>();
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Init()
        {
        } // Trigger static constructor.
    }
}