﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Math;
using UnityEngine;
using UnityEngine.XR;

namespace Scivana.App.XR
{
    public class XrControllerInput
    {
        public bool Trigger { get; private set; }

        public bool Grip { get; private set; }

        public UnitScaleTransformation? Orientation { get; private set; } =
            UnitScaleTransformation.identity;

        public event Action TriggerDown;
        public event Action TriggerUp;

        public event Action GripDown;
        public event Action GripUp;

        public event Action DPadNorthDown;
        public event Action DPadNorthUp;

        public event Action DPadSouthDown;
        public event Action DPadSouthUp;

        public Vector2 DPad { get; private set; } = Vector2.zero;

        public bool DPadSouth { get; private set; }

        public bool DPadNorth { get; private set; }

        public event Action OrientationUpdated;

        public void Update(InputDevice device)
        {
            UpdateTrigger(device);
            UpdateGrip(device);
            UpdateOrientation(device);
            UpdateTrackpad(device);
        }

        public IValued<UnitScaleTransformation?> GetOrientation()
        {
            var valued = new Valued<UnitScaleTransformation?>();

            OrientationUpdated += () => valued.Value = Orientation;

            return valued;
        }

        private void UpdateOrientation(InputDevice device)
        {
            var hasNewPosition = device.TryGetFeatureValue(CommonUsages.devicePosition,
                                                           out var newPosition);
            var hasNewRotation = device.TryGetFeatureValue(CommonUsages.deviceRotation,
                                                           out var newRotation);
            if (hasNewPosition || hasNewRotation)
            {
                if (!hasNewPosition)
                    newPosition = Orientation.Value.position;
                if (!hasNewRotation)
                    newRotation = Orientation.Value.rotation;

                Orientation = new UnitScaleTransformation(newPosition, newRotation);
                OrientationUpdated?.Invoke();
            }
        }

        private void UpdateGrip(InputDevice device)
        {
            var newGrip = false;
            device.TryGetFeatureValue(CommonUsages.gripButton, out newGrip);
            if (newGrip != Grip)
            {
                if (Grip)
                    GripUp?.Invoke();
                Grip = newGrip;
                if (Grip)
                    GripDown?.Invoke();
            }
        }

        private void UpdateTrigger(InputDevice device)
        {
            var newTrigger = false;
            device.TryGetFeatureValue(CommonUsages.triggerButton, out newTrigger);
            if (newTrigger != Trigger)
            {
                if (Trigger)
                    TriggerUp?.Invoke();
                Trigger = newTrigger;
                if (Trigger)
                    TriggerDown?.Invoke();
            }
        }

        private void UpdateTrackpad(InputDevice device)
        {
            device.TryGetFeatureValue(CommonUsages.primary2DAxis, out var newDPad);
            DPad = newDPad;

            var newDPadNorth = DPad.y > 0.9f;
            if (newDPadNorth != DPadNorth)
            {
                if (DPadNorth)
                    DPadNorthUp?.Invoke();
                DPadNorth = newDPadNorth;
                if (DPadNorth)
                    DPadNorthDown?.Invoke();
            }

            var newDPadSouth = DPad.y < -0.9f;
            if (newDPadSouth != DPadSouth)
            {
                if (DPadSouth)
                    DPadSouthUp?.Invoke();
                DPadSouth = newDPadSouth;
                if (DPadSouth)
                    DPadSouthDown?.Invoke();
            }
        }
    }
}