﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using Scivana.Core.Math;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR;
using CommonUsages = UnityEngine.InputSystem.CommonUsages;
using InputDevice = UnityEngine.InputSystem.InputDevice;

namespace Scivana.App.XR
{
    public class XrInput<TActions>
        where TActions : class, IInputActionCollection, new()
    {
        public TActions Actions { get; private set; }

        public TActions Left { get; private set; }

        public TActions Right { get; private set; }

        public IValued<UnitScaleTransformation?> HeadsetOrientation => headsetOrientation;

        public IValued<UnitScaleTransformation?> LeftControllerOrientation => leftOrientation;

        public IValued<UnitScaleTransformation?> RightControllerOrientation => rightOrientation;

        private XRController left;

        private XRController right;

        private XRHMD headset;

        private Valued<UnitScaleTransformation?> headsetOrientation;

        private Valued<UnitScaleTransformation?> leftOrientation;

        private Valued<UnitScaleTransformation?> rightOrientation;

        public XrInput()
        {
            Actions = new TActions();
            Actions.Enable();

            Left = new TActions();
            Left.Enable();

            Right = new TActions();
            Right.Enable();

            if (InputSystem.GetDevice<XRController>(CommonUsages.LeftHand) is { } newLeft)
            {
                left = newLeft;
                Left.devices = new InputDevice[]
                {
                    left
                };
            }

            if (InputSystem.GetDevice<XRController>(CommonUsages.RightHand) is { } newRight)
            {
                right = newRight;
                Right.devices = new InputDevice[]
                {
                    right
                };
            }

            if (InputSystem.GetDevice<XRHMD>() is { } newHMD)
            {
                headset = newHMD;
            }

            InputSystem.onDeviceChange += DeviceChanged;

            headsetOrientation = new Valued<UnitScaleTransformation?>();
            leftOrientation = new Valued<UnitScaleTransformation?>();
            rightOrientation = new Valued<UnitScaleTransformation?>();
        }

        public void Update()
        {
            if (left != null && left.isTracked.ReadValue() > 0)
            {
                leftOrientation.Value = new UnitScaleTransformation(left.devicePosition.ReadValue(),
                                                                    left.deviceRotation
                                                                        .ReadValue());
            }
            else
            {
                leftOrientation.Value = null;
            }

            if (right != null && right.isTracked.ReadValue() > 0)
            {
                rightOrientation.Value = new UnitScaleTransformation(
                    right.devicePosition.ReadValue(),
                    right.deviceRotation.ReadValue());
            }
            else
            {
                rightOrientation.Value = null;
            }

            if (headset != null && headset.isTracked.ReadValue() > 0)
            {
                headsetOrientation.Value = new UnitScaleTransformation(
                    headset.devicePosition.ReadValue(),
                    headset.deviceRotation.ReadValue());
            }
            else
            {
                headsetOrientation.Value = null;
            }
        }


        private void DeviceChanged(InputDevice device, InputDeviceChange arg2)
        {
            if (arg2 == InputDeviceChange.Destroyed ||
                arg2 == InputDeviceChange.Disconnected ||
                arg2 == InputDeviceChange.Removed)
            {
                if (device == left)
                {
                    left = null;
                    Left.devices = new InputDevice[0];
                    leftOrientation.Value = null;
                }

                if (device == right)
                {
                    right = null;
                    Left.devices = new InputDevice[0];
                    rightOrientation.Value = null;
                }

                if (device == headset)
                {
                    headset = null;
                    Left.devices = new InputDevice[0];
                    headsetOrientation.Value = null;
                }

                return;
            }

            if (device.usages.Contains(CommonUsages.LeftHand) && device is XRController newLeft)
            {
                left = newLeft;
                Left.devices = new InputDevice[]
                {
                    left
                };
            }

            if (device.usages.Contains(CommonUsages.RightHand) && device is XRController newRight)
            {
                right = newRight;
                Right.devices = new InputDevice[]
                {
                    right
                };
            }

            if (device is XRHMD newHMD)
            {
                headset = newHMD;
            }
        }

        public IValued<UnitScaleTransformation?> GetOrientation(InputDeviceRole hand)
        {
            if (hand == InputDeviceRole.LeftHanded && left != null)
            {
                return leftOrientation;
            }

            if (hand == InputDeviceRole.RightHanded && right != null)
            {
                return rightOrientation;
            }

            return null;
        }

        public TActions Get(InputDeviceRole hand)
        {
            if (hand == InputDeviceRole.LeftHanded)
                return Left;
            if (hand == InputDeviceRole.RightHanded)
                return Right;
            throw new InvalidOperationException($"Cannot get input for {hand}");
        }
    }
}