﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR;
using CommonUsages = UnityEngine.InputSystem.CommonUsages;
using InputDevice = UnityEngine.InputSystem.InputDevice;

namespace Scivana.App.XR
{
    [Serializable]
    public class TrackedObject : MonoBehaviour
    {
        [SerializeField]
        private InputDeviceCharacteristics usage = InputDeviceCharacteristics.Left;

        private InternedString Usage
        {
            get
            {
                return usage switch
                {
                    InputDeviceCharacteristics.Left => CommonUsages.LeftHand,
                    InputDeviceCharacteristics.Right => CommonUsages.RightHand,
                    _ => throw new InvalidOperationException($"Cannot track position of {usage}")
                };
            }
        }

        private TrackedDevice device;

        private void OnEnable()
        {
            var controller = InputSystem.GetDevice<XRController>(Usage);
            if (controller != null)
            {
                device = controller;
                transform.position = device.devicePosition.ReadValue();
                transform.rotation = device.deviceRotation.ReadValue();
            }

            InputSystem.onDeviceChange += OnDeviceChange;
        }

        private void OnDisable()
        {
            InputSystem.onDeviceChange -= OnDeviceChange;
        }

        private void OnDeviceChange(InputDevice device, InputDeviceChange change)
        {
            if (change == InputDeviceChange.Destroyed ||
                change == InputDeviceChange.Removed ||
                change == InputDeviceChange.Disconnected)
            {
                if (this.device == device)
                    this.device = null;
                return;
            }

            if (device.usages.Contains(Usage) && device is TrackedDevice tracked)
            {
                this.device = tracked;
            }
        }

        private void Update()
        {
            if (device != null)
            {
                transform.position = device.devicePosition.ReadValue();
                transform.rotation = device.deviceRotation.ReadValue();
            }
        }
    }
}