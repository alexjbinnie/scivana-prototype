/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using JetBrains.Annotations;

namespace Scivana.Trajectory.Event
{
    /// <summary>
    /// Event arguments for when a <see cref="Frame" /> has been updated.
    /// </summary>
    public class FrameChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrameChangedEventArgs" /> class.
        /// </summary>
        public FrameChangedEventArgs(Frame frame, [NotNull] FrameChanges changes)
        {
            Frame = frame;
            Changes = changes;
        }

        /// <summary>
        /// The new <see cref="Frame" />.
        /// </summary>
        public Frame Frame { get; }

        /// <summary>
        /// Information about what has changed since the previous frame.
        /// </summary>
        [NotNull]
        public FrameChanges Changes { get; }
    }
}