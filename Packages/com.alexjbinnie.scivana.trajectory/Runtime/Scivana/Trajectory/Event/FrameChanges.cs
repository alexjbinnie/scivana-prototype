/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace Scivana.Trajectory.Event
{
    // TODO: Consider rewording to avoid switching between the language of keys and fields

    /// <summary>
    /// A record of which fields have been altered from one frame to another. By tracking exactly which
    /// fields have been altered between frames, data only needs to be recalculated when the fields it
    /// depends on is changed. Either use the predefined constants <see cref="None" /> (to indicate no
    /// field has changed), <see cref="All" /> (to indicate all fields have changed) or use
    /// <see cref="WithChanges" /> to indicate explicitly which fields have been changed.
    /// </summary>
    public class FrameChanges
    {
        /// <summary>
        /// The set of fields that have been explicitly marked as having changed.
        /// </summary>
        private readonly HashSet<string> changed = new HashSet<string>();

        /// <summary>
        /// An override flag that indicates that all fields have been changed, regardless of their presence in
        /// <see cref="changed" />.
        /// </summary>
        private bool haveAllChanged;

        private FrameChanges()
        {
        }

        /// <summary>
        /// A <see cref="FrameChanges" /> where no key is marked as having changed.
        /// </summary>
        public static FrameChanges None => new FrameChanges()
        {
            haveAllChanged = false,
        };

        /// <summary>
        /// A <see cref="FrameChanges" /> where all keys are marked as having changed.
        /// </summary>
        public static FrameChanges All => new FrameChanges
        {
            haveAllChanged = true,
        };

        /// <summary>
        /// A <see cref="FrameChanges" /> where only the provided keys are marked as having changed.
        /// </summary>
        public static FrameChanges WithChanges(params string[] keys)
        {
            var changes = None;
            foreach (var key in keys)
                changes.MarkAsChanged(key);
            return changes;
        }

        /// <summary>
        /// Indicates whether any keys have been changed in comparison to a previous frame.
        /// </summary>
        public bool HasAnythingChanged => haveAllChanged || changed.Count > 0;

        /// <summary>
        /// Merge another <see cref="FrameChanges" /> into this one, such that this now represents combination
        /// of the two.
        /// </summary>
        public void MergeChanges(FrameChanges otherChanges)
        {
            haveAllChanged = haveAllChanged || otherChanges.haveAllChanged;
            changed.UnionWith(otherChanges.changed);
        }

        /// <summary>
        /// Check if the given key is marked as changed from the previous frame.
        /// </summary>
        public bool HasChanged(string key)
        {
            return haveAllChanged || changed.Contains(key);
        }

        /// <inheritdoc cref="HasChanged(string)" />
        public bool HasChanged(TypedKey property)
        {
            return HasChanged(property.Key);
        }

        /// <summary>
        /// Mark the given key as having been changed from the previous frame.
        /// </summary>
        public void MarkAsChanged(string key)
        {
            changed.Add(key);
        }
    }
}