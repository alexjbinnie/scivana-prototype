/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace Scivana.Trajectory
{
    /// <summary>
    /// Represents an ordered pair of indices, used to represent a bond between two particles with indices
    /// A and B.
    /// </summary>
    [Serializable]
    public readonly struct BondPair : IEquatable<BondPair>
    {
        /// <summary>
        /// The index of the first particle
        /// </summary>
        public readonly int A;

        /// <summary>
        /// The index of the second particle
        /// </summary>
        public readonly int B;

        /// <summary>
        /// Initializes a new instance of the <see cref="BondPair" /> struct, representing a bond between two
        /// indices <paramref name="a" /> and <paramref name="b" />.
        /// </summary>
        public BondPair(int a, int b)
        {
            A = a;
            B = b;
        }

        /// <summary>
        /// Get the first and second particles involved in this bond, by their 0-based index.
        /// </summary>
        /// <exception cref="IndexOutOfRangeException">When an index which is not 0 or 1 is requested.</exception>
        public int this[int i]
        {
            get
            {
                return i switch
                {
                    0 => A,
                    1 => B,
                    _ => throw new IndexOutOfRangeException(),
                };
            }
        }

        public static bool operator ==(BondPair left, BondPair right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(BondPair left, BondPair right)
        {
            return !(left == right);
        }

        /// <inheritdoc />
        public bool Equals(BondPair other)
        {
            return A == other.A && B == other.B;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is BondPair other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return (A * 397) ^ B;
            }
        }
    }
}