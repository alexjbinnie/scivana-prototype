/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Scivana.Core.Science;

namespace Scivana.Trajectory.Import
{
    public abstract class LineByLineParser
    {
        /// <summary>
        /// The current line the parser is looking at.
        /// </summary>
        protected string CurrentLine { get; private set; }

        private int LineNumber { get; set; }

        private TextReader CurrentReader { get; set; }

        /// <summary>
        /// Call the <see cref="Parse()" /> function on a given source, catching exceptions
        /// and wrapping them with the line number.
        /// </summary>
        protected void Parse(TextReader reader)
        {
            CurrentReader = reader;
            CurrentLine = reader.ReadLine();
            LineNumber = 1;
            try
            {
                Parse();
            }
            catch (Exception e)
            {
                throw new ImportException(CurrentLine, LineNumber, e);
            }
        }

        protected abstract void Parse();

        protected bool HasLine => CurrentLine != null;

        protected void NextLine()
        {
            if (CurrentReader.Peek() > -1)
            {
                CurrentLine = CurrentReader.ReadLine();
                LineNumber++;
                LogProgress($"Line {LineNumber}");
            }
            else
            {
                CurrentLine = null;
            }
        }

        protected void SkipLines(int n)
        {
            for (var i = 0; i < n; i++)
            {
                if (CurrentLine == null)
                    return;
                NextLine();
            }
        }

        protected void FinishParsing()
        {
            CurrentLine = null;
        }

        protected bool IsBlankLine()
        {
            return string.IsNullOrEmpty(CurrentLine.Trim());
        }

        protected bool IsCommentLine(char commentChar)
        {
            return CurrentLine.Length > 0 && CurrentLine[0] == commentChar;
        }

        protected float ParseFloat(string str, string errorMsg)
        {
            if (!float.TryParse(str, out var value))
                throw new ArgumentException(errorMsg + $" ({str})");
            return value;
        }

        protected int ParseInt(string str, string errorMsg)
        {
            if (!int.TryParse(str, out var value))
                throw new ArgumentException(errorMsg);
            return value;
        }

        protected string ParseString(string str)
        {
            return str;
        }

        private static Dictionary<string, Element> symbolsToElements =
            new Dictionary<string, Element>();

        // Setup all possible combinations (i.e. Cu, cu, CU, cU), to avoid performance problems of repeated calls of ToLower().
        static LineByLineParser()
        {
            foreach (Element element in Enum.GetValues(typeof(Element)))
            {
                var symbol = element.GetSymbol();
                if (element == Element.Virtual)
                    continue;
                if (symbol.Length == 1)
                {
                    symbolsToElements.Add(symbol.ToUpper(CultureInfo.InvariantCulture), element);
                    symbolsToElements.Add(symbol.ToLower(CultureInfo.InvariantCulture), element);
                }
                else
                {
                    symbolsToElements.Add(symbol.ToUpper(CultureInfo.InvariantCulture), element);
                    symbolsToElements.Add(symbol.ToLower(CultureInfo.InvariantCulture), element);
                    symbolsToElements.Add(
                        new string(new[]
                        {
                            char.ToUpper(symbol[0], CultureInfo.InvariantCulture),
                            char.ToLower(symbol[1], CultureInfo.InvariantCulture)
                        }),
                        element);
                    symbolsToElements.Add(
                        new string(new[]
                        {
                            char.ToLower(symbol[0], CultureInfo.InvariantCulture),
                            char.ToUpper(symbol[1], CultureInfo.InvariantCulture)
                        }), element);
                }
            }
        }

        private readonly IProgress<string> progress;

        /// <summary>
        /// Log a message to a progress reporter if present.
        /// </summary>
        protected void LogProgress(string message)
        {
            progress?.Report(message);
        }

        protected LineByLineParser(IProgress<string> progress = null)
        {
            this.progress = progress;
        }

        protected Element? ParseElementSymbol(string symbol)
        {
            if (string.IsNullOrEmpty(symbol))
                return null;
            symbol = symbol.Trim();
            return symbolsToElements.TryGetValue(symbol, out var element)
                       ? (Element?) element
                       : null;
        }

        protected GroupCollection MatchLine(string pattern, string errorMsg)
        {
            var match = Regex.Match(CurrentLine, pattern);
            if (!match.Success)
                throw new ArgumentException(errorMsg);

            return match.Groups;
        }
    }
}