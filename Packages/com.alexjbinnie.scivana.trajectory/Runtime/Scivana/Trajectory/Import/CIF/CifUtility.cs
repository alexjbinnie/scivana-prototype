/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Text.RegularExpressions;

namespace Scivana.Trajectory.Import.CIF
{
    public static class CifUtility
    {
        private const string DataItemRegexString = @"^_(\w+).(\w+)(?:\s+(.*[^\s]))?";

        /// <summary>
        /// Get the category from a data item line.
        /// </summary>
        public static string GetCategory(string line)
        {
            var i = line.IndexOf(".", StringComparison.Ordinal);
            return line.Substring(1, i - 1);
        }

        /// <summary>
        /// Get the category, keyword and value from a data item line.
        /// </summary>
        public static (string Category, string Keyword, string value) GetCategoryAndKeyword(
            string line)
        {
            var match = Regex.Match(line, DataItemRegexString).Groups;
            return (match[1].Value, match[2].Value, match[3].Success ? match[3].Value : null);
        }
    }
}