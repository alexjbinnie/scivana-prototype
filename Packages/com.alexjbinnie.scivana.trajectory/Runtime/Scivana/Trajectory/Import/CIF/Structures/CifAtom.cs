/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core.Science;
using UnityEngine;

namespace Scivana.Trajectory.Import.CIF.Structures
{
    /// <summary>
    /// An atom read from the atom_site table of a mmCIF file.
    /// </summary>
    internal class CifAtom
    {
        /// <summary>
        /// Absolute index of the atom in the <see cref="CifSystem" />.
        /// </summary>
        public int AbsoluteIndex { get; set; }

        /// <summary>
        /// ID used to identify the atom_site of this atom.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ID defining the unique name of this atom within the residue.
        /// </summary>
        public string AtomId { get; set; }

        /// <summary>
        /// An alternate location ID, used to mark multiple possible positions.
        /// </summary>
        public string AltId { get; set; }

        /// <summary>
        /// Position of the atom in nanometers.
        /// </summary>
        public Vector3 Position { get; set; }

        /// <summary>
        /// The residue this atom belongs to.
        /// </summary>
        public CifResidue Residue { get; set; }

        /// <summary>
        /// The atomic element of this atom.
        /// </summary>
        public Element Element { get; set; }
    }
}