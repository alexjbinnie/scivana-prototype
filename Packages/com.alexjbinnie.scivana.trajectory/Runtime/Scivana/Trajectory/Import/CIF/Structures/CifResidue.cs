/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace Scivana.Trajectory.Import.CIF.Structures
{
    /// <summary>
    /// A residue inferred from an mmCIF file by a component ID and a sequence ID.
    /// </summary>
    internal class CifResidue
    {
        /// <summary>
        /// The absolute index of the residue in the <see cref="CifSystem" />
        /// </summary>
        public int AbsoluteIndex { get; set; }

        private readonly List<CifAtom> atoms = new List<CifAtom>();

        private readonly Dictionary<string, CifAtom> atomsByAtomId =
            new Dictionary<string, CifAtom>();

        /// <summary>
        /// The atoms that are in this residue.
        /// </summary>
        public IReadOnlyCollection<CifAtom> Atoms => atoms;

        /// <summary>
        /// Add an atom to this residue, assigning its residue to this.
        /// </summary>
        public void AddAtom(CifAtom atom)
        {
            atom.Residue = this;
            atoms.Add(atom);
            atomsByAtomId.Add(atom.AtomId, atom);
        }

        /// <summary>
        /// The component ID read from comp_id.
        /// </summary>
        public string ComponentId { get; set; }

        /// <summary>
        /// Find the atom with the given atom_id
        /// </summary>
        public CifAtom FindAtomWithAtomId(string atomId)
        {
            return atomsByAtomId.TryGetValue(atomId, out var value) ? value : null;
        }

        /// <summary>
        /// The sequence ID read from seq_id.
        /// </summary>
        public int? SequenceId { get; set; }

        /// <summary>
        /// The asymmetric unit this residue belongs to.
        /// </summary>
        public CifAsymmetricUnit AsymmetricUnit { get; set; }
    }
}