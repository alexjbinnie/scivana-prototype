/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

#endif

namespace Scivana.Trajectory.Import.CIF.Components
{
    /// <summary>
    /// A global <see cref="ScriptableObject" /> which holds the mmCIF component
    /// dictionary.
    /// </summary>
    public class ChemicalComponentDictionary : ScriptableObject,
                                               ISerializationCallbackReceiver
    {
        private static ChemicalComponentDictionary instance;

        /// <summary>
        /// The chemical component dictionary.
        /// </summary>
        public static ChemicalComponentDictionary Instance
        {
            get
            {
                if (instance == null)
                    LoadInstance();
                return instance;
            }
        }

        /// <summary>
        /// Load the dictionary from a file, creating it if it is missing.
        /// </summary>
        private static void LoadInstance()
        {
            instance = Resources.Load<ChemicalComponentDictionary>(InstanceFile);

            if (instance == null)
            {
#if UNITY_EDITOR
                instance = CreateInstance<ChemicalComponentDictionary>();
                AssetDatabase.CreateAsset(instance, FullInstanceFile);
#endif
                if (instance == null)
                    throw new InvalidOperationException(
                        $"Cannot find mmCIF chemical component dictionary at {InstanceFile}");
            }
        }


#if UNITY_EDITOR
        [MenuItem("Scivana/Load mmCIF Chemical Component Dictionary")]
        [UsedImplicitly]
        private static void LoadChemicalComponentDictionary()
        {
            var filename =
                EditorUtility.OpenFilePanel("Select mmCIF Chemical Component Dictionary", "",
                                            "cif");
            Instance.LoadChemicalComponentDictionaryFromFile(filename);
        }

        private void LoadChemicalComponentDictionaryFromFile(string filename)
        {
            if (string.IsNullOrEmpty(filename))
                return;

            var file = File.OpenRead(filename);

            chemicalComponents.Clear();

            var stream = new StreamReader(file);
            foreach (var component in CifChemicalComponentBondsImport.ImportMultiple(stream))
                chemicalComponents[component.ResId] = component;

            Debug.Log($"<b>Narupa</b>: Loaded {chemicalComponents.Count} CIF components.");

            SerializeDictionary();

            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }

#endif

        private const string InstanceFile = "ChemicalComponentDictionary";

        private const string FullInstanceFile =
            "Assets/Narupa/Trajectory/Import/Resources/ChemicalComponentDictionary.asset";

        public ChemicalComponent GetResidue(string residueName)
        {
            return chemicalComponents.TryGetValue(residueName, out var value) ? value : null;
        }

        public void OnBeforeSerialize()
        {
        }

        [SerializeField]
        [HideInInspector]
        private string compressedData = "";

        private void SerializeDictionary()
        {
            using var origStream = new MemoryStream();
            using var stream = new DeflateStream(origStream, CompressionMode.Compress);
            using var writer = new BinaryWriter(stream);

            writer.Write(chemicalComponents.Count);
            foreach (var component in chemicalComponents.Values)
            {
                writer.Write(component.ResId);
                writer.Write(component.Bonds.Count());
                foreach (var bond in component.bonds)
                {
                    writer.Write(bond.a);
                    writer.Write(bond.b);
                    writer.Write((byte) bond.order);
                }
            }

            compressedData = Convert.ToBase64String(origStream.ToArray());

            writer.Dispose();
        }

        private void DeserializeDictionary()
        {
            if (string.IsNullOrEmpty(compressedData))
            {
                return;
            }

            var bytes = Convert.FromBase64String(compressedData);
            using var origStream = new MemoryStream(bytes);
            using (var stream = new DeflateStream(origStream, CompressionMode.Decompress))
            {
                try
                {
                    using var reader = new BinaryReader(stream);

                    var count = reader.ReadInt32();

                    for (var i = 0; i < count; i++)
                    {
                        var resId = reader.ReadString();
                        var component = new ChemicalComponent()
                        {
                            ResId = resId
                        };
                        var bondCount = reader.ReadInt32();
                        for (var j = 0; j < bondCount; j++)
                        {
                            var a = reader.ReadString();
                            var b = reader.ReadString();
                            var order = reader.ReadByte();
                            component.bonds.Add(new ChemicalComponent.Bond()
                            {
                                a = a,
                                b = b,
                                order = order
                            });
                        }

                        chemicalComponents[component.ResId] = component;
                    }

                    reader.Dispose();
                }
                catch (EndOfStreamException)
                {
                }
            }
        }

        public void OnAfterDeserialize()
        {
            DeserializeDictionary();
        }

        private readonly Dictionary<string, ChemicalComponent> chemicalComponents =
            new Dictionary<string, ChemicalComponent>();
    }
}