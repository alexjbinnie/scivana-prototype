/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using Scivana.Trajectory.Import.CIF.Components;

namespace Scivana.Trajectory.Import.CIF
{
    /// <summary>
    /// mmCIF importer for reading in the chemical component dictionary.
    /// </summary>
    internal class CifChemicalComponentBondsImport : CifBaseImport
    {
        protected override void ParseDataBlockLine()
        {
            if (chemicalComponent != null)
                chemicalComponents.Add(chemicalComponent);
            chemicalComponent = new ChemicalComponent();
            chemicalComponent.ResId = CurrentLine.Substring(5);
        }

        private readonly List<ChemicalComponent> chemicalComponents = new List<ChemicalComponent>();

        private ChemicalComponent chemicalComponent;

        public static IReadOnlyList<ChemicalComponent> ImportMultiple(TextReader reader)
        {
            var parser = new CifChemicalComponentBondsImport();
            parser.Parse(reader);
            parser.chemicalComponents.Add(parser.chemicalComponent);
            return parser.chemicalComponents;
        }

        protected override ParseTableRow GetTableHandler(string category, List<string> keywords)
        {
            if (category == "chem_comp_bond")
            {
                return GetBondHandler(keywords);
            }

            return null;
        }

        protected override bool ShouldParseCategory(string category)
        {
            return category == "chem_comp_bond";
        }

        private ParseTableRow GetBondHandler(IList<string> keywords)
        {
            var indexAtom1 = keywords.IndexOf("atom_id_1");
            var indexAtom2 = keywords.IndexOf("atom_id_2");
            var indexOrder = keywords.IndexOf("value_order");

            return data =>
            {
                var atom1 = data[indexAtom1].Trim();
                var atom2 = data[indexAtom2].Trim();
                var order = ParseBondOrder(data[indexOrder].Trim());

                chemicalComponent.AddBond(atom1, atom2, order);
            };
        }

        private int ParseBondOrder(string value)
        {
            switch (value)
            {
                case "SING":
                    return 1;
                case "DOUB":
                    return 2;
                case "TRIP":
                    return 3;
                default:
                    throw new ArgumentException($"Unknown bond type {value}");
            }
        }
    }
}