/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using JetBrains.Annotations;
using Scivana.Core;
using Scivana.Core.Science;
using UnityEngine;

namespace Scivana.Trajectory
{
    /// <summary>
    /// A single frame in a trajectory. It is a snapshot of a chemical system, represented by a set of
    /// properties. These properties include such things as particle positions, types and bonds.
    /// </summary>
    public class Frame
    {
        /// <summary>
        /// Internal representation of an IParticle list by references arrays
        /// </summary>
        [NotNull]
        private readonly ParticleReferenceList particles;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frame" /> class by creating an empty frame.
        /// </summary>
        public Frame()
        {
            particles = new ParticleReferenceList(this);
        }

        public TType Get<TType>(TypedKey<TType> property)
        {
            return Data.GetValueOrDefault<TType>(property.Key);
        }

        public void Set<TType>(TypedKey<TType> property, TType value)
        {
            Data[property.Key] = value;
        }

        /// <summary>
        /// The set of particles contained in this frame.
        /// </summary>
        [NotNull]
        public IReadOnlyList<IParticle> Particles => particles;

        /// <summary>
        /// The set of bonds contained in this frame.
        /// </summary>
        [NotNull]
        public IReadOnlyList<BondPair> Bonds => Get(FrameFields.Bonds) ?? new BondPair[0];

        /// <summary>
        /// The raw data that underlies this frame.
        /// </summary>
        public IDictionary<string, object> Data { get; } = new Dictionary<string, object>();

        /// <inheritdoc cref="FrameFields.ParticlePositions" />
        public Vector3[] ParticlePositions
        {
            get => Get(FrameFields.ParticlePositions);
            set => Set(FrameFields.ParticlePositions, value);
        }

        /// <inheritdoc cref="FrameFields.ParticlePositions" />
        public Element[] ParticleElements
        {
            get => Get(FrameFields.ParticleElements);
            set => Set(FrameFields.ParticleElements, value);
        }

        /// <inheritdoc cref="FrameFields.ParticlePositions" />
        public string[] ParticleTypes
        {
            get => Get(FrameFields.ParticleTypes);
            set => Set(FrameFields.ParticleTypes, value);
        }

        /// <inheritdoc cref="FrameFields.Bonds" />
        public BondPair[] BondPairs
        {
            get => Get(FrameFields.Bonds);
            set => Set(FrameFields.Bonds, value);
        }

        /// <summary>
        /// Obtain a copy of a frame, referencing the old arrays such that if a new array
        /// is not provided, a Frame will use the last obtained value.
        /// </summary>
        public static Frame ShallowCopy([NotNull] Frame originalFrame)
        {
            var copiedFrame = new Frame();
            foreach (var (key, value) in originalFrame.Data)
                copiedFrame.Data[key] = value;
            return copiedFrame;
        }
    }
}