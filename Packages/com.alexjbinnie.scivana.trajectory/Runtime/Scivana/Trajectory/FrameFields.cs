/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Narupa.Protocol.Trajectory;
using Scivana.Core.Math;
using Scivana.Core.Science;
using UnityEngine;

namespace Scivana.Trajectory
{
    /// <summary>
    /// Standard names and types for <see cref="Frame" /> fields.
    /// </summary>
    public static class FrameFields
    {
        /// <summary>
        /// The pairs of bonds between particle indices.
        /// </summary>
        public static readonly TypedKey<BondPair[]> Bonds
            = new TypedKey<BondPair[]>(FrameData.BondArrayKey);

        /// <summary>
        /// The bond order of each bond.
        /// </summary>
        public static readonly TypedKey<int[]> BondOrders
            = new TypedKey<int[]>(FrameData.BondOrderArrayKey);

        /// <summary>
        /// The number of entities (also known as chains), which are collections of one or more residues.
        /// </summary>
        public static readonly TypedKey<int?> EntityCount
            = new TypedKey<int?>(FrameData.ChainCountValueKey);

        /// <summary>
        /// The names of each entity (also known as chains).
        /// </summary>
        public static readonly TypedKey<string[]> EntityName
            = new TypedKey<string[]>(FrameData.ChainNameArrayKey);

        // TODO: Find out what unit this is in.

        /// <summary>
        /// The current kinetic energy.
        /// </summary>
        public static readonly TypedKey<float?> KineticEnergy
            = new TypedKey<float?>(FrameData.KineticEnergyValueKey);

        /// <summary>
        /// The number of particles.
        /// </summary>
        public static readonly TypedKey<int?> ParticleCount
            = new TypedKey<int?>(FrameData.ParticleCountValueKey);

        /// <summary>
        /// The atomic element of each particle.
        /// </summary>
        public static readonly TypedKey<Element[]> ParticleElements
            = new TypedKey<Element[]>(FrameData.ParticleElementArrayKey);

        /// <summary>
        /// The name of each particle.
        /// </summary>
        public static readonly TypedKey<string[]> ParticleNames
            = new TypedKey<string[]>(FrameData.ParticleNameArrayKey);

        /// <summary>
        /// The position of each particle in angstroms.
        /// </summary>
        public static readonly TypedKey<Vector3[]> ParticlePositions
            = new TypedKey<Vector3[]>(FrameData.ParticlePositionArrayKey);

        /// <summary>
        /// The index of the residue that each particle belongs to.
        /// </summary>
        public static readonly TypedKey<int[]> ParticleResidues
            = new TypedKey<int[]>(FrameData.ParticleResidueArrayKey);

        /// <summary>
        /// The type of each particle.
        /// </summary>
        public static readonly TypedKey<string[]> ParticleTypes
            = new TypedKey<string[]>(FrameData.ParticleTypeArrayKey);

        /// <summary>
        /// The potential energy of the system.
        /// </summary>
        public static readonly TypedKey<float> PotentialEnergy
            = new TypedKey<float>(FrameData.PotentialEnergyValueKey);

        /// <summary>
        /// The index of the entity (chain) that each residue belongs to.
        /// </summary>
        public static readonly TypedKey<int[]> ResidueEntities
            = new TypedKey<int[]>(FrameData.ResidueChainArrayKey);

        /// <summary>
        /// The number of residues.
        /// </summary>
        public static readonly TypedKey<int> ResidueCount
            = new TypedKey<int>(FrameData.ResidueCountValueKey);

        /// <summary>
        /// The name of each residue.
        /// </summary>
        public static readonly TypedKey<string[]> ResidueNames
            = new TypedKey<string[]>(FrameData.ResidueNameArrayKey);

        /// <summary>
        /// The simulation box the frame is contained within.
        /// </summary>
        public static readonly TypedKey<AffineTransformation?> BoxTransformation
            = new TypedKey<AffineTransformation?>("system.box.vectors");

        /// <summary>
        /// All the standard frame fields that can be specified.
        /// </summary>
        public static readonly TypedKey[] All =
        {
            Bonds,
            BondOrders,
            EntityCount,
            EntityName,
            KineticEnergy,
            ParticleCount,
            ParticleElements,
            ParticleNames,
            ParticlePositions,
            ParticleResidues,
            ParticleTypes,
            PotentialEnergy,
            ResidueEntities,
            ResidueCount,
            ResidueNames,
            BoxTransformation,
        };
    }
}