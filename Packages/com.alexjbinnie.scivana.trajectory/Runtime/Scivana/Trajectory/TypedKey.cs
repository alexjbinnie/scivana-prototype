﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace Scivana.Trajectory
{
    /// <summary>
    /// A non-generic representation of a key with a corresponding type.
    /// </summary>
    public class TypedKey
    {
        /// <summary>
        /// The key.
        /// </summary>
        public string Key { get; }

        /// <summary>
        /// The type.
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TypedKey" /> class.
        /// </summary>
        public TypedKey(string key, Type type)
        {
            Key = key;
            Type = type;
        }
    }

    /// <summary>
    /// A combination of a string key with a corresponding type. This allows methods to be written as
    /// Get(property)
    /// instead of Get<TType>(key), hence helping to obfuscate the type involved.
    /// </summary>
    public class TypedKey<TType> : TypedKey
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TypedKey{TType}" /> class.
        /// </summary>
        public TypedKey(string key)
            : base(key, typeof(TType))
        {
        }
    }
}