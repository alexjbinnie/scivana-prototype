/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Trajectory.Event;

namespace Scivana.Trajectory
{
    /// <summary>
    /// Maintains a single <see cref="Frame" />, which is updated by receiving new
    /// data. When updated, old fields are maintained except when replaced by data in the
    /// previous frame.
    /// </summary>
    public class TrajectorySnapshot : ITrajectorySnapshot
    {
        /// <inheritdoc cref="ITrajectorySnapshot.CurrentFrame" />
        public Frame CurrentFrame { get; private set; }

        /// <summary>
        /// The changes that <see cref="CurrentFrame" /> has relative to the previous frame.
        /// </summary>
        public FrameChanges CurrentFrameChanges { get; private set; }

        /// <summary>
        /// Set the current frame to completely empty, with every field changed.
        /// </summary>
        public void Clear() => SetCurrentFrame(new Frame(), FrameChanges.All);

        /// <summary>
        /// Set the current frame, replacing the existing one.
        /// </summary>
        public void SetCurrentFrame(Frame frame, FrameChanges changes)
        {
            CurrentFrame = frame;
            CurrentFrameChanges = changes;
            FrameChanged?.Invoke(CurrentFrame, CurrentFrameChanges);
        }

        /// <inheritdoc cref="ITrajectorySnapshot.FrameChanged" />
        public event FrameChanged FrameChanged;
    }
}