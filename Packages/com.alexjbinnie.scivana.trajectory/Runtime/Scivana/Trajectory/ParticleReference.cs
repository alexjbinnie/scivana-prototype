/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using JetBrains.Annotations;
using Scivana.Core.Science;
using UnityEngine;

namespace Scivana.Trajectory
{
    /// <summary>
    /// A particle which exists only as an index, pointing to various arrays which
    /// contain information such as name and type.
    /// </summary>
    internal class ParticleReference : IParticle
    {
        private readonly int index;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParticleReference" /> class  that represents the
        /// particle at <paramref name="index" /> in <paramref name="frame" />.
        /// </summary>
        public ParticleReference([NotNull] Frame frame, int index)
        {
            Frame = frame;
            this.index = index;
        }

        /// <summary>
        /// The frame the particle is a part of.
        /// </summary>
        [NotNull]
        public Frame Frame { get; }

        /// <inheritdoc />
        public int Index => index;

        /// <inheritdoc />
        [CanBeNull]
        public string Type => Frame.Get(FrameFields.ParticleTypes)?[index];

        /// <inheritdoc />
        public Vector3 Position => Frame.Get(FrameFields.ParticlePositions)[index];

        /// <inheritdoc />
        public Element? Element => Frame.Get(FrameFields.ParticleElements)?[index];
    }
}