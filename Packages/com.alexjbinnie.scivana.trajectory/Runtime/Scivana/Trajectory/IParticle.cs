﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core.Science;
using UnityEngine;

namespace Scivana.Trajectory
{
    /// <summary>
    /// A particle in a trajectory. Minimally, it has a 0-based index in the frame and a position.
    /// </summary>
    public interface IParticle
    {
        /// <summary>
        /// The 0-based index of the particle
        /// </summary>
        int Index { get; }

        /// <summary>
        /// A string describing the 'type' of the particle
        /// </summary>
        string Type { get; }

        /// <summary>
        /// The position of the particle
        /// </summary>
        Vector3 Position { get; }

        /// <summary>
        /// The element of the particle, if it is an atom.
        /// </summary>
        Element? Element { get; }
    }
}