/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Core.Science;
using UnityEngine;

namespace Scivana.Trajectory.Tests
{
    public class FrameTests
    {
        [Test]
        public void ShallowCopy()
        {
            var originalFrame = new Frame()
            {
                ParticlePositions = new[]
                {
                    Vector3.one, Vector3.left
                },
                ParticleElements = new[]
                {
                    Element.Hydrogen, Element.Oxygen
                }
            };
            originalFrame.Data["customdata"] = 1.4f;

            var clonedFrame = Frame.ShallowCopy(originalFrame);

            CollectionAssert.AreEqual(originalFrame.ParticlePositions,
                                      clonedFrame.ParticlePositions);
            CollectionAssert.AreEqual(originalFrame.ParticleElements, clonedFrame.ParticleElements);
            Assert.AreEqual(originalFrame.Data["customdata"], clonedFrame.Data["customdata"]);
        }

        [Test]
        public void ParticlesIteration()
        {
            var positions = new[]
            {
                Vector3.one, Vector3.left
            };
            var frame = new Frame()
            {
                ParticlePositions = positions
            };

            var index = 0;
            foreach (var particle in frame.Particles)
            {
                Assert.AreEqual(positions[index], particle.Position);
                index++;
            }
        }

        [Test]
        public void SetFloatValue()
        {
            var frame = new Frame();
            frame.Data["id"] = 0.5f;
            Assert.AreEqual(0.5f, frame.Data["id"]);
        }

        [Test]
        public void SetStringValue()
        {
            var frame = new Frame();
            frame.Data["id"] = "test";
            Assert.AreEqual("test", frame.Data["id"]);
        }

        [Test]
        public void HasFloatValue()
        {
            var frame = new Frame();
            frame.Data["id"] = 0.5f;
            Assert.IsTrue(frame.Data.ContainsKey("id"));
        }

        [Test]
        public void Element_Provided()
        {
            var frame = new Frame()
            {
                ParticlePositions = new[]
                {
                    Vector3.zero
                },
                ParticleElements = new[]
                {
                    Element.Carbon
                }
            };
            Assert.AreEqual(Element.Carbon, frame.Particles[0].Element);
        }

        [Test]
        public void Element_Missing()
        {
            var frame = new Frame()
            {
                ParticlePositions = new[]
                {
                    Vector3.zero
                }
            };
            Assert.IsNull(frame.Particles[0].Element);
        }

        [Test]
        public void Index()
        {
            var frame = new Frame()
            {
                ParticlePositions = new[]
                {
                    Vector3.zero
                }
            };
            Assert.AreEqual(0, frame.Particles[0].Index);
        }

        [Test]
        public void Type_Provided()
        {
            var frame = new Frame()
            {
                ParticlePositions = new[]
                {
                    Vector3.zero
                },
                ParticleTypes = new[]
                {
                    "type"
                }
            };
            Assert.AreEqual("type", frame.Particles[0].Type);
        }

        [Test]
        public void Type_Missing()
        {
            var frame = new Frame()
            {
                ParticlePositions = new[]
                {
                    Vector3.zero
                }
            };
            Assert.IsNull(frame.Particles[0].Type);
        }


        [Test]
        public void Particles_AsIEnumerable()
        {
            var frame = new Frame()
            {
                ParticlePositions = new[]
                {
                    Vector3.zero, Vector3.one
                },
            };
            CollectionAssert.IsNotEmpty(frame.Particles);
        }


        [Test]
        public void Bonds_Assignment()
        {
            var frame = new Frame
            {
                BondPairs = new[]
                {
                    new BondPair(0, 1), new BondPair(1, 2)
                }
            };
            CollectionAssert.AreEqual(new[]
            {
                new BondPair(0, 1), new BondPair(1, 2)
            }, frame.Bonds);
        }
    }
}