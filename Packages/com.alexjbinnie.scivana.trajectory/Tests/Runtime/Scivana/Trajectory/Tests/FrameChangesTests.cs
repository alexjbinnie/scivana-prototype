/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Trajectory.Event;

namespace Scivana.Frame.Tests
{
    internal class FrameChangesTests
    {
        [Test]
        public void Initial_HasAnythingChanged()
        {
            var changes = FrameChanges.None;
            Assert.IsFalse(changes.HasAnythingChanged);
        }

        [Test]
        public void Initial_HasRandomChanged()
        {
            var changes = FrameChanges.None;
            Assert.IsFalse(changes.HasChanged("id"));
        }

        [Test]
        public void SetIsChanged()
        {
            var changes = FrameChanges.None;
            changes.MarkAsChanged("id");
            Assert.IsTrue(changes.HasChanged("id"));
        }

        [Test]
        public void SetIsChanged_HasAnythingChanged()
        {
            var changes = FrameChanges.None;
            changes.MarkAsChanged("id");
            Assert.IsTrue(changes.HasAnythingChanged);
        }

        [Test]
        public void Merge_ChangesWithEmpty()
        {
            var original = FrameChanges.None;
            var next = FrameChanges.None;
            next.MarkAsChanged("id");
            original.MergeChanges(next);
            Assert.IsTrue(original.HasChanged("id"));
        }

        [Test]
        public void MergeAllIntoNone()
        {
            var original = FrameChanges.None;
            var next = FrameChanges.All;
            original.MergeChanges(next);
            Assert.IsTrue(original.HasChanged("id"));
        }

        [Test]
        public void Merge_EmptyWithChanges()
        {
            var original = FrameChanges.None;
            original.MarkAsChanged("id");

            var next = FrameChanges.None;

            original.MergeChanges(next);
            Assert.IsTrue(original.HasChanged("id"));
        }
    }
}