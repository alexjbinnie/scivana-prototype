/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using NSubstitute;
using NUnit.Framework;
using Scivana.Trajectory.Event;

namespace Scivana.Trajectory.Tests
{
    internal class TrajectorySnapshotTests
    {
        private TrajectorySnapshot trajectory;

        [SetUp]
        public void Setup()
        {
            trajectory = new TrajectorySnapshot();
        }

        [Test]
        public void Initial_NoFrame()
        {
            Assert.IsNull(trajectory.CurrentFrame);
        }

        [Test]
        public void UpdateFrame_IsCalled()
        {
            var callback = Substitute.For<Action>();

            trajectory.FrameChanged += (f, c) => callback();

            callback.Received(0)();

            trajectory.SetCurrentFrame(new Frame(), FrameChanges.All);

            callback.Received(1)();

            trajectory.SetCurrentFrame(new Frame(), FrameChanges.All);

            callback.Received(2)();
        }
    }
}