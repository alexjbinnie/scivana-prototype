/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.IO;
using NUnit.Framework;
using Scivana.Trajectory.Import.CIF;
using Scivana.Trajectory.Import.CIF.Components;
using Scivana.Trajectory.Import.CIF.Structures;
using UnityEngine;
using Assert = UnityEngine.Assertions.Assert;

namespace Scivana.Trajectory.Tests.Import.Cif
{
    /// <summary>
    /// Contains non-standard monomer 9XQ, covalent bond, disulphide bond, multiple PDB
    /// models
    /// </summary>
    public class Protein5OLF
    {
        private CifSystem component;

        [SetUp]
        public void Setup()
        {
            var file = Resources.Load("5OLF.cif");
            component = CifImport.ImportSystem(new StringReader(file.ToString()),
                                               ChemicalComponentDictionary.Instance);
        }

        [Test]
        public void DisulphideBond()
        {
            var atom1 = component.FindAtomById("SG", "CYS", 3, "A");
            var atom2 = component.FindAtomById("SG", "CYS", 7, "A");
            Assert.IsNotNull(component.GetBond(atom1, atom2));
        }

        [Test]
        public void CovalentBond()
        {
            var atom1 = component.FindAtomById("C", "9XQ", 1, "A");
            var atom2 = component.FindAtomById("N", "ALA", 2, "A");
            Assert.IsNotNull(component.GetBond(atom1, atom2));
        }
    }
}