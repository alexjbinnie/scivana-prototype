/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;

namespace Scivana.Trajectory.Tests.Import.Cif
{
    public class MultiLineTableTests
    {
        internal IReadOnlyList<CifTestImport.DataTable> Import(string str)
        {
            return CifTestImport.Import(str);
        }

        [Test]
        public void SingleLine()
        {
            var data = Import(@"loop_
_abc.def 
_abc.ghi
_abc.jkl
value1 value2 value3");

            Assert.AreEqual(1, data.Count);
            Assert.AreEqual("abc", data[0].Category);
            CollectionAssert.AreEqual(new[]
            {
                "def", "ghi", "jkl"
            }, data[0].Keywords);
            Assert.AreEqual(1, data[0].Values.Count);
            CollectionAssert.AreEqual(new[]
            {
                "value1", "value2", "value3"
            }, data[0].Values[0]);
        }

        [Test]
        public void MultipleLines()
        {
            var data = Import(@"loop_
_abc.def 
_abc.ghi
_abc.jkl
value1 value2 value3
value4 value5 value6");

            Assert.AreEqual(1, data.Count);
            Assert.AreEqual("abc", data[0].Category);
            CollectionAssert.AreEqual(new[]
            {
                "def", "ghi", "jkl"
            }, data[0].Keywords);
            Assert.AreEqual(2, data[0].Values.Count);
            CollectionAssert.AreEqual(new[]
            {
                "value1", "value2", "value3"
            }, data[0].Values[0]);
            CollectionAssert.AreEqual(new[]
            {
                "value4", "value5", "value6"
            }, data[0].Values[1]);
        }

        [Test]
        public void Delimited_DoubleQuotes()
        {
            var data = Import(@"loop_
_abc.def 
_abc.ghi
_abc.jkl
value1 "" value2 value2 "" value3");

            Assert.AreEqual(1, data.Count);
            Assert.AreEqual("abc", data[0].Category);
            CollectionAssert.AreEqual(new[]
            {
                "def", "ghi", "jkl"
            }, data[0].Keywords);
            Assert.AreEqual(1, data[0].Values.Count);
            CollectionAssert.AreEqual(new[]
                                      {
                                          "value1", " value2 value2 ", "value3"
                                      },
                                      data[0].Values[0]);
        }

        [Test]
        public void Delimited_SingleQuotes()
        {
            var data = Import(@"loop_
_abc.def 
_abc.ghi
_abc.jkl
value1 ' value2 value2 ' value3");

            Assert.AreEqual(1, data.Count);
            Assert.AreEqual("abc", data[0].Category);
            CollectionAssert.AreEqual(new[]
            {
                "def", "ghi", "jkl"
            }, data[0].Keywords);
            Assert.AreEqual(1, data[0].Values.Count);
            CollectionAssert.AreEqual(new[]
                                      {
                                          "value1", " value2 value2 ", "value3"
                                      },
                                      data[0].Values[0]);
        }

        [Test]
        public void SplitOverMultipleLines()
        {
            var data = Import(@"loop_
_abc.def 
_abc.ghi
_abc.jkl
value1
value2
value3");

            Assert.AreEqual(1, data[0].Values.Count);
            CollectionAssert.AreEqual(new[]
                                      {
                                          "value1", "value2", "value3"
                                      },
                                      data[0].Values[0]);
        }

        [Test]
        public void TextBlock()
        {
            var data = Import(@"loop_
_abc.def 
_abc.ghi
_abc.jkl
value1 value2 value3
value1
;value 2
and more
;
value3");

            Assert.AreEqual(2, data[0].Values.Count);
            CollectionAssert.AreEqual(new[]
                                      {
                                          "value1", "value2", "value3"
                                      },
                                      data[0].Values[0]);
            CollectionAssert.AreEqual(new[]
                                      {
                                          "value1", "value 2\nand more", "value3"
                                      },
                                      data[0].Values[1]);
        }
    }
}