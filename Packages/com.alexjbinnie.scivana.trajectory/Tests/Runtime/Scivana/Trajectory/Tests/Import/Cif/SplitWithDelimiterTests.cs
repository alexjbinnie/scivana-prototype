/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using NUnit.Framework;
using Scivana.Trajectory.Import.CIF;

namespace Scivana.Trajectory.Tests.Import.Cif
{
    public class SplitWithDelimiterTests
    {
        [Test]
        public void Simple()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def", "ghi"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc def ghi"));
        }

        [Test]
        public void MultipleSpaces()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def", "ghi"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc  def   ghi"));
        }

        [Test]
        public void PrefixSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def", "ghi"
                                      },
                                      CifBaseImport.SplitWithDelimiter("  abc def ghi"));
        }

        [Test]
        public void SuffixSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def", "ghi"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc def ghi  "));
        }

        [Test]
        public void DelimitSingle()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def", "ghi"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc \"def\" ghi"));
        }

        [Test]
        public void DelimitWithSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def ghi"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc \"def ghi\""));
        }

        [Test]
        public void DelimitWithMultipleSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def  ghi"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc \"def  ghi\""));
        }

        [Test]
        public void DoubleQuotes_PrefixSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", " def ghi", "jkl"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc \" def ghi\" jkl"));
        }

        [Test]
        public void DoubleQuotes_SuffixSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def ghi ", "jkl"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc \"def ghi \" jkl"));
        }

        [Test]
        public void SingleQuotes_PrefixSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", " def ghi", "jkl"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc ' def ghi' jkl"));
        }

        [Test]
        public void SingleQuotes_SuffixSpace()
        {
            CollectionAssert.AreEqual(new[]
                                      {
                                          "abc", "def ghi ", "jkl"
                                      },
                                      CifBaseImport.SplitWithDelimiter("abc 'def ghi ' jkl"));
        }
    }
}