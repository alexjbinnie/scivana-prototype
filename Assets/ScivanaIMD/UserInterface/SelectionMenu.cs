﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Network.Multiplayer;
using Scivana.UI;
using Scivana.UI.Traits;
using Scivana.Visualisation.Properties;
using ScivanaIMD.Selection.Data;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class SelectionMenu : MonoBehaviour
    {
        [SerializeField]
        private SelectionMenuItem menuItemPrefab;

        [SerializeField]
        private Transform selectionList;

        [SerializeField]
        internal WidgetButton addButton;

        [SerializeField]
        internal WidgetButton backButton;

        public SelectionMenu InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            return instance;
        }

        private Simulation simulation;

        internal IndexedPool<SelectionMenuItem> menuItems;

        private void Awake()
        {
            Assert.IsNotNull(menuItemPrefab);
            Assert.IsNotNull(selectionList);
            Assert.IsNotNull(addButton);
            Assert.IsNotNull(backButton);

            SelectionMenuItem CreateInstance() => menuItemPrefab.InstantiateInactive(selectionList);

            void ActivateInstance(SelectionMenuItem instance) =>
                instance.gameObject.SetActive(true);

            void DeactivateInstance(SelectionMenuItem instance) =>
                instance.gameObject.SetActive(false);

            menuItems = new IndexedPool<SelectionMenuItem>(CreateInstance, ActivateInstance,
                                                           DeactivateInstance);
        }

        private void OnEnable()
        {
            Assert.IsNotNull(menuItemPrefab);
            Assert.IsNotNull(selectionList);
            Assert.IsNotNull(addButton);

            RefreshSelection();
            simulation.Selections.ItemCreated += SelectionsOnItemCreated;
            simulation.Selections.ItemUpdated += SelectionsOnItemUpdated;
            simulation.Selections.ItemRemoved += SelectionsOnItemRemoved;

            addButton.Triggered += AddNewSelection;
            backButton.Triggered += () => ScivanaPrototype.App.GotoSimulationMenu();
        }

        private void AddNewSelection()
        {
            var selection = new ParticleSelection()
            {
                Name = "Selection"
            };
            var key = "selection." + Guid.NewGuid();
            simulation.Selections[key] = selection;
        }

        private void OnDisable()
        {
            simulation.Selections.ItemCreated -= SelectionsOnItemCreated;
            simulation.Selections.ItemUpdated -= SelectionsOnItemUpdated;
            simulation.Selections.ItemRemoved -= SelectionsOnItemRemoved;
        }

        private void SelectionsOnItemRemoved(string obj)
        {
            RefreshSelection();
        }

        private void SelectionsOnItemUpdated(string obj)
        {
            RefreshSelection();
        }

        private void SelectionsOnItemCreated(string obj)
        {
            RefreshSelection();
        }

        private void RefreshSelection()
        {
            var items = simulation.Selections.Resources;

            void SetSelection(KeyValuePair<string, MultiplayerResource<ParticleSelection>> selection,
                              SelectionMenuItem item)
            {
                item.SetSelection(selection.Value);
            }

            menuItems.MapConfig(items, SetSelection);

            addButton.transform.SetAsLastSibling();
        }

        private MultiplayerResource<ParticleSelection> highlightedSelection = null;

        private ArrayProperty<int> highlightedParticles = new ArrayProperty<int>();

        public IReadOnlyProperty<int[]> HighlightedParticles => highlightedParticles;

        [UsedImplicitly]
        private void OnWidgetHoverEnter(Widget widget)
        {
            if (widget.GetComponent<SelectionMenuItem>() is { } item)
            {
                highlightedSelection = item.Selection;
                highlightedParticles.Value = highlightedSelection.Value.ParticleIds?.ToArray();
            }
        }

        [UsedImplicitly]
        private void OnWidgetHoverExit(Widget widget)
        {
            if (widget.GetComponent<SelectionMenuItem>() is { } item &&
                item.Selection == highlightedSelection)
            {
                highlightedSelection = null;
                highlightedParticles.UndefineValue();
            }
        }
    }
}