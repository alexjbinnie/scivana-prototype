﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Network.Multiplayer;
using Scivana.UI.Traits;
using ScivanaIMD.Annotations;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public class AnnotationMenuItem : MonoBehaviour
    {
        [SerializeField]
        internal WidgetButton button;

        [SerializeField]
        private WidgetLabel label;

        [SerializeField]
        private WidgetIcon icon;

        [SerializeField]
        private WidgetToggle hideButton;

        [SerializeField]
        private Sprite rulerIcon;

        private MultiplayerResource<Annotation> annotation;

        public MultiplayerResource<Annotation> Annotation
        {
            set
            {
                if (annotation != null)
                {
                    annotation.ValueChanged -= RefreshHide;
                }

                annotation = value;
                if (annotation != null)
                {
                    label.Value = annotation.Value.DisplayName;
                    icon.Value = GetIcon(annotation.Value);
                    RefreshHide();
                    annotation.ValueChanged += RefreshHide;
                }
            }
        }

        private void RefreshHide()
        {
            hideButton.SetToggle(!annotation.Value.Hide, false);
        }

        private void OnDisable()
        {
            Annotation = null;
        }

        private void Awake()
        {
            button.Triggered += OnClick;
            hideButton.Toggled += (show) =>
            {
                if (annotation != null)
                {
                    var value = annotation.Value;
                    value.Hide = !show;
                    annotation.SetLocalValue(value);
                }
            };
        }

        public void OnClick()
        {
            ScivanaPrototype.App.GotoEditAnnotationMenu(annotation);
        }

        private Sprite GetIcon(Annotation annotation)
        {
            return annotation switch
            {
                RulerAnnotation _ => rulerIcon,
                _ => null
            };
        }
    }
}