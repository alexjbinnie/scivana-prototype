﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Linq;
using Scivana.Core;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public enum ProgressState
    {
        InProgress,
        Completed
    }

    public class ProgressChain : MonoBehaviour
    {
        private ProgressState[] states = new ProgressState[0];

        [SerializeField]
        private ProgressChainItem itemPrefab;

        [SerializeField]
        private Transform separator;

        public void SetState(ProgressState[] states)
        {
            var children = Enumerable.Range(0, transform.childCount)
                                     .Select(i => transform.GetChild(i).gameObject)
                                     .Where(child => child.activeSelf)
                                     .ToArray();
            foreach (var child in children)
                Destroy(child);

            for (var i = 0; i < states.Length - 1; i++)
            {
                var item = Instantiate(itemPrefab, transform);
                item.SetProgress(states[i]);
                item.SetActive(true);
                var sep = Instantiate(separator, transform);
                sep.SetActive(true);
            }

            if (states.Length > 0)
            {
                var item = Instantiate(itemPrefab, transform);
                item.SetProgress(states[states.Length - 1]);
                item.SetActive(true);
            }
        }
    }
}