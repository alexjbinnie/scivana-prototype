﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.UI.Traits;
using ScivanaIMD.UI;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public class ColorPickerDialog : Dialog
    {
        [SerializeField]
        private WidgetForegroundColor swatch;

        [SerializeField]
        private ColorSlidersHSV sliders;

        private Color color;

        public Color Color
        {
            get => color;
            set
            {
                color = value;
                swatch.Value = color;
                sliders.Color = color;
            }
        }

        public ColorPickerDialog InstantiateAsInactive(string prompt,
                                                       Color color,
                                                       Action<Color> callback)
        {
            var instance = this.InstantiateInactive();
            instance.prompt = prompt;
            instance.color = color;
            instance.Accepted += () => callback(instance.Color);
            return instance;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            swatch.Value = color;
            sliders.Color = color;
            sliders.ColorChangedBySliders += SliderChanged;
        }

        private void SliderChanged()
        {
            color = sliders.Color;
            swatch.Value = color;
        }
    }
}