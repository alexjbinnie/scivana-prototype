﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Visualisation.Properties;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public class ParticleSequenceDialog : Dialog
    {
        [SerializeField]
        private ProgressChain chain;

        private int length;

        private List<int> indices = new List<int>();

        private ArrayProperty<int> selectedIndices = new ArrayProperty<int>();

        public IReadOnlyProperty<int[]> SelectedIndices => selectedIndices;

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshChain();
        }

        private void RefreshChain()
        {
            var states = new ProgressState[length];
            for (var i = 0; i < length; i++)
                states[i] = i < indices.Count ? ProgressState.Completed : ProgressState.InProgress;
            chain.SetState(states);
            selectedIndices.Value = indices.ToArray();
        }

        public void AddIndex(int particleIndex)
        {
            indices.Add(particleIndex);
            RefreshChain();
            if (indices.Count == length)
            {
                Accept();
            }
        }

        public ParticleSequenceDialog InstantiateAsInactive(int count)
        {
            var instance = this.InstantiateInactive();
            instance.length = count;
            return instance;
        }
    }
}