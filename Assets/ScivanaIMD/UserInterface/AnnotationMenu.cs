﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.Network.Multiplayer;
using Scivana.UI.Traits;
using ScivanaIMD.Annotations;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class AnnotationMenu : MonoBehaviour
    {
        [SerializeField]
        private AnnotationMenuItem menuItemPrefab;

        [SerializeField]
        private Transform annotationList;

        [SerializeField]
        internal WidgetButton addRulerButton;

        [SerializeField]
        private WidgetButton backButton;

        public AnnotationMenu InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            return instance;
        }

        private Simulation simulation;

        internal IndexedPool<AnnotationMenuItem> menuItems;

        private void Awake()
        {
            Assert.IsNotNull(menuItemPrefab);
            Assert.IsNotNull(annotationList);
            Assert.IsNotNull(addRulerButton);
            Assert.IsNotNull(backButton);

            menuItems = IndexedPool.Create(menuItemPrefab, annotationList);
        }

        private void OnEnable()
        {
            Assert.IsNotNull(menuItemPrefab);
            Assert.IsNotNull(annotationList);
            Assert.IsNotNull(addRulerButton);

            RefreshAnnotations();
            simulation.Annotations.ItemCreated += OnAnnotationCreated;
            simulation.Annotations.ItemUpdated += OnAnnotationUpdated;
            simulation.Annotations.ItemRemoved += OnAnnotationRemoved;

            addRulerButton.Triggered += AddNewRuler;
            backButton.Triggered += () => ScivanaPrototype.App.GotoSimulationMenu();
        }

        private void AddNewRuler()
        {
            var ruler = new RulerAnnotation()
            {
                DisplayName = "Ruler"
            };
            var key = "annotation." + Guid.NewGuid();
            simulation.Annotations[key] = ruler;
        }

        private void OnDisable()
        {
            simulation.Annotations.ItemCreated -= OnAnnotationCreated;
            simulation.Annotations.ItemUpdated -= OnAnnotationUpdated;
            simulation.Annotations.ItemRemoved -= OnAnnotationRemoved;
        }

        private void OnAnnotationRemoved(string obj)
        {
            RefreshAnnotations();
        }

        private void OnAnnotationUpdated(string obj)
        {
            RefreshAnnotations();
        }

        private void OnAnnotationCreated(string obj)
        {
            RefreshAnnotations();
        }

        private void RefreshAnnotations()
        {
            var items = simulation.Annotations.Resources.Values;

            static void SetAnnotation(MultiplayerResource<Annotation> annotation,
                                      AnnotationMenuItem item)
            {
                item.Annotation = annotation;
            }

            menuItems.MapConfig(items, SetAnnotation);
        }
    }
}