﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.UI.Traits;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public class Dialog : MonoBehaviour
    {
        protected string prompt;

        [SerializeField]
        internal WidgetButton acceptButton;

        [SerializeField]
        internal WidgetButton cancelButton;

        [SerializeField]
        private WidgetLabel promptLabel;

        public event Action Accepted;
        public event Action Finished;

        protected virtual void OnEnable()
        {
            if (promptLabel != null)
                promptLabel.Value = prompt;
            if (acceptButton != null)
                acceptButton.Triggered += Accept;
            if (cancelButton != null)
                cancelButton.Triggered += Cancel;
        }

        public void Accept()
        {
            Accepted?.Invoke();
            Finished?.Invoke();
            ScivanaPrototype.App.CloseDialog(this);
        }

        public void Cancel()
        {
            Finished?.Invoke();
            ScivanaPrototype.App.CloseDialog(this);
        }
    }
}