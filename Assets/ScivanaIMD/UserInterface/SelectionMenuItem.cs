﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Globalization;
using Scivana.Network.Multiplayer;
using ScivanaIMD.Selection.Data;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace ScivanaIMD.UserInterface
{
    public class SelectionMenuItem : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text mainLabel;

        [SerializeField]
        private TMP_Text fullLabel;

        [SerializeField]
        private Button button;

        public MultiplayerResource<ParticleSelection> Selection { get; private set; }

        private void Awake()
        {
            Assert.IsNotNull(mainLabel);
            Assert.IsNotNull(fullLabel);
            Assert.IsNotNull(button);
            button.onClick.AddListener(Edit);
        }

        public void Edit()
        {
            ScivanaPrototype.App.GotoEditSelectionMenu(Selection);
        }

        public void SetSelection(MultiplayerResource<ParticleSelection> selection)
        {
            Selection = selection;
            var value = selection.Value;
            if (string.IsNullOrEmpty(value.Name))
            {
                mainLabel.text = "?";
                fullLabel.text = "Unnamed";
            }
            else
            {
                mainLabel.text = value.Name
                                      .ToUpper(CultureInfo.InvariantCulture)
                                      .Substring(0, 1);
                fullLabel.text = value.Name;
            }
        }
    }
}