﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Scivana.Core;
using Scivana.Network.Multiplayer;
using Scivana.UI.Traits;
using ScivanaIMD.Selection.Data;
using ScivanaIMD.State;
using ScivanaIMD.UI;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class EditSelection : UserInterfaceSection
    {
        private MultiplayerResource<ParticleSelection> selection;

        [SerializeField]
        internal WidgetLabel nameLabel;

        [SerializeField]
        private WidgetLabel sizeLabel;

        [SerializeField]
        private WidgetButton moreButton;

        [SerializeField]
        private Overlay moreOverlay;

        [SerializeField]
        internal WidgetButton renameButton;

        [SerializeField]
        internal WidgetButton addAllButton;

        [SerializeField]
        internal WidgetButton clearButton;

        [SerializeField]
        internal WidgetButton deleteButton;

        [SerializeField]
        internal WidgetButton backButton;

        [SerializeField]
        internal WidgetButton addInVrButton;

        [SerializeField]
        internal WidgetButton removeInVrButton;

        private void Awake()
        {
            Assert.IsNotNull(nameLabel);
            Assert.IsNotNull(sizeLabel);
            Assert.IsNotNull(moreButton);
            Assert.IsNotNull(moreOverlay);
            Assert.IsNotNull(renameButton);
            Assert.IsNotNull(addAllButton);
            Assert.IsNotNull(clearButton);
            Assert.IsNotNull(deleteButton);
            Assert.IsNotNull(backButton);
            Assert.IsNotNull(addInVrButton);
            Assert.IsNotNull(removeInVrButton);
        }

        private void OnEnable()
        {
            moreButton.Triggered += () => moreOverlay.EnterOverlay();
            addAllButton.Triggered += AddAllToSelection;
            clearButton.Triggered += ClearSelection;
            deleteButton.Triggered += DeleteSelection;
            backButton.Triggered += () => ScivanaPrototype.App.GotoSelectionMenu();
            renameButton.Triggered += () =>
                ScivanaPrototype.App.GotoAlphabeticKeyboard(selection.Value.Name,
                                                            "Enter a name for the selection:",
                                                            RenameSelection);
            addInVrButton.Triggered +=
                () => ScivanaPrototype.App.GotoModifySelection(selection.Value.ParticleIds,
                                                               ModifySelectionTool.ModificationModes.Add);
            removeInVrButton.Triggered +=
                () => ScivanaPrototype.App.GotoModifySelection(selection.Value.ParticleIds,
                                                               ModifySelectionTool.ModificationModes.Remove);
        }

        private void AddAllToSelection()
        {
            moreOverlay.LeaveOverlay();
            var value = selection.Value;
            value.ParticleIds = null;
            selection.SetLocalValue(value);
        }

        private void ClearSelection()
        {
            moreOverlay.LeaveOverlay();
            var value = selection.Value;
            value.ParticleIds = new List<int>();
            selection.SetLocalValue(value);
        }

        private void DeleteSelection()
        {
            moreOverlay.LeaveOverlay();
            selection.Remove();
        }

        private void RenameSelection(string name)
        {
            moreOverlay.LeaveOverlay();
            var value = selection.Value;
            value.Name = name;
            selection.SetLocalValue(value);
        }

        internal MultiplayerResource<ParticleSelection> Selection
        {
            get => selection;
            set
            {
                if (selection != null)
                    selection.ValueChanged -= SelectionOnValueChanged;
                selection = value;
                selection.ValueChanged += SelectionOnValueChanged;
                UpdateInfo();
            }
        }

        private void SelectionOnValueChanged()
        {
            if (selection.HasValue)
            {
                UpdateInfo();
            }
            else
            {
                // Selection deleted, return to selection menu
                ScivanaPrototype.App.GotoSelectionMenu();
            }
        }

        private void UpdateInfo()
        {
            var selection = this.selection.Value;
            nameLabel.Value = selection.Name;
            var size = selection
                       .ParticleIds?
                       .Count
                       .ToString(CultureInfo.InvariantCulture) ?? "All";
            sizeLabel.Value = $"Size: {size}";
        }

        public EditSelection InstantiateAsInactive(MultiplayerResource<ParticleSelection> selection)
        {
            var instance = this.InstantiateInactive();
            instance.Selection = selection;
            return instance;
        }

        public void SetParticleIndices(IEnumerable<int> indices)
        {
            var value = selection.Value;
            value.ParticleIds = indices?.ToList();
            selection.SetLocalValue(value);
        }
    }
}