﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI.Traits;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public class ManualConnect : MonoBehaviour
    {
        [SerializeField]
        private WidgetButton editAddress;

        [SerializeField]
        private WidgetButton editPort;

        [SerializeField]
        private WidgetButton back;

        [SerializeField]
        private WidgetButton connect;

        [SerializeField]
        private WidgetLabel addressLabel;

        [SerializeField]
        private WidgetLabel portLabel;

        private string address = "127.0.0.1";

        private int port = 38801;

        private string PreviousAddressKey = "previous_address";

        private string PreviousPortKey = "previous_port";

        private void Awake()
        {
            if (PlayerPrefs.HasKey(PreviousAddressKey))
            {
                var value = PlayerPrefs.GetString(PreviousAddressKey);
                if (!string.IsNullOrEmpty(value))
                    address = value;
            }

            if (PlayerPrefs.HasKey(PreviousPortKey))
            {
                port = PlayerPrefs.GetInt(PreviousPortKey);
            }

            back.Triggered += () => ScivanaPrototype.App.GotoMainMenu();
            connect.Triggered += () => ScivanaPrototype.App.ConnectToSimulation(address, port);
            editAddress.Triggered += () => ScivanaPrototype.App
                                                           .GotoFullscreenKeyboard(address,
                                                                                   "Enter an IP address",
                                                                                   SetAddress);
            editPort.Triggered += () => ScivanaPrototype.App
                                                        .GotoFullscreenKeyboard(port.ToString(),
                                                                                "Enter a port number",
                                                                                SetPort);
            RefreshAddressLabel();
            RefreshPortLabel();
        }

        private void SetPort(string str)
        {
            if (!string.IsNullOrEmpty(str) && int.TryParse(str, out var port))
            {
                this.port = port;
                PlayerPrefs.SetInt(PreviousPortKey, port);
                RefreshPortLabel();
            }
        }

        private void SetAddress(string str)
        {
            address = str;
            PlayerPrefs.SetString(PreviousAddressKey, address);
            RefreshAddressLabel();
        }


        private void RefreshPortLabel()
        {
            portLabel.Value = port.ToString();
        }

        private void RefreshAddressLabel()
        {
            addressLabel.Value = address;
        }
    }
}