/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI.Traits;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    [RequireComponent(typeof(WidgetButton))]
    public class KeyboardKey : MonoBehaviour
    {
        [SerializeField]
        private string key;

        [SerializeField]
        private string[] alternateKeys;

        [SerializeField]
        private bool hasUpper;

        [SerializeField]
        private string upperKey;

        [SerializeField]
        private string[] alternateUpperKey;

        private bool isAlternate;
        private WidgetButton button;
        private WidgetLabel label;
        private Keyboard keyboard;

        public void ShowAlternate()
        {
            var keyboard = GetComponentInParent<Keyboard>();
            var isUpper = hasUpper && keyboard.IsShift;
            var alternate = isUpper ? alternateUpperKey : alternateKeys;
            if (alternate.Length > 0)
                keyboard.ShowAlternateKeyOverlay(alternate);
        }

        private void Awake()
        {
            button = GetComponent<WidgetButton>();
            label = GetComponent<WidgetLabel>();
            keyboard = GetComponentInParent<Keyboard>();
        }

        private void OnEnable()
        {
            button.Triggered += PressButton;
            if (label != null)
            {
                UpdateLabel();
                keyboard.ShiftChanged += UpdateLabel;
            }
        }

        private void OnDisable()
        {
            button.Triggered -= PressButton;
            if (label != null)
            {
                keyboard.ShiftChanged -= UpdateLabel;
            }
        }

        private void PressButton()
        {
            var isUpper = hasUpper && keyboard.IsShift;
            keyboard.EnterCharacter(isUpper ? upperKey : key);
            if (isAlternate)
                keyboard.CloseAlternateKeyOverlay();
        }

        private void UpdateLabel()
        {
            label.Value = hasUpper && keyboard.IsShift
                              ? upperKey
                              : key;
        }

        public void SetKey(string key, bool isAlternate = false)
        {
            this.key = key;
            hasUpper = false;
            upperKey = "";
            this.isAlternate = isAlternate;
        }
    }
}