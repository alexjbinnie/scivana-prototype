﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Essd;
using Newtonsoft.Json.Linq;
using Scivana.Core.Async;
using Scivana.UI.Traits;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public class Autoconnecting : MonoBehaviour
    {
        [SerializeField]
        private WidgetLabel label;

        [SerializeField]
        internal WidgetButton cancelButton;

        private float startTime;

        private const int MillisecondsTimeout = 1000;

        private readonly CancellationTokenSource cancellation = new CancellationTokenSource();

        protected void Awake()
        {
            cancelButton.Triggered += ScivanaPrototype.App.GotoMainMenu;
        }

        private void OnEnable()
        {
            startTime = Time.time;

            Search().AwaitInBackground();
        }

        protected void OnDisable()
        {
            cancellation.Cancel();
            cancellation.Dispose();
        }

        private async Task Search()
        {
            var client = new Client();
            var services = await Task.Run(() => client.SearchForServices(MillisecondsTimeout),
                                          cancellation.Token);
            if (services.Count > 0)
                OnFoundService(services.First());
        }

        private ServiceHub foundService;

        private void OnFoundService(ServiceHub service)
        {
            var port = (service.Properties["services"] as JObject)?["trajectory"]
                       .ToObject<int>() ?? -1;
            if (port > 0)
            {
                foundService = service;
                ScivanaPrototype.App.ConnectToSimulation(foundService.Address, port);
            }
        }

        private void Update()
        {
            var elapsed = Time.time - startTime;

            if (foundService == null)
                label.Value = $"Searching for Servers... ({elapsed:0.0}s)";
            else
                label.Value = $"Found Service";
        }
    }
}