﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Core;
using Scivana.UI.Traits;
using ScivanaIMD.UI;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class Keyboard : Dialog
    {
        [SerializeField]
        private WidgetLabel inputField;

        [SerializeField]
        private GameObject alphabeticKeyboard;

        [SerializeField]
        private GameObject numericKeyboard;

        [SerializeField]
        private Sprite shiftOff;

        [SerializeField]
        private Sprite shiftOn;

        [SerializeField]
        private WidgetIcon shiftIcon;

        [SerializeField]
        private Overlay alternateKeysOverlay;

        [SerializeField]
        private GameObject alternateKeyList;

        [SerializeField]
        private KeyboardKey alternateKeyPrefab;

        private readonly List<KeyboardKey> currentAlternateKeys = new List<KeyboardKey>();

        public void ShowAlternateKeyOverlay(string[] keys)
        {
            alternateKeysOverlay.EnterOverlay();
            foreach (var key in keys)
            {
                var instance = alternateKeyPrefab.InstantiateInactive(alternateKeyList.transform);
                instance.SetKey(key, true);
                instance.SetActive(true);
                currentAlternateKeys.Add(instance);
            }
        }

        public void CloseAlternateKeyOverlay()
        {
            foreach (var key in currentAlternateKeys)
                Destroy(key.gameObject);
            currentAlternateKeys.Clear();
            alternateKeysOverlay.LeaveOverlay();
        }

        public event Action ShiftChanged;

        public bool IsShift { get; private set; }

        public string Text
        {
            get => inputField.Value;
            set => inputField.Value = value;
        }

        private void Awake()
        {
            Assert.IsNotNull(inputField);
        }

        public Keyboard InstantiateAsInactive(string text, string prompt, Action<string> callback)
        {
            var instance = this.InstantiateInactive();
            instance.inputField.Value = text;
            instance.prompt = prompt;
            instance.Accepted += () => callback(instance.Text);
            return instance;
        }

        public void EnterCharacter(string key)
        {
            inputField.Value += key;
            SetShift(false);
        }

        public void GotoNumericKeyboard()
        {
            numericKeyboard.SetActive(true);
            alphabeticKeyboard.SetActive(false);
            SetShift(false);
        }

        public void GotoAlphabeticKeyboard()
        {
            numericKeyboard.SetActive(false);
            alphabeticKeyboard.SetActive(true);
            SetShift(false);
        }

        public void ClickShift()
        {
            SetShift(!IsShift);
        }

        private void SetShift(bool shift)
        {
            IsShift = shift;
            if (shiftIcon.isActiveAndEnabled)
                shiftIcon.Value = IsShift ? shiftOn : shiftOff;
            ShiftChanged?.Invoke();
        }

        public void Backspace()
        {
            if (inputField.Value.Length > 0)
                inputField.Value = inputField.Value.Substring(0, inputField.Value.Length - 1);
        }
    }
}