﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.Trajectory;
using Scivana.UI.Traits;
using Scivana.Visualisation.Data;
using ScivanaIMD.Simulations;
using ScivanaIMD.State;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class ModifySelectionDialog : Dialog
    {
        [SerializeField]
        private GameObject inspectionBox;

        [SerializeField]
        private WidgetForegroundColor inspectedAtomIcon;

        [SerializeField]
        private WidgetLabel inspectedAtomName;

        [SerializeField]
        private WidgetLabel inspectedResidueName;

        [SerializeField]
        private WidgetLabel inspectedResidueFullName;

        [SerializeField]
        private WidgetLabel selectionSize;

        [SerializeField]
        private ElementColorMapping atomColors;

        private Simulation simulation;

        private ModifySelectionTool tool;

        private ModifySelectionContext context;

        public ModifySelectionDialog InstantiateAsInactive(Simulation simulation,
                                                           ModifySelectionContext context,
                                                           ModifySelectionTool tool)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            instance.context = context;
            instance.tool = tool;
            return instance;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Assert.IsNotNull(inspectionBox);
            Assert.IsNotNull(inspectedAtomIcon);
            Assert.IsNotNull(inspectedAtomName);
            Assert.IsNotNull(inspectedResidueName);
            Assert.IsNotNull(inspectedResidueFullName);

            RefreshAtom();
            tool.HighlightedParticleChanged += RefreshAtom;

            RefreshSize();
            context.SelectedIndicesChanged += RefreshSize;
        }

        private void RefreshAtom()
        {
            var index = tool.HighlightedParticle;
            if (!index.HasValue)
            {
                inspectionBox.SetActive(false);
                return;
            }

            inspectionBox.SetActive(true);
            var particleIndex = index.Value;

            var names = simulation.CurrentFrame.Get(FrameFields.ParticleNames);
            if (names == null)
            {
                inspectedAtomName.Value = "-";
            }
            else
            {
                inspectedAtomName.Value = names[particleIndex];
            }

            var elements = simulation.CurrentFrame.Get(FrameFields.ParticleElements);
            if (elements == null)
            {
                inspectedAtomIcon.Value = Color.gray;
            }
            else
            {
                var element = elements[particleIndex];
                inspectedAtomIcon.Value = atomColors.Map(element);
            }

            var residues = simulation.CurrentFrame.Get(FrameFields.ParticleResidues);
            var residueNames = simulation.CurrentFrame.Get(FrameFields.ResidueNames);

            if (residues == null || residueNames == null)
            {
                inspectedResidueName.Value = "-";
            }
            else
            {
                var residueIndex = residues[particleIndex];
                inspectedResidueName.Value = residueNames[residueIndex];
            }

            inspectedResidueFullName.Value = "";
        }

        private void RefreshSize()
        {
            var size = context.SelectionSize;
            selectionSize.Value = $"Current Size: {size}";
        }
    }
}