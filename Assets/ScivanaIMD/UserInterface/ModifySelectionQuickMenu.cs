﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.UI.Traits;
using ScivanaIMD.State;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    /// <summary>
    /// A quick menu for the right hand for the modify selection tool, which allows the grouping and
    /// modification type (adding or subtracting) to be controlled.
    /// </summary>
    public class ModifySelectionQuickMenu : UserInterfaceSection
    {
        [SerializeField]
        internal WidgetButton groupingTypeButton;

        [SerializeField]
        internal WidgetIcon groupingTypeIcon;

        [SerializeField]
        internal WidgetToggle modificationTypeToggle;

        [SerializeField]
        private Sprite particleGroupingIcon;

        [SerializeField]
        private Sprite residueGroupingIcon;

        [SerializeField]
        private Sprite chainGroupingIcon;

        private ModifySelectionTool tool;

        public ModifySelectionQuickMenu InstantiateAsInactive(ModifySelectionTool tool)
        {
            var instance = this.InstantiateInactive();
            instance.tool = tool;
            return instance;
        }

        private void OnEnable()
        {
            Assert.IsNotNull(groupingTypeButton);
            Assert.IsNotNull(groupingTypeIcon);
            Assert.IsNotNull(modificationTypeToggle);

            UpdateModificationType();
            modificationTypeToggle.Toggled += (toggle) =>
                tool.ModificationMode = toggle
                                            ? ModifySelectionTool.ModificationModes.Add
                                            : ModifySelectionTool.ModificationModes.Remove;
            tool.ModificationModeChanged += UpdateModificationType;

            UpdateGroupingType();
            groupingTypeButton.Triggered += NextGroupingType;
            tool.GroupingModeChanged += UpdateGroupingType;
        }

        private void UpdateGroupingType()
        {
            switch (tool.GroupingMode)
            {
                case ModifySelectionTool.GroupingModes.Particle:
                    groupingTypeIcon.Value = particleGroupingIcon;
                    break;
                case ModifySelectionTool.GroupingModes.Residue:
                    groupingTypeIcon.Value = residueGroupingIcon;
                    break;
                case ModifySelectionTool.GroupingModes.Chain:
                    groupingTypeIcon.Value = chainGroupingIcon;
                    break;
            }
        }

        private void NextGroupingType()
        {
            switch (tool.GroupingMode)
            {
                case ModifySelectionTool.GroupingModes.Particle:
                    tool.GroupingMode = ModifySelectionTool.GroupingModes.Residue;
                    break;
                case ModifySelectionTool.GroupingModes.Residue:
                    tool.GroupingMode = ModifySelectionTool.GroupingModes.Chain;
                    break;
                case ModifySelectionTool.GroupingModes.Chain:
                    tool.GroupingMode = ModifySelectionTool.GroupingModes.Particle;
                    break;
            }
        }

        private void UpdateModificationType()
        {
            modificationTypeToggle.SetToggle(tool.ModificationMode == ModifySelectionTool.ModificationModes.Add, false);
        }
    }
}