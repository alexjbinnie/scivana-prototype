﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Runtime.Serialization;
using Scivana.Core;
using Scivana.Network.Multiplayer;
using Scivana.UI.Traits;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.UserInterface
{
    public class SceneVisuals : MonoBehaviour
    {
        [SerializeField]
        private WidgetToggle toggleBox;

        [SerializeField]
        private WidgetToggle toggleAxes;

        [SerializeField]
        private WidgetButton recolorBox;

        [SerializeField]
        private WidgetButton backButton;

        public const string boxKey = "scene.box";

        public const string axesKey = "scene.axes";

        private MultiplayerResource<SceneBox> box;

        private MultiplayerResource<SceneAxes> axes;

        public SceneVisuals InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();
            instance.box = simulation.Multiplayer.GetSharedResource<SceneBox>(boxKey);
            instance.axes = simulation.Multiplayer.GetSharedResource<SceneAxes>(axesKey);

            instance.recolorBox.Triggered += () =>
                ScivanaPrototype.App.GotoColorPicker(instance.BoxColor,
                                                     "Choose the box color",
                                                     instance.SetBoxColor);

            instance.toggleBox.Toggled += instance.SetBoxVisibility;
            instance.toggleAxes.Toggled += instance.SetAxesVisibility;
            instance.backButton.Triggered += ScivanaPrototype.App.GotoSimulationMenu;
            return instance;
        }

        private void SetBoxVisibility(bool toggled)
        {
            var value = box.HasValue ? box.Value : new SceneBox();
            value.Hide = !toggled;
            box.SetLocalValue(value);
        }

        private void SetAxesVisibility(bool toggled)
        {
            var value = axes.HasValue ? axes.Value : new SceneAxes();
            value.Hide = !toggled;
            axes.SetLocalValue(value);
        }

        private Color BoxColor => box.HasValue ? box.Value.Color ?? Color.white : Color.white;

        private void SetBoxColor(Color color)
        {
            var value = box.HasValue ? box.Value : new SceneBox();
            value.Color = color;
            box.SetLocalValue(value);
        }

        private void Start()
        {
            RefreshBoxToggle();
            box.ValueChanged += RefreshBoxToggle;
            RefreshAxesToggle();
            axes.ValueChanged += RefreshAxesToggle;
        }

        private void RefreshAxesToggle()
        {
            toggleAxes.SetToggle(ShowAxes, false);
        }

        private void RefreshBoxToggle()
        {
            toggleBox.SetToggle(ShowBox, false);
        }

        private bool ShowBox => !box.HasValue || !box.Value.Hide;

        private bool ShowAxes => axes.HasValue && !axes.Value.Hide;

        [DataContract]
        internal class SceneBox
        {
            [DataMember(Name = "hide", IsRequired = false)]
            public bool Hide = false;

            [DataMember(Name = "color")]
            public Color? Color = null;
        }

        [DataContract]
        internal class SceneAxes
        {
            [DataMember(Name = "hide", IsRequired = false)]
            public bool Hide = false;
        }
    }
}