﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading.Tasks;
using Grpc.Core;
using Scivana.Core;
using Scivana.Core.Async;
using Scivana.UI.Traits;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class Connecting : MonoBehaviour
    {
        public Connecting InstantiateAsInactive(string address, int port)
        {
            var instance = this.InstantiateInactive();

            var channel = ScivanaPrototype.Networking.GetChannel(address, port);

            instance.connection = new SimulationConnection(channel);

            instance.minTime = Time.time;

            instance.connectingLabel.Value = $"Connecting to {address}";

            return instance;
        }

        internal enum ConnectionState
        {
            Connecting,
            Connected,
            FailedToConnect
        }

        private void OnEnable()
        {
            GetCommands().AwaitInBackground();

            connection.Trajectory.FrameChanged += (_, __) => trajectoryConnected = ConnectionState.Connected;
            connection.Trajectory.Disconnected += _ => trajectoryConnected = ConnectionState.FailedToConnect;
            connection.Multiplayer.ReceiveStateUpdate += () => multiplayerConnected = ConnectionState.Connected;
            connection.Multiplayer.Disconnected += _ => multiplayerConnected = ConnectionState.FailedToConnect;
        }

        private async Task GetCommands()
        {
            try
            {
                await connection.Trajectory.GetCommandsAsync();
            }
            catch (RpcException)
            {
                commandsConnected = ConnectionState.FailedToConnect;
                return;
            }

            commandsConnected = ConnectionState.Connected;
        }

        private void Awake()
        {
            Assert.IsNotNull(trajectoryIcon);
            Assert.IsNotNull(multiplayerIcon);
            Assert.IsNotNull(commandsIcon);
            Assert.IsNotNull(successSprite);
            Assert.IsNotNull(loadingSprite);

            cancelButton.Triggered += ScivanaPrototype.App.GotoMainMenu;
        }

        [SerializeField]
        private WidgetButton cancelButton;

        [SerializeField]
        private WidgetLabel connectingLabel;

        [SerializeField]
        private WidgetIcon trajectoryIcon;

        [SerializeField]
        private WidgetIcon multiplayerIcon;

        [SerializeField]
        private WidgetIcon commandsIcon;

        [SerializeField]
        private Sprite successSprite;

        [SerializeField]
        private Sprite loadingSprite;

        [SerializeField]
        private Sprite failedSprite;

        private SimulationConnection connection;

        internal ConnectionState trajectoryConnected = ConnectionState.Connecting;

        internal ConnectionState multiplayerConnected = ConnectionState.Connecting;

        internal ConnectionState commandsConnected = ConnectionState.Connecting;

        private float minTime;

        private bool hasConnected = false;

        private void Update()
        {
            trajectoryIcon.Value = GetState(trajectoryConnected);
            multiplayerIcon.Value = GetState(multiplayerConnected);
            commandsIcon.Value = GetState(commandsConnected);

            if (trajectoryConnected == ConnectionState.Connected &&
                multiplayerConnected == ConnectionState.Connected &&
                commandsConnected == ConnectionState.Connected &&
                Time.time - minTime > 1f)
            {
                hasConnected = true;
                ScivanaPrototype.App.OpenSimulation(connection);
                ScivanaPrototype.App.GotoInteractionMode();
            }
        }

        private void OnDestroy()
        {
            if (!hasConnected)
                connection?.Dispose();
        }

        private Sprite GetState(ConnectionState state)
        {
            return state switch
            {
                ConnectionState.Connecting => loadingSprite,
                ConnectionState.Connected => successSprite,
                ConnectionState.FailedToConnect => failedSprite,
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
            };
        }
    }
}