﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Linq;
using Scivana.Core;
using Scivana.Network.Multiplayer;
using Scivana.UI.Traits;
using ScivanaIMD.Avatars;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class UsersMenu : MonoBehaviour
    {
        [SerializeField]
        internal WidgetLabel usernameLabel;

        [SerializeField]
        internal WidgetForegroundColor usernameColor;

        [SerializeField]
        internal WidgetButton renameUserButton;

        [SerializeField]
        internal WidgetButton recolorUserButton;

        [SerializeField]
        internal UserMenuItem menuItemPrefab;

        [SerializeField]
        internal Transform userList;

        [SerializeField]
        internal WidgetButton backButton;

        public UsersMenu InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            return instance;
        }

        private Simulation simulation;

        internal IndexedPool<UserMenuItem> menuItems;

        private void Awake()
        {
            Assert.IsNotNull(renameUserButton);
            Assert.IsNotNull(recolorUserButton);
            Assert.IsNotNull(backButton);

            menuItems = IndexedPool.Create(menuItemPrefab, userList);

            renameUserButton.Triggered += () =>
                ScivanaPrototype.App.GotoAlphabeticKeyboard(PlayerName.GetPlayerName(),
                                                            "Enter your user name",
                                                            PlayerName.SetPlayerName);

            recolorUserButton.Triggered += () =>
                ScivanaPrototype.App.GotoColorPicker(PlayerColor.GetPlayerColor(),
                                                     "Choose your user color",
                                                     PlayerColor.SetPlayerColor);

            backButton.Triggered += () =>
                ScivanaPrototype.App.GotoSimulationMenu();
        }

        private void OnEnable()
        {
            simulation.Avatars.ItemCreated += OnAvatarCreated;
            simulation.Avatars.ItemUpdated += OnAvatarUpdated;
            simulation.Avatars.ItemRemoved += OnAvatarRemoved;

            backButton.Triggered += () => ScivanaPrototype.App.GotoSimulationMenu();

            RefreshUserName();
            RefreshUserColor();
            PlayerColor.PlayerColorChanged += RefreshUserColor;
            PlayerName.PlayerNameChanged += RefreshUserName;
        }

        private void RefreshUserName()
        {
            usernameLabel.Value = PlayerName.GetPlayerName();
        }

        private void RefreshUserColor()
        {
            usernameColor.Value = PlayerColor.GetPlayerColor();
        }

        private void OnDisable()
        {
            simulation.Avatars.ItemCreated -= OnAvatarCreated;
            simulation.Avatars.ItemUpdated -= OnAvatarUpdated;
            simulation.Avatars.ItemRemoved -= OnAvatarRemoved;

            PlayerColor.PlayerColorChanged -= RefreshUserColor;
            PlayerName.PlayerNameChanged -= RefreshUserName;
        }

        private void OnAvatarRemoved(string obj)
        {
            RefreshAvatars();
        }

        private void OnAvatarUpdated(string obj)
        {
            RefreshAvatars();
        }

        private void OnAvatarCreated(string obj)
        {
            RefreshAvatars();
        }

        private void RefreshAvatars()
        {
            var items = simulation.Avatars.Resources.Values.Where(a => a.HasValue);

            static void SetAvatar(MultiplayerResource<MultiplayerAvatar> avatar,
                                  UserMenuItem item)
            {
                item.Avatar = avatar;
            }

            menuItems.MapConfig(items, SetAvatar);
        }
    }
}