﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.UI;
using Scivana.UI.Traits;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class SimulationQuickMenu : UserInterfaceSection
    {
        private Simulation simulation;

        public SimulationQuickMenu InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            return instance;
        }

        [SerializeField]
        internal WidgetButton playButton;

        [SerializeField]
        internal WidgetButton pauseButton;

        [SerializeField]
        internal WidgetButton resetButton;

        [SerializeField]
        internal WidgetButton menuButton;

        private void Awake()
        {
            Assert.IsNotNull(playButton);
            Assert.IsNotNull(pauseButton);
            Assert.IsNotNull(resetButton);
            Assert.IsNotNull(menuButton);

            playButton.Triggered += simulation.Trajectory.Play;
            pauseButton.Triggered += simulation.Trajectory.Pause;
            resetButton.Triggered += simulation.Trajectory.Reset;
            menuButton.Triggered += ScivanaPrototype.App.GotoSimulationMenu;
        }

        protected override string GetTooltip(Widget widget)
        {
            if (widget == playButton)
                return "Play Simulation";
            if (widget == pauseButton)
                return "Pause Simulation";
            if (widget == resetButton)
                return "Reset Simulation";
            if (widget == menuButton)
                return "Open Menu";
            return null;
        }
    }
}