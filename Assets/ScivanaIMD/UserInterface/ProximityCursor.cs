﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using UnityEngine;
using UnityEngine.XR;

namespace ScivanaIMD.UserInterface
{
    /// <summary>
    /// Spawns a UI cursor when near to a canvas
    /// </summary>
    public class ProximityCursor : MonoBehaviour
    {
        private RectTransform canvas;
        private InputDeviceRole role;

        public static ProximityCursor InstantiateAsInactive(RectTransform canvas,
                                                            InputDeviceRole role)
        {
            var instance = Instantiation.InstantiateInactive<ProximityCursor>();
            instance.canvas = canvas;
            instance.role = role;
            return instance;
        }

        private void Update()
        {
            Refresh();
        }

        private void Refresh()
        {
            var cursor = ScivanaPrototype.Input.GetOrientation(role);
            if (cursor.Value.HasValue)
            {
                var position = cursor.Value.Value.TransformPoint(new Vector3(0, 0, 0.1f));
                var local = canvas.transform.InverseTransformPoint(position);
                var width = canvas.sizeDelta.x;
                var height = canvas.sizeDelta.y;

                var inside = local.x > -width / 2f && local.x < width / 2f && local.y > -height / 2f &&
                             local.y < height / 2f && local.z > -30f && local.z < 80f;

                if (inside)
                {
                    ScivanaPrototype.App.EnsureUICursor(canvas,
                                                        role == InputDeviceRole.LeftHanded,
                                                        role == InputDeviceRole.RightHanded);
                }
                else
                {
                    ScivanaPrototype.App.RemoveUICursor();
                }
            }
        }
    }
}