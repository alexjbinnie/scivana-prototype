﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI;
using Scivana.UI.Traits;
using ScivanaIMD.Interaction;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class InteractionQuickMenu : UserInterfaceSection
    {
        [SerializeField]
        private WidgetToggle velocityResetToggle;

        [SerializeField]
        private WidgetToggle massWeightedToggle;

        [SerializeField]
        private WidgetButton forceTypeButton;

        [SerializeField]
        private WidgetIcon forceTypeIcon;

        [SerializeField]
        private Sprite springIcon;

        [SerializeField]
        private Sprite gaussianIcon;

        [SerializeField]
        private WidgetLabel forceLabel;

        private InteractionPreferences preferences;

        private void OnEnable()
        {
            Assert.IsNotNull(velocityResetToggle);
            Assert.IsNotNull(massWeightedToggle);
            Assert.IsNotNull(forceTypeIcon);
            Assert.IsNotNull(forceTypeButton);
            Assert.IsNotNull(springIcon);
            Assert.IsNotNull(gaussianIcon);
            Assert.IsNotNull(forceLabel);

            preferences = ScivanaPrototype.Preferences<InteractionPreferences>();

            UpdateVelocityReset();
            velocityResetToggle.Toggled += (toggle) =>
                preferences.ResetVelocities.Value = toggle;
            preferences.ResetVelocities.ValueChanged += UpdateVelocityReset;

            UpdateMassWeighted();
            massWeightedToggle.Toggled += (toggle) =>
                preferences.MassWeighted.Value = toggle;
            preferences.MassWeighted.ValueChanged += UpdateMassWeighted;

            UpdateForceScale();
            preferences.ForceScale.ValueChanged += UpdateForceScale;

            UpdateInteractionType();
            forceTypeButton.Triggered += ToggleInteractionType;
            preferences.InteractionType.ValueChanged += UpdateInteractionType;
        }

        private void OnDisable()
        {
            preferences.ResetVelocities.ValueChanged -= UpdateVelocityReset;
            preferences.MassWeighted.ValueChanged -= UpdateMassWeighted;
            preferences.ForceScale.ValueChanged -= UpdateForceScale;
            preferences.InteractionType.ValueChanged -= UpdateInteractionType;
        }

        private void UpdateVelocityReset()
        {
            velocityResetToggle.SetToggle(preferences.ResetVelocities.Value, false);
        }

        private void UpdateMassWeighted()
        {
            massWeightedToggle.SetToggle(preferences.MassWeighted.Value, false);
        }

        private void UpdateForceScale()
        {
            forceLabel.Value = $"{preferences.ForceScale.Value}×";
        }

        private void UpdateInteractionType()
        {
            if (preferences.InteractionType.Value == "spring")
            {
                forceTypeIcon.Value = springIcon;
            }
            else
            {
                forceTypeIcon.Value = gaussianIcon;
            }
        }

        private void ToggleInteractionType()
        {
            if (preferences.InteractionType.Value == "gaussian")
            {
                preferences.InteractionType.Value = "spring";
            }
            else
            {
                preferences.InteractionType.Value = "gaussian";
            }
        }

        protected override string GetTooltip(Widget widget)
        {
            if (widget == forceTypeButton)
            {
                if (preferences.InteractionType.Value == "gaussian")
                    return "Interaction Type: Gaussian";
                else if (preferences.InteractionType.Value == "spring")
                    return "Interaction Type: Spring";
                else
                    return "Interaction Type: Custom";
            }

            if (widget == massWeightedToggle)
            {
                var label = preferences.MassWeighted.Value ? "On" : "Off";
                return $"Mass Weighting: {label}";
            }

            if (widget == velocityResetToggle)
            {
                var label = preferences.ResetVelocities.Value ? "On" : "Off";
                return $"Velocity Reset: {label}";
            }

            return null;
        }
    }
}