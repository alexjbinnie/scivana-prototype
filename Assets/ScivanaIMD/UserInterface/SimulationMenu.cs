﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.UI.Traits;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class SimulationMenu : UserInterfaceSection
    {
        [SerializeField]
        internal WidgetButton disconnectButton;

        [SerializeField]
        internal WidgetButton selectionButton;

        [SerializeField]
        internal WidgetButton visualsButton;

        [SerializeField]
        internal WidgetButton annotationsButton;

        [SerializeField]
        internal WidgetButton interactButton;

        [SerializeField]
        internal WidgetButton inspectButton;

        [SerializeField]
        internal WidgetButton userButton;

        private void Awake()
        {
            Assert.IsNotNull(disconnectButton);
            Assert.IsNotNull(selectionButton);
            Assert.IsNotNull(visualsButton);
            Assert.IsNotNull(annotationsButton);
            Assert.IsNotNull(interactButton);
            Assert.IsNotNull(inspectButton);
            Assert.IsNotNull(userButton);
        }

        private void OnEnable()
        {
            disconnectButton.Triggered += ScivanaPrototype.App.GotoMainMenu;
            selectionButton.Triggered += ScivanaPrototype.App.GotoSelectionMenu;
            annotationsButton.Triggered += ScivanaPrototype.App.GotoAnnotationMenu;
            visualsButton.Triggered += ScivanaPrototype.App.GotoSceneVisuals;
            userButton.Triggered += ScivanaPrototype.App.GotoUsersMenu;
            interactButton.Triggered += ScivanaPrototype.App.GotoInteractionMode;
        }
    }
}