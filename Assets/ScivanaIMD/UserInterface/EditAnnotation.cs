﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.Network.Multiplayer;
using Scivana.UI.Traits;
using ScivanaIMD.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.UserInterface
{
    public class EditAnnotation : UserInterfaceSection
    {
        private MultiplayerResource<Annotation> annotation;

        [SerializeField]
        private WidgetLabel nameLabel;

        [SerializeField]
        internal WidgetButton renameButton;

        [SerializeField]
        internal WidgetButton deleteButton;

        [SerializeField]
        internal WidgetButton editButton;


        [SerializeField]
        private WidgetButton backButton;

        private void Awake()
        {
            Assert.IsNotNull(nameLabel);
            Assert.IsNotNull(renameButton);
            Assert.IsNotNull(deleteButton);
            Assert.IsNotNull(backButton);
        }

        private void OnEnable()
        {
            deleteButton.Triggered += DeleteAnnotation;
            backButton.Triggered += () => ScivanaPrototype.App.GotoAnnotationMenu();
            renameButton.Triggered += () =>
                ScivanaPrototype.App.GotoAlphabeticKeyboard(annotation.Value.DisplayName,
                                                            "Enter a name for the annotation:",
                                                            RenameAnnotation);
            editButton.Triggered += () =>
            {
                if (annotation.Value is RulerAnnotation)
                {
                    ScivanaPrototype.App.StartParticleSequenceSelection(2, SetIndices);
                }
            };
        }

        private void SetIndices(int[] indices)
        {
            if (annotation.Value is RulerAnnotation ruler && indices.Length == 2)
            {
                ruler.Index1 = indices[0];
                ruler.Index2 = indices[1];
                annotation.SetLocalValue(ruler);
            }
        }

        private void DeleteAnnotation()
        {
            annotation.Remove();
        }

        private void RenameAnnotation(string name)
        {
            var value = annotation.Value;
            value.DisplayName = name;
            annotation.SetLocalValue(value);
        }

        private MultiplayerResource<Annotation> Annotation
        {
            set
            {
                if (annotation != null)
                    annotation.ValueChanged -= OnAnnotationChanged;
                annotation = value;
                annotation.ValueChanged += OnAnnotationChanged;
                UpdateInfo();
            }
        }

        private void OnAnnotationChanged()
        {
            if (annotation.HasValue)
            {
                UpdateInfo();
            }
            else
            {
                // Annotation deleted, return to annotation menu
                ScivanaPrototype.App.GotoAnnotationMenu();
            }
        }

        private void UpdateInfo()
        {
            var annotation = this.annotation.Value;
            nameLabel.Value = annotation.DisplayName;
        }

        public EditAnnotation InstantiateAsInactive(MultiplayerResource<Annotation> annotation)
        {
            var instance = this.InstantiateInactive();
            instance.Annotation = annotation;
            return instance;
        }
    }
}