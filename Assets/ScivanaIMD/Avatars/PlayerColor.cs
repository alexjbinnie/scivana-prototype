﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Visualisation.Parsing;
using UnityEngine;
using UnityEngine.UI;

namespace ScivanaIMD.Avatars
{
    public class PlayerColor : MonoBehaviour
    {
        private static string ColorKey = "scivana.player.color";

        private static Color DefaultColor = new Color(255, 102, 0);

        [SerializeField]
        private Image userIcon;

        [SerializeField]
        private GameObject presetButtonParent;

        public static Color GetPlayerColor()
        {
            if (PlayerPrefs.HasKey(ColorKey))
            {
                var value = PlayerPrefs.GetString(ColorKey);
                if (VisualisationParser.ParseColor(value) is { } color)
                {
                    return color;
                }
            }

            return DefaultColor;
        }

        public static void SetPlayerColor(Color color)
        {
            PlayerPrefs.SetString(ColorKey, "#" + ColorUtility.ToHtmlStringRGB(color));
            PlayerColorChanged?.Invoke();
        }

        public static event Action PlayerColorChanged;

        private void Awake()
        {
            PlayerColorChanged += UpdateIconColor;
            UpdateIconColor();
            foreach (var childObj in presetButtonParent.transform)
            {
                var child = (childObj as Transform);
                var button = child.GetComponent<Button>();
                if (button == null)
                    continue;
                var color = child.Find("Icon").Find("Color").GetComponent<Image>().color;
                button.onClick.AddListener(() => { SetPlayerColor(color); });
            }
        }

        private void UpdateIconColor()
        {
            if (userIcon != null)
                userIcon.color = GetPlayerColor();
        }
    }
}