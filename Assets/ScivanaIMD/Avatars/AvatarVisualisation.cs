﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Linq;
using Scivana.Core;
using Scivana.Network.Multiplayer;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.Avatars
{
    /// <summary>
    /// Responsible for the visualisation of the other multiplayer avatars present in the scene.
    /// </summary>
    public class AvatarVisualisation : MonoBehaviour
    {
        [SerializeField]
        private AvatarModel headsetPrefab;

        [SerializeField]
        private AvatarModel controllerPrefab;

        private void Awake()
        {
            Assert.IsNotNull(headsetPrefab);
            Assert.IsNotNull(controllerPrefab);
        }

        private Simulation simulation;

        internal IndexedPool<AvatarModel> headsetObjects;
        internal IndexedPool<AvatarModel> controllerObjects;

        public AvatarVisualisation InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            return instance;
        }

        private void Update()
        {
            UpdateRendering();
        }

        private void OnEnable()
        {
            headsetObjects = IndexedPool.Create(headsetPrefab);
            controllerObjects = IndexedPool.Create(controllerPrefab);
        }

        private void UpdateRendering()
        {
            var headsets = simulation
                           .Multiplayer
                           .Avatars
                           .OtherPlayerAvatars
                           .SelectMany(avatar => avatar.Components, (avatar, component) =>
                                           (Avatar: avatar, Component: component))
                           .Where(res => res.Component.Name ==
                                         MultiplayerAvatar.HeadsetName);

            var controllers = simulation
                              .Multiplayer
                              .Avatars
                              .OtherPlayerAvatars
                              .SelectMany(avatar => avatar.Components,
                                          (avatar, component) =>
                                              (Avatar: avatar, Component: component))
                              .Where(res => res.Component.Name ==
                                            MultiplayerAvatar.LeftHandName
                                         || res.Component.Name ==
                                            MultiplayerAvatar.RightHandName);

            headsetObjects.MapConfig(headsets, UpdateAvatarComponent);
            controllerObjects.MapConfig(controllers, UpdateAvatarComponent);

            void UpdateAvatarComponent(
                (MultiplayerAvatar Avatar, MultiplayerAvatar.Component Component) value,
                AvatarModel model)
            {
                var componentTransform = model.transform;
                componentTransform.parent = transform;
                componentTransform.localPosition = value.Component.Position;
                componentTransform.localRotation = value.Component.Rotation;
                //model.SetPlayerColor(value.Avatar.Color);
                //model.SetPlayerName(value.Avatar.Name);
            }
        }
    }
}