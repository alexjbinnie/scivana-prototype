﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.Core.Math;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.Avatars
{
    /// <summary>
    /// Responsible for the updating of the local multiplayer avatar with the positions, name
    /// and color of the current player.
    /// </summary>
    public class AvatarSynchronisation : MonoBehaviour
    {
        private Simulation simulation;

        public static AvatarSynchronisation InstantiateAsInactive(Simulation simulation)
        {
            var instance = Instantiation.InstantiateInactive<AvatarSynchronisation>();
            instance.simulation = simulation;
            return instance;
        }

        private void OnEnable()
        {
            PlayerColor.PlayerColorChanged += UpdateLocalPlayerColor;
            PlayerName.PlayerNameChanged += UpdateLocalPlayerName;

            SyncAvatar();

            UpdateLocalPlayerName();
            UpdateLocalPlayerColor();
        }

        private void OnDisable()
        {
            PlayerColor.PlayerColorChanged -= UpdateLocalPlayerColor;
            PlayerName.PlayerNameChanged -= UpdateLocalPlayerName;
        }

        private void UpdateLocalPlayerName()
        {
            simulation.Multiplayer.Avatars.LocalAvatar.Name = PlayerName.GetPlayerName();
        }

        private void UpdateLocalPlayerColor()
        {
            simulation.Multiplayer.Avatars.LocalAvatar.Color = PlayerColor.GetPlayerColor();
        }

        private void Update()
        {
            SyncAvatar();
        }

        private void SyncAvatar()
        {
            var worldToMultiplayer = UnitScaleTransformation
                                     .FromTransformRelativeToWorld(transform).inverse;

            if (simulation.Multiplayer.IsOpen)
            {
                var headTransform =
                    ScivanaPrototype.Input.HeadsetOrientation.Value;
                var leftHandTransform =
                    ScivanaPrototype.Input.LeftControllerOrientation.Value;
                var rightHandTransform =
                    ScivanaPrototype.Input.RightControllerOrientation.Value;

                simulation.Multiplayer.Avatars.LocalAvatar.SetTransformations(
                    worldToMultiplayer * headTransform,
                    worldToMultiplayer * leftHandTransform,
                    worldToMultiplayer * rightHandTransform);

                simulation.Multiplayer.Avatars.FlushLocalAvatar();
            }
        }
    }
}