/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;

namespace ScivanaIMD.Avatars
{
    public class PlayerName : MonoBehaviour
    {
        private static string NameKey = "scivana.player.name";

        [SerializeField]
        private TMP_Text text;

        public static string GetPlayerName()
        {
            if (PlayerPrefs.HasKey(NameKey))
            {
                var value = PlayerPrefs.GetString(NameKey);
                if (!string.IsNullOrEmpty(value))
                    return value;
            }

            return Convert(Environment.UserName);
        }

        private static Regex usernameTextRegex = new Regex("[a-zA-Z]+");

        private static string Convert(string username)
        {
            var matches = usernameTextRegex.Matches(username);
            if (matches.Count > 0)
            {
                var name = matches[0].Value;
                if (name.Length > 7)
                    name = name.Substring(0, 7);
                return name;
            }

            return "User";
        }

        public static void SetPlayerName(string name)
        {
            PlayerPrefs.SetString(NameKey, name);
            PlayerNameChanged?.Invoke();
        }

        public static event Action PlayerNameChanged;

        private void Awake()
        {
            PlayerNameChanged += UpdateUserText;
            UpdateUserText();
        }

        private void UpdateUserText()
        {
            if (text != null)
                text.text = GetPlayerName();
        }
    }
}