/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Linq;
using Scivana.State;
using UnityEditor;
using UnityEngine;

namespace ScivanaIMD.Editor
{
    public class ApplicationWindow : EditorWindow
    {
        [MenuItem("Window/Narupa/Application")]
        public static void ShowExample()
        {
            var wnd = GetWindow<ApplicationWindow>();
            wnd.titleContent = new GUIContent("Application");
        }

        private GUIStyle contextLabel;

        private GUIStyle ContextLabel => contextLabel ??= new GUIStyle(EditorStyles.boldLabel);

        private void OnGUI()
        {
            if (!Application.isPlaying)
                return;

            var stateManagement = ScivanaPrototype.App.StateManagement;

            if (stateManagement == null)
                return;

            var contexts = stateManagement.Contexts.ToArray();
            var states = stateManagement.States.ToArray();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space(100);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("");
            foreach (var context in contexts)
            {
                var rect = GUILayoutUtility.GetRect(new GUIContent(context.ToString()),
                                                    ContextLabel, GUILayout.Width(32));
                var pos = new Vector2(rect.xMax, rect.center.y);
                GUIUtility.RotateAroundPivot(45, pos);
                rect.xMin -= 100;
                rect.xMax -= 16;
                rect.y += 16;
                EditorGUI.LabelField(rect, new GUIContent(context.ToString()),
                                     ContextLabel);
                GUIUtility.RotateAroundPivot(-45, pos);
            }

            EditorGUILayout.EndHorizontal();

            foreach (var state in states)
            {
                using (new EditorGUI.DisabledScope(state.CurrentState != ContextStateState.Active))
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField($"[{GetStateUsage(state)}] {GetStateName(state)}");
                    foreach (var context in contexts)
                    {
                        var usage = GetUsage(state, context);
                        var txt = " ";
                        switch (usage)
                        {
                            case ContextUsage.Absolute:
                                txt = "A";
                                break;
                            case ContextUsage.Exclusive:
                                txt = "E";
                                break;
                            case ContextUsage.Inclusive:
                                txt = "I";
                                break;
                        }

                        EditorGUILayout.LabelField(txt, GUILayout.Width(32));
                    }

                    EditorGUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.EndVertical();
        }

        private static ContextUsage? GetUsage(IState state, Context context)
        {
            foreach (var (existingContext, existingUsage) in state.Contexts)
            {
                if (existingContext == context)
                    return existingUsage;
            }

            return null;
        }

        private static string GetStateUsage(IState state)
        {
            switch (state.CurrentState)
            {
                case ContextStateState.Active:
                    return "ACT";
                case ContextStateState.Suspended:
                    return "SUS";
                case ContextStateState.Uninitialized:
                    return "NEW";
                case ContextStateState.ScheduledForDeletion:
                    return "DEL";
            }

            return "";
        }

        private static string GetStateName(IState state)
        {
            return state.ToString();
        }
    }
}