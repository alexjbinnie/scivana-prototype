﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Linq;
using NUnit.Framework;
using ScivanaIMD.Simulations;
using ScivanaIMD.State;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class ModifySelectionTests : BaseSceneTests
    {
        public IEnumerator OpenModifySelection(bool clear = true)
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var quickMenu = GetSimulationQuickMenu();
            yield return PressButton(quickMenu.menuButton);

            var menu = GetSimulationMenu();
            yield return PressButton(menu.selectionButton);

            var selectionMenu = Get<SelectionMenu>();
            yield return PressButton(selectionMenu.addButton);

            selectionMenu.menuItems[0].Edit();
            yield return null;

            var editSelection = Get<EditSelection>();

            if (clear)
            {
                yield return PressButton(editSelection.clearButton);
                CollectionAssert.IsEmpty(editSelection.Selection
                                                      .Value
                                                      .ParticleIds);
            }

            yield return PressButton(editSelection.addInVrButton);

            Assert.IsNotNull(Get<ModifySelectionContext>());
            Assert.IsNotNull(Get<ModifySelectionTool>());
            Assert.IsNotNull(Get<AtomProximitySelector>());
            Assert.IsNotNull(Get<ControllerInteractHandler>());
        }

        [UnityTest]
        public IEnumerator CanOpenFromSimulationMenu()
        {
            yield return OpenModifySelection();
        }

        [UnityTest]
        public IEnumerator InitialHighlightNothing()
        {
            yield return OpenModifySelection();

            CollectionAssert.IsEmpty(Get<ModifySelectionTool>().HighlightedIndices.Value);
        }

        private IEnumerator PositionCursorAt(Vector3 worldPoint)
        {
            var cursor = Get<Cursors>();
            var offset = cursor.rightCursor.transform.InverseTransformPoint(cursor.RightPoint.position);
            yield return SetRightControllerOrientation(worldPoint - offset, Quaternion.identity);
        }

        [UnityTest]
        public IEnumerator PointCursorAtSpecificWorldPoint()
        {
            yield return OpenModifySelection();

            var point = new Vector3(0.2f, -1.2f, 0.6f);
            yield return PositionCursorAt(point);

            var cursor = Get<Cursors>();
            Assert.AreEqual(point, cursor.RightPoint.position);
        }

        [UnityTest]
        public IEnumerator AddSpecificAtom()
        {
            yield return OpenModifySelection();

            var index = 59;
            yield return HighlightAtom(index);

            var tool = Get<ModifySelectionTool>();
            CollectionAssert.AreEqual(new[] { index }, tool.HighlightedIndices.Value);

            yield return PullRightTrigger();

            var context = Get<ModifySelectionContext>();

            CollectionAssert.AreEqual(new[] { index }, context.SelectedIndices.Value);
        }

        [UnityTest]
        public IEnumerator RemoveSingleAtom()
        {
            yield return OpenModifySelection();

            var index = 59;
            var residueIndices = Enumerable.Range(57, 7).ToArray();
            yield return HighlightAtom(index);

            var tool = Get<ModifySelectionTool>();
            tool.ModificationMode = ModifySelectionTool.ModificationModes.Add;
            tool.GroupingMode = ModifySelectionTool.GroupingModes.Residue;

            yield return PullRightTrigger();

            tool.ModificationMode = ModifySelectionTool.ModificationModes.Remove;
            tool.GroupingMode = ModifySelectionTool.GroupingModes.Particle;

            yield return PullRightTrigger();

            var context = Get<ModifySelectionContext>();
            CollectionAssert.AreEqual(new[] { 57, 58, 60, 61, 62, 63 }, context.SelectedIndices.Value);
        }

        [UnityTest]
        public IEnumerator RemoveAtomFromFullSelection()
        {
            yield return OpenModifySelection(clear: false);

            var index = 59;
            yield return HighlightAtom(index);

            var tool = Get<ModifySelectionTool>();
            tool.ModificationMode = ModifySelectionTool.ModificationModes.Remove;
            tool.GroupingMode = ModifySelectionTool.GroupingModes.Particle;

            yield return PullRightTrigger();

            var context = Get<ModifySelectionContext>();
            Assert.AreEqual(306, context.SelectedIndices.Value.Length);

            var dialog = Get<ModifySelectionDialog>();
            yield return PressButton(dialog.acceptButton);

            var menu = Get<EditSelection>();
            Assert.IsNotNull(menu);
            Assert.AreEqual(306, menu.Selection.Value.ParticleIds.Count);
        }

        private IEnumerator HighlightAtom(int index)
        {
            Assert.IsNotNull(Get<Simulation>().CurrentFrame);
            var position = Get<Simulation>().CurrentFrame.ParticlePositions[index];
            var root = Get<SimulationVisualisationRoot>().RightHandSpace;
            var worldPosition = root.TransformPoint(position);

            yield return PositionCursorAt(worldPosition);
        }

        [UnityTest]
        public IEnumerator AddSpecificResidue()
        {
            yield return OpenModifySelection();

            var tool = Get<ModifySelectionTool>();
            tool.GroupingMode = ModifySelectionTool.GroupingModes.Residue;
            var residueIndices = Enumerable.Range(57, 7).ToArray();

            yield return HighlightAtom(59);

            CollectionAssert.AreEqual(residueIndices, tool.HighlightedIndices.Value);

            yield return PullRightTrigger();

            var context = Get<ModifySelectionContext>();

            CollectionAssert.AreEqual(residueIndices, context.SelectedIndices.Value);
        }

        [UnityTest]
        public IEnumerator AddSpecificChain()
        {
            yield return OpenModifySelection();

            var tool = Get<ModifySelectionTool>();
            tool.GroupingMode = ModifySelectionTool.GroupingModes.Chain;
            var chainIndices = Enumerable.Range(0, 93).ToArray();

            yield return HighlightAtom(59);

            CollectionAssert.AreEqual(chainIndices, tool.HighlightedIndices.Value);

            yield return PullRightTrigger();

            var context = Get<ModifySelectionContext>();

            CollectionAssert.AreEqual(chainIndices, context.SelectedIndices.Value);
        }

        public IEnumerator OpenQuickMenu()
        {
            yield return SetRightControllerOrientation();
            yield return MoveRightThumbstickDown();
        }

        public IEnumerator CloseQuickMenu()
        {
            yield return ResetRightThumbstick();
        }

        [UnityTest]
        public IEnumerator CanOpenAndCloseQuickMenu()
        {
            yield return OpenModifySelection();

            Assert.IsNotNull(Get<ModifySelectionDialog>());
            Assert.IsNotNull(Get<HandUiOpenMenuListener>());
            Assert.IsNull(Get<ModifySelectionQuickMenu>());

            yield return OpenQuickMenu();

            Assert.IsNull(Get<ModifySelectionDialog>());
            Assert.IsNotNull(Get<ModifySelectionQuickMenu>());

            yield return CloseQuickMenu();

            Assert.IsNotNull(Get<ModifySelectionDialog>());
            Assert.IsNull(Get<ModifySelectionQuickMenu>());
        }

        [UnityTest]
        public IEnumerator ToggleModificationModeFromQuickMenu()
        {
            yield return OpenModifySelection();

            yield return OpenQuickMenu();

            var tool = Get<ModifySelectionTool>();
            var quickMenu = Get<ModifySelectionQuickMenu>();

            Assert.AreEqual(ModifySelectionTool.ModificationModes.Add,
                            tool.ModificationMode);

            yield return PressButton(quickMenu.modificationTypeToggle);

            Assert.AreEqual(ModifySelectionTool.ModificationModes.Remove,
                            tool.ModificationMode);

            yield return PressButton(quickMenu.modificationTypeToggle);

            Assert.AreEqual(ModifySelectionTool.ModificationModes.Add,
                            tool.ModificationMode);
        }

        [UnityTest]
        public IEnumerator ToggleGroupingModeFromQuickMenu()
        {
            yield return OpenModifySelection();

            yield return OpenQuickMenu();

            var tool = Get<ModifySelectionTool>();
            var quickMenu = Get<ModifySelectionQuickMenu>();

            Assert.AreEqual(ModifySelectionTool.GroupingModes.Particle,
                            tool.GroupingMode);

            yield return PressButton(quickMenu.groupingTypeButton);

            Assert.AreEqual(ModifySelectionTool.GroupingModes.Residue,
                            tool.GroupingMode);

            yield return PressButton(quickMenu.groupingTypeButton);

            Assert.AreEqual(ModifySelectionTool.GroupingModes.Chain,
                            tool.GroupingMode);

            yield return PressButton(quickMenu.groupingTypeButton);

            Assert.AreEqual(ModifySelectionTool.GroupingModes.Particle,
                            tool.GroupingMode);
        }
    }
}