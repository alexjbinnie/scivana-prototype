﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using NUnit.Framework;
using ScivanaIMD.UserInterface;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class EditAnnotationTests : BaseSceneTests
    {
        [UnityTest]
        public IEnumerator CreateRulerAndOpenEditor()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var quickMenu = GetSimulationQuickMenu();
            yield return PressButton(quickMenu.menuButton);

            var menu = GetSimulationMenu();
            yield return PressButton(menu.annotationsButton);

            var annotationsMenu = Get<AnnotationMenu>();
            Assert.IsNotNull(annotationsMenu);
            CollectionAssert.IsEmpty(annotationsMenu.menuItems);

            yield return PressButton(annotationsMenu.addRulerButton);
            Assert.AreEqual(1, annotationsMenu.menuItems.ActiveInstanceCount);

            var menuItem = annotationsMenu.menuItems[0];
            yield return PressButton(menuItem.button);

            var editAnnotation = Get<EditAnnotation>();
            Assert.IsNotNull(editAnnotation);

            yield return PressButton(editAnnotation.editButton);
            Assert.IsNotNull(Get<ParticleSequenceDialog>());
        }
    }
}