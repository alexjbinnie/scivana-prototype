﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using NUnit.Framework;
using Scivana.App.XR;
using Scivana.Core.Testing;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests.ControllerTests
{
    public abstract class VrControllerTests<TController>
        where TController : XRController, new()
    {
        protected InputTestFixture input = new InputTestFixture();
        private TController leftController;
        private TController rightController;

        [UnitySetUp]
        public IEnumerator Setup()
        {
            input.Setup();

            InputSystem.RegisterBindingComposite<ButtonFrom2DAxis>();

            SceneManager.LoadScene("ScivanaIMD/Scenes/Scivana");
            yield return null;

            leftController = InputSystem.AddDevice<TController>();
            InputSystem.AddDeviceUsage(leftController, CommonUsages.LeftHand);

            rightController = InputSystem.AddDevice<TController>();
            InputSystem.AddDeviceUsage(rightController, CommonUsages.RightHand);
        }

        private IEnumerator TestAction(InputAction leftAction, InputAction rightAction, Action<TController> press)
        {
            var leftCallback = CallbackAssert.Callback<InputAction.CallbackContext>();
            leftAction.started += leftCallback;

            press(leftController);
            yield return null;

            CallbackAssert.WasCalled(leftCallback);

            var rightCallback = CallbackAssert.Callback<InputAction.CallbackContext>();
            rightAction.started += rightCallback;

            press(rightController);
            yield return null;

            CallbackAssert.WasCalled(rightCallback);
        }

        [UnityTest]
        public IEnumerator InteractWorks()
        {
            yield return TestAction(ScivanaPrototype.Input.Left.VR.Interact,
                                    ScivanaPrototype.Input.Right.VR.Interact,
                                    PressTrigger);
        }

        [UnityTest]
        public IEnumerator QuickMenuWorks()
        {
            yield return TestAction(ScivanaPrototype.Input.Left.VR.QuickMenu,
                                    ScivanaPrototype.Input.Right.VR.QuickMenu,
                                    OpenQuickMenu);
        }

        [UnityTest]
        public IEnumerator GrabWorks()
        {
            yield return TestAction(ScivanaPrototype.Input.Left.VR.Grab,
                                    ScivanaPrototype.Input.Right.VR.Grab,
                                    GrabSpace);
        }

        [TearDown]
        public void TearDown()
        {
            input.TearDown();
        }

        protected abstract void PressTrigger(TController controller);

        protected abstract void OpenQuickMenu(TController controller);

        protected abstract void GrabSpace(TController controller);
    }
}