﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Linq;
using Google.Protobuf.WellKnownTypes;
using NSubstitute;
using NUnit.Framework;
using Scivana.Core;
using Scivana.Core.Async;
using Scivana.Core.Collections;
using Scivana.Core.Serialization;
using Scivana.Core.Testing;
using Scivana.Network;
using Scivana.Network.Multiplayer;
using ScivanaIMD.Interaction;
using ScivanaIMD.Simulations;
using ScivanaIMD.State;
using ScivanaIMD.State.Modes;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.TestTools.Utils;

namespace ScivanaIMD.PlayTests
{
    [TestFixture]
    public class ConnectedToSimulationTests : BaseSceneTests
    {
        [Test]
        public void IsInitialSceneValid()
        {
            Assert.IsNotNull(GetSimulation());
            Assert.IsNotNull(Get<SimulationVisualisationRoot>());
            Assert.IsNotNull(Get<SimulationLayersVisualisation>());

            Assert.IsNotNull(prototype.StateManagement
                                      .GetState<InteractionMode>());
        }

        [UnityTest]
        public IEnumerator AreCursorsPositionedAtControllerPositions()
        {
            var leftPosition = new Vector3(2.1f, -0.4f, 1.3f);
            var leftRotation = Quaternion.LookRotation(new Vector3(0.2f, -0.4f, 1.1f));
            yield return SetLeftControllerOrientation(leftPosition, leftRotation);

            var rightPosition = new Vector3(0.2f, -1.3f, 0.5f);
            var rightRotation = Quaternion.LookRotation(new Vector3(-0.2f, 1.2f, 0.3f));
            yield return SetRightControllerOrientation(rightPosition, rightRotation);

            yield return null;

            Assert.IsNotNull(Get<Cursors>());
            var cursor = Get<Cursors>();

            Assert.IsNotNull(cursor.leftCursor);
            Assert.IsNotNull(cursor.rightCursor);

            Assert.AreEqual(leftPosition, cursor.leftCursor.transform.position);
            Assert.AreEqual(leftRotation, cursor.leftCursor.transform.rotation);

            Assert.AreEqual(rightPosition, cursor.rightCursor.transform.position);
            Assert.AreEqual(rightRotation, cursor.rightCursor.transform.rotation);
        }

        /// <summary>
        /// Test that pulling the grip button on one hand and moving it shifts the simulation box.
        /// </summary>
        [UnityTest]
        public IEnumerator CanMoveBoxWithSingleHand()
        {
            var leftPosition = new Vector3(2.1f, -0.4f, 1.3f);
            var leftRotation = Quaternion.LookRotation(new Vector3(0.2f, -0.4f, 1.1f));
            yield return SetLeftControllerOrientation(leftPosition, leftRotation);

            var manipulation = Get<BoxManipulation>();

            Assert.IsNull(manipulation.CurrentManipulation);

            yield return PressLeftControllerGrip();

            Assert.IsInstanceOf<RigidGrabManipulation>(manipulation.CurrentManipulation);

            leftPosition += Vector3.right;
            yield return SetLeftControllerOrientation(leftPosition, leftRotation);

            yield return ReleaseLeftControllerGrip();

            var simulation = GetSimulation();

            Assert.That(simulation.SimulationPose.Value.position,
                        Is.EqualTo(Vector3.right)
                          .Using(Vector3EqualityComparer.Instance));

            Assert.That(simulation.SimulationPose.Value.rotation,
                        Is.EqualTo(Quaternion.identity)
                          .Using(QuaternionEqualityComparer.Instance));

            Assert.That(simulation.SimulationPose.Value.scale,
                        Is.EqualTo(1f));
        }

        [UnityTest]
        public IEnumerator CanOpenAndCloseSimulationQuickMenu()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            Assert.IsNotNull(GetSimulationQuickMenu());

            yield return ResetLeftThumbstick();

            Assert.IsNull(GetSimulationQuickMenu());
        }

        [UnityTest]
        public IEnumerator DoesQuickMenuPlayButtonCallPlayCommand()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var callback = CallbackAssert.Callback<string, Struct>();
            commandsService.ReceivedCommand += callback;

            var menu = GetSimulationQuickMenu();

            yield return PressButton(menu.playButton);

            CallbackAssert.WasCalled(callback,
                                     Arg.Is("playback/play"),
                                     Arg.Any<Struct>());
        }

        [UnityTest]
        public IEnumerator DoesQuickMenuPauseButtonCallsPauseCommand()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var callback = CallbackAssert.Callback<string, Struct>();
            commandsService.ReceivedCommand += callback;

            var menu = GetSimulationQuickMenu();

            yield return PressButton(menu.pauseButton);

            CallbackAssert.WasCalled(callback,
                                     Arg.Is("playback/pause"),
                                     Arg.Any<Struct>());
        }

        [UnityTest]
        public IEnumerator DoesQuickMenuResetButtonCallsResetCommand()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var callback = CallbackAssert.Callback<string, Struct>();
            commandsService.ReceivedCommand += callback;

            var menu = GetSimulationQuickMenu();

            yield return PressButton(menu.resetButton);

            CallbackAssert.WasCalled(callback,
                                     Arg.Is("playback/reset"),
                                     Arg.Any<Struct>());
        }

        [UnityTest]
        public IEnumerator OpenSimulationMenuFromQuickMenu()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var menu = GetSimulationQuickMenu();
            yield return PressButton(menu.menuButton);

            Assert.IsNotNull(GetSimulationMenu());
        }

        [UnityTest]
        public IEnumerator IsAvatarSyncedWithNoComponentsWhenNoTrackedDevices()
        {
            yield return new WaitForSeconds(0.1f);

            var avatars = multiplayerService.Resources
                                            .WhereKey(key => key.StartsWithIgnoreCase("avatar."))
                                            .ToList();
            Assert.AreEqual(1, avatars.Count);

            var (key, avatarValue) = avatars[0];

            var localKey = GetSimulation().Multiplayer.AccessToken;

            Assert.AreEqual($"avatar.{localKey}", key);

            var avatar = Serialization.FromDataStructure<MultiplayerAvatar>(avatarValue.ToObject());

            Assert.IsNotNull(avatar);
            Assert.AreEqual(0, avatar.Components.Count);
        }

        [UnityTest]
        public IEnumerator IsAvatarSyncedWithThreeComponentsWhenTrackedDevices()
        {
            var leftPosition = new Vector3(0.2f, -1.4f, 0.6f);
            var leftRotation = Quaternion.LookRotation(new Vector3(-0.8f, -0.3f, 1.4f));
            yield return SetLeftControllerOrientation(leftPosition, leftRotation);

            var rightPosition = new Vector3(-4.3f, 0.2f, 0.1f);
            var rightRotation = Quaternion.LookRotation(new Vector3(2.3f, -3.4f, 1.1f));
            yield return SetRightControllerOrientation(rightPosition, rightRotation);

            var headPosition = new Vector3(-1.4f, 0.7f, 2.1f);
            var headRotation = Quaternion.LookRotation(new Vector3(2.3f, -3.4f, 1.1f));
            yield return SetHeadsetOrientation(headPosition, headRotation);

            yield return new WaitForSeconds(0.1f);

            var avatars = multiplayerService.Resources
                                            .WhereKey(k => k.StartsWithIgnoreCase("avatar."))
                                            .ToList();
            var (key, avatarValue) = avatars[0];

            var localKey = GetSimulation().Multiplayer.AccessToken;

            var avatar = Serialization.FromDataStructure<MultiplayerAvatar>(avatarValue.ToObject());

            Assert.AreEqual(3, avatar.Components.Count);

            var syncedLeft = avatar.Components
                                   .FirstOrDefault(c => c.Name == MultiplayerAvatar.LeftHandName);

            var syncedRight = avatar.Components
                                    .FirstOrDefault(c => c.Name == MultiplayerAvatar.RightHandName);

            var syncedHeadset = avatar.Components
                                      .FirstOrDefault(c => c.Name == MultiplayerAvatar.HeadsetName);

            Assert.IsNotNull(syncedLeft);
            Assert.IsNotNull(syncedRight);
            Assert.IsNotNull(syncedHeadset);

            Assert.That(syncedLeft.Position,
                        Is.EqualTo(leftPosition)
                          .Using(Vector3EqualityComparer.Instance));

            Assert.That(syncedLeft.Rotation,
                        Is.EqualTo(leftRotation)
                          .Using(QuaternionEqualityComparer.Instance));

            Assert.That(syncedRight.Position,
                        Is.EqualTo(rightPosition)
                          .Using(Vector3EqualityComparer.Instance));

            Assert.That(syncedRight.Rotation,
                        Is.EqualTo(rightRotation)
                          .Using(QuaternionEqualityComparer.Instance));

            Assert.That(syncedHeadset.Position,
                        Is.EqualTo(headPosition)
                          .Using(Vector3EqualityComparer.Instance));

            Assert.That(syncedHeadset.Rotation,
                        Is.EqualTo(headRotation)
                          .Using(QuaternionEqualityComparer.Instance));
        }

        [UnityTest]
        public IEnumerator GotoServerDisconnectedWhenServerIsDisconnect()
        {
            var callback = CallbackAssert.Callback<Exception>();
            GetSimulation().Multiplayer.Disconnected += callback;

            server.CloseAsync().AwaitInForegroundWithTimeout(1000);

            yield return new WaitForSeconds(1f);

            CallbackAssert.WasCalled(callback);

            Assert.IsNull(GetSimulation());

            Assert.IsNotNull(Get<ServerDisconnected>());
        }

        [UnityTest]
        public IEnumerator ClickingSimulationMenuDisconnectLeavesServer()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var quickMenu = GetSimulationQuickMenu();
            yield return PressButton(quickMenu.menuButton);

            var menu = GetSimulationMenu();

            yield return PressButton(menu.disconnectButton);

            Assert.IsNull(GetSimulation());
            Assert.IsNotNull(Get<MainMenu>());
        }
    }
}