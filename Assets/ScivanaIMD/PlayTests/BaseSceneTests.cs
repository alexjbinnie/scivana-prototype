﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Scivana.App.XR;
using Scivana.Core.Async;
using Scivana.Core.Serialization;
using Scivana.Core.Testing;
using Scivana.Network.Frame;
using Scivana.Network.Multiplayer;
using Scivana.Network.Tests;
using Scivana.Network.Tests.Commands;
using Scivana.Network.Tests.Multiplayer;
using Scivana.Network.Tests.Trajectory;
using Scivana.Trajectory;
using Scivana.Trajectory.Import.CIF;
using Scivana.Trajectory.Import.CIF.Components;
using Scivana.UI.Traits;
using ScivanaIMD.Avatars;
using ScivanaIMD.Selection.Data;
using ScivanaIMD.Simulations;
using ScivanaIMD.UserInterface;
using Unity.XR.Oculus.Input;
using Unity.XR.OpenVR;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Assert = UnityEngine.Assertions.Assert;
using Keyboard = ScivanaIMD.UserInterface.Keyboard;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace ScivanaIMD.PlayTests
{
    public class BaseSceneTests
    {
        protected ScivanaPrototype prototype;
        protected GrpcServer server;

        protected InputTestFixture input = new InputTestFixture();
        protected OpenVRHMD headset;
        protected OpenVROculusTouchController leftController;
        protected OpenVROculusTouchController rightController;
        protected CommandService commandsService;
        protected MultiplayerService multiplayerService;

        [UnitySetUp]
        public virtual IEnumerator SetUp()
        {
            input.Setup();

            InputSystem.RegisterBindingComposite<ButtonFrom2DAxis>();

            SceneManager.LoadScene("ScivanaIMD/Scenes/Scivana");
            yield return null;
            prototype = Object.FindObjectOfType<ScivanaPrototype>();

            Frame frame = null;
            using (var file = File.Open($"{Application.dataPath}/ScivanaIMD/PlayTests/1A7Z.cif", FileMode.Open))
            {
                using var reader = new StreamReader(file);
                frame = CifImport.Import(reader, ChemicalComponentDictionary.Instance);
            }

            Assert.IsNotNull(frame);

            commandsService = new CommandService();
            var trajectoryService = new QueueTrajectoryService(FrameConverter.ToFrameData(frame));
            multiplayerService = new MultiplayerService();
            multiplayerService.SetValueDirect("test", 0);

            server = new GrpcServer(commandsService,
                                    trajectoryService,
                                    multiplayerService);

            prototype.ConnectToSimulation("localhost", server.Port);

            leftController = InputSystem.AddDevice<OpenVROculusTouchController>();
            InputSystem.AddDeviceUsage(leftController, CommonUsages.LeftHand);

            rightController = InputSystem.AddDevice<OpenVROculusTouchController>();
            InputSystem.AddDeviceUsage(rightController, CommonUsages.RightHand);

            headset = InputSystem.AddDevice<OpenVRHMD>();

            yield return CoroutineAssert.WaitUntilNotNull(GetSimulation, 10f);

            Assert.IsNotNull(GetSimulation().CurrentFrame);
        }

        [TearDown]
        public void TearDown()
        {
            server.CloseAsync().AwaitInForegroundWithTimeout(1000);
            input.TearDown();
        }

        protected IEnumerator PullRightTrigger()
        {
            input.Set(rightController.trigger, 1f);
            input.Set(rightController.triggerButton, 1f);
            yield return null;
            input.Set(rightController.trigger, 0f);
            input.Set(rightController.triggerButton, 0f);
            yield return null;
        }

        protected IEnumerator MoveLeftThumbstickDown()
        {
            input.Set(leftController.joystick, new Vector2(0, -1f));
            yield return null;
        }

        protected IEnumerator ResetLeftThumbstick()
        {
            input.Set(leftController.joystick, new Vector2(0, 0f));
            yield return null;
        }

        protected IEnumerator MoveRightThumbstickDown()
        {
            input.Set(rightController.joystick, new Vector2(0, -1f));
            yield return null;
        }

        protected IEnumerator ResetRightThumbstick()
        {
            input.Set(rightController.joystick, new Vector2(0, 0f));
            yield return null;
        }

        protected TType Get<TType>(bool includeInactive = false)
            where TType : class
        {
            return prototype.StateManagement
                            .GetState<TType>(includeInactive)
                            ?.Instance;
        }

        protected SimulationQuickMenu GetSimulationQuickMenu()
        {
            return Get<SimulationQuickMenu>();
        }

        protected Simulation GetSimulation()
        {
            return prototype.CurrentSimulation;
        }

        protected IEnumerator PressButton(WidgetButton button)
        {
            button.Trigger();
            yield return null;
        }

        protected IEnumerator PressButton(WidgetToggle button)
        {
            button.SetToggle(!button.Toggle);
            yield return null;
        }

        protected IEnumerator SetLeftControllerOrientation()
        {
            yield return SetLeftControllerOrientation(Vector3.left, Quaternion.identity);
        }

        protected IEnumerator SetLeftControllerOrientation(Vector3 position, Quaternion rotation)
        {
            input.Set(leftController.isTracked, 1f);
            input.Set(leftController.devicePosition, position);
            input.Set(leftController.deviceRotation, rotation);
            yield return null;
        }

        protected IEnumerator SetRightControllerOrientation()
        {
            yield return SetRightControllerOrientation(Vector3.right, Quaternion.identity);
        }

        protected IEnumerator SetRightControllerOrientation(Vector3 position, Quaternion rotation)
        {
            input.Set(rightController.isTracked, 1f);
            input.Set(rightController.devicePosition, position);
            input.Set(rightController.deviceRotation, rotation);
            yield return null;
        }

        protected IEnumerator SetHeadsetOrientation(Vector3 position, Quaternion rotation)
        {
            input.Set(headset.isTracked, 1f);
            input.Set(headset.devicePosition, position);
            input.Set(headset.deviceRotation, rotation);
            yield return null;
        }

        protected IEnumerator PressLeftControllerGrip()
        {
            input.Set(leftController.gripButton, 1);
            yield return null;
        }

        protected IEnumerator ReleaseLeftControllerGrip()
        {
            input.Set(leftController.gripButton, 0);
            yield return null;
        }

        protected SimulationMenu GetSimulationMenu()
        {
            return Get<SimulationMenu>();
        }

        protected AvatarVisualisation GetAvatarVisualisation()
        {
            return Get<AvatarVisualisation>();
        }

        protected Keyboard GetKeyboard()
        {
            return Get<Keyboard>();
        }

        protected ColorPickerDialog GetColorPicker()
        {
            return Get<ColorPickerDialog>();
        }

        protected void AddSelectionToServer(string id, ParticleSelection selection)
        {
            multiplayerService.SetValueDirect($"selection.{id}", Serialization.ToDataStructure(selection));
        }

        protected void AddFakeAvatars(int n = 1)
        {
            for (var i = 0; i < n; i++)
            {
                var avatar = new MultiplayerAvatar
                {
                    Name = $"user{i}",
                    Color = Random.ColorHSV(),
                    Components = new List<MultiplayerAvatar.Component>
                    {
                        new MultiplayerAvatar.Component
                        {
                            Name = MultiplayerAvatar.LeftHandName,
                            Position = Random.insideUnitSphere,
                            Rotation = Random.rotationUniform,
                        },
                        new MultiplayerAvatar.Component
                        {
                            Name = MultiplayerAvatar.RightHandName,
                            Position = Random.insideUnitSphere,
                            Rotation = Random.rotationUniform,
                        },
                        new MultiplayerAvatar.Component
                        {
                            Name = MultiplayerAvatar.HeadsetName,
                            Position = Random.insideUnitSphere,
                            Rotation = Random.rotationUniform,
                        },
                    },
                };
                var guid = Guid.NewGuid().ToString();
                multiplayerService.SetValueDirect($"avatar.{guid}", Serialization.ToDataStructure(avatar));
            }
        }
    }
}