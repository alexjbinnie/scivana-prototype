﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class AvatarVisualisationTests : BaseSceneTests
    {
        [UnityTest]
        public IEnumerator NoAvatarsVisualisedWhenAlone()
        {
            yield return null;

            var avatarVisualisation = GetAvatarVisualisation();

            CollectionAssert.IsEmpty(avatarVisualisation.headsetObjects);
            CollectionAssert.IsEmpty(avatarVisualisation.controllerObjects);
        }

        [UnityTest]
        public IEnumerator CanVisualiseOtherAvatars()
        {
            AddFakeAvatars(4);

            yield return new WaitForSeconds(0.5f);

            var avatarVisualisation = GetAvatarVisualisation();

            Assert.AreEqual(4, avatarVisualisation.headsetObjects.ActiveInstanceCount);
            Assert.AreEqual(8, avatarVisualisation.controllerObjects.ActiveInstanceCount);
        }
    }
}