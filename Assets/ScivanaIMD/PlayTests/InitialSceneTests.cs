/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using NUnit.Framework;
using Scivana.State;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Assert = UnityEngine.Assertions.Assert;

namespace ScivanaIMD.PlayTests
{
    public class InitialSceneTests
    {
        private ScivanaPrototype prototype;

        [UnitySetUp]
        public IEnumerator SetUp()
        {
            SceneManager.LoadScene("ScivanaIMD/Scenes/Scivana");
            yield return null;
            prototype = Object.FindObjectOfType<ScivanaPrototype>();
        }

        [Test]
        public void HasPrototype()
        {
            Assert.IsNotNull(prototype);
        }

        [Test]
        public void HasNoSimulationState()
        {
            Assert.IsNull(prototype.CurrentSimulation);
        }

        [Test]
        public void IsShowingMainMenu()
        {
            Assert.IsTrue(prototype.StateManagement.GetFirstActiveState(Contexts.FullScreenUI).Instance is MainMenu);
        }
    }
}