﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Scivana.Core.Properties;
using Scivana.Core.Serialization;
using ScivanaIMD.Selection.Data;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class VisualisationTests : BaseSceneTests
    {
        [Test]
        public void InitiallyDefaultLayer()
        {
            var root = Get<SimulationLayersVisualisation>();
            Assert.AreEqual(1, root.layers.Count);
            CollectionAssert.AreEqual(new[] { 0 }, root.layers.Keys);
            Assert.IsNotNull(root.defaultVisualisation);
            var layer = root.layers[0];
            Assert.AreEqual(1, layer.currentMembers.Count);
            Assert.AreEqual(root.defaultVisualisation, layer.currentMembers[0].Visualisation);
        }

        [UnityTest]
        public IEnumerator AddNewVisualisation()
        {
            yield return AddVisualisationToServer("visualiser.test", new ParticleVisualiser
            {
                DisplayName = "Visuals",
                Visualiser = "cycles",
            });

            var root = Get<SimulationLayersVisualisation>();
            Assert.AreEqual(1, root.layers.Count);
            Assert.IsNull(root.defaultVisualisation);
            var layer = root.layers[0];
            Assert.AreEqual(1, layer.currentMembers.Count);
            Assert.AreEqual("Visuals", layer.currentMembers[0].name);
        }

        [UnityTest]
        public IEnumerator AddNewVisualisationWithMissingSelection()
        {
            yield return AddVisualisationToServer("visualiser.test", new ParticleVisualiser
            {
                SelectionKey = "selection.missing",
                DisplayName = "Visuals",
                Visualiser = "cycles",
            });

            var root = Get<SimulationLayersVisualisation>();
            Assert.AreEqual(1, root.layers.Count);
            var layer = root.layers[0];
            Assert.AreEqual(1, layer.currentMembers.Count);
            var vis = layer.currentMembers[0];
            Assert.AreEqual("Visuals", vis.name);
            Assert.IsTrue(vis.FilteredIndices.HasNullOrNoValue());
        }

        [UnityTest]
        public IEnumerator AddNewVisualisationThenAddSelection()
        {
            yield return AddVisualisationToServer("visualiser.test", new ParticleVisualiser
            {
                SelectionKey = "selection.delayed",
                DisplayName = "Visuals",
                Visualiser = "cycles",
            });

            yield return AddSelectionToServer("selection.delayed", new ParticleSelection
            {
                ParticleIds = new List<int>
                {
                    0, 1, 2, 3,
                },
            });

            var root = Get<SimulationLayersVisualisation>();
            Assert.AreEqual(1, root.layers.Count);
            var layer = root.layers[0];
            Assert.AreEqual(1, layer.currentMembers.Count);
            var vis = layer.currentMembers[0];
            Assert.AreEqual("Visuals", vis.name);
            Assert.AreEqual(new[] { 0, 1, 2, 3 }, vis.FilteredIndices.Value);
        }

        [UnityTest]
        public IEnumerator AddNewVisualisationAndSelection()
        {
            yield return AddSelectionToServer("selection.delayed", new ParticleSelection
            {
                ParticleIds = new List<int>
                {
                    0, 1, 2, 3,
                },
            });

            yield return AddVisualisationToServer("visualiser.test", new ParticleVisualiser
            {
                SelectionKey = "selection.delayed",
                DisplayName = "Visuals",
                Visualiser = "cycles",
            });

            var root = Get<SimulationLayersVisualisation>();
            Assert.AreEqual(1, root.layers.Count);
            var layer = root.layers[0];
            Assert.AreEqual(1, layer.currentMembers.Count);
            var vis = layer.currentMembers[0];
            Assert.AreEqual("Visuals", vis.name);
            Assert.AreEqual(new[] { 0, 1, 2, 3 }, vis.FilteredIndices.Value);
        }

        [UnityTest]
        public IEnumerator AddNewVisualisationThenRemoveSelection()
        {
            yield return AddSelectionToServer("selection.removed", new ParticleSelection
            {
                ParticleIds = new List<int>
                {
                    0, 1, 2, 3,
                },
            });

            yield return AddVisualisationToServer("visualiser.test", new ParticleVisualiser
            {
                SelectionKey = "selection.removed",
                DisplayName = "Visuals",
                Visualiser = "cycles",
            });

            yield return RemoveVisualisation("selection.removed");

            var root = Get<SimulationLayersVisualisation>();
            var layer = root.layers[0];
            var vis = layer.currentMembers[0];
            Assert.IsTrue(vis.FilteredIndices.HasNullOrNoValue());
        }

        [UnityTest]
        public IEnumerator AddTwoVisualisationsOnDifferentLayers()
        {
            yield return AddVisualisationToServer("visualiser.test", new ParticleVisualiser
            {
                Layer = 0,
                DisplayName = "Visuals",
                Visualiser = "cycles",
            });
            yield return AddVisualisationToServer("visualiser.test2", new ParticleVisualiser
            {
                Layer = 1,
                DisplayName = "Visuals2",
                Visualiser = "liquorice",
            });

            var root = Get<SimulationLayersVisualisation>();
            Assert.AreEqual(2, root.layers.Count);
            Assert.IsNull(root.defaultVisualisation);

            var layer1 = root.layers[0];
            Assert.AreEqual(1, layer1.currentMembers.Count);
            Assert.AreEqual("Visuals", layer1.currentMembers[0].name);

            var layer2 = root.layers[1];
            Assert.AreEqual(1, layer2.currentMembers.Count);
            Assert.AreEqual("Visuals2", layer2.currentMembers[0].name);
        }

        [UnityTest]
        public IEnumerator AddNewVisualisationAndRemoveAgainRevertsToDefault()
        {
            yield return AddVisualisationToServer("visualiser.test", new ParticleVisualiser
            {
                DisplayName = "Visuals",
                Visualiser = "cycles",
            });

            yield return RemoveVisualisation("visualiser.test");

            InitiallyDefaultLayer();
        }

        private IEnumerator RemoveVisualisation(string key)
        {
            multiplayerService.RemoveValueDirect(key);

            yield return new WaitForSeconds(0.5f);
        }

        private IEnumerator AddVisualisationToServer(string key, ParticleVisualiser value)
        {
            multiplayerService.SetValueDirect(key, Serialization.ToDataStructure(value));

            yield return new WaitForSeconds(0.5f);
        }

        private IEnumerator AddSelectionToServer(string key, ParticleSelection value)
        {
            multiplayerService.SetValueDirect(key, Serialization.ToDataStructure(value));

            yield return new WaitForSeconds(0.5f);
        }
    }
}