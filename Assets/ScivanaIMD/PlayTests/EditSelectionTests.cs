﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using ScivanaIMD.Selection.Data;
using ScivanaIMD.State;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class EditSelectionTests : BaseSceneTests
    {
        private EditSelection GetEditSelection()
        {
            return Get<EditSelection>();
        }

        private SelectionMenu GetSelectionMenu()
        {
            return Get<SelectionMenu>();
        }

        [UnityTest]
        public IEnumerator CanOpenFromSimulationMenu()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var quickMenu = GetSimulationQuickMenu();
            yield return PressButton(quickMenu.menuButton);

            var addedSelection = new ParticleSelection
            {
                Name = "Sele",
                ParticleIds = new List<int>
                {
                    0,
                    2,
                    3,
                }
            };
            AddSelectionToServer("abc", addedSelection);

            yield return new WaitForSeconds(0.5f);

            var menu = GetSimulationMenu();
            yield return PressButton(menu.selectionButton);

            var selectionMenu = GetSelectionMenu();
            selectionMenu.menuItems[0].Edit();
            yield return null;

            Assert.IsNotNull(GetEditSelection());
        }

        [UnityTest]
        public IEnumerator ClearSelection()
        {
            yield return CanOpenFromSimulationMenu();

            var editSelection = GetEditSelection();
            var selection = editSelection.Selection;

            CollectionAssert.AreEqual(new[]
                                      {
                                          0, 2, 3
                                      },
                                      selection.Value.ParticleIds);

            yield return PressButton(editSelection.clearButton);

            CollectionAssert.IsEmpty(selection.Value.ParticleIds);
        }

        [UnityTest]
        public IEnumerator AddAllToSelection()
        {
            yield return CanOpenFromSimulationMenu();

            var editSelection = GetEditSelection();
            var selection = editSelection.Selection;

            CollectionAssert.AreEqual(new[]
                                      {
                                          0, 2, 3
                                      },
                                      selection.Value.ParticleIds);

            yield return PressButton(editSelection.addAllButton);

            Assert.IsNull(selection.Value.ParticleIds);
        }

        [UnityTest]
        public IEnumerator BackButtonReturnsToSelectionMenu()
        {
            yield return CanOpenFromSimulationMenu();

            var editSelection = GetEditSelection();

            yield return PressButton(editSelection.backButton);

            Assert.IsNull(GetEditSelection());
            Assert.IsNotNull(GetSelectionMenu());
        }


        [UnityTest]
        public IEnumerator DeleteSelectionDestroysSelectionAndReturnsToSelectionMenu()
        {
            yield return CanOpenFromSimulationMenu();

            var editSelection = GetEditSelection();

            yield return PressButton(editSelection.deleteButton);

            Assert.IsNull(GetEditSelection());
            var selectionMenu = GetSelectionMenu();
            Assert.IsNotNull(selectionMenu);
            CollectionAssert.IsEmpty(selectionMenu.menuItems);
            CollectionAssert.IsEmpty(GetSimulation().Selections);
        }

        [UnityTest]
        public IEnumerator RenameSelection()
        {
            yield return CanOpenFromSimulationMenu();

            var editSelection = GetEditSelection();
            var selection = editSelection.Selection;

            Assert.AreNotEqual("newname", selection.Value.Name);

            yield return PressButton(editSelection.renameButton);

            var keyboard = GetKeyboard();
            Assert.IsNotNull(keyboard);
            keyboard.Text = "newname";
            keyboard.Accept();
            yield return null;

            Assert.IsNotNull(GetEditSelection());
            Assert.AreEqual("newname", selection.Value.Name);
        }

        [UnityTest]
        public IEnumerator StartAddInVrTool()
        {
            yield return CanOpenFromSimulationMenu();

            var editSelection = GetEditSelection();

            yield return PressButton(editSelection.addInVrButton);

            var tool = prototype.StateManagement.GetState<ModifySelectionTool>();
        }
    }
}