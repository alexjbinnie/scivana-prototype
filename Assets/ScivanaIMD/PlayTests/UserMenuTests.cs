﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using NUnit.Framework;
using ScivanaIMD.Avatars;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class UserMenuTests : BaseSceneTests
    {
        [UnityTest]
        public IEnumerator CanOpenFromSimulationMenu()
        {
            yield return SetLeftControllerOrientation();
            yield return SetRightControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var quickMenu = GetSimulationQuickMenu();
            yield return PressButton(quickMenu.menuButton);

            var menu = GetSimulationMenu();
            yield return PressButton(menu.userButton);

            Assert.IsNotNull(Get<UsersMenu>());
        }

        [UnityTest]
        public IEnumerator NoRightQuickMenu()
        {
            yield return CanOpenFromSimulationMenu();

            yield return MoveRightThumbstickDown();

            Assert.IsNotNull(Get<UsersMenu>());
        }

        [UnityTest]
        public IEnumerator BackButtonReturnsToSimulationMenu()
        {
            yield return CanOpenFromSimulationMenu();

            var menu = Get<UsersMenu>();
            yield return PressButton(menu.backButton);

            Assert.IsNull(Get<UsersMenu>(true));
            Assert.IsNotNull(Get<SimulationMenu>());
        }

        [UnityTest]
        public IEnumerator UsersNameAndColorDisplayedCorrectly()
        {
            PlayerName.SetPlayerName("username");
            PlayerColor.SetPlayerColor(Color.yellow);

            yield return CanOpenFromSimulationMenu();

            var menu = Get<UsersMenu>();

            Assert.AreEqual("username", menu.usernameLabel.Value);
            Assert.AreEqual(Color.yellow, menu.usernameColor.Value);

            PlayerName.SetPlayerName("username2");
            PlayerColor.SetPlayerColor(Color.red);
            yield return null;

            Assert.AreEqual("username2", menu.usernameLabel.Value);
            Assert.AreEqual(Color.red, menu.usernameColor.Value);
        }

        [UnityTest]
        public IEnumerator ShowsSingleAvatarWhenAlone()
        {
            PlayerName.SetPlayerName("username");
            PlayerColor.SetPlayerColor(Color.yellow);

            yield return CanOpenFromSimulationMenu();

            var menu = Get<UsersMenu>();

            Assert.AreEqual(1, menu.menuItems.ActiveInstanceCount);
            var item = menu.menuItems[0];
            Assert.AreEqual(Color.yellow, item.icon.Value);
            Assert.AreEqual("username", item.label.Value);
        }

        [UnityTest]
        public IEnumerator ShowOtherAvatarsInList()
        {
            PlayerName.SetPlayerName("username");
            PlayerColor.SetPlayerColor(Color.yellow);

            AddFakeAvatars(4);
            yield return null;

            yield return CanOpenFromSimulationMenu();

            var menu = Get<UsersMenu>();

            Assert.AreEqual(5, menu.menuItems.ActiveInstanceCount);
        }

        private UsersMenu GetUsersMenu()
        {
            return Get<UsersMenu>();
        }

        [UnityTest]
        public IEnumerator RenameUserUsingKeyboard()
        {
            PlayerName.SetPlayerName("username");
            PlayerColor.SetPlayerColor(Color.yellow);

            yield return null;

            yield return CanOpenFromSimulationMenu();

            var menu = GetUsersMenu();

            yield return PressButton(menu.renameUserButton);

            var keyboard = GetKeyboard();
            Assert.IsNotNull(keyboard);

            var k = prototype.StateManagement;

            keyboard.Text = "newuser";
            keyboard.Accept();
            yield return null;

            menu = GetUsersMenu();
            Assert.IsNotNull(menu);

            Assert.AreEqual("newuser", PlayerName.GetPlayerName());
            Assert.AreEqual("newuser", menu.usernameLabel.Value);
        }

        [UnityTest]
        public IEnumerator RecolorUser()
        {
            PlayerName.SetPlayerName("username");
            PlayerColor.SetPlayerColor(Color.yellow);

            yield return null;

            yield return CanOpenFromSimulationMenu();

            var menu = GetUsersMenu();

            yield return PressButton(menu.recolorUserButton);

            var colorPicker = GetColorPicker();
            Assert.IsNotNull(colorPicker);

            colorPicker.Color = Color.green;
            colorPicker.Accept();
            yield return null;

            menu = GetUsersMenu();
            Assert.IsNotNull(menu);

            Assert.AreEqual(Color.green, PlayerColor.GetPlayerColor());
            Assert.AreEqual(Color.green, menu.usernameColor.Value);
        }
    }
}