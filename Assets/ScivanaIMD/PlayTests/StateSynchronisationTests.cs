﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using NSubstitute;
using NUnit.Framework;
using Scivana.Core.Testing;
using UnityEngine;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class StateSynchronisationTests : BaseSceneTests
    {
        [UnityTest]
        public IEnumerator ValueSycnedFromServerToClient()
        {
            var callback = CallbackAssert.Callback<string, object>();
            GetSimulation().Multiplayer.RemoteSharedStateKeyUpdated += callback;

            multiplayerService.SetValueDirect("abc", "my_string");
            yield return new WaitForSeconds(0.5f);

            CallbackAssert.WasCalled(callback, Arg.Is("abc"), Arg.Is("my_string"));
        }

        [UnityTest]
        public IEnumerator FakeAvatarsAreSynchronised()
        {
            AddFakeAvatars(4);
            yield return new WaitForSeconds(0.5f);

            Assert.AreEqual(5, GetSimulation().Avatars.Count);
        }
    }
}