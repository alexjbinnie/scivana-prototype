﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using Narupa.Protocol.Trajectory;
using Scivana.Core.Async;
using Scivana.Network.Tests;
using Scivana.Network.Tests.Commands;
using Scivana.Network.Tests.Multiplayer;
using Scivana.Network.Tests.Trajectory;
using ScivanaIMD.Simulations;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class ConnectToServerTests
    {
        private ScivanaPrototype prototype;


        protected TType Get<TType>()
            where TType : class
        {
            return prototype.StateManagement
                            .GetState<TType>()
                            ?.Instance;
        }


        [UnitySetUp]
        public IEnumerator SetUp()
        {
            SceneManager.LoadScene("ScivanaIMD/Scenes/Scivana");
            yield return null;
            prototype = Object.FindObjectOfType<ScivanaPrototype>();
        }

        [UnityTest]
        public IEnumerator ConnectToMissingServer()
        {
            prototype.ConnectToSimulation("localhost", 43521);

            yield return new WaitForSeconds(3f);

            var connecting = Get<Connecting>();

            Assert.IsNotNull(connecting);

            Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                            connecting.commandsConnected);

            Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                            connecting.trajectoryConnected);

            Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                            connecting.multiplayerConnected);
        }

        [UnityTest]
        public IEnumerator ConnectToServerWithOnlyCommandService()
        {
            var commandService = new CommandService();

            var server = new GrpcServer(commandService);

            try
            {
                prototype.ConnectToSimulation("localhost", server.Port);

                yield return new WaitForSeconds(1f);

                var connecting = Get<Connecting>();

                Assert.IsNotNull(connecting);

                Assert.AreEqual(Connecting.ConnectionState.Connected,
                                connecting.commandsConnected);

                Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                                connecting.trajectoryConnected);

                Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                                connecting.multiplayerConnected);
            }
            finally
            {
                server.CloseAsync().AwaitInForegroundWithTimeout(1000);
            }
        }

        [UnityTest]
        public IEnumerator ConnectToServerWithOnlyTrajectoryService()
        {
            var trajectoryService = new QueueTrajectoryService(new FrameData());

            var server = new GrpcServer(trajectoryService);

            try
            {
                prototype.ConnectToSimulation("localhost", server.Port);

                yield return new WaitForSeconds(1f);

                var connecting = Get<Connecting>();

                Assert.IsNotNull(connecting);

                Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                                connecting.commandsConnected);

                Assert.AreEqual(Connecting.ConnectionState.Connected,
                                connecting.trajectoryConnected);

                Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                                connecting.multiplayerConnected);
            }
            finally
            {
                server.CloseAsync().AwaitInForegroundWithTimeout(1000);
            }
        }

        [UnityTest]
        public IEnumerator ConnectToServerWithOnlyMultiplayerService()
        {
            var multiplayerService = new MultiplayerService();
            multiplayerService.SetValueDirect("test", 0);

            var server = new GrpcServer(multiplayerService);

            try
            {
                prototype.ConnectToSimulation("localhost", server.Port);

                yield return new WaitForSeconds(1f);

                var connecting = Get<Connecting>();

                Assert.IsNotNull(connecting);

                Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                                connecting.commandsConnected);

                Assert.AreEqual(Connecting.ConnectionState.FailedToConnect,
                                connecting.trajectoryConnected);

                Assert.AreEqual(Connecting.ConnectionState.Connected,
                                connecting.multiplayerConnected);
            }
            finally
            {
                server.CloseAsync().AwaitInForegroundWithTimeout(1000);
            }
        }

        [UnityTest]
        public IEnumerator ConnectToCompleteServer()
        {
            var commandsService = new CommandService();
            var trajectoryService = new QueueTrajectoryService(new FrameData());
            var multiplayerService = new MultiplayerService();
            multiplayerService.SetValueDirect("test", 0);

            var server = new GrpcServer(commandsService,
                                        trajectoryService,
                                        multiplayerService);

            try
            {
                prototype.ConnectToSimulation("localhost", server.Port);

                yield return new WaitForSeconds(2f);

                Assert.IsNotNull(prototype.StateManagement.GetState<Simulation>());
            }
            finally
            {
                server.CloseAsync().AwaitInForegroundWithTimeout(1000);
            }
        }
    }
}