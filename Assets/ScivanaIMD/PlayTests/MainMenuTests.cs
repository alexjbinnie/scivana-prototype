﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class MainMenuTests
    {
        private ScivanaPrototype prototype;
        private MainMenu mainMenu;


        protected TType Get<TType>()
            where TType : class
        {
            return prototype.StateManagement
                            .GetState<TType>()
                            ?.Instance;
        }


        [UnitySetUp]
        public IEnumerator SetUp()
        {
            SceneManager.LoadScene("ScivanaIMD/Scenes/Scivana");
            yield return null;
            prototype = Object.FindObjectOfType<ScivanaPrototype>();
            mainMenu = Get<MainMenu>();
        }

        [UnityTest]
        public IEnumerator AutoconnectButtonGoesToAutoconnecting()
        {
            mainMenu.autoconnectButton.Trigger();

            yield return null;

            Assert.IsNull(mainMenu);
            Assert.IsNotNull(Get<Autoconnecting>());
        }

        [UnityTest]
        public IEnumerator CancelingAutoconnectGoesToMainMenu()
        {
            mainMenu.autoconnectButton.Trigger();

            yield return null;

            var autoconnect = Get<Autoconnecting>();

            autoconnect.cancelButton.Trigger();

            yield return null;

            Assert.IsNull(autoconnect);
            Assert.IsNotNull(Get<MainMenu>());
        }
    }
}