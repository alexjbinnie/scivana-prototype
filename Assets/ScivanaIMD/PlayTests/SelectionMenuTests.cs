﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ScivanaIMD.Selection.Data;
using ScivanaIMD.State.Modes;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.TestTools;

namespace ScivanaIMD.PlayTests
{
    public class SelectionMenuTests : BaseSceneTests
    {
        private SelectionMenu GetSelectionMenu()
        {
            return Get<SelectionMenu>();
        }

        [UnityTest]
        public IEnumerator CanOpenFromSimulationMenu()
        {
            yield return SetLeftControllerOrientation();

            yield return MoveLeftThumbstickDown();

            var quickMenu = GetSimulationQuickMenu();
            yield return PressButton(quickMenu.menuButton);

            var menu = GetSimulationMenu();
            yield return PressButton(menu.selectionButton);

            Assert.IsNotNull(Get<SelectionMenu>());
        }

        [UnityTest]
        public IEnumerator InSelectionMode()
        {
            yield return CanOpenFromSimulationMenu();

            Assert.IsNotNull(Get<SelectionMode>());
            Assert.IsNull(Get<InteractionMode>());
        }

        [UnityTest]
        public IEnumerator NoSelectionsInitially()
        {
            yield return CanOpenFromSimulationMenu();

            var menu = GetSelectionMenu();

            CollectionAssert.IsEmpty(GetSimulation().Selections);
            CollectionAssert.IsEmpty(menu.menuItems);
        }

        [UnityTest]
        public IEnumerator AddSelectionButton()
        {
            yield return CanOpenFromSimulationMenu();

            var menu = GetSelectionMenu();

            yield return PressButton(menu.addButton);

            Assert.AreEqual(1, GetSimulation().Selections.Count);
            Assert.AreEqual(1, menu.menuItems.ActiveInstanceCount);

            var selection = GetSimulation().Selections.First().Value;
            Assert.IsNull(selection.ParticleIds);
        }

        [UnityTest]
        public IEnumerator RemoteSelectionAddedBeforeMenuOpenedAppearsInList()
        {
            var addedSelection = new ParticleSelection
            {
                Name = "Sele",
                ParticleIds = new List<int>
                {
                    0,
                    2,
                    3,
                },
            };
            AddSelectionToServer("abc", addedSelection);
            yield return CanOpenFromSimulationMenu();

            var menu = GetSelectionMenu();

            Assert.AreEqual(1, GetSimulation().Selections.Count);
            Assert.AreEqual(1, menu.menuItems.ActiveInstanceCount);

            var selection = GetSimulation().Selections.First().Value;
            Assert.AreEqual(addedSelection.ParticleIds, selection.ParticleIds);
            Assert.AreEqual(addedSelection.Name, selection.Name);
        }

        [UnityTest]
        public IEnumerator RemoteSelectionAddedAfterOpenedAppearsInList()
        {
            yield return CanOpenFromSimulationMenu();

            var menu = GetSelectionMenu();

            yield return null;

            Assert.AreEqual(0, GetSimulation().Selections.Count);
            Assert.AreEqual(0, menu.menuItems.ActiveInstanceCount);

            var addedSelection = new ParticleSelection
            {
                Name = "Sele",
                ParticleIds = new List<int>
                {
                    0,
                    2,
                    3,
                },
            };
            AddSelectionToServer("abc", addedSelection);

            yield return new WaitForSeconds(0.5f);

            Assert.AreEqual(1, GetSimulation().Selections.Count);
            Assert.AreEqual(1, menu.menuItems.ActiveInstanceCount);

            var selection = GetSimulation().Selections.First().Value;
            Assert.AreEqual(addedSelection.ParticleIds, selection.ParticleIds);
            Assert.AreEqual(addedSelection.Name, selection.Name);
        }
    }
}