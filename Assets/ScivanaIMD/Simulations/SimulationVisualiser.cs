/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Visualisation;
using UnityEngine;

namespace ScivanaIMD.Simulations
{
    /// <summary>
    /// Object to create to simply visualise a simulation using a specific visualiser.
    /// </summary>
    public class SimulationVisualiser : MonoBehaviour
    {
        public static SimulationVisualiser InstantiateAsInactive(object data,
                                                                 SimulationVisualisationRoot root)
        {
            var visualiser = VisualiserFactory.ConstructVisualiser(data);
            visualiser.gameObject.SetActive(false);
            visualiser.transform.SetParent(root.RightHandSpace);
            visualiser.transform.SetToLocalIdentity();
            var instance = visualiser.gameObject.AddComponent<SimulationVisualiser>();
            instance.visualiser = visualiser;
            instance.root = root;
            return instance;
        }

        private Visualiser visualiser;
        private SimulationVisualisationRoot root;

        private void OnEnable()
        {
            visualiser.ParentAdaptor.Value = root.FrameAdaptor;
        }

        private void OnDisable()
        {
            visualiser.ParentAdaptor.Value = null;
        }

        public SimulationVisualiser SetParticleFilter(IReadOnlyProperty<int[]> property)
        {
            visualiser.ParticleFilter.LinkedProperty = property;
            return this;
        }
    }
}