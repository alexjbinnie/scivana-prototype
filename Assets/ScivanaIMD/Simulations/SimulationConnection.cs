﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Async;
using Scivana.Network;
using Scivana.Network.Multiplayer;
using Scivana.Network.Trajectory;

namespace ScivanaIMD.Simulations
{
    /// <summary>
    /// Represents a connection to a running Narupa simulation.
    /// </summary>
    public class SimulationConnection : IDisposable
    {
        public TrajectorySession Trajectory { get; } = new TrajectorySession();

        public MultiplayerSession Multiplayer { get; } = new MultiplayerSession();

        private GrpcConnection connection;

        public string Address => connection.Address;

        public int Port => connection.Port;

        public SimulationConnection(GrpcConnection connection)
        {
            this.connection = connection;
            Trajectory.OpenClient(connection);
            Multiplayer.OpenClient(connection);
        }

        public void Dispose()
        {
            Trajectory?.Dispose();
            Multiplayer?.Dispose();
            connection?.CloseAsync().AwaitInBackground();
        }
    }
}