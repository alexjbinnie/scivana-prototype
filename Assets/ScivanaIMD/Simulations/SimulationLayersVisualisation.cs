﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Scivana.Core;
using ScivanaIMD.Selection;
using UnityEngine;

namespace ScivanaIMD.Simulations
{
    public class SimulationLayersVisualisation : MonoBehaviour
    {
        public static SimulationLayersVisualisation InstantiateAsInactive(
            SimulationVisualisationRoot visualisationRoot)
        {
            var instance = Instantiation.InstantiateInactive<SimulationLayersVisualisation>();
            instance.visualisationRoot = visualisationRoot;
            instance.SetParent(visualisationRoot.RightHandSpace);
            instance.transform.SetToLocalIdentity();
            return instance;
        }

        /// <summary>
        /// The <see cref="VisualisationLayer" />s that make up this scene.
        /// </summary>
        internal readonly Dictionary<int, VisualisationLayer> layers =
            new Dictionary<int, VisualisationLayer>();

        private Dictionary<string, ParticleVisualisation> currentVisualisations =
            new Dictionary<string, ParticleVisualisation>();

        private SimulationVisualisationRoot visualisationRoot;
        internal DefaultVisualisation defaultVisualisation;

        private Simulation Simulation => visualisationRoot.Simulation;

        protected void Start()
        {
            Simulation.Visualisations.ItemCreated += OnMultiplayerVisualisationCreated;
            Simulation.Visualisations.ItemUpdated += OnMultiplayerVisualisationUpdated;
            Simulation.Visualisations.ItemRemoved += OnMultiplayerVisualisationRemoved;

            foreach (var visualisation in Simulation.Visualisations.Keys)
                OnMultiplayerVisualisationCreated(visualisation);

            if (Simulation.Visualisations.Count == 0)
                CreateDefaultVisualiser();
        }

        /// <summary>
        /// Get or create the visualisation layer with the given index
        /// </summary>
        private VisualisationLayer GetOrCreateLayer(int ordinal)
        {
            if (layers.ContainsKey(ordinal))
                return layers[ordinal];
            var layer = VisualisationLayer.InstantiateAsInactive(visualisationRoot,
                                                                 ordinal,
                                                                 transform);
            layer.SetActive(true);
            layer.gameObject.name = $"Layer {ordinal}";
            layer.Removed += () => DestroyLayer(layer);
            layers[ordinal] = layer;
            return layer;
        }

        /// <summary>
        /// Destroy the given visualisation layer
        /// </summary>
        private void DestroyLayer(VisualisationLayer layer)
        {
            layers.Remove(layer.Layer);
            Destroy(layer.gameObject);
        }


        private void CreateDefaultVisualiser()
        {
            defaultVisualisation = new DefaultVisualisation();
            var layer = GetOrCreateLayer(defaultVisualisation.Layer);
            layer.AddVisualisation(defaultVisualisation);
        }

        private void RemoveDefaultVisualiser()
        {
            defaultVisualisation.Destroy();
            defaultVisualisation = null;
        }

        private void OnMultiplayerVisualisationCreated(string key)
        {
            if (defaultVisualisation != null)
                RemoveDefaultVisualiser();

            var value = Simulation.Visualisations[key];
            var vis = new ParticleVisualisation(value);
            if (value.SelectionKey != null)
                vis.LinkedSelection = Simulation.Selections.GetResource(value.SelectionKey);
            currentVisualisations[key] = vis;
            var layer = GetOrCreateLayer(vis.Layer);
            layer.AddVisualisation(vis);
        }

        private void OnMultiplayerVisualisationUpdated(string key)
        {
            OnMultiplayerVisualisationRemoved(key);
            OnMultiplayerVisualisationCreated(key);
        }

        private void OnMultiplayerVisualisationRemoved(string key)
        {
            currentVisualisations[key].Delete();
            currentVisualisations.Remove(key);

            if (Simulation.Visualisations.Count == 0)
                CreateDefaultVisualiser();
        }
    }
}