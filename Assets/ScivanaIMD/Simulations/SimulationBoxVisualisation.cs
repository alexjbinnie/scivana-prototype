/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using Scivana.Visualisation;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.Simulations
{
    /// <summary>
    /// Updates a <see cref="BoxVisualiser" /> with the simulation box of a
    /// <see cref="Frame" />.
    /// </summary>
    public class SimulationBoxVisualisation : MonoBehaviour
    {
        public SimulationBoxVisualisation InstantiateAsInactive(
            Simulation simulation,
            Transform rightHandedSpace)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            instance.transform.parent = rightHandedSpace;
            return instance;
        }

        /// <summary>
        /// Source of the <see cref="Frame" />.
        /// </summary>
        private Simulation simulation;

        /// <summary>
        /// The <see cref="BoxVisualiser" /> that will render the box.
        /// </summary>
        [SerializeField]
        private BoxVisualiser boxVisualiser;

        private void Awake()
        {
            Assert.IsNotNull(boxVisualiser);
            Assert.IsNotNull(simulation);

            boxVisualiser.enabled = false;
        }

        private void OnEnable()
        {
            simulation.Trajectory.FrameChanged += OnFrameChanged;
            OnFrameChanged(simulation.Trajectory.CurrentFrame, FrameChanges.All);
        }

        private void OnDisable()
        {
            simulation.Trajectory.FrameChanged -= OnFrameChanged;
        }

        /// <summary>
        /// Callback for when the frame is updated.
        /// </summary>
        private void OnFrameChanged(Frame frame, FrameChanges changes)
        {
            if (changes.HasChanged(FrameFields.BoxTransformation))
            {
                var box = frame?.Get(FrameFields.BoxTransformation);
                if (box == null)
                {
                    boxVisualiser.enabled = false;
                }
                else
                {
                    boxVisualiser.enabled = true;
                    boxVisualiser.SetBox(box.Value);
                }
            }
        }

        public void SetColor(Color? color)
        {
            boxVisualiser.SetColor(color);
        }
    }
}