﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core.Math;
using Scivana.Network.Multiplayer;
using Scivana.Network.Trajectory;
using Scivana.State;
using Scivana.Trajectory;
using ScivanaIMD.Annotations;
using ScivanaIMD.Interaction;
using ScivanaIMD.Selection.Data;

namespace ScivanaIMD.Simulations
{
    /// <summary>
    /// Represents a molecular simulation, backed by a connection to a remote server. This should
    /// be created after the connection is established and it is ensured that the necessary
    /// streams have been opened. This class provides shorthand access for various synced
    /// properties of the system, as well as access to the <see cref="CurrentFrame" />.
    /// </summary>
    public class Simulation : IStateEnteredHandler, IStateExitedHandler
    {
        /// <summary>
        /// Create a simulation that is based upon <paramref name="connection" />
        /// </summary>
        public Simulation(SimulationConnection connection)
        {
            Connection = connection;
        }


        private void OnDisconnected(Exception obj)
        {
            ScivanaPrototype.App.GotoServerDisconnected(Connection.Address, Connection.Port);
        }


        public void OnStateExited()
        {
            Trajectory.Disconnected -= OnDisconnected;
            Multiplayer.Disconnected -= OnDisconnected;
            Connection.Dispose();
        }

        public void OnStateEntered()
        {
            Interactions = Multiplayer.GetSharedCollection<ParticleInteraction>("interaction.");
            Selections = Multiplayer.GetSharedCollection<ParticleSelection>("selection.");
            Annotations = Multiplayer.GetSharedCollection<Annotation>("annotation.");
            Visualisations = Multiplayer.GetSharedCollection<ParticleVisualiser>("visualiser.");
            SimulationPose = Multiplayer.GetSharedResource<UniformScaleTransformation>("scene");

            Trajectory.Disconnected += OnDisconnected;
            Multiplayer.Disconnected += OnDisconnected;
        }


        private SimulationConnection Connection { get; }

        public MultiplayerSession Multiplayer => Connection.Multiplayer;

        public TrajectorySession Trajectory => Connection.Trajectory;

        public MultiplayerCollection<ParticleInteraction> Interactions { get; private set; }

        public MultiplayerCollection<ParticleSelection> Selections { get; private set; }

        public MultiplayerCollection<Annotation> Annotations { get; private set; }

        public MultiplayerCollection<MultiplayerAvatar> Avatars => Multiplayer.Avatars;


        public MultiplayerCollection<ParticleVisualiser> Visualisations { get; private set; }

        public MultiplayerResource<UniformScaleTransformation> SimulationPose { get; private set; }

        /// <summary>
        /// The current frame of the simulation.
        /// </summary>
        public Frame CurrentFrame => Trajectory.CurrentFrame;
    }
}