﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.Core.Math;
using Scivana.Network.Multiplayer;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using Scivana.Visualisation.Node.Adaptor;
using ScivanaIMD.Annotations;
using ScivanaIMD.Interaction;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.Simulations
{
    /// <summary>
    /// Parent object responsible for visualising the a simulation within the scene. It handles the
    /// creation of objects which manage the visualisation of the system, multiplayer box,
    /// interactions, annotations and multiplayer avatars.
    /// </summary>
    public sealed class SimulationVisualisationRoot : MonoBehaviour
    {
        [SerializeField]
        private SimulationBoxVisualisation boxVisualisationPrefab;

        [SerializeField]
        private GameObject axesPrefab;

        [SerializeField]
        private InteractionsVisualisation interactionRendererPrefab;

        [SerializeField]
        private AnnotationsVisualisation annotationVisualisationPrefab;

        [SerializeField]
        private Transform rightHandedSpace;

        public Transform RightHandSpace => rightHandedSpace;

        private Simulation simulation;

        public Simulation Simulation => simulation;

        /// <inheritdoc cref="FrameAdaptor" />
        /// <remarks>
        /// This is automatically generated on <see cref="Awake()" />.
        /// </remarks>
        private FrameAdaptorNode frameAdaptor;

        /// <summary>
        /// The <see cref="FrameAdaptor" /> that exposes all the data present in the frame
        /// in a way that is compatible with the visualisation system.
        /// </summary>
        public FrameAdaptorNode FrameAdaptor => frameAdaptor;

        private void Awake()
        {
            Assert.IsNotNull(boxVisualisationPrefab);
            Assert.IsNotNull(axesPrefab);
            Assert.IsNotNull(interactionRendererPrefab);
            Assert.IsNotNull(annotationVisualisationPrefab);
            frameAdaptor = new FrameAdaptorNode();
        }

        private void OnEnable()
        {
            simulation.Trajectory.FrameChanged += frameAdaptor.UpdateFrame;
            frameAdaptor.UpdateFrame(simulation.CurrentFrame, FrameChanges.All);
        }

        private void OnDisable()
        {
            simulation.Trajectory.FrameChanged -= frameAdaptor.UpdateFrame;
        }


        private MultiplayerResource<SceneVisuals.SceneBox> sceneBox;

        private MultiplayerResource<SceneVisuals.SceneAxes> sceneAxes;
        private SimulationBoxVisualisation box;
        private GameObject axes;


        public SimulationVisualisationRoot InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();

            instance.simulation = simulation;

            instance.box = boxVisualisationPrefab
                           .InstantiateAsInactive(simulation, instance.rightHandedSpace)
                           .SetToLocalIdentity()
                           .SetActive(true);

            instance.axes = axesPrefab
                            .InstantiateInactive(instance.rightHandedSpace)
                            .SetToLocalIdentity();
            instance.axes.SetActive(true);

            interactionRendererPrefab
                .InstantiateAsInactive(simulation, instance.rightHandedSpace)
                .SetToLocalIdentity()
                .SetActive(true);

            annotationVisualisationPrefab
                .InstantiateAsInactive(simulation, instance.rightHandedSpace)
                .SetToLocalIdentity()
                .SetActive(true);

            instance.sceneBox = simulation.Multiplayer.GetSharedResource<SceneVisuals.SceneBox>(SceneVisuals.boxKey);
            instance.sceneAxes = simulation.Multiplayer.GetSharedResource<SceneVisuals.SceneAxes>(SceneVisuals.axesKey);

            instance.Setup();

            return instance;
        }

        private void Setup()
        {
            UpdateSimulationBoxPose();
            simulation.SimulationPose.ValueChanged += UpdateSimulationBoxPose;

            UpdateBoxVisualisation();
            sceneBox.ValueChanged += UpdateBoxVisualisation;

            UpdateAxesVisualisation();
            sceneAxes.ValueChanged += UpdateAxesVisualisation;
        }

        private void UpdateAxesVisualisation()
        {
            var showAxes = sceneAxes.HasValue && !sceneAxes.Value.Hide;
            axes.SetActive(showAxes);
        }

        private void UpdateBoxVisualisation()
        {
            var showBox = !sceneBox.HasValue || !sceneBox.Value.Hide;
            var boxColor = sceneBox.HasValue ? sceneBox.Value.Color : null;
            box.SetActive(showBox);
            if (showBox)
                box.SetColor(boxColor);
        }

        private void Update()
        {
            frameAdaptor.Refresh();
        }

        private void UpdateSimulationBoxPose()
        {
            if (simulation.SimulationPose.HasValue)
                simulation.SimulationPose.Value.CopyToTransformRelativeToParent(transform);
        }

        /// <summary>
        /// The number of particles in the current frame, or 0 if no frame is present.
        /// </summary>
        public int ParticleCount => Simulation.CurrentFrame?.Get(FrameFields.ParticleCount) ?? 0;
    }
}