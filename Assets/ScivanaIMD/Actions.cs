// GENERATED AUTOMATICALLY FROM 'Assets/ScivanaIMD/Actions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace ScivanaIMD
{
    public class @Actions : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @Actions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""Actions"",
    ""maps"": [
        {
            ""name"": ""VR"",
            ""id"": ""63200020-b109-4e10-aae7-31137352e2ee"",
            ""actions"": [
                {
                    ""name"": ""Position"",
                    ""type"": ""Value"",
                    ""id"": ""b6d885de-6bda-4350-819e-806f6d9404c1"",
                    ""expectedControlType"": ""Vector3"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Rotation"",
                    ""type"": ""Value"",
                    ""id"": ""624ee1af-eb34-4e32-b284-eef14eb8fa47"",
                    ""expectedControlType"": ""Quaternion"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Grab"",
                    ""type"": ""Button"",
                    ""id"": ""36f8681f-e1aa-4e50-aaa6-3e926391b91b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""2949d87e-61c4-421c-9769-6ddaf83dc132"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Quick Menu"",
                    ""type"": ""Button"",
                    ""id"": ""0ef7db23-4dc7-40e3-a400-99769381cd4b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4076a447-6b1b-45b1-b23d-26acc1d86f29"",
                    ""path"": ""<XRController>/devicePosition"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Position"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""54ee6fa2-1e4a-44ff-bcd1-c73b78372ea1"",
                    ""path"": ""<XRController>/deviceRotation"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Button From 2D Axis"",
                    ""id"": ""1b92550d-2703-4ec9-be89-a615d8ff5d82"",
                    ""path"": ""ButtonFrom2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""axis"",
                    ""id"": ""b04b01f5-194e-437f-bd7b-6154ceb41d68"",
                    ""path"": ""<OpenVRControllerIndex>/joystick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Button From 2D Axis"",
                    ""id"": ""5f0d3935-93a4-4ab2-aa7b-a3364e30b701"",
                    ""path"": ""ButtonFrom2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""axis"",
                    ""id"": ""a9277245-87af-4272-b6eb-403268bd977c"",
                    ""path"": ""<OpenVROculusTouchController>/joystick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Button From 2D Axis"",
                    ""id"": ""95fb83eb-cf22-4068-864e-918bf07ab838"",
                    ""path"": ""ButtonFrom2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""axis"",
                    ""id"": ""ec2bc31c-4c4a-45ee-b9ac-7078a75b8830"",
                    ""path"": ""<ViveWand>/touchpad"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Button From 2D Axis"",
                    ""id"": ""94fdd8f2-6375-40a9-9b0b-27e832e31433"",
                    ""path"": ""ButtonFrom2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""axis"",
                    ""id"": ""04554da8-cab3-4fad-8fb9-99dc44893ab7"",
                    ""path"": ""<OpenVRViveCosmosController>/joystick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Button From 2D Axis"",
                    ""id"": ""90cede9e-ae46-4017-94a8-2b8744824174"",
                    ""path"": ""ButtonFrom2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""axis"",
                    ""id"": ""7946659e-017f-410c-9c5f-257c23655242"",
                    ""path"": ""<OpenVRControllerWMR>/joystick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Button From 2D Axis"",
                    ""id"": ""4756bb0c-5b03-4ebe-ab2c-3e3e243c3949"",
                    ""path"": ""ButtonFrom2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""axis"",
                    ""id"": ""3b47d540-fd9d-4df1-828a-4dd33bc7193e"",
                    ""path"": ""<OculusTouchController>/thumbstick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Quick Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""095c0511-005e-4b3c-a667-5121f1c4c49b"",
                    ""path"": ""<OpenVRControllerIndex>/aButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bcc99b7e-8523-4834-832c-b33c3ebd825a"",
                    ""path"": ""<OpenVROculusTouchController>/gripButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""df1cbb79-dbcd-4a23-8d9c-d2685994f967"",
                    ""path"": ""<ViveWand>/gripButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a3b39819-73ab-471b-a55c-fb6b2879ac18"",
                    ""path"": ""<OpenVRViveCosmosController>/gripButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a31856a1-3862-40cf-9567-21f0b46c792c"",
                    ""path"": ""<OpenVRControllerWMR>/gripButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ff58a9b5-f267-467a-af7d-940a126f400e"",
                    ""path"": ""<OculusTouchController>/gripPressed"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e0ac07da-cac5-4f3e-bd2a-99305534aeaf"",
                    ""path"": ""<OpenVRControllerIndex>/triggerButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7be20177-b140-4a4a-8da3-55d35a47a8b4"",
                    ""path"": ""<OpenVROculusTouchController>/triggerButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""64abeea7-2c5b-4845-aad7-9cc350811405"",
                    ""path"": ""<ViveWand>/triggerButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b8ae1074-51cd-43d0-a7cc-6f1554132e6d"",
                    ""path"": ""<OpenVRViveCosmosController>/triggerButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f46e903e-4586-4463-8df5-ecf9e50b8f32"",
                    ""path"": ""<OpenVRControllerWMR>/triggerButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3bb233c1-e311-4a46-b372-2b120e474e86"",
                    ""path"": ""<OculusTouchController>/triggerPressed"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // VR
            m_VR = asset.FindActionMap("VR", throwIfNotFound: true);
            m_VR_Position = m_VR.FindAction("Position", throwIfNotFound: true);
            m_VR_Rotation = m_VR.FindAction("Rotation", throwIfNotFound: true);
            m_VR_Grab = m_VR.FindAction("Grab", throwIfNotFound: true);
            m_VR_Interact = m_VR.FindAction("Interact", throwIfNotFound: true);
            m_VR_QuickMenu = m_VR.FindAction("Quick Menu", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // VR
        private readonly InputActionMap m_VR;
        private IVRActions m_VRActionsCallbackInterface;
        private readonly InputAction m_VR_Position;
        private readonly InputAction m_VR_Rotation;
        private readonly InputAction m_VR_Grab;
        private readonly InputAction m_VR_Interact;
        private readonly InputAction m_VR_QuickMenu;
        public struct VRActions
        {
            private @Actions m_Wrapper;
            public VRActions(@Actions wrapper) { m_Wrapper = wrapper; }
            public InputAction @Position => m_Wrapper.m_VR_Position;
            public InputAction @Rotation => m_Wrapper.m_VR_Rotation;
            public InputAction @Grab => m_Wrapper.m_VR_Grab;
            public InputAction @Interact => m_Wrapper.m_VR_Interact;
            public InputAction @QuickMenu => m_Wrapper.m_VR_QuickMenu;
            public InputActionMap Get() { return m_Wrapper.m_VR; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(VRActions set) { return set.Get(); }
            public void SetCallbacks(IVRActions instance)
            {
                if (m_Wrapper.m_VRActionsCallbackInterface != null)
                {
                    @Position.started -= m_Wrapper.m_VRActionsCallbackInterface.OnPosition;
                    @Position.performed -= m_Wrapper.m_VRActionsCallbackInterface.OnPosition;
                    @Position.canceled -= m_Wrapper.m_VRActionsCallbackInterface.OnPosition;
                    @Rotation.started -= m_Wrapper.m_VRActionsCallbackInterface.OnRotation;
                    @Rotation.performed -= m_Wrapper.m_VRActionsCallbackInterface.OnRotation;
                    @Rotation.canceled -= m_Wrapper.m_VRActionsCallbackInterface.OnRotation;
                    @Grab.started -= m_Wrapper.m_VRActionsCallbackInterface.OnGrab;
                    @Grab.performed -= m_Wrapper.m_VRActionsCallbackInterface.OnGrab;
                    @Grab.canceled -= m_Wrapper.m_VRActionsCallbackInterface.OnGrab;
                    @Interact.started -= m_Wrapper.m_VRActionsCallbackInterface.OnInteract;
                    @Interact.performed -= m_Wrapper.m_VRActionsCallbackInterface.OnInteract;
                    @Interact.canceled -= m_Wrapper.m_VRActionsCallbackInterface.OnInteract;
                    @QuickMenu.started -= m_Wrapper.m_VRActionsCallbackInterface.OnQuickMenu;
                    @QuickMenu.performed -= m_Wrapper.m_VRActionsCallbackInterface.OnQuickMenu;
                    @QuickMenu.canceled -= m_Wrapper.m_VRActionsCallbackInterface.OnQuickMenu;
                }
                m_Wrapper.m_VRActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Position.started += instance.OnPosition;
                    @Position.performed += instance.OnPosition;
                    @Position.canceled += instance.OnPosition;
                    @Rotation.started += instance.OnRotation;
                    @Rotation.performed += instance.OnRotation;
                    @Rotation.canceled += instance.OnRotation;
                    @Grab.started += instance.OnGrab;
                    @Grab.performed += instance.OnGrab;
                    @Grab.canceled += instance.OnGrab;
                    @Interact.started += instance.OnInteract;
                    @Interact.performed += instance.OnInteract;
                    @Interact.canceled += instance.OnInteract;
                    @QuickMenu.started += instance.OnQuickMenu;
                    @QuickMenu.performed += instance.OnQuickMenu;
                    @QuickMenu.canceled += instance.OnQuickMenu;
                }
            }
        }
        public VRActions @VR => new VRActions(this);
        public interface IVRActions
        {
            void OnPosition(InputAction.CallbackContext context);
            void OnRotation(InputAction.CallbackContext context);
            void OnGrab(InputAction.CallbackContext context);
            void OnInteract(InputAction.CallbackContext context);
            void OnQuickMenu(InputAction.CallbackContext context);
        }
    }
}
