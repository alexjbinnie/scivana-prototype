﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ScivanaIMD.Annotations
{
    public class AnnotationConverter : JsonConverter
    {
        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer)
        {
            var obj = JToken.Load(reader);

            if (!(obj is JObject jobject))
                return null;

            var typeName = jobject.GetValue("type", StringComparison.InvariantCultureIgnoreCase)
                                  ?.Value<string>();

            if (typeName == null)
                return null;

            var type = GetAnnotationType(typeName);

            if (type == null)
                return null;

            if (objectType != type && !type.IsSubclassOf(objectType))
                return null;

            return jobject.ToObject(type);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var token = JToken.FromObject(value);

            if (token.Type != JTokenType.Object)
            {
                token.WriteTo(writer);
            }
            else
            {
                var jobject = (JObject) token;

                var type = GetAnnotationType(value);

                jobject.Add("type", JToken.FromObject(type));

                jobject.WriteTo(writer);
            }
        }

        private static string GetAnnotationType(object value)
        {
            return value switch
            {
                RulerAnnotation _ => "ruler",
                var _ => throw new NotImplementedException(
                             $"Cannot serialize annotation of type {value.GetType()}")
            };
        }

        private static Type GetAnnotationType(string name)
        {
            return name.ToLowerInvariant() switch
            {
                "ruler" => typeof(RulerAnnotation),
                var _ => null
            };
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Annotation) || objectType.IsSubclassOf(typeof(Annotation));
        }
    }
}