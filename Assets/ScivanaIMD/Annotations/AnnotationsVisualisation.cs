﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Linq;
using Scivana.Core;
using Scivana.Trajectory;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.Annotations
{
    public class AnnotationsVisualisation : MonoBehaviour
    {
        private Simulation simulation;

        public AnnotationsVisualisation InstantiateAsInactive(Simulation simulation,
                                                              Transform space)
        {
            var instance = this.InstantiateInactive(space);
            instance.simulation = simulation;
            return instance;
        }

        [SerializeField]
        private RulerRenderer rulerPrefab;

        private IndexedPool<RulerRenderer> rulerPool;

        private void Awake()
        {
            rulerPool = IndexedPool.Create(rulerPrefab, transform);
        }

        // Update is called once per frame
        private void Update()
        {
            var annotations = simulation.Annotations
                                        .Values
                                        .Where(annotation => !annotation.Hide)
                                        .OfType<RulerAnnotation>()
                                        .Where(ruler => ruler.Index1 >= 0 &&
                                                        ruler.Index2 >= 0 &&
                                                        ruler.Index1 != ruler.Index2);

            var positions = simulation.CurrentFrame.Get(FrameFields.ParticlePositions);

            rulerPool.MapConfig(annotations, MapConfigToInstance);

            void MapConfigToInstance(RulerAnnotation annotation, RulerRenderer ruler)
            {
                var pos1 = positions[annotation.Index1];
                var pos2 = positions[annotation.Index2];
                var distance = 10f * Vector3.Distance(pos1, pos2);

                ruler.Setup(pos1,
                            pos2,
                            $"{distance:0.0} Å");
            }
        }
    }
}