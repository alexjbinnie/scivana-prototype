﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;
using UnityEngine.Events;

namespace ScivanaIMD.Annotations
{
    public class RulerRenderer : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<string> updateText;

        [SerializeField]
        private Transform ruler;

        public void Setup(Vector3 pt1, Vector3 pt2, string label)
        {
            transform.localPosition = 0.5f * (pt1 + pt2);
            transform.localRotation = Quaternion.LookRotation(pt2 - pt1);

            var width = ruler.transform.localScale.x;

            ruler.transform.localScale = new Vector3(width, width, Vector3.Distance(pt1, pt2));

            updateText?.Invoke(label);
        }
    }
}