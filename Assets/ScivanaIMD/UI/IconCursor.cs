﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using UnityEngine;

namespace ScivanaIMD.UI
{
    /// <summary>
    /// A cursor with an icon within it
    /// </summary>
    public class IconCursor : MonoBehaviour
    {
        [SerializeField]
        private Renderer pointer;

        [SerializeField]
        private SpriteRenderer icon;

        public IconCursor InstantiateInactive(Sprite icon, Color color)
        {
            var instance = this.InstantiateInactive();
            instance.Color = color;
            instance.Icon = icon;
            return instance;
        }

        public Sprite Icon
        {
            set { icon.sprite = value; }
        }

        public Color Color
        {
            set
            {
                pointer.material.color = value;
                icon.material.color = value;
            }
        }
    }
}