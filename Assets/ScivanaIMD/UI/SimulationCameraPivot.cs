/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using Scivana.Core.Math;
using Scivana.Trajectory;
using Scivana.Trajectory.Event;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.UI
{
    /// <summary>
    /// Script which allows a camera to view a simulation
    /// </summary>
    public class SimulationCameraPivot : MonoBehaviour
    {
        private Simulation simulation;
        private new Camera camera;

        public static SimulationCameraPivot InstantiateInactive(
            Simulation simulation,
            Camera camera)
        {
            var instance = Instantiation.InstantiateInactive<SimulationCameraPivot>();
            instance.simulation = simulation;
            instance.camera = camera;
            return instance;
        }

        private void OnEnable()
        {
            simulation.SimulationPose.ValueChanged += RefreshPosition;
            simulation.Trajectory.FrameChanged += FrameUpdated;
            RefreshPosition();
        }

        private void FrameUpdated(Frame frame, FrameChanges changes)
        {
            if (changes.HasChanged(FrameFields.BoxTransformation.Key))
                RefreshPosition();
        }

        private void OnDisable()
        {
            simulation.SimulationPose.ValueChanged -= RefreshPosition;
            simulation.Trajectory.FrameChanged -= FrameUpdated;
        }

        public UniformScaleTransformation GetBoxTransformation()
        {
            return simulation.SimulationPose.HasValue
                       ? simulation.SimulationPose.Value
                       : UniformScaleTransformation.identity;
        }

        private void RefreshPosition()
        {
            var sim = GetBoxTransformation();

            var box = simulation.CurrentFrame.Get(FrameFields.BoxTransformation);
            if (!box.HasValue)
                return;

            var center = box.Value.origin +
                         box.Value.xAxis * 0.5f +
                         box.Value.yAxis * 0.5f +
                         box.Value.zAxis * 0.5f;

            camera.transform.position = sim.TransformPoint(center) + 2 * Vector3.back;
        }
    }
}