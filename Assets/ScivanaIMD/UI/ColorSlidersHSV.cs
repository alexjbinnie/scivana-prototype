/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using Scivana.UI.Traits;
using UnityEngine;

namespace ScivanaIMD.UI
{
    [ExecuteAlways]
    public class ColorSlidersHSV : MonoBehaviour
    {
        [SerializeField]
        private WidgetSlider hueSlider;

        [SerializeField]
        private WidgetSlider saturationSlider;

        [SerializeField]
        private WidgetSlider valueSlider;

        [SerializeField]
        private WidgetGradient hueGradient;

        [SerializeField]
        private WidgetGradient saturationGradient;

        [SerializeField]
        private WidgetGradient valueGradient;

        [SerializeField]
        private Color color;

        public event Action ColorChangedBySliders;

        public Color Color
        {
            get => color;
            set
            {
                color = value;
                RefreshColor();
            }
        }

        private void OnEnable()
        {
            RefreshColor();
            hueSlider.ValueChanged += ValueChanged;
            saturationSlider.ValueChanged += ValueChanged;
            valueSlider.ValueChanged += ValueChanged;
        }

        private void ValueChanged()
        {
            color = Color.HSVToRGB(hueSlider.Value, saturationSlider.Value, valueSlider.Value);
            ColorChangedBySliders?.Invoke();
        }

        private static readonly float[] samplePoints =
        {
            0f, 0.167f, 0.333f, 0.5f, 0.667f, 0.833f, 1f
        };

        private void RefreshColor()
        {
            float H, S, V;
            Color.RGBToHSV(color, out H, out S, out V);
            hueSlider.Value = H;
            saturationSlider.Value = S;
            valueSlider.Value = V;

            var hueGradient = new Gradient();
            hueGradient.colorKeys = samplePoints
                                    .Select(k => new GradientColorKey(Color.HSVToRGB(k, 1f, 1f), k))
                                    .ToArray();

            var saturationGradient = new Gradient();
            saturationGradient.colorKeys = samplePoints
                                           .Select(k => new GradientColorKey(Color.HSVToRGB(H, k, V), k))
                                           .ToArray();

            var valueGradient = new Gradient();
            valueGradient.colorKeys = samplePoints
                                      .Select(k => new GradientColorKey(Color.HSVToRGB(H, S, k), k))
                                      .ToArray();

            this.hueGradient.Value = hueGradient;
            this.saturationGradient.Value = saturationGradient;
            this.valueGradient.Value = valueGradient;
        }
    }
}