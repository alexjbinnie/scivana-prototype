﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace ScivanaIMD.UI
{
    public class Overlay : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup baseGroup;

        [SerializeField]
        private CanvasGroup overlayGroup;

        [SerializeField]
        private GameObject overlay;

        public void EnterOverlay()
        {
            baseGroup.alpha = 0.2f;
            baseGroup.interactable = false;
            overlayGroup.alpha = 1f;
            overlayGroup.ignoreParentGroups = true;
            overlay.gameObject.SetActive(true);
        }

        public void LeaveOverlay()
        {
            baseGroup.alpha = 1f;
            baseGroup.interactable = true;
            overlay.gameObject.SetActive(false);
        }
    }
}