﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;

namespace ScivanaIMD.UI
{
    /// <summary>
    /// Floats a canvas in the distance in front of a VR user, and moves it when they look away
    /// </summary>
    public class FloatingCanvas : MonoBehaviour
    {
        [SerializeField]
        private float distance;

        [SerializeField]
        private float height;

        [SerializeField]
        private AnimationCurve angularResponse;

        [SerializeField]
        private float responseStrength;

        [SerializeField]
        private float maxSpeed = 1f;

        [SerializeField]
        private CanvasGroup group;

        private Vector3 currentDesiredForward;

        // Update is called once per frame
        private void Update()
        {
            var camera = Camera.main;
            var cameraTransform = camera.transform;

            var desiredForward = cameraTransform.forward;
            desiredForward.y = 0;
            desiredForward = desiredForward.normalized;
            var pos = cameraTransform.position;
            pos.y += height;

            var currentForward = transform.position - pos;
            currentForward.y = 0;
            currentForward = currentForward.normalized;

            var angle = Vector3.Angle(currentForward, desiredForward);

            responseStrength += angularResponse.Evaluate(angle * Mathf.Deg2Rad) * Time.deltaTime;
            responseStrength = Mathf.Clamp(responseStrength, 0, maxSpeed);

            if (responseStrength > 0.5f)
            {
                currentDesiredForward = desiredForward;
            }

            var newForward = Vector3.RotateTowards(currentForward, currentDesiredForward,
                                                   responseStrength * Time.deltaTime,
                                                   100f * responseStrength * Time.deltaTime);

            var newPos = Vector3.MoveTowards(transform.position, pos + newForward * distance,
                                             responseStrength * Time.deltaTime);

            group.alpha = 1f - 0.8f * GetAlpha();

            transform.position = newPos;
            transform.rotation = Quaternion.LookRotation(newForward, Vector3.up);
        }

        private float GetAlpha()
        {
            return responseStrength;
        }
    }
}