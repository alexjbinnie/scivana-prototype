﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;
using UnityEngine.Assertions;

namespace ScivanaIMD.Interaction
{
    [ExecuteAlways]
    public class SineWaveInteractionRenderer : MonoBehaviour
    {
        [SerializeField]
        private Vector3 startPoint = Vector3.zero;

        [SerializeField]
        private Vector3 endPoint = Vector3.one;

        [SerializeField]
        private new LineRenderer renderer;

        [SerializeField]
        private float segmentsPerMeter = 250;

        [SerializeField]
        private float frequency = 80;

        [SerializeField]
        private float speed = 15;

        [SerializeField]
        private float amplitude = 0.015f;

        [SerializeField]
        private float width = 0.02f;

        [SerializeField]
        private float scaling = 0.1f;

        public Vector3 StartPosition
        {
            get => startPoint;
            set => startPoint = value;
        }

        public Vector3 EndPosition
        {
            get => endPoint;
            set => endPoint = value;
        }

        private void Awake()
        {
            Assert.IsNotNull(renderer);
        }

        private void Update()
        {
            RefreshRenderer();
        }

        private void RefreshRenderer()
        {
            var cameraTransform = Camera.main.transform;

            var dist = Vector3.Distance(startPoint, endPoint);
            var dir = endPoint - startPoint;

            var scaling = 1f + this.scaling / dist;

            var frequency = this.frequency * scaling;
            var amplitude = this.amplitude / scaling;
            var width = this.width / scaling;
            var segmentsPerMeter = Mathf.Clamp(this.segmentsPerMeter * scaling, 1, 10000f);

            int segments = (int) Mathf.Clamp(segmentsPerMeter * dist, 2, 10000f);

            var cameraDir = cameraTransform.InverseTransformDirection(dir);
            cameraDir.z = 0;
            var x = cameraDir.x;
            cameraDir.x = -cameraDir.y;
            cameraDir.y = x;
            var up = cameraTransform.TransformDirection(cameraDir);
            up = (up - Vector3.Project(up, dir)).normalized;

            renderer.positionCount = segments;

            renderer.startWidth = width;
            renderer.endWidth = width;

            for (var i = 0; i < segments; i++)
            {
                float t = i / (segments - 1f);
                var p = Vector3.Lerp(startPoint, endPoint, t);
                p += up * (Mathf.Sin(t * dist * frequency - speed * Time.time)
                         * Mathf.Sin(t * Mathf.PI) * amplitude);
                renderer.SetPosition(i, p);
            }
        }

        public void SetPositions(Vector3 start, Vector3 end)
        {
            StartPosition = start;
            EndPosition = end;
        }
    }
}