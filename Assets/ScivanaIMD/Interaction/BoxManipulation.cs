﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.App.XR;
using Scivana.Core;
using Scivana.Core.Math;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

namespace ScivanaIMD.Interaction
{
    public class BoxManipulation : MonoBehaviour
    {
        public BoxManipulation InstantiateAsInactive(Simulation simulation)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            return instance;
        }

        private Simulation simulation;

        private IValued<UnitScaleTransformation?> leftHand;
        private IValued<UnitScaleTransformation?> rightHand;

        private void Start()
        {
            leftHand = ScivanaPrototype.Input.GetOrientation(InputDeviceRole.LeftHanded);
            rightHand = ScivanaPrototype.Input.GetOrientation(InputDeviceRole.RightHanded);
        }

        private void OnEnable()
        {
            var leftAction = ScivanaPrototype.Input.Left.VR.Grab;
            var rightAction = ScivanaPrototype.Input.Right.VR.Grab;

            leftAction.started += OnGrab;
            leftAction.canceled += OnGrabEnd;

            rightAction.started += OnGrab;
            rightAction.canceled += OnGrabEnd;
        }

        private void OnGrab(InputAction.CallbackContext context)
        {
            GrabSimulationBoxStarted(context.GetHand());
        }

        private void OnGrabEnd(InputAction.CallbackContext context)
        {
            GrabSimulationBoxCancelled(context.GetHand());
        }

        private void GrabSimulationBoxStarted(InputDeviceRole handedness)
        {
            if (handedness == InputDeviceRole.LeftHanded)
            {
                if (manipulation is RigidGrabManipulation manip && manip.Grab == rightHand)
                    StartDoubleManipulation();
                else
                    StartSingleManipulation(leftHand);
            }
            else if (handedness == InputDeviceRole.RightHanded)
            {
                if (manipulation is RigidGrabManipulation manip && manip.Grab == leftHand)
                    StartDoubleManipulation();
                else
                    StartSingleManipulation(rightHand);
            }
        }

        private void GrabSimulationBoxCancelled(InputDeviceRole handedness)
        {
            if (manipulation is PinchManipulation)
            {
                if (handedness == InputDeviceRole.LeftHanded)
                    StartSingleManipulation(rightHand);
                else if (handedness == InputDeviceRole.RightHanded)
                    StartSingleManipulation(leftHand);
            }
            else
            {
                manipulation = null;
            }
        }

        private IManipulation<UniformScaleTransformation?> manipulation;

        public IManipulation<UniformScaleTransformation?> CurrentManipulation => manipulation;

        private void Update()
        {
            var value = manipulation?.Update();
            if (value.HasValue)
                simulation.SimulationPose.UpdateValueAndAttemptLock(value.Value);
        }

        public void StartSingleManipulation(IValued<UnitScaleTransformation?> grab)
        {
            manipulation = new RigidGrabManipulation(grab, GetBoxTransformation());
        }

        public void StartDoubleManipulation()
        {
            manipulation = new PinchManipulation(leftHand, rightHand, GetBoxTransformation());
        }

        public UniformScaleTransformation GetBoxTransformation()
        {
            return simulation.SimulationPose.HasValue
                       ? simulation.SimulationPose.Value
                       : UniformScaleTransformation.identity;
        }
    }
}