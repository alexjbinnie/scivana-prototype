﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.App.XR;
using Scivana.Core.Math;
using UnityEngine;

namespace ScivanaIMD.Interaction
{
    public class PinchManipulation : IManipulation<UniformScaleTransformation?>
    {
        public IValued<UnitScaleTransformation?> LeftGrab { get; }
        public IValued<UnitScaleTransformation?> RightGrab { get; }
        private readonly UniformScaleTransformation grabToTarget;

        public PinchManipulation(IValued<UnitScaleTransformation?> leftGrab,
                                 IValued<UnitScaleTransformation?> rightGrab,
                                 UniformScaleTransformation target)
        {
            LeftGrab = leftGrab;
            RightGrab = rightGrab;
            if (!LeftGrab.Value.HasValue || !RightGrab.Value.HasValue)
                throw new ArgumentException("Cannot start manipulation with invalid position");
            var grab = ComputeMidpointTransformation(LeftGrab.Value.Value, RightGrab.Value.Value);
            grabToTarget = grab.inverse * target;
        }

        public UniformScaleTransformation? Update()
        {
            if (LeftGrab.Value.HasValue && RightGrab.Value.HasValue)
                return ComputeMidpointTransformation(LeftGrab.Value.Value, RightGrab.Value.Value)
                     * grabToTarget;
            return null;
        }

        /// <summary>
        /// Compute a transformation that represents the "average" of two input
        /// transformations.
        /// </summary>
        private static UniformScaleTransformation ComputeMidpointTransformation(
            UnitScaleTransformation controlTransformation1,
            UnitScaleTransformation controlTransformation2)
        {
            // position the origin between the two control points
            var position1 = controlTransformation1.position;
            var position2 = controlTransformation2.position;
            var position = Vector3.LerpUnclamped(position1, position2, 0.5f);

            // base the scale on the separation between the two control points
            var scale = Vector3.Distance(position1, position2);

            // choose an orientation where the line between the two control points
            // is one axis, and another axis is roughly the compromise between the
            // two up vectors of the control points
            var rotation1 = controlTransformation1.rotation;
            var rotation2 = controlTransformation2.rotation;
            var midRotation = Quaternion.SlerpUnclamped(rotation1, rotation2, 0.5f);

            var up = midRotation * Vector3.forward;
            var right = (position2 - position1).normalized;

            var rotation = Quaternion.LookRotation(right, up);

            return new UniformScaleTransformation(position, rotation, scale);
        }
    }
}