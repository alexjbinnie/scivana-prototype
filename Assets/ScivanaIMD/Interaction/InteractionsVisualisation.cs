﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;
using Scivana.Core;
using Scivana.Trajectory;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.Interaction
{
    /// <summary>
    /// Manage instances of InteractionWaveRenderer so that all known
    /// interactions are rendered using Mike's pretty sine wave method from
    /// Narupa 1
    /// </summary>
    public class InteractionsVisualisation : MonoBehaviour
    {
        public InteractionsVisualisation InstantiateAsInactive(Simulation simulation,
                                                               Transform rightHandedSpace)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            instance.transform.parent = rightHandedSpace;
            return instance;
        }

        private Simulation simulation;

        [SerializeField]
        private SineWaveInteractionRenderer waveTemplate;

        private IndexedPool<SineWaveInteractionRenderer> wavePool;

        private void Start()
        {
            wavePool = new IndexedPool<SineWaveInteractionRenderer>(
                CreateInstanceCallback, ActivateInstanceCallback, DeactivateInstanceCallback);
        }

        private void DeactivateInstanceCallback(SineWaveInteractionRenderer obj)
        {
            obj.gameObject.SetActive(false);
        }

        private void ActivateInstanceCallback(SineWaveInteractionRenderer obj)
        {
            obj.gameObject.SetActive(true);
        }

        private SineWaveInteractionRenderer CreateInstanceCallback()
        {
            var renderer = Instantiate(waveTemplate, transform, true);
            renderer.gameObject.SetActive(true);
            return renderer;
        }

        private void Update()
        {
            var interactions = simulation.Interactions;
            var frame = simulation.CurrentFrame;

            var positions = frame.Get(FrameFields.ParticlePositions);

            wavePool.MapConfig(interactions.Values, MapConfigToInstance);

            void MapConfigToInstance(ParticleInteraction interaction,
                                     SineWaveInteractionRenderer renderer)
            {
                var particlePositionSim = ComputeParticleCentroid(interaction.Particles);
                var particlePositionWorld = transform.TransformPoint(particlePositionSim);

                renderer.SetPositions(particlePositionWorld,
                                      transform.TransformPoint(interaction.Position));
            }

            Vector3 ComputeParticleCentroid(IReadOnlyList<int> particleIds)
            {
                var centroid = particleIds.Aggregate(Vector3.zero,
                                                     (current, t) =>
                                                         current + positions[t]);

                return centroid / particleIds.Count;
            }
        }
    }
}