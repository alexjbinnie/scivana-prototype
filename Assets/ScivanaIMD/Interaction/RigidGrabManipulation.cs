﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.App.XR;
using Scivana.Core.Math;

namespace ScivanaIMD.Interaction
{
    public class RigidGrabManipulation : IManipulation<UniformScaleTransformation?>
    {
        public IValued<UnitScaleTransformation?> Grab { get; }

        private readonly UniformScaleTransformation grabToTarget;

        public RigidGrabManipulation(IValued<UnitScaleTransformation?> grab,
                                     UniformScaleTransformation target)
        {
            Grab = grab;
            if (!Grab.Value.HasValue)
                throw new ArgumentException("Cannot start manipulation with invalid position");
            grabToTarget = Grab.Value.Value.inverse * target;
        }

        public UniformScaleTransformation? Update()
        {
            if (Grab.Value.HasValue)
                return Grab.Value.Value * grabToTarget;
            return null;
        }
    }
}