﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.App.XR;
using Scivana.Core;
using Scivana.Core.Math;
using Scivana.Trajectory;
using ScivanaIMD.Simulations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

namespace ScivanaIMD.Interaction
{
    public class ParticlesManipulation : MonoBehaviour
    {
        public ParticlesManipulation InstantiateAsInactive(Simulation simulation,
                                                           Transform rightHandedSpace)
        {
            var instance = this.InstantiateInactive();
            instance.simulation = simulation;
            instance.transform.parent = rightHandedSpace;
            instance.SetToLocalIdentity();
            return instance;
        }

        private Simulation simulation;

        private IValued<UnitScaleTransformation?> leftHand;
        private IValued<UnitScaleTransformation?> rightHand;

        private string leftHandInteraction;
        private string rightHandInteraction;

        private void Start()
        {
            leftHand = ScivanaPrototype.Input.GetOrientation(InputDeviceRole.LeftHanded);
            rightHand = ScivanaPrototype.Input.GetOrientation(InputDeviceRole.RightHanded);
        }

        private InteractionPreferences preferences;

        private void OnEnable()
        {
            preferences = ScivanaPrototype.Preferences<InteractionPreferences>();

            ScivanaPrototype.Input.Left.VR.Interact.performed += OnInteractStart;
            ScivanaPrototype.Input.Left.VR.Interact.canceled += OnInteractStop;

            ScivanaPrototype.Input.Right.VR.Interact.performed += OnInteractStart;
            ScivanaPrototype.Input.Right.VR.Interact.canceled += OnInteractStop;
        }

        private void OnInteractStart(InputAction.CallbackContext obj)
        {
            StartParticleInteraction(obj.GetHand());
        }

        private void OnDisable()
        {
            ScivanaPrototype.Input.Left.VR.Interact.performed -= OnInteractStart;
            ScivanaPrototype.Input.Left.VR.Interact.canceled -= OnInteractStop;

            ScivanaPrototype.Input.Right.VR.Interact.performed -= OnInteractStart;
            ScivanaPrototype.Input.Right.VR.Interact.canceled -= OnInteractStop;

            EndParticleInteraction(InputDeviceRole.LeftHanded);
            EndParticleInteraction(InputDeviceRole.RightHanded);
        }

        private void OnInteractStop(InputAction.CallbackContext obj)
        {
            EndParticleInteraction(obj.GetHand());
        }

        private void StartParticleInteraction(InputDeviceRole handedness)
        {
            if (handedness == InputDeviceRole.LeftHanded && leftHand.Value.HasValue)
            {
                EndParticleInteraction(InputDeviceRole.LeftHanded);

                leftHandInteraction = CreateInteractionId();
                simulation.Interactions[leftHandInteraction] =
                    CreateParticleInteraction(leftHand.Value.Value);
                UpdateInteraction(leftHandInteraction, leftHand.Value.Value);
            }
            else if (handedness == InputDeviceRole.RightHanded && rightHand.Value.HasValue)
            {
                EndParticleInteraction(InputDeviceRole.RightHanded);

                rightHandInteraction = CreateInteractionId();
                simulation.Interactions[rightHandInteraction] =
                    CreateParticleInteraction(rightHand.Value.Value);
                UpdateInteraction(rightHandInteraction, rightHand.Value.Value);
            }
        }

        private void EndParticleInteraction(InputDeviceRole handedness)
        {
            if (handedness == InputDeviceRole.LeftHanded && leftHandInteraction != null)
            {
                CloseInteraction(leftHandInteraction);
                leftHandInteraction = null;
            }
            else if (handedness == InputDeviceRole.RightHanded && rightHandInteraction != null)
            {
                CloseInteraction(rightHandInteraction);
                rightHandInteraction = null;
            }
        }

        private void CloseInteraction(string id)
        {
            simulation.Interactions.Remove(id);
        }

        private static string CreateInteractionId()
        {
            return "interaction." + Guid.NewGuid();
        }

        private void UpdateInteraction(string id, UnitScaleTransformation grab)
        {
            var interaction = simulation.Interactions[id];
            interaction.Position = transform.InverseTransformPoint(grab.position);
            interaction.ResetVelocities = preferences.ResetVelocities.Value;
            interaction.Scale = preferences.ForceScale.Value;
            interaction.InteractionType = preferences.InteractionType.Value;
            interaction.MassWeighted = preferences.MassWeighted.Value;
            simulation.Interactions[id] = interaction;
        }

        private void Update()
        {
            if (leftHandInteraction != null && leftHand.Value.HasValue)
            {
                UpdateInteraction(leftHandInteraction, leftHand.Value.Value);
            }

            if (rightHandInteraction != null && rightHand.Value.HasValue)
            {
                UpdateInteraction(rightHandInteraction, rightHand.Value.Value);
            }
        }

        /// <summary>
        /// Attempt to grab the nearest particle, returning null if no interaction is
        /// possible.
        /// </summary>
        /// <param name="grabberPose">The transformation of the grabbing pivot.</param>
        private ParticleInteraction CreateParticleInteraction(Transformation grabberPose)
        {
            var particleIndex = GetClosestParticleToWorldPosition(grabberPose.position);

            if (!particleIndex.HasValue)
                return null;

            var indices = GetInteractionIndices(particleIndex.Value);

            var interaction = new ParticleInteraction
            {
                Particles = indices.ToList()
            };

            return interaction;
        }

        public enum InteractionTarget
        {
            Singular,
            Residue
        }

        [SerializeField]
        private InteractionTarget interactionTarget = InteractionTarget.Singular;

        private IEnumerable<int> GetInteractionIndices(int particleIndex)
        {
            switch (interactionTarget)
            {
                case InteractionTarget.Singular:
                    yield return particleIndex;
                    break;
                case InteractionTarget.Residue:
                    var frame = simulation.CurrentFrame;

                    var residues = frame.Get(FrameFields.ParticleResidues);

                    if (residues == null || residues.Length == 0)
                    {
                        yield return particleIndex;
                        break;
                    }

                    var residue = residues[particleIndex];
                    if (residue == -1)
                    {
                        yield return particleIndex;
                        break;
                    }

                    for (var i = 0; i < residues.Length; i++)
                    {
                        if (residues[i] == residue)
                            yield return i;
                    }

                    break;
            }
        }

        /// <summary>
        /// Get the closest particle to a given point in world space.
        /// </summary>
        private int? GetClosestParticleToWorldPosition(Vector3 worldPosition,
                                                       float cutoff = Mathf.Infinity)
        {
            var position = transform.InverseTransformPoint(worldPosition);

            var frame = simulation.CurrentFrame;

            var bestSqrDistance = cutoff * cutoff;
            int? bestParticleIndex = null;

            var positions = frame.Get(FrameFields.ParticlePositions);

            for (var i = 0; i < positions.Length; ++i)
            {
                var particlePosition = positions[i];
                var sqrDistance = Vector3.SqrMagnitude(position - particlePosition);

                if (sqrDistance < bestSqrDistance)
                {
                    bestSqrDistance = sqrDistance;
                    bestParticleIndex = i;
                }
            }

            return bestParticleIndex;
        }
    }
}