/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using UnityEngine;

namespace ScivanaIMD.Interaction
{
    /// <summary>
    /// An interaction with an iMD enabled simulation.
    /// </summary>
    [DataContract]
    public class ParticleInteraction
    {
        /// <summary>
        /// The position of the interaction.
        /// </summary>
        [DataMember(Name = "position")]
        public Vector3 Position { get; set; }

        /// <summary>
        /// The particle indices involved in the interaction.
        /// </summary>
        [DataMember(Name = "particles")]
        public List<int> Particles { get; set; }

        /// <summary>
        /// The interaction type, such as 'string' or 'gaussian'.
        /// </summary>
        [DataMember(Name = "interaction_type")]
        public string InteractionType { get; set; } = "gaussian";

        /// <summary>
        /// The scale of the force.
        /// </summary>
        [DataMember(Name = "scale")]
        public float Scale { get; set; }

        /// <summary>
        /// Should this interaction be mass weighted?
        /// </summary>
        [DataMember(Name = "mass_weighted")]
        public bool MassWeighted { get; set; } = true;

        /// <summary>
        /// Should this interaction reset velocities after being applied?
        /// </summary>
        [DataMember(Name = "reset_velocities")]
        public bool ResetVelocities { get; set; }

        /// <summary>
        /// The maximum force asserted by this interaction.
        /// </summary>
        [DataMember(Name = "max_force")]
        public float MaxForce { get; set; } = float.PositiveInfinity;

        /// <summary>
        /// Arbitrary key-value data associated with this interaction.
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, object> Other { get; } = new Dictionary<string, object>();
    }
}