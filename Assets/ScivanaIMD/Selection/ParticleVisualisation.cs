/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Network.Multiplayer;
using ScivanaIMD.Selection.Data;

namespace ScivanaIMD.Selection
{
    /// <summary>
    /// Represents an instance of a visualisation, wrapping a
    /// <see cref="ParticleVisualiser" /> and
    /// watching the corresponding <see cref="ParticleSelection" /> for changes in
    /// what is
    /// selected.
    /// </summary>
    public class ParticleVisualisation : IParticleVisualisation
    {
        /// <summary>
        /// Invoked when the selection used by this visualisation is altered. This should
        /// trigger
        /// a recalculation of which particle indices belong in each visualisation, based
        /// upon
        /// their priorities and layers.
        /// </summary>
        public event Action SelectionUpdated;

        /// <summary>
        /// Invoked when this visualisation should be removed.
        /// </summary>
        public event Action Removed;

        private ParticleVisualiser visualisation;

        private ParticleSelection selection;

        /// <inheritdoc cref="ParticleVisualiser.DisplayName" />
        public string DisplayName => visualisation.DisplayName ?? "Unnamed";

        /// <inheritdoc cref="ParticleVisualiser.Priority" />
        public float Priority => visualisation.Priority ?? 0;

        /// <inheritdoc cref="ParticleVisualiser.Layer" />
        public int Layer => visualisation.Layer ?? 0;

        /// <inheritdoc cref="ParticleVisualiser.Hide" />
        public bool Hide => visualisation.Hide ?? false;

        /// <inheritdoc cref="ParticleVisualiser.Visualiser" />
        public object Visualiser => visualisation.Visualiser;

        /// <summary>
        /// The particle indices that can be drawn by this visualisation. Depending on
        /// other
        /// visualisations present, not all of these particles may be drawn.
        /// </summary>
        public IReadOnlyList<int> ParticleIndices => selection?.ParticleIds;

        /// <summary>
        /// Create a <see cref="ParticleVisualisation" /> that wraps the given
        /// <see cref="ParticleVisualiser" /> provided in the shared state.
        /// </summary>
        public ParticleVisualisation(ParticleVisualiser visualisation)
        {
            this.visualisation = visualisation;
        }

        private MultiplayerResource<ParticleSelection> linkedSelection;

        internal MultiplayerResource<ParticleSelection> LinkedSelection
        {
            set
            {
                if (linkedSelection != null)
                {
                    linkedSelection.ValueChanged -= LinkedSelectionChanged;
                }

                linkedSelection = value;
                if (linkedSelection != null)
                {
                    linkedSelection.ValueChanged += LinkedSelectionChanged;
                    if (linkedSelection.HasValue)
                        selection = linkedSelection.Value;
                }
                else
                {
                    selection = default;
                }
            }
        }

        private void LinkedSelectionChanged()
        {
            if (linkedSelection.HasValue)
                selection = linkedSelection.Value;
            else
                selection = null;
            SelectionUpdated?.Invoke();
        }

        public void Delete()
        {
            LinkedSelection = null;
            Removed?.Invoke();
        }
    }
}