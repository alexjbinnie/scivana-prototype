/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ScivanaIMD.Selection.Data
{
    /// <summary>
    /// A set of properties that apply to the given selection of particles which affect
    /// how the
    /// user can interact with them.
    /// </summary>
    [DataContract]
    public class InteractionGroupData
    {
        /// <summary>
        /// The selection these properties apply to.
        /// </summary>
        [DataMember(Name = "selection")]
        public string SelectionKey { get; set; }

        /// <summary>
        /// Should an interaction with these atoms have reset velocities applied to them
        /// after an
        /// interaction has finished?
        /// </summary>
        [DataMember(Name = "reset_velocities")]
        public bool ResetVelocities { get; set; }

        /// <summary>
        /// User-defined properties for this interaction group
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, object> OtherData { get; set; }
    }
}