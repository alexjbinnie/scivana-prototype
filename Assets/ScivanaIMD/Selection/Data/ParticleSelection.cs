/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ScivanaIMD.Selection.Data
{
    /// <summary>
    /// A set of particles indices which are used for defining behaviours such as
    /// interaction
    /// groups and visualisations.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ParticleSelection
    {
        /// <summary>
        /// The ordered, 0-based indices of all particles in this selection.
        /// </summary>
        [DataMember(Name = "particle_ids")]
        public List<int> ParticleIds { get; set; }

        /// <summary>
        /// The user-facing display name of the selection.
        /// </summary>
        [DataMember(Name = "display_name")]
        public string Name { get; set; }

        /// <summary>
        /// User-defined properties for this selection.
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, object> OtherData { get; set; }
    }
}