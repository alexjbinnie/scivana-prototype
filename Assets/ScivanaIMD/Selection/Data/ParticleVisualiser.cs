/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ScivanaIMD.Selection.Data
{
    /// <summary>
    /// A visualisation of a set of particles, using a given renderer. Visualisations
    /// on the same
    /// layer are mutually exclusive, with those of higher priority taking precedence
    /// over those
    /// below.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ParticleVisualiser
    {
        /// <summary>
        /// A user-facing display name for the visualisation.
        /// </summary>
        [DataMember(Name = "display_name")]
        public string DisplayName { get; set; }

        /// <summary>
        /// The key of the particle selection that this visualisation is linked to.
        /// </summary>
        [DataMember(Name = "selection")]
        public string SelectionKey { get; set; }

        /// <summary>
        /// The definition of the visualiser this visualisation should use.
        /// </summary>
        [DataMember(Name = "visualiser")]
        public object Visualiser { get; set; }

        /// <summary>
        /// Should this visualisation be hidden from view?
        /// </summary>
        [DataMember(Name = "hide")]
        public bool? Hide { get; set; }

        /// <summary>
        /// The layer this visualisation is on. This dictates the order in which it is
        /// drawn to the
        /// screen, with layers being drawn from lowest to highest.
        /// </summary>
        [DataMember(Name = "layer")]
        public int? Layer { get; set; }

        /// <summary>
        /// The priority this visualisation has within its layer. Visualisations with
        /// higher
        /// priority are drawn after those with lower priority. If a particle would belong
        /// to two
        /// or more visualisers in the same layer, it is only drawn by the visualiser with
        /// the
        /// highest priority.
        /// </summary>
        [DataMember(Name = "priority")]
        public float? Priority { get; set; }

        /// <summary>
        /// User-defined properties for this selection.
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, object> OtherData { get; set; }
    }
}