/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Scivana.Core;
using Scivana.Core.Math;
using Scivana.Visualisation;
using Scivana.Visualisation.Properties;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.Selection
{
    /// <summary>
    /// Scene representation of a selection, which will render the selection using a
    /// given visualiser.
    /// </summary>
    public class VisualisationInstance : MonoBehaviour
    {
        public static VisualisationInstance InstantiateAsInactive(
            SimulationVisualisationRoot visualisationRoot,
            IParticleVisualisation visualisation,
            Transform parent = null)
        {
            var instance = Instantiation.InstantiateInactive<VisualisationInstance>(parent);
            instance.visualisationRoot = visualisationRoot;
            instance.visualisation = visualisation;
            return instance;
        }

        private SimulationVisualisationRoot visualisationRoot;

        /// <summary>
        /// The indices of particles that should be rendered in this selection.
        /// </summary>
        public ArrayProperty<int> FilteredIndices { get; } = new ArrayProperty<int>();

        private int[] filteredIndices = Array.Empty<int>();

        /// <summary>
        /// The indices of particles not rendered by this or any higher selections.
        /// </summary>
        /// ///
        /// <remark>
        /// Unfiltered indices in this selection form the set of indices to be filtered
        /// by the selections beneath it in the stack. By default, any indices left at the
        /// bottom of the stack
        /// will be rendered by the base selection.
        /// </remark>
        public ArrayProperty<int> UnfilteredIndices { get; } = new ArrayProperty<int>();

        private int[] unfilteredIndices = Array.Empty<int>();

        private IParticleVisualisation visualisation;

        public IParticleVisualisation Visualisation => visualisation;

        private VisualisationLayer layer;

        private Visualiser currentVisualiser;

        private void Awake()
        {
            layer = GetComponentInParent<VisualisationLayer>();
        }

        private void OnEnable()
        {
            visualisation.SelectionUpdated += OnSelectionUpdated;
            visualisation.Removed += OnRemoved;
        }

        private void OnDisable()
        {
            visualisation.SelectionUpdated -= OnSelectionUpdated;
            visualisation.Removed -= OnRemoved;
        }

        private void OnSelectionUpdated()
        {
            layer.RecalculateIndices(this);
            UpdateVisualiser();
        }

        private void OnRemoved()
        {
            layer.RemoveInstance(this);
        }

        /// <summary>
        /// Given a selection that is at a higher level in the layer, which will have drawn
        /// some particles, work out which particles that have not been drawn should be
        /// drawn by this selection and which should be left for another selection further
        /// down the stack.
        /// </summary>
        public void CalculateFilteredIndices(VisualisationInstance upperInstance, int maxCount)
        {
            var indices = upperInstance != null ? upperInstance.unfilteredIndices : null;

            FilterIndices(indices,
                          visualisation.ParticleIndices,
                          maxCount,
                          ref filteredIndices,
                          ref unfilteredIndices);

            if (filteredIndices == null)
                FilteredIndices.UndefineValue();
            else
                FilteredIndices.Value = filteredIndices;
            UnfilteredIndices.Value = unfilteredIndices;
        }

        public static void FilterIndices([CanBeNull] IReadOnlyCollection<int> indices,
                                         [CanBeNull] IReadOnlyList<int> filter,
                                         int maxCount,
                                         ref int[] filteredIndices,
                                         ref int[] unfilteredIndices)
        {
            if (filter != null)
            {
                if (filter.Count == 0) // Selection is empty
                {
                    filteredIndices = Array.Empty<int>();

                    unfilteredIndices = indices?.ToArray();
                }
                else
                {
                    // Calculate the subset of indices which belong in this selection
                    FilterIndices(indices ?? Enumerable.Range(0, maxCount).ToArray(),
                                  filter,
                                  ref filteredIndices,
                                  ref unfilteredIndices);
                }
            }
            else // This selection selects everything
            {
                filteredIndices = indices?.ToArray();

                unfilteredIndices = Array.Empty<int>();
            }
        }

        /// <summary>
        /// Given a set of indices and a filter, split the indices into the set which are
        /// in filter and those which are not.
        /// </summary>
        /// <param name="indices">A set of indices to filter</param>
        /// <param name="filter">A set of indices to search for in indices</param>
        /// <param name="filteredIndices">
        /// An array to fill with indices present in both inputs.
        /// </param>
        /// <param name="unfilteredIndices">
        /// An array to fill with indices present in indices, but not filter.
        /// </param>
        private static void FilterIndices([NotNull] IReadOnlyCollection<int> indices,
                                          [NotNull] IReadOnlyList<int> filter,
                                          ref int[] filteredIndices,
                                          ref int[] unfilteredIndices)
        {
            var totalIndicesCount = indices.Count;
            var maxSize = Mathf.Min(filter.Count, totalIndicesCount);
            Array.Resize(ref filteredIndices, maxSize);
            Array.Resize(ref unfilteredIndices, totalIndicesCount);

            var filteredIndex = 0;
            var unfilteredIndex = 0;

            // For each index, check if it is in the filter and add it to the appropriate array
            foreach (var unhandledIndex in indices)
                if (SearchAlgorithms.BinarySearch(unhandledIndex, filter))
                    filteredIndices[filteredIndex++] = unhandledIndex;
                else
                    unfilteredIndices[unfilteredIndex++] = unhandledIndex;

            Array.Resize(ref filteredIndices, filteredIndex);
            Array.Resize(ref unfilteredIndices, unfilteredIndex);
        }

        /// <summary>
        /// Update the visualiser based upon the data stored in the selection.
        /// </summary>
        public void UpdateVisualiser()
        {
            // The hide property turns off any visualiser
            if (visualisation.Hide)
            {
                SetVisualiser(null);
                return;
            }

            Visualiser visualiser = null;

            // Construct a visualiser from any provided renderer info
            if (visualisation.Visualiser != null)
                visualiser = VisualiserFactory.ConstructVisualiser(visualisation.Visualiser);

            // Use the predefined ball and stick renderer as a default
            if (visualiser == null)
            {
                visualiser = VisualiserFactory.ConstructVisualiser("ball and stick");
            }

            SetVisualiser(visualiser);
        }

        /// <summary>
        /// Set the visualiser of this selection
        /// </summary>
        private void SetVisualiser(Visualiser newVisualiser)
        {
            if (currentVisualiser != null)
            {
                Destroy(currentVisualiser.gameObject);
            }

            if (newVisualiser == null)
                return;

            currentVisualiser = newVisualiser;
            currentVisualiser.transform.parent = transform;
            currentVisualiser.transform.SetToLocalIdentity();

            SetupAdaptorAndFilter();
        }

        /// <summary>
        /// Sets up the visualiser by connecting it to the scene's adaptor and linking the
        /// filter to the selection.
        /// </summary>
        private void SetupAdaptorAndFilter()
        {
            currentVisualiser.ParentAdaptor.Value = visualisationRoot.FrameAdaptor;
            currentVisualiser.ParticleFilter.LinkedProperty = FilteredIndices;
        }
    }
}