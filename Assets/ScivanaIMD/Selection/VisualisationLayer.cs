/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Scivana.Core;
using Scivana.Core.Math;
using Scivana.Core.Properties;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.Selection
{
    /// <summary>
    /// A group of <see cref="VisualisationInstance" />s which are mutually exclusive.
    /// </summary>
    /// <remarks>
    /// Selections within a visualisation layer display an atom precisely once,
    /// with the selection settings overwriting one another in the order that they are
    /// established.
    /// For example, a <see cref="VisualisationLayer" /> with selections [1,2,3] and
    /// [3,4,5] will display atoms [1,2] in the first (lower) selection and atoms
    /// [3,4,5] in the second (upper) selection.
    /// </remarks>
    public class VisualisationLayer : MonoBehaviour
    {
        public static VisualisationLayer InstantiateAsInactive(
            SimulationVisualisationRoot visualisationRoot,
            int layer,
            Transform parent = null)
        {
            var instance = Instantiation.InstantiateInactive<VisualisationLayer>(parent);
            instance.visualisationRoot = visualisationRoot;
            instance.layer = layer;
            return instance;
        }

        private SimulationVisualisationRoot visualisationRoot;

        private int layer;

        internal readonly List<VisualisationInstance> currentMembers =
            new List<VisualisationInstance>();

        public event Action Removed;

        /// <summary>
        /// The set of visualisations on this layer.
        /// </summary>
        public IReadOnlyList<VisualisationInstance> Members => currentMembers;

        public int Layer => layer;

        private class PriorityComparer : IComparer
        {
            public int Compare(object a, object b)
            {
                if (a is DefaultVisualisation)
                    return -1;
                if (b is DefaultVisualisation)
                    return 1;
                if (a is ParticleVisualisation vis1 && b is ParticleVisualisation vis2)
                {
                    if (vis1.Priority > vis2.Priority)
                        return 1;
                    if (vis1.Priority < vis2.Priority)
                        return -1;
                }

                return 0;
            }
        }

        private static PriorityComparer priorityComparer = new PriorityComparer();

        /// <summary>
        /// Add a visualisation to this layer.
        /// </summary>
        public VisualisationInstance AddVisualisation(IParticleVisualisation visualisation)
        {
            var instance = VisualisationInstance.InstantiateAsInactive(visualisationRoot,
                                                                       visualisation,
                                                                       transform);
            instance.SetActive(true);
            instance.gameObject.name = visualisation.DisplayName;

            if (currentMembers.Any())
            {
                var insertIndex = 0;
                foreach (var existingSelection in currentMembers)
                    if (priorityComparer.Compare(existingSelection.Visualisation, visualisation) <
                        0)
                        insertIndex++;
                currentMembers.Insert(insertIndex, instance);
            }
            else
            {
                currentMembers.Add(instance);
            }

            RecalculateIndices(instance);
            instance.UpdateVisualiser();
            return instance;
        }

        /// <summary>
        /// Find the selection on this layer which contains the particle of the given
        /// index.
        /// </summary>
        [CanBeNull]
        public VisualisationInstance GetSelectionForParticle(int particleIndex)
        {
            for (var i = currentMembers.Count - 1; i >= 0; i--)
            {
                var selection = currentMembers[i];
                if (!selection.FilteredIndices.HasNonNullValue())
                    return selection;
                if (SearchAlgorithms.BinarySearch(particleIndex, selection.FilteredIndices.Value))
                    return selection;
            }

            return null;
        }

        public void RecalculateIndices(VisualisationInstance visualisation)
        {
            var index = currentMembers.IndexOf(visualisation);
            if (index < 0)
            {
                throw new ArgumentException(
                    "Tried to update visualisation not in layer.");
            }

            for (var i = index; i >= 0; i--)
            {
                currentMembers[i].CalculateFilteredIndices(
                    i == currentMembers.Count - 1 ? null : currentMembers[i + 1],
                    visualisationRoot.ParticleCount);
            }
        }

        public void RemoveInstance(VisualisationInstance instance)
        {
            Destroy(instance.gameObject);
            currentMembers.Remove(instance);
            if (currentMembers.Count > 0)
                RecalculateIndices(currentMembers.Last());
            else
                Removed?.Invoke();
        }
    }
}