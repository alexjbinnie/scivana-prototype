﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

namespace ScivanaIMD.State
{
    public class ControllerInteractHandler : MonoBehaviour
    {
        public Action Triggered;

        private InputDeviceRole role;

        public static ControllerInteractHandler InstantiateAsInactive(InputDeviceRole role)
        {
            var instance = Instantiation.InstantiateInactive<ControllerInteractHandler>();
            instance.role = role;
            return instance;
        }

        private void OnEnable()
        {
            ScivanaPrototype.Input.Get(role).VR.Interact.performed += Trigger;
        }

        private void OnDisable()
        {
            ScivanaPrototype.Input.Get(role).VR.Interact.performed -= Trigger;
        }

        private void Trigger(InputAction.CallbackContext callbackContext)
        {
            Triggered?.Invoke();
        }
    }
}