﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Trajectory;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.State
{
    /// <summary>
    /// Handles highlighting specific atoms
    /// </summary>
    public class PhysicalParticleSelector : MonoBehaviour
    {
        [SerializeField]
        private Simulation simulation;

        [SerializeField]
        private Transform cursor;

        [SerializeField]
        private Vector3 cursorOffset;

        [SerializeField]
        private float cutoff;

        public int? ParticleIndex { get; private set; }

        private void Update()
        {
            var index =
                GetClosestParticleToWorldPosition(cursor.TransformPoint(cursorOffset), cutoff);
            if (index != ParticleIndex)
            {
                ParticleIndex = index;
            }
        }

        /// <summary>
        /// Get the closest particle to a given point in world space.
        /// </summary>
        private int? GetClosestParticleToWorldPosition(Vector3 worldPosition,
                                                       float cutoff = Mathf.Infinity)
        {
            var position = transform.InverseTransformPoint(worldPosition);

            var frame = simulation.CurrentFrame;

            var bestSqrDistance = cutoff * cutoff;
            int? bestParticleIndex = null;

            var positions = frame.Get(FrameFields.ParticlePositions);

            for (var i = 0; i < positions.Length; ++i)
            {
                var particlePosition = positions[i];
                var sqrDistance = Vector3.SqrMagnitude(position - particlePosition);

                if (sqrDistance < bestSqrDistance)
                {
                    bestSqrDistance = sqrDistance;
                    bestParticleIndex = i;
                }
            }

            return bestParticleIndex;
        }
    }
}