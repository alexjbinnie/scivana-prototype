﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.Trajectory;
using Scivana.Visualisation.Properties;
using ScivanaIMD.Simulations;

namespace ScivanaIMD.State
{
    public class ModifySelectionTool
    {
        private Simulation simulation;

        public ModifySelectionTool(Simulation simulation)
        {
            this.simulation = simulation;
        }

        public enum GroupingModes
        {
            Particle,
            Chain,
            Residue
        }

        public enum ModificationModes
        {
            Add,
            Remove
        }

        private GroupingModes groupingMode = GroupingModes.Particle;

        public event Action GroupingModeChanged;

        public GroupingModes GroupingMode
        {
            get => groupingMode;
            set
            {
                var changed = groupingMode != value;
                groupingMode = value;
                GroupingModeChanged?.Invoke();
                if (changed)
                    RefreshFromHighlightedAtom(highlightedParticle);
            }
        }

        private ModificationModes modificationMode = ModificationModes.Add;

        public event Action ModificationModeChanged;

        public ModificationModes ModificationMode
        {
            get => modificationMode;
            set
            {
                modificationMode = value;
                ModificationModeChanged?.Invoke();
            }
        }

        private int? highlightedParticle;

        public int? HighlightedParticle => highlightedParticle;

        public event Action HighlightedParticleChanged;

        /// <summary>
        /// Refresh the highlighted indices from the given highlighted particle, applying the current grouping
        /// setting of the tool.
        /// </summary>
        public void RefreshFromHighlightedAtom(int? particleIndex)
        {
            highlightedParticle = particleIndex;
            HighlightedParticleChanged?.Invoke();

            if (particleIndex == null)
            {
                highlightedIndices.Value = new int[0];
                return;
            }

            var residues = simulation.CurrentFrame.Get(FrameFields.ParticleResidues);
            int residueIndex;
            List<int> list;

            switch (GroupingMode)
            {
                case GroupingModes.Particle:
                    highlightedIndices.Value = new[] { particleIndex.Value };
                    return;

                case GroupingModes.Residue:
                    if (residues == null)
                    {
                        highlightedIndices.Value = new[] { particleIndex.Value };
                        return;
                    }

                    residueIndex = residues[particleIndex.Value];
                    list = new List<int>();
                    for (var i = 0; i < residues.Length; i++)
                    {
                        if (residues[i] == residueIndex)
                            list.Add(i);
                    }

                    highlightedIndices.Value = list.ToArray();
                    return;

                case GroupingModes.Chain:
                    var chains = simulation.CurrentFrame.Get(FrameFields.ResidueEntities);

                    if (residues == null || chains == null)
                    {
                        highlightedIndices.Value = new[] { particleIndex.Value };
                        return;
                    }

                    residueIndex = residues[particleIndex.Value];
                    var chainIndex = chains[residueIndex];
                    list = new List<int>();
                    for (var i = 0; i < residues.Length; i++)
                    {
                        if (chains[residues[i]] == chainIndex)
                            list.Add(i);
                    }

                    highlightedIndices.Value = list.ToArray();
                    return;
            }
        }

        private ArrayProperty<int> highlightedIndices = new ArrayProperty<int>()
        {
            Value = new int[0],
        };

        public ArrayProperty<int> HighlightedIndices => highlightedIndices;

        /// <summary>
        /// Apply the current tool (either adding or subtracting some subset of atoms) to the given context.
        /// </summary>
        public void Apply(ModifySelectionContext context)
        {
            switch (ModificationMode)
            {
                case ModificationModes.Add:
                    context.AddParticleIndices(highlightedIndices.Value);
                    return;
                case ModificationModes.Remove:
                    context.RemoveParticleIndices(highlightedIndices.Value);
                    return;
                default:
                    throw new InvalidOperationException($"Unrecognized modification mode {ModificationMode}");
            }
        }
    }
}