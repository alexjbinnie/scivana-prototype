﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Scivana.Core.Properties;
using Scivana.Visualisation.Properties;

namespace ScivanaIMD.State
{
    /// <summary>
    /// Represents a context in which atoms are being modified.
    /// </summary>
    public class ModifySelectionContext
    {
        private List<int> currentSelection = new List<int>();
        private ArrayProperty<int> selectedIndices = new ArrayProperty<int>();
        private int maxSize = 0;

        public IReadOnlyProperty<int[]> SelectedIndices => selectedIndices;
        public int SelectionSize => currentSelection?.Count ?? maxSize;

        public event Action SelectedIndicesChanged;

        public ModifySelectionContext(IEnumerable<int> initialSelection, int maxSize)
        {
            if (initialSelection == null)
                currentSelection = null;
            else
                currentSelection.AddRange(initialSelection);
            this.maxSize = maxSize;
            RefreshIndicesFromSelection();
        }

        private void RefreshIndicesFromSelection()
        {
            selectedIndices.Value = currentSelection?.ToArray();
            SelectedIndicesChanged?.Invoke();
        }

        public void AddParticleIndices(IEnumerable<int> indices)
        {
            if (currentSelection == null)
                return;
            currentSelection.AddRange(indices);
            currentSelection = currentSelection.Distinct().ToList();
            currentSelection.Sort();
            RefreshIndicesFromSelection();
        }

        public void RemoveParticleIndices(IEnumerable<int> indices)
        {
            if (currentSelection == null)
                currentSelection = Enumerable.Range(0, maxSize).ToList();
            foreach (var i in indices)
                currentSelection.Remove(i);
            currentSelection = currentSelection.Distinct().ToList();
            currentSelection.Sort();
            RefreshIndicesFromSelection();
        }

        public void ClearAllParticleIndices()
        {
            if (currentSelection == null)
                currentSelection = new List<int>();
            else
                currentSelection.Clear();
            RefreshIndicesFromSelection();
        }

        public void AddAllParticleIndices()
        {
            currentSelection = null;
            RefreshIndicesFromSelection();
        }
    }
}