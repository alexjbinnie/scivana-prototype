﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

namespace ScivanaIMD.State
{
    public class HandUiOpenMenuListener : MonoBehaviour
    {
        private event Action menuOpened;

        private InputDeviceRole hand;

        private void OnEnable()
        {
            ScivanaPrototype.Input.Get(hand).VR.QuickMenu.started += OpenMenu;
        }

        private void OnDisable()
        {
            ScivanaPrototype.Input.Get(hand).VR.QuickMenu.started -= OpenMenu;
        }

        private void OpenMenu(InputAction.CallbackContext callbackContext)
        {
            menuOpened?.Invoke();
        }

        public static HandUiOpenMenuListener InstantiateInactive<TCanvas>(Func<TCanvas> createInstance,
                                                                          InputDeviceRole hand)
            where TCanvas : MonoBehaviour
        {
            var obj = new GameObject
            {
                name = "Open Hand UI"
            };
            obj.SetActive(false);
            var menu = obj.AddComponent<HandUiOpenMenuListener>();
            menu.menuOpened += () => ScivanaPrototype.App.OpenQuickAccessMenu(createInstance(), hand);
            ;
            menu.hand = hand;
            return menu;
        }
    }
}