﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

namespace ScivanaIMD.State
{
    public class HandUiCloseMenuListener : MonoBehaviour
    {
        private InputDeviceRole hand;

        private void OnEnable()
        {
            ScivanaPrototype.Input.Get(hand).VR.QuickMenu.canceled += CloseMenu;
        }

        private void OnDisable()
        {
            ScivanaPrototype.Input.Get(hand).VR.QuickMenu.canceled -= CloseMenu;
        }

        private void CloseMenu(InputAction.CallbackContext callbackContext)
        {
            ScivanaPrototype.App.CloseQuickAccessMenu();
        }

        public static HandUiCloseMenuListener InstantiateInactive(InputDeviceRole hand)
        {
            var obj = new GameObject
            {
                name = typeof(HandUiCloseMenuListener).Name
            };
            obj.SetActive(false);
            var menu = obj.AddComponent<HandUiCloseMenuListener>();
            menu.hand = hand;
            return menu;
        }
    }
}