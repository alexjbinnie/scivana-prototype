﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Scivana.Core;
using ScivanaIMD.UI;
using UnityEngine;

namespace ScivanaIMD.State
{
    /// <summary>
    /// Represents a cursor, nominally one for each VR controller, that allows interacting with objects in
    /// 3D space such as molecules or user interfaces.
    /// </summary>
    public class Cursors : MonoBehaviour
    {
        [SerializeField]
        internal GameObject leftCursor;

        [SerializeField]
        internal GameObject rightCursor;

        [SerializeField]
        internal Transform leftPivot;

        [SerializeField]
        internal Transform rightPivot;

        [SerializeField]
        internal IconCursor leftIconCursor;

        [SerializeField]
        internal IconCursor rightIconCursor;

        public Cursors InstantiateAsInactive(bool isLeftActive,
                                             bool isRightActive,
                                             Color? color = null,
                                             Sprite sprite = null)
        {
            var instance = this.InstantiateInactive();
            if (!isLeftActive)
                instance.leftCursor.SetActive(false);
            if (!isRightActive)
                instance.rightCursor.SetActive(false);
            if (color.HasValue)
                Color = color.Value;
            if (sprite != null)
                Icon = sprite;
            return instance;
        }

        public Color Color
        {
            set
            {
                if (leftIconCursor != null)
                    leftIconCursor.Color = value;
                if (rightIconCursor != null)
                    rightIconCursor.Color = value;
            }
        }

        public Sprite Icon
        {
            set
            {
                if (leftIconCursor != null)
                    leftIconCursor.Icon = value;
                if (rightIconCursor != null)
                    rightIconCursor.Icon = value;
            }
        }

        /// <summary>
        /// The world space position of the point that the left hand cursor is considered to be centered.
        /// </summary>
        public Transform LeftPoint => leftPivot.transform;

        /// <summary>
        /// The world space position of the point that the left hand cursor is considered to be centered.
        /// </summary>
        public Transform RightPoint => rightPivot.transform;
    }
}