﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using Scivana.Core;
using Scivana.Core.Properties;
using Scivana.Trajectory;
using Scivana.Visualisation.Properties;
using ScivanaIMD.Simulations;
using UnityEngine;

namespace ScivanaIMD.State
{
    /// <summary>
    /// Handles highlighting a specific atom with a cursor.
    /// </summary>
    public class AtomProximitySelector : MonoBehaviour
    {
        private Transform cursorTransform;

        private Simulation simulation;

        private Transform visualisationTransform;

        private float cutoff;

        /// <summary>
        /// Instantiate an inactive <see cref="AtomProximitySelector" />.
        /// </summary>
        /// <param name="simulation">The simulation which contains the atoms to interact with.</param>
        /// <param name="visualisationTransform">The transform that the atom positions are relative to.</param>
        /// <param name="cursorTransform">
        /// The world space transform of the cursor that will indicate which
        /// particle is highlighted.
        /// </param>
        /// <param name="cutoff">
        /// The cutoff, relative to the local space of
        /// <paramref name="visualisationTransform" />.
        /// </param>
        public static AtomProximitySelector InstantiateAsInactive(Simulation simulation,
                                                                  Transform visualisationTransform,
                                                                  Transform cursorTransform,
                                                                  float cutoff)
        {
            var instance = Instantiation.InstantiateInactive<AtomProximitySelector>();
            instance.simulation = simulation;
            instance.visualisationTransform = visualisationTransform;
            instance.cursorTransform = cursorTransform;
            instance.cutoff = cutoff;
            return instance;
        }

        private int? selectedParticleId = null;

        /// <summary>
        /// The index of the particle which is currently being hovered over, or null if there is no particle
        /// within the cutoff distance from the cursor.
        /// </summary>
        public int? SelectedParticleId => selectedParticleId;

        private ArrayProperty<int> highlightedIndices = new ArrayProperty<int>();

        public IReadOnlyProperty<int[]> HighlightedIndices => highlightedIndices;

        /// <summary>
        /// Callback for when the selected particle is changed.
        /// </summary>
        public event Action SelectedParticleChanged;

        private void Update()
        {
            var closest = GetClosestParticleToWorldPosition(cursorTransform.position,
                                                            visualisationTransform,
                                                            simulation.CurrentFrame,
                                                            cutoff);
            var changed = closest != selectedParticleId;
            selectedParticleId = closest;
            if (changed)
            {
                SelectedParticleChanged?.Invoke();
                if (selectedParticleId.HasValue)
                    highlightedIndices.Value = new int[] { selectedParticleId.Value };
                else
                    highlightedIndices.UndefineValue();
            }
        }

        /// <summary>
        /// Get the closest particle to a given point in world space.
        /// </summary>
        private static int? GetClosestParticleToWorldPosition(
            Vector3 worldPoint,
            Transform frameTransform,
            Frame frame,
            float cutoff = Mathf.Infinity)
        {
            var position = frameTransform.InverseTransformPoint(worldPoint);

            var bestSqrDistance = cutoff * cutoff;
            int? bestParticleIndex = null;

            var positions = frame.Get(FrameFields.ParticlePositions);

            if (positions == null)
                return null;

            for (var i = 0; i < positions.Length; ++i)
            {
                var particlePosition = positions[i];
                var sqrDistance = Vector3.SqrMagnitude(position - particlePosition);

                if (sqrDistance < bestSqrDistance)
                {
                    bestSqrDistance = sqrDistance;
                    bestParticleIndex = i;
                }
            }

            return bestParticleIndex;
        }
    }
}