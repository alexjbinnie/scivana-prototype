﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.XR.Interaction.Toolkit;

namespace ScivanaIMD.State
{
    /// <summary>
    /// Assigns a color to a renderer (in this case, the UI cursor) depending on whether it
    /// is currently interacting with a UI element.
    /// </summary>
    public class UiCursorColor : MonoBehaviour
    {
        [SerializeField]
        private XRRayInteractor interactor;

        [SerializeField]
        private Color colorSelected;

        [SerializeField]
        private Color colorUnselected;

        [SerializeField]
        private new Renderer renderer;

        private void OnEnable()
        {
            Assert.IsNotNull(interactor);
            Assert.IsNotNull(renderer);
        }

        private void Update()
        {
            var isValidTarget = false;
            var position = Vector3.zero;
            var normal = Vector3.zero;
            var endPosition = 0;
            interactor.TryGetHitInfo(ref position, ref normal, ref endPosition, ref isValidTarget);
            renderer.material.color = isValidTarget ? colorSelected : colorUnselected;
        }
    }
}