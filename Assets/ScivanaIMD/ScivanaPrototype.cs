﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Scivana.App.XR;
using Scivana.Core;
using Scivana.Core.Async;
using Scivana.Core.Serialization;
using Scivana.Network.Multiplayer;
using Scivana.State;
using ScivanaIMD.Annotations;
using ScivanaIMD.Avatars;
using ScivanaIMD.Interaction;
using ScivanaIMD.Selection.Data;
using ScivanaIMD.Simulations;
using ScivanaIMD.State;
using ScivanaIMD.State.Modes;
using ScivanaIMD.UI;
using ScivanaIMD.UserInterface;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using Keyboard = ScivanaIMD.UserInterface.Keyboard;

namespace ScivanaIMD
{
    public class ScivanaPrototype : MonoBehaviour
    {
        [SerializeField]
        private MainMenu mainMenu;

        [SerializeField]
        private ManualConnect manualConnectUi;

        [SerializeField]
        private Autoconnecting autoconnectingUi;

        [SerializeField]
        private Connecting connectingUi;

        [SerializeField]
        private ServerDisconnected serverDisconnectedUi;

        [SerializeField]
        private ExceptionWindow exceptionWindowUi;

        [SerializeField]
        private SimulationVisualisationRoot simulationDisplay;

        [SerializeField]
        private BoxManipulation boxManipulationPrefab;

        [SerializeField]
        private ParticlesManipulation particleManipulationPrefab;

        [SerializeField]
        private Cursors uiInteractionPrefab;

        [SerializeField]
        private Canvas fullScreenCanvas;

        [SerializeField]
        private Canvas leftArmCanvas;

        [SerializeField]
        private bool useExceptionWindow;

        [SerializeField]
        private Cursors interactionCursorPrefab;

        [SerializeField]
        private HandUiCanvas handUiCanvasPrefab;

        [SerializeField]
        private InteractionQuickMenu interactionMenuPrefab;

        [SerializeField]
        private SimulationQuickMenu simulationQuickMenuPrefab;

        [SerializeField]
        private SimulationMenu simulationMenuPrefab;

        [SerializeField]
        private SelectionMenu selectionMenuPrefab;

        [SerializeField]
        private AnnotationMenu annotationMenuPrefab;

        [SerializeField]
        private SceneVisuals sceneVisualsPrefab;

        [SerializeField]
        private UsersMenu usersMenuPrefab;

        [SerializeField]
        private ModifySelectionQuickMenu modifySelectionQuickMenuPrefab;

        [SerializeField]
        private ModifySelectionDialog modifySelectionDialogPrefab;

        [SerializeField]
        private EditSelection editSelectionPrefab;

        [SerializeField]
        private EditAnnotation editAnnotationPrefab;

        [SerializeField]
        private Cursors editParticleCursorsPrefab;

        [SerializeField]
        private ParticleSequenceDialog particleSequenceSelectorPrefab;

        [SerializeField]
        private AvatarVisualisation avatarVisualisationPrefab;


        [SerializeField]
        private Keyboard keyboardPrefab;

        [SerializeField]
        private ColorPickerDialog colorPickerPrefab;

        [SerializeField]
        private XRRig xrRig;

        [SerializeField]
        private Camera mainCamera;

        [SerializeField]
        private Color positiveColor;

        [SerializeField]
        private Color negativeColor;

        [SerializeField]
        private Sprite iconAdd;

        [SerializeField]
        private Sprite iconSubtract;

        public static ScivanaPrototype App { get; private set; }

        public static ScivanaPrototypeNetworking Networking { get; private set; }

        public static XrInput<Actions> Input { get; private set; }

        public ContextManager StateManagement => stateManagement;

        private static List<object> preferences = new List<object>();

        public static TPreferences Preferences<TPreferences>() where TPreferences : new()
        {
            foreach (var preference in preferences)
                if (preference is TPreferences desiredPreference)
                    return desiredPreference;
            var newPreference = new TPreferences();
            preferences.Add(newPreference);
            return newPreference;
        }

        private void Awake()
        {
            App = this;
            InputSystem.RegisterBindingComposite<ButtonFrom2DAxis>();
            Networking = new ScivanaPrototypeNetworking();
            Input = new XrInput<Actions>();

            Serialization.AddConverter(new AnnotationConverter());

            if (IsVREnabled)
                SetupVirtualRealityMode();
            else
                SetupDesktopMode();
        }

        private void Start()
        {
            GotoMainMenu();
            Application.logMessageReceived += ApplicationOnlogMessageReceived;
        }

        private void Update()
        {
            Input.Update();
        }

        private void ApplicationOnlogMessageReceived(string condition,
                                                     string stacktrace,
                                                     LogType type)
        {
            if (useExceptionWindow && type == LogType.Exception)
            {
                stateManagement.ClearAllStates();
                ShowExceptionWindow(condition);
            }
        }

        public static bool IsVREnabled => true;

        private readonly ContextManager stateManagement = new ContextManager();

        public State<TMonoBehaviour> GotoFullscreenUi<TMonoBehaviour>(TMonoBehaviour behaviour,
                                                                      ContextUsage usage = ContextUsage.Absolute)
            where TMonoBehaviour : MonoBehaviour
        {
            if (behaviour == null)
                throw new ArgumentNullException(nameof(behaviour));
            behaviour.transform.SetParent(fullScreenCanvas.transform, false);

            var ui = behaviour.AsState($"Full Screen UI '{behaviour.name}'")
                              .AddContext(Contexts.FullScreenUI, usage);

            using (StateManagement.ModifyStates())
            {
                stateManagement.AddState(ui);
                stateManagement.AddState(GetUiInteractionState(ui));
            }

            return ui;
        }

        public State<TMonoBehaviour> AddUiToLeftArm<TMonoBehaviour>(
            State<TMonoBehaviour> state,
            ContextUsage usage = ContextUsage.Absolute)
            where TMonoBehaviour : MonoBehaviour
        {
            using (stateManagement.ModifyStates())
            {
                if (usage == ContextUsage.Absolute)
                    stateManagement.DeleteStatesWithContext(Contexts.LeftArmUI);
                stateManagement.DeleteStatesWithContext(Contexts.QuickMenuUI);
                if (state == null)
                    throw new ArgumentNullException(nameof(state));
                state.Instance.SetParent(leftArmCanvas.transform, false);
                state.AddContext(Contexts.DisplayedUI, ContextUsage.Exclusive);
                state.AddContext(Contexts.LeftArmUI, usage);

                AddState(state);
                AddState(GetUiInteractionState(state, isLeftActive: false));

                return state;
            }
        }

        private State<Cursors> GetUiInteractionState(IState parent,
                                                     bool isLeftActive = true,
                                                     bool isRightActive = true)
        {
            var instance = uiInteractionPrefab.InstantiateAsInactive(isLeftActive, isRightActive);
            instance.transform.parent = xrRig.transform;
            return instance.AsState("UI Interaction Cursor",
                                    false)
                           .AddContext(Contexts.ControllerCursors, ContextUsage.Exclusive)
                           .AddContext(Contexts.UIInteraction, ContextUsage.Exclusive)
                           .AddDependency(parent);
        }

        public void AddState(IState state)
        {
            stateManagement.AddState(state);
        }

        public void CloseStates<TType>()
            where TType : class
        {
            stateManagement.DeleteStates<TType>();
        }

        /// <summary>
        /// Open the autoconnecting UI and begin searching for a server.
        /// </summary>
        public void BeginAutoconnect()
        {
            GotoFullscreenUi(autoconnectingUi.InstantiateInactive());
        }

        public void GotoManualConnect()
        {
            GotoFullscreenUi(manualConnectUi.InstantiateInactive());
        }

        public void ConnectToSimulation(string address, int port)
        {
            GotoFullscreenUi(connectingUi.InstantiateAsInactive(address, port));
        }

        public void GotoServerDisconnected(string address, int port)
        {
            stateManagement.DeleteStates<Simulation>();
            GotoFullscreenUi(serverDisconnectedUi.InstantiateAsInactive(address, port));
        }

        public void ShowExceptionWindow(string error)
        {
            GotoFullscreenUi(exceptionWindowUi.InstantiateAsInactive(error));
        }

        public void GotoSelectionMode()
        {
            var simulation = stateManagement.GetState<Simulation>();
            var visualisationRoot = stateManagement.GetState<SimulationVisualisationRoot>();

            var selectionMode = new SelectionMode().AsState($"Mode '{typeof(SelectionMode)}'")
                                                   .AddContext(Contexts.Mode, ContextUsage.Absolute)
                                                   .AddDependency(simulation)
                                                   .AddDependency(visualisationRoot);

            AddState(selectionMode);
        }

        public void ClearMode()
        {
            var mode = stateManagement.GetFirstActiveState(Contexts.Mode);
            if (mode != null)
                stateManagement.DeleteState(mode);
        }

        public State<TType> EnsureMode<TType>()
            where TType : class, new()
        {
            if (stateManagement.GetFirstActiveState(Contexts.Mode) is State<TType> existing)
                return existing;

            var newState = new TType().AsState($"Mode '{typeof(TType)}'")
                                      .AddContext(Contexts.Mode, ContextUsage.Absolute)
                                      .AddDependency(CurrentSimulation);

            stateManagement.AddState(newState);

            if (typeof(TType) == typeof(SelectionMode))
            {
                var visRoot = stateManagement.GetState<SimulationVisualisationRoot>();

                var vis = SimulationVisualiser
                          .InstantiateAsInactive("selection pastel", visRoot.Instance)
                          .AsState("Base Visualiser for Selection Mode")
                          .AddContext(Contexts.SimulationVisualisation,
                                      ContextUsage.Exclusive)
                          .AddDependency(visRoot)
                          .AddDependency(newState);
                stateManagement.AddState(vis);
            }

            return newState;
        }

        public void GotoInteractionMode()
        {
            using (stateManagement.ModifyStates())
            {
                stateManagement.DeleteStatesWithContext(Contexts.QuickMenuUI);
                stateManagement.DeleteStatesWithContext(Contexts.LeftArmUI);

                var simulation = stateManagement.GetState<Simulation>();
                var visualisationRoot = stateManagement
                    .GetState<SimulationVisualisationRoot>();

                var interactionMode = EnsureMode<InteractionMode>();

                var particleManipulation = particleManipulationPrefab
                                           .InstantiateAsInactive(simulation,
                                                                  visualisationRoot.Instance.RightHandSpace)
                                           .AsState("Particle Manipulation")
                                           .AddContext(Contexts.ControllerCursors, ContextUsage.Exclusive)
                                           .AddDependency(interactionMode);

                stateManagement.AddState(particleManipulation);


                var interactionCursor = interactionCursorPrefab.InstantiateInactive()
                                                               .AsState("Interaction Cursors")
                                                               .AddDependency(particleManipulation);

                stateManagement.AddState(interactionCursor);

                var interactionQuickMenuListener = HandUiOpenMenuListener
                                                   .InstantiateInactive(
                                                       () => interactionMenuPrefab.InstantiateInactive(),
                                                       InputDeviceRole.RightHanded)
                                                   .AsState("Interaction Quick Menu Listener")
                                                   .AddDependency(interactionMode);

                stateManagement.AddState(interactionQuickMenuListener);

                var highlightInteractionParticles = HighlightInteractedParticles
                                                    .InstantiateAsInactive(visualisationRoot.Instance)
                                                    .AsState("Interaction Particle Highlighting")
                                                    .AddContext(Contexts.SimulationVisualisationHighlight,
                                                                ContextUsage.Exclusive)
                                                    .AddDependency(visualisationRoot)
                                                    .AddDependency(interactionMode);

                stateManagement.AddState(highlightInteractionParticles);
            }
        }

        public void OpenSimulation(SimulationConnection connection)
        {
            // Clear up old simulations
            stateManagement.DeleteStates<Simulation>();
            stateManagement.DeleteStatesWithContext(Contexts.FullScreenUI);

            var simulation = new Simulation(connection).AsState("Simulation");
            stateManagement.AddState(simulation);

            var visualisationRoot = simulationDisplay.InstantiateAsInactive(simulation)
                                                     .AsState("Simulation Display")
                                                     .AddDependency(simulation);
            stateManagement.AddState(visualisationRoot);

            using (stateManagement.ModifyStates())
            {
                // Avatar synchronisation
                if (IsVREnabled)
                {
                    var avatarSync = AvatarSynchronisation
                                     .InstantiateAsInactive(simulation)
                                     .AsState("Avatar Synchronisation")
                                     .AddDependency(simulation);

                    stateManagement.AddState(avatarSync);
                }

                var boxManipulation = boxManipulationPrefab
                                      .InstantiateAsInactive(simulation)
                                      .AsState("Simulation Box Manipulation")
                                      .AddDependency(visualisationRoot);

                stateManagement.AddState(boxManipulation);

                var simulationMenu = HandUiOpenMenuListener
                                     .InstantiateInactive(
                                         () => simulationQuickMenuPrefab.InstantiateAsInactive(simulation),
                                         InputDeviceRole.LeftHanded)
                                     .AsState("Simulation Menu Listener")
                                     .AddDependency(visualisationRoot);

                stateManagement.AddState(simulationMenu);

                var visualisationLayersVisualisation = SimulationLayersVisualisation
                                                       .InstantiateAsInactive(visualisationRoot.Instance)
                                                       .AsState("Layers Visualisations")
                                                       .AddContext(Contexts.SimulationVisualisation,
                                                                   ContextUsage.Exclusive)
                                                       .AddDependency(visualisationRoot);

                stateManagement.AddState(visualisationLayersVisualisation);

                var avatarVisualisation = avatarVisualisationPrefab
                                          .InstantiateAsInactive(simulation)
                                          .AsState("Avatar Visualisation")
                                          .AddDependency(simulation);

                stateManagement.AddState(avatarVisualisation);


                if (!IsVREnabled)
                {
                    var cameraPivot = SimulationCameraPivot
                                      .InstantiateInactive(simulation, mainCamera)
                                      .AsState("Camera Pivot")
                                      .AddDependency(visualisationRoot);

                    stateManagement.AddState(cameraPivot);
                }
            }
        }

        public void GotoMainMenu()
        {
            stateManagement.DeleteStates<Simulation>();
            GotoFullscreenUi(mainMenu.InstantiateInactive());
        }

        /// <summary>
        /// Open a UI menu on a given hand.
        /// </summary>
        /// <param name="instance">
        /// An instance containing user interface widgets, which
        /// will be positioned at the current world position of the controller specified by
        /// <paramref name="hand" />
        /// </param>
        /// <param name="hand">
        /// Specify which hand, whether <see cref="InputDeviceRole.LeftHanded" />
        /// or <see cref="InputDeviceRole.RightHanded" />
        /// </param>
        /// <remarks>
        /// This creates a canvas on the given hand, creates a cursor to interact with the canvas,
        /// and a listener which will close the menu when the south dpad is released.
        /// This will automatically close any other quick access menu's which are currently open.
        /// </remarks>
        public void OpenQuickAccessMenu<TMonoBehaviour>(TMonoBehaviour instance,
                                                        InputDeviceRole hand)
            where TMonoBehaviour : MonoBehaviour
        {
            if (!Input.GetOrientation(hand).Value.HasValue)
            {
                throw new InvalidOperationException(
                    $"Cannot get orientation of {hand} controller to open menu.");
            }

            if (hand != InputDeviceRole.LeftHanded && hand != InputDeviceRole.RightHanded)
            {
                throw new InvalidOperationException(
                    $"Can only create a quick access menu on either the left or right hand. Provided input was instead {hand}");
            }

            var position = Input.GetOrientation(hand).Value.Value.position;

            var uiState = handUiCanvasPrefab.InstantiateInactive()
                                            .SetWorldPosition(position)
                                            .AsState("Hand UI Root")
                                            .AddContext(Contexts.QuickMenuUI, ContextUsage.Absolute)
                                            .AddContext(Contexts.DisplayedUI, ContextUsage.Exclusive)
                                            .AddDependency(CurrentSimulation);


            var state = instance.AsState($"Quick Menu '{instance.name}'");
            state.AddDependency(uiState);
            state.Instance.transform.SetParent(uiState.Instance.transform, false);

            var cursorState = GetUiInteractionState(uiState);

            var closeState = HandUiCloseMenuListener
                             .InstantiateInactive(hand)
                             .AsState("Close Quick Menu Listener")
                             .AddDependency(uiState);

            using (stateManagement.ModifyStates())
            {
                stateManagement.AddState(uiState);
                stateManagement.AddState(state);
                stateManagement.AddState(cursorState);
                stateManagement.AddState(closeState);
            }
        }

        public void CloseQuickAccessMenu()
        {
            stateManagement.DeleteStatesWithContext(Contexts.QuickMenuUI);
        }

        /// <summary>
        /// Open the simulation menu, which provides access to various tools.
        /// </summary>
        public void GotoSimulationMenu()
        {
            AddUiToLeftArm(simulationMenuPrefab.InstantiateInactive()
                                               .AsState("Simulation Menu")
                                               .AddDependency(CurrentSimulation));
        }

        /// <summary>
        /// Open the alphanumeric keyboard.
        /// </summary>
        /// <param name="text">The text that will initially appear on the keyboard.</param>
        /// <param name="header">
        /// The header that will appear at the top of the keyboard, such
        /// as 'Enter name:'.
        /// </param>
        /// <param name="callback">
        /// Callback for when the user submits what they have entered.
        /// </param>
        public void GotoAlphabeticKeyboard(string text, string header, Action<string> callback)
        {
            AddUiToLeftArm(keyboardPrefab.InstantiateAsInactive(text, header, callback)
                                         .AsState("Keyboard"),
                           ContextUsage.Exclusive);
        }

        public void GotoFullscreenKeyboard(string text, string header, Action<string> callback)
        {
            GotoFullscreenUi(keyboardPrefab.InstantiateAsInactive(text, header, callback),
                             ContextUsage.Exclusive);
        }

        /// <summary>
        /// Open a color picker dialog.
        /// </summary>
        public void GotoColorPicker(Color value, string prompt, Action<Color> callback)
        {
            AddUiToLeftArm(colorPickerPrefab.InstantiateAsInactive(prompt, value, callback)
                                            .AsState("Color Picker"),
                           ContextUsage.Exclusive);
        }

        public State<Simulation> CurrentSimulation => stateManagement.GetState<Simulation>();

        /// <summary>
        /// Goto the selection menu, which lists the current selections involved in the simulation.
        /// </summary>
        /// <remarks>
        /// This will automatically switch the current mode to <see cref="SelectionMode" />, where
        /// the whole simulation is drawn in a specific way to enable easy selections of things.
        /// </remarks>
        public void GotoSelectionMenu()
        {
            using (stateManagement.ModifyStates())
            {
                var mode = EnsureMode<SelectionMode>();

                stateManagement.DeleteStates<SelectionContext>();

                var ui = AddUiToLeftArm(selectionMenuPrefab.InstantiateAsInactive(CurrentSimulation)
                                                           .AsState("Selection Menu")
                                                           .AddDependency(CurrentSimulation)
                                                           .AddDependency(mode));

                var visRoot = stateManagement.GetState<SimulationVisualisationRoot>();

                var highlighting = HighlightSpecificParticles
                                   .InstantiateAsInactive(visRoot.Instance,
                                                          ui.Instance.HighlightedParticles)
                                   .AsState("Selection Menu Highlighting")
                                   .AddContext(Contexts.SimulationVisualisationHighlight,
                                               ContextUsage.Exclusive)
                                   .AddDependency(visRoot)
                                   .AddDependency(ui);


                stateManagement.AddState(highlighting);
            }
        }

        public void GotoAnnotationMenu()
        {
            using (stateManagement.ModifyStates())
            {
                ClearMode();
                AddUiToLeftArm(annotationMenuPrefab.InstantiateAsInactive(CurrentSimulation)
                                                   .AsState("Annotations Menu")
                                                   .AddDependency(CurrentSimulation));
            }
        }

        public void GotoSceneVisuals()
        {
            using (stateManagement.ModifyStates())
            {
                ClearMode();
                AddUiToLeftArm(sceneVisualsPrefab.InstantiateAsInactive(CurrentSimulation)
                                                 .AsState("Scene Visuals Menu")
                                                 .AddDependency(CurrentSimulation));
            }
        }

        public void GotoUsersMenu()
        {
            using (stateManagement.ModifyStates())
            {
                ClearMode();
                AddUiToLeftArm(usersMenuPrefab.InstantiateAsInactive(CurrentSimulation)
                                              .AsState("Users Menu")
                                              .AddDependency(CurrentSimulation));
            }
        }


        public void GotoEditSelectionMenu(MultiplayerResource<ParticleSelection> selection)
        {
            using (stateManagement.ModifyStates())
            {
                var mode = stateManagement.GetState<SelectionMode>();
                var visRoot = stateManagement.GetState<SimulationVisualisationRoot>();

                var context = new SelectionContext(selection)
                              .AsState("Edit Selection Context")
                              .AddDependency(mode);

                var outline = SimulationVisualiser
                              .InstantiateAsInactive("selection outline", visRoot.Instance)
                              .SetParticleFilter(context.Instance.SelectedParticles)
                              .AsState("Current Selection Outline")
                              .AddContext(Contexts.SelectionOutline,
                                          ContextUsage.Exclusive)
                              .AddDependency(visRoot)
                              .AddDependency(context);

                stateManagement.AddState(context);

                stateManagement.AddState(outline);

                AddUiToLeftArm(editSelectionPrefab.InstantiateAsInactive(selection)
                                                  .AsState("Edit Selection User Interface")
                                                  .AddDependency(context)
                                                  .AddDependency(CurrentSimulation));
            }
        }

        public void GotoEditAnnotationMenu(MultiplayerResource<Annotation> annotation)
        {
            AddUiToLeftArm(editAnnotationPrefab.InstantiateAsInactive(annotation)
                                               .AsState("Edit Annotation User Interface")
                                               .AddDependency(CurrentSimulation));
        }

        public void CloseDialog(Dialog dialog)
        {
            stateManagement.DeleteStateOfInstance(dialog);
        }

        public void SetupDesktopMode()
        {
            fullScreenCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            fullScreenCanvas.GetComponent<FloatingCanvas>().enabled = false;

            leftArmCanvas.renderMode = RenderMode.ScreenSpaceOverlay;

            mainCamera.transform.localPosition = new Vector3(0, 1.6f, -0.5f);
        }

        public void SetupVirtualRealityMode()
        {
        }

        private void OnDestroy()
        {
            stateManagement.MarkForDestruction();
            foreach (var state in stateManagement.States)
                state.ExitState();
            Networking.CloseAsync().AwaitInBackground();
        }

        public void GotoModifySelection(IEnumerable<int> initialSelection, ModifySelectionTool.ModificationModes mode)
        {
            var visualisationRoot = stateManagement.GetState<SimulationVisualisationRoot>();
            var simulation = CurrentSimulation;
            var maxSize = simulation.Instance
                                    .CurrentFrame
                                    .Particles
                                    .Count;

            var frameBox = stateManagement.GetState<SimulationVisualisationRoot>()
                                          .Instance
                                          .RightHandSpace;

            var context = new ModifySelectionContext(initialSelection, maxSize);
            var tool = new ModifySelectionTool(simulation)
            {
                ModificationMode = mode,
            };
            var cursor = editParticleCursorsPrefab.InstantiateAsInactive(false, true);
            var clickHandler = ControllerInteractHandler.InstantiateAsInactive(InputDeviceRole.RightHanded);
            var selector = AtomProximitySelector.InstantiateAsInactive(simulation,
                                                                       frameBox,
                                                                       cursor.RightPoint,
                                                                       0.2f);

            var highlighting = HighlightSpecificParticles.InstantiateAsInactive(visualisationRoot.Instance,
                                                                                tool.HighlightedIndices);

            selector.SelectedParticleChanged += () => tool.RefreshFromHighlightedAtom(selector.SelectedParticleId);
            clickHandler.Triggered += () => tool.Apply(context);

            void UpdateHighlightingColor()
            {
                highlighting.Color = tool.ModificationMode == ModifySelectionTool.ModificationModes.Add
                                         ? positiveColor
                                         : negativeColor;
            }

            var dialog = modifySelectionDialogPrefab.InstantiateAsInactive(simulation,
                                                                           context,
                                                                           tool);

            tool.ModificationModeChanged += UpdateHighlightingColor;
            UpdateHighlightingColor();

            EditSelection editSelectionContext = stateManagement.GetState<EditSelection>();

            void RefreshCursor()
            {
                cursor.Color = tool.ModificationMode == ModifySelectionTool.ModificationModes.Add
                                   ? positiveColor
                                   : negativeColor;
                cursor.Icon = tool.ModificationMode == ModifySelectionTool.ModificationModes.Add
                                  ? iconAdd
                                  : iconSubtract;
            }

            RefreshCursor();
            tool.ModificationModeChanged += RefreshCursor;

            dialog.Accepted += () => editSelectionContext.SetParticleIndices(context.SelectedIndices.Value);
            dialog.Finished += () => stateManagement.DeleteStateOfInstance(context);

            using (StateManagement.ModifyStates())
            {
                var contextState = stateManagement.AddState(context.AsState("Modify Selection Context")
                                                                   .AddDependency(simulation));
                var toolState = stateManagement.AddState(tool.AsState("Modify Selection Tool")
                                                             .AddDependency(contextState));

                var dialogState = AddUiToLeftArm(dialog.AsState("Modify Selection User Interface")
                                                       .AddDependency(toolState)
                                                       .AddDependency(simulation),
                                                 ContextUsage.Exclusive);

                // Listener that switches to UI mode when the right hand is near the UI on the left arm.
                var proximityCursor = ProximityCursor.InstantiateAsInactive(dialog.GetComponent<RectTransform>(),
                                                                            InputDeviceRole.RightHanded)
                                                     .AsState("User Interface Cursor Switcher")
                                                     .AddDependency(dialogState);
                stateManagement.AddState(proximityCursor);

                var cursorState = stateManagement.AddState(cursor.AsState("Modify Selection Cursor")
                                                                 .AddContext(Contexts.ControllerCursors,
                                                                             ContextUsage.Exclusive)
                                                                 .AddDependency(contextState)
                                                                 .AddDependency(dialogState));

                var handlerState = stateManagement.AddState(clickHandler.AsState("Modify Selection Click Handler")
                                                                        .AddDependency(contextState)
                                                                        .AddDependency(toolState));
                stateManagement.AddState(selector.AsState("Atom Proximity Selector")
                                                 .AddDependency(cursorState)
                                                 .AddDependency(toolState)
                                                 .AddDependency(simulation));

                stateManagement.AddState(HandUiOpenMenuListener
                                         .InstantiateInactive(() => modifySelectionQuickMenuPrefab
                                                                  .InstantiateAsInactive(tool),
                                                              InputDeviceRole.RightHanded)
                                         .AsState("Modify Selection Quick Menu Listener")
                                         .AddDependency(toolState));

                // Highlighting what the cursor is over, accounting for the current setting.
                var highlightingState = highlighting.AsState("Hovered Atom Highlighting")
                                                    .AddContext(Contexts.SimulationVisualisationHighlight,
                                                                ContextUsage.Exclusive)
                                                    .AddDependency(visualisationRoot)
                                                    .AddDependency(toolState)
                                                    .AddDependency(cursorState);
                stateManagement.AddState(highlightingState);

                // Outline around what is currently in the selection.
                var outline = SimulationVisualiser
                              .InstantiateAsInactive("selection outline", visualisationRoot.Instance)
                              .SetParticleFilter(context.SelectedIndices)
                              .AsState("Current Selection Outline")
                              .AddContext(Contexts.SelectionOutline,
                                          ContextUsage.Exclusive)
                              .AddDependency(visualisationRoot)
                              .AddDependency(contextState);
                stateManagement.AddState(outline);
            }
        }

        public void EnsureUICursor(RectTransform canvas, bool leftHand, bool rightHand)
        {
            var existing = stateManagement.GetFirstActiveState(Contexts.UIInteraction);
            if (existing == null)
            {
                var canvasState = stateManagement.GetActiveStateOfInstance(canvas);
                var uiState = GetUiInteractionState(canvasState, leftHand, rightHand);
                stateManagement.AddState(uiState);
            }
        }

        public void RemoveUICursor()
        {
            var existing = stateManagement.GetFirstActiveState(Contexts.UIInteraction);
            if (existing != null)
            {
                stateManagement.DeleteState(existing);
            }
        }

        public void StartParticleSequenceSelection(int count, Action<int[]> callback)
        {
            var visualisationRoot = stateManagement.GetState<SimulationVisualisationRoot>();
            var simulation = CurrentSimulation;
            var frameBox = stateManagement.GetState<SimulationVisualisationRoot>()
                                          .Instance
                                          .RightHandSpace;

            var dialog = particleSequenceSelectorPrefab.InstantiateAsInactive(count);

            var cursor = editParticleCursorsPrefab.InstantiateAsInactive(false, true);

            var clickHandler = ControllerInteractHandler.InstantiateAsInactive(InputDeviceRole.RightHanded);

            var selector = AtomProximitySelector.InstantiateAsInactive(simulation,
                                                                       frameBox,
                                                                       cursor.RightPoint,
                                                                       0.2f);

            var highlighting = HighlightSpecificParticles.InstantiateAsInactive(visualisationRoot.Instance,
                                                                                selector.HighlightedIndices);

            clickHandler.Triggered += () =>
            {
                if (selector.SelectedParticleId.HasValue)
                    dialog.AddIndex(selector.SelectedParticleId.Value);
            };

            dialog.Accepted += () => callback?.Invoke(dialog.SelectedIndices.Value);
            dialog.Finished += () => stateManagement.DeleteStateOfInstance(dialog);

            using (StateManagement.ModifyStates())
            {
                var dialogState = AddUiToLeftArm(dialog.AsState("Choose N Particles User Interface")
                                                       .AddDependency(simulation),
                                                 ContextUsage.Exclusive);

                // Listener that switches to UI mode when the right hand is near the UI on the left arm.
                var proximityCursor = ProximityCursor.InstantiateAsInactive(dialog.GetComponent<RectTransform>(),
                                                                            InputDeviceRole.RightHanded)
                                                     .AsState("User Interface Cursor Switcher")
                                                     .AddDependency(dialogState);
                stateManagement.AddState(proximityCursor);

                var cursorState = stateManagement.AddState(cursor.AsState("Modify Selection Cursor")
                                                                 .AddContext(Contexts.ControllerCursors,
                                                                             ContextUsage.Exclusive)
                                                                 .AddDependency(dialogState));

                var handlerState = stateManagement.AddState(clickHandler.AsState("Modify Selection Click Handler")
                                                                        .AddDependency(cursorState));
                stateManagement.AddState(selector.AsState("Atom Proximity Selector")
                                                 .AddDependency(cursorState)
                                                 .AddDependency(simulation));

                // Highlighting what the cursor is over, accounting for the current setting.
                var highlightingState = highlighting.AsState("Hovered Atom Highlighting")
                                                    .AddContext(Contexts.SimulationVisualisationHighlight,
                                                                ContextUsage.Exclusive)
                                                    .AddDependency(visualisationRoot)
                                                    .AddDependency(cursorState);
                stateManagement.AddState(highlightingState);

                // Outline around what has currently been chosen
                var outline = SimulationVisualiser
                              .InstantiateAsInactive("selection outline", visualisationRoot.Instance)
                              .SetParticleFilter(dialog.SelectedIndices)
                              .AsState("Current Selection Outline")
                              .AddContext(Contexts.SelectionOutline,
                                          ContextUsage.Exclusive)
                              .AddDependency(visualisationRoot)
                              .AddDependency(dialogState);
                stateManagement.AddState(outline);
            }
        }
    }
}