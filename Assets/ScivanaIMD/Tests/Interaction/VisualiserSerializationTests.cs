﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using NUnit.Framework;
using Scivana.Core.Serialization;
using ScivanaIMD.Selection.Data;

namespace ScivanaIMD.Tests.Interaction
{
    public class VisualiserSerializationTests
    {
        [Test]
        public void DeserializeStringVisualiser()
        {
            var str = "{ 'visualiser': 'name' }";

            var visualiser = Serialization.FromJSON<ParticleVisualiser>(str);
            Assert.AreEqual("name", visualiser.Visualiser);
        }

        [Test]
        public void DeserializeDictionaryVisualiser()
        {
            var str = "{ 'visualiser': { 'type': 'name' } }";

            var visualiser = Serialization.FromJSON<ParticleVisualiser>(str);
            CollectionAssert.AreEqual(new Dictionary<string, object>
                                      {
                                          ["type"] = "name",
                                      },
                                      visualiser.Visualiser as IDictionary<string, object>);
            Assert.IsTrue(visualiser.Visualiser.GetType() == typeof(Dictionary<string, object>));
        }

        [Test]
        public void DeserializeNestedDictionaryVisualiser()
        {
            var str = "{ 'visualiser': { 'type': { 'nested': 'name' } } }";

            var visualiser = Serialization.FromJSON<ParticleVisualiser>(str);
            CollectionAssert.AreEqual(new Dictionary<string, object>
                                      {
                                          ["type"] = new Dictionary<string, object>()
                                          {
                                              ["nested"] = "name"
                                          },
                                      },
                                      visualiser.Visualiser as IDictionary<string, object>);
            Assert.IsTrue(visualiser.Visualiser.GetType() == typeof(Dictionary<string, object>));
        }
    }
}