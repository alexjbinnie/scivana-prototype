﻿/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using Newtonsoft.Json;
using NUnit.Framework;
using ScivanaIMD.Annotations;
using Assert = UnityEngine.Assertions.Assert;

namespace ScivanaIMD.Tests.Annotations
{
    public class AnnotationConverterTests
    {
        [Test]
        public void DeserializeToRulerAnnotation()
        {
            const string validRuler =
                "{ \"type\": \"ruler\", \"particle_id1\": 2, \"particle_id2\": 5 }";
            var ruler = JsonConvert.DeserializeObject<RulerAnnotation>(validRuler,
                                                                       new AnnotationConverter());
            Assert.IsNotNull(ruler);
            Assert.AreEqual(2, ruler.Index1);
            Assert.AreEqual(5, ruler.Index2);
        }

        [Test]
        public void DeserializeToAnnotation()
        {
            const string validRuler =
                "{ \"type\": \"ruler\", \"particle_id1\": 2, \"particle_id2\": 5 }";
            var ruler = JsonConvert.DeserializeObject<Annotation>(validRuler,
                                                                  new AnnotationConverter())
                            as RulerAnnotation;
            Assert.IsNotNull(ruler);
            Assert.AreEqual(2, ruler.Index1);
            Assert.AreEqual(5, ruler.Index2);
        }

        [Test]
        public void DeserializeMissingType()
        {
            const string validRuler = "{ \"particle_id1\": 2, \"particle_id2\": 5 }";
            var obj = JsonConvert.DeserializeObject<Annotation>(validRuler,
                                                                new AnnotationConverter());
            Assert.IsNull(obj);
        }
    }
}