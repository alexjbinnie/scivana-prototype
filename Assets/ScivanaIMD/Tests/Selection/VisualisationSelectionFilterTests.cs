/*
 * This file is part of Scivana. This file is not a contribution to Narupa.
 * Author: Alex Jamieson-Binnie (alexjbinnie@alexjbinnie.com)
 * Copyright (c) 2020 University of Bristol. All rights reserved.
 *
 * File originally part of the Narupa iMD project, and is used under the terms
 * of the GNU General Public License version 3
 * Originally copied on the 22nd July 2020
 * Copyright (c) 2019, 2020 University of Bristol under the name Intangible
 * Realities Laboratory. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using NUnit.Framework;
using ScivanaIMD.Selection;

namespace ScivanaIMD.Tests.Selection
{
    internal class VisualisationSelectionFilterTests
    {
        public class ThreeSelectionIndices
        {
            public int[] FirstLayerFiltered = Array.Empty<int>();
            public int[] FirstLayerUnfiltered = Array.Empty<int>();
            public int[] SecondLayerFiltered = Array.Empty<int>();
            public int[] SecondLayerUnfiltered = Array.Empty<int>();
            public int[] ThirdLayerFiltered = Array.Empty<int>();
            public int[] ThirdLayerUnfiltered = Array.Empty<int>();
        }

        public struct TestParameters
        {
            public IReadOnlyList<int> First;
            public IReadOnlyList<int> Second;
            public IReadOnlyList<int> Third;
            public int MaxCount;
            public ThreeSelectionIndices Expected;
        }

        private static void TestThreeFilters(IReadOnlyList<int> first,
                                             IReadOnlyList<int> second,
                                             IReadOnlyList<int> third,
                                             int maxCount,
                                             ThreeSelectionIndices expected)
        {
            var result = new ThreeSelectionIndices();

            VisualisationInstance.FilterIndices(null,
                                                third,
                                                maxCount,
                                                ref result.ThirdLayerFiltered,
                                                ref result.ThirdLayerUnfiltered);

            VisualisationInstance.FilterIndices(result.ThirdLayerUnfiltered,
                                                second,
                                                maxCount,
                                                ref result.SecondLayerFiltered,
                                                ref result.SecondLayerUnfiltered);

            VisualisationInstance.FilterIndices(result.SecondLayerUnfiltered,
                                                first,
                                                maxCount,
                                                ref result.FirstLayerFiltered,
                                                ref result.FirstLayerUnfiltered);

            CollectionAssert.AreEqual(expected.ThirdLayerFiltered, result.ThirdLayerFiltered);
            CollectionAssert.AreEqual(expected.SecondLayerFiltered, result.SecondLayerFiltered);
            CollectionAssert.AreEqual(expected.FirstLayerFiltered, result.FirstLayerFiltered);
        }

        private static IEnumerable<TestParameters> GetParameters()
        {
            // All < All < All
            yield return new TestParameters
            {
                First = null,
                Second = null,
                Third = null,
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = Array.Empty<int>(),
                    SecondLayerFiltered = Array.Empty<int>(),
                    ThirdLayerFiltered = null,
                }
            };

            // All < All < None
            yield return new TestParameters
            {
                First = null,
                Second = null,
                Third = Array.Empty<int>(),
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = Array.Empty<int>(),
                    SecondLayerFiltered = null,
                    ThirdLayerFiltered = Array.Empty<int>(),
                }
            };

            // All < None < None
            yield return new TestParameters
            {
                First = null,
                Second = Array.Empty<int>(),
                Third = Array.Empty<int>(),
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = null,
                    SecondLayerFiltered = Array.Empty<int>(),
                    ThirdLayerFiltered = Array.Empty<int>(),
                }
            };

            // All < [2, 4, 7] < [1, 3, 6] # Disjoint, non empty base
            yield return new TestParameters
            {
                First = null,
                Second = new[]
                {
                    2, 4, 7
                },
                Third = new[]
                {
                    1, 3, 6
                },
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = new[]
                    {
                        0, 5
                    },
                    SecondLayerFiltered = new[]
                    {
                        2, 4, 7
                    },
                    ThirdLayerFiltered = new[]
                    {
                        1, 3, 6
                    },
                }
            };

            // All < [0, 2, 4, 7] < [1, 3, 5, 6] # Disjoint, empty base
            yield return new TestParameters
            {
                First = null,
                Second = new[]
                {
                    0, 2, 4, 7
                },
                Third = new[]
                {
                    1, 3, 5, 6
                },
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = Array.Empty<int>(),
                    SecondLayerFiltered = new[]
                    {
                        0, 2, 4, 7
                    },
                    ThirdLayerFiltered = new[]
                    {
                        1, 3, 5, 6
                    },
                }
            };

            // All < [2, 3, 4, 7] < [1, 2, 3, 5, 7] # Overlap, non empty base
            yield return new TestParameters
            {
                First = null,
                Second = new[]
                {
                    2, 3, 4, 7
                },
                Third = new[]
                {
                    1, 2, 3, 5, 7
                },
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = new[]
                    {
                        0, 6
                    },
                    SecondLayerFiltered = new[]
                    {
                        4
                    },
                    ThirdLayerFiltered = new[]
                    {
                        1, 2, 3, 5, 7
                    },
                }
            };

            // All < [0, 2, 3, 4, 6, 7] < [1, 2, 3, 5, 7] # Overlap, empty base
            yield return new TestParameters
            {
                First = null,
                Second = new[]
                {
                    0, 2, 3, 4, 6, 7
                },
                Third = new[]
                {
                    1, 2, 3, 5, 7
                },
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = Array.Empty<int>(),
                    SecondLayerFiltered = new[]
                    {
                        0, 4, 6
                    },
                    ThirdLayerFiltered = new[]
                    {
                        1, 2, 3, 5, 7
                    },
                }
            };

            // All < [0, 2, 4, 6, 7] < None
            yield return new TestParameters
            {
                First = null,
                Second = new[]
                {
                    0, 2, 4, 6, 7
                },
                Third = Array.Empty<int>(),
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = new[]
                    {
                        1, 3, 5
                    },
                    SecondLayerFiltered = new[]
                    {
                        0, 2, 4, 6, 7
                    },
                    ThirdLayerFiltered = Array.Empty<int>(),
                }
            };

            // All < [0, 2, 4, 6, 7] < [0, 2, 4, 6, 7] # Duplicate
            yield return new TestParameters
            {
                First = null,
                Second = new[]
                {
                    0, 2, 4, 6, 7
                },
                Third = new[]
                {
                    0, 2, 4, 6, 7
                },
                MaxCount = 8,
                Expected = new ThreeSelectionIndices
                {
                    FirstLayerFiltered = new[]
                    {
                        1, 3, 5
                    },
                    SecondLayerFiltered = Array.Empty<int>(),
                    ThirdLayerFiltered = new[]
                    {
                        0, 2, 4, 6, 7
                    },
                }
            };
        }

        [Test]
        public void TestThreeFilters([ValueSource(nameof(GetParameters))] TestParameters parameters)
        {
            TestThreeFilters(parameters.First,
                             parameters.Second,
                             parameters.Third,
                             parameters.MaxCount,
                             parameters.Expected);
        }
    }
}