# Scivana

Scivana is a VR application for viewing and interacting with simulations that are running on a [Narupa server](https://gitlab.com/intangiblerealities/narupa-protocol).

It is a modification of the [Narupa iMD](https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd) application, which is developed by the [Intangible Realities Laboratory](https://gitlab.com/intangiblerealities) at the University of Bristol. 

# Disclaimer

Scivana is a separate project which **is not** a part of or contribution to Narupa, and does not have any affiliation with either Narupa or the Intangible Realities Laboratory.
The author would like to make it clear that issues arising due to anything bearing the name Scivana should **not** be reported on the Narupa repositories, and instead reported on this repository.

# License

This project is released under the GNU General Public License version 3. All code written for Scivana is (c) University of Bristol, All Rights Reserved.

All code originating from the Narupa iMD project is taken and modified under the terms of the GPLv3. The header of each file indicates 
if it was originally part of the Narupa iMD codebase. All code from Narupa iMD is (c) University of Bristol, All Rights Reserved.
